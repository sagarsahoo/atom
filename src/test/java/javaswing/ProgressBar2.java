package javaswing;

import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

public class ProgressBar2  {
	
	JProgressBar progressBar = new JProgressBar();
	
	public void buttonClicked(){
		class MyWorker extends SwingWorker<String, Object> {
		     protected String doInBackground() {
		       progressBar.setVisible(true);
		       progressBar.setIndeterminate(true);

		       // Do my downloading code
		       return "Done.";
		     }

		     protected void done() {
		        progressBar.setVisible(false);
		     }
		  }

		  new MyWorker().execute();
		   
	}
	


}

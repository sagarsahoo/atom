package javaswing;

import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

public class ProgressBar extends JFrame {
	static JProgressBar progressBar = new JProgressBar();
	public static void main(String[] args) {
		buttonClicked();
	}
	
	public static void buttonClicked() {
		class MyWorker extends SwingWorker<String, Object> {
		     protected String doInBackground() {
		       progressBar.setVisible(true);
		       progressBar.setIndeterminate(true);

		       for(int i=0;i<100000000;i++){
		    	   System.out.println("progressing");
		       }
		       
		       // Do my downloading code
		       return "Done.";
		     }

		     protected void done() {
		        progressBar.setVisible(false);
		     }
		  }

		  new MyWorker().execute();

		}

}

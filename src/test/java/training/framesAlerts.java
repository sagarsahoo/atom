package training;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class framesAlerts {
  @Test
  public void f() {
	  
	  WebDriver driver = new FirefoxDriver();
	  driver.get("http://www.w3schools.com/js/tryit.asp?filename=tryjs_alert");
	  
	  WebElement frameElement = driver.findElement(By.id("iframeResult"));
	  driver.switchTo().frame(frameElement);
	  
	  driver.findElement(By.xpath("//button[text()='Try it']")).click();
	  
	  //Alert
	  //driver.switchTo().alert().accept();
	  driver.switchTo().alert().dismiss();
	  
  }
}

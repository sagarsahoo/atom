package training;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Frames {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		
		driver.get("http://www.w3schools.com/js/tryit.asp?filename=tryjs_alert");
		
		WebElement fr = driver.findElement(By.id("iframeResult"));
		driver.switchTo().frame(fr);
		
		WebElement bt = driver.findElement(By.xpath("html/body/button"));
		bt.click();
		
		Alert alt = driver.switchTo().alert();
		alt.accept();
		/*alt.dismiss(); //cancel
		alt.sendKeys("gyuttt");
		alt.getText();*/
	}

}

package training;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class SessionDay1 {

	public static void main(String[] args) {

		//Creating FF profile
		/*WebDriver _driver;
		ProfilesIni profile = new ProfilesIni();
		FirefoxProfile fp = profile.getProfile("default");
		_driver = new FirefoxDriver(fp);*/
		WebDriver _driver = new FirefoxDriver();  // this is a driver of type FF, - it will launch it own instance of FF

		_driver.get("https://www.makemytrip.com/"); //It will wait till the page is load - recemended to use this 
		//_driver.navigate().to("https://www.makemytrip.com/"); //THis will not wait

		WebElement HotelsLink = _driver.findElement(By.xpath("//span[text()='Flights']"));
		HotelsLink.click();

		/*WebElement FromLoc = _driver.findElement(By.id("from_city_data_box"));
		FromLoc.sendKeys("Test");*/

		_driver.findElement(By.name("way_fields")).click();

		WebElement  fromPlace = _driver.findElement(By.id("from_typeahead1"));

		fromPlace.clear();
		fromPlace.sendKeys("Bangalore");
		
		WebElement dd = _driver.findElement(By.id("rfttg"));
		
		Select dd_sel = new Select(dd);
		dd_sel.selectByIndex(1);
	}

}

package training2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Browser {
	

	public static void main(String[] args) {

		WebDriver wd_driver;
		System.setProperty("webdriver.chrome.driver", "C:\\Accenture\\Project\\selenium\\workspace\\Automation\\TestResources\\ChromeDriver\\chromedriver.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		ChromeOptions options = new ChromeOptions();
		//options.addArguments("test-type");
		options.addArguments("start-maximized");
		//capabilities.setCapability("chrome.binary","C:\\Accenture\\Project\\selenium\\workspace\\Automation\\TestResources\\ChromeDriver\\chromedriver.exe");
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		wd_driver = new ChromeDriver(capabilities);


	/*	//}else if (browser_name.equalsIgnoreCase("IE")){
		System.setProperty("webdriver.ie.driver", "C:\\Accenture\\Project\\selenium\\workspace\\Automation\\TestResources\\InternetExplorer\\IEDriverServer.exe");
		DesiredCapabilities caps = DesiredCapabilities.internetExplorer(); 
		//caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true); 
		//caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		wd_driver = new InternetExplorerDriver(caps);
*/
	}
	}

package training2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DemoClass1 {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.makemytrip.com/");
		driver.manage().window().maximize();
		
		WebElement flighLink = driver.findElement(By.xpath("//*[@id='widget_row']/div[1]/div/div[2]/ul/li[2]/a/span[2]"));
		flighLink.click();
		
		WebElement we = driver.findElement(By.id("from_typeahead1"));
		we.clear();
		we.sendKeys("Bangalore, India (BLR)");

	}

}

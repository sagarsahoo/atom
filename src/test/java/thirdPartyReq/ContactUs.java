package thirdPartyReq;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ContactUs {
	
	static WebDriver _driver;
	static String url="https://rewards.wholefoodsmarket.com/";
	List<String> ExcelValues = new ArrayList<String>();
	static String ExcelFile = "C:\\Users\\sagar.kumar.sahoo\\Desktop\\ContactUs.xls";
	static String excel_sheet = "ContactSheet";
	static String[][] data = null; 
	
	static String firstNameHeader="FirstName";
	static String lastNameHeader = "LastName";
	static String numHeader = "Number";
	static String emailIDHeader = "EmailID";
	static String requestHeader = "Request";
	static String flagHeader = "flag";
	
	static String firstName;
	static String lastName;
	static String cardNum;
	static String EmailID;
	static String request;
	
	static int firstNameCol;
	static int lastNameCol;
	static int cardNumCol;
	static int EmailIDCol;
	static int requestCol;
	static int flagCol;
	//static int execRow;
		
	
	//Main method actual execution
	public static void main(String[] args) {
		try{
			//Getting the execution data
			data = ExcelFunctions.getExcelData(ExcelFile,excel_sheet);
			
			//Initializing the driver to Firefox
			_driver = new FirefoxDriver();
			_driver.manage().window().maximize();
			_driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			//passing the url
			_driver.get(url);
			
			//loop as per number of records in the Excel
			for(int cnt=1;cnt<data.length;cnt++){
				
				//Getting the execution data
				getExecutionValues(cnt);
				
				WebDriverWait wt = new WebDriverWait(_driver,30);				
				wt.until(ExpectedConditions.presenceOfElementLocated(By.id("contact")));
				Thread.sleep(5000);
				
				//clicking the contactUs link
				_driver.findElement(By.id("contact")).click();
				
				//First Name
				wt.until(ExpectedConditions.presenceOfElementLocated(By.id("contactUsFirstName")));
				_driver.findElement(By.id("contactUsFirstName")).sendKeys(firstName);
				
				//Last Name
				wt.until(ExpectedConditions.presenceOfElementLocated(By.id("contactUsLastName")));
				_driver.findElement(By.id("contactUsLastName")).sendKeys(lastName);
				
				//card Number
				wt.until(ExpectedConditions.presenceOfElementLocated(By.id("contactUsCardNo")));
				_driver.findElement(By.id("contactUsCardNo")).sendKeys(cardNum);
				
				//Email id
				wt.until(ExpectedConditions.presenceOfElementLocated(By.id("contactUsEmail")));
				_driver.findElement(By.id("contactUsEmail")).sendKeys(EmailID);
				
				//request
				wt.until(ExpectedConditions.presenceOfElementLocated(By.id("contactUsComment")));
				_driver.findElement(By.id("contactUsComment")).sendKeys(request);
				
				//Submit the request
				wt.until(ExpectedConditions.presenceOfElementLocated(By.id("contctUsSubmitBtn")));
				_driver.findElement(By.id("contctUsSubmitBtn")).click();
				
			}
					
			//update the test data sheet
			//ExcelFunctions.setFlag(ExcelFile, excel_sheet, execRow,flagCol);
			//String[][] updatedData = ExcelFunctions.getExcelData(ExcelFile,excel_sheet);
			
			System.out.println("*********************** END of Test Case **************************");
						
			_driver.close();
			_driver.quit();
			
		}catch(Exception e){
			System.out.println("Exception occurred in main class - "+ e);
			_driver.quit();
		}		
	}
	
	
	
	//Setting the test data values in the variables
	public static void getExecutionValues(int execRow ){
		//Getting the execution row
		//execRow = ExcelFunctions.getExecutionRow(data,flagHeader);
		System.out.println("Execution for the row - "+ execRow);
		
		firstNameCol=ExcelFunctions.getColumnNumber(data,firstNameHeader);
		firstName = data[execRow][firstNameCol];
		
		lastNameCol = ExcelFunctions.getColumnNumber(data,lastNameHeader);
		lastName = data[execRow][lastNameCol];
		
		cardNumCol = ExcelFunctions.getColumnNumber(data,numHeader);
		cardNum = data[execRow][cardNumCol];
		
		EmailIDCol = ExcelFunctions.getColumnNumber(data,emailIDHeader);
		EmailID = data[execRow][EmailIDCol];
		
		requestCol = ExcelFunctions.getColumnNumber(data,requestHeader);
		request = data[execRow][requestCol];
		
		flagCol = ExcelFunctions.getColumnNumber(data,flagHeader);
		
		for(int cnt=0;cnt<data[0].length;cnt++){
			System.out.print(data[execRow][cnt]+ " | ");
		}
		System.out.println();
	}
	
}

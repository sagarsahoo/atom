package thirdPartyReq;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class ExcelFunctions {
	
	// stroing the Excel data in a 2-D array
  public static String[][] getExcelData(String ExceFile, String excel_sheet) {
	  String[][] data = null;
	  try{
		  File excel = new File(ExceFile);
			FileInputStream fis =  new FileInputStream(excel);
			HSSFWorkbook wb = new HSSFWorkbook (fis);
			HSSFSheet ws = wb.getSheet(excel_sheet);
			FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
			//Counting Number of rows and columns
			int rowNum = ws.getLastRowNum() + 1;
			int colNum = ws.getRow(0).getLastCellNum();
			
			data = new String[rowNum][colNum];
			
			//keeping the excel values in an 2D variable
			for (int i=0 ;i < rowNum ; i++){
				HSSFRow row = ws.getRow(i);
				for(int j=0 ; j < colNum ; j++){
					HSSFCell cell = row.getCell(j);
					//HSSFCell cell=row.getCell(j, org.apache.poi.ss.usermodel.Row.CREATE_NULL_AS_BLANK );
					String value = convertCellToString.cellTostringConvert(cell);
					data[i][j] = value.trim();
					System.out.print(value.trim()+ " | ");
				}
				System.out.println();
			}
		  return data;
	  }catch(Exception e){
		  System.out.println("Exception in storing Exceldata is - "+ e);
		  return null;
	  }
  }
  
  // Finding the test data row to be executed
  public static int getExecutionRow(String[][] excelData,String colName){
	  int reqRow=0;
	  int resultCol = getColumnNumber(excelData,colName);
	  try{
		  for(int cnt=0;cnt<excelData.length;cnt++){
			  if(excelData[cnt][resultCol].equalsIgnoreCase("N")){
				  reqRow++;
				  System.out.println("Execution row is - "+ (cnt+1));
				  return cnt;
			  }
		  }
		  if(reqRow==0){
			  System.out.println("No new data avalable");
			  return reqRow;
		  }
		  return reqRow;		  
	  }catch(Exception e){
		  System.out.println("Exception in getting the test data row is - " + e);
		 return  reqRow;
	  }
  }
  
  //Getting the column index from the Excel file for the Header located
  public static int getColumnNumber(String[][] excelData,String colName){
	  int matchCnt=0;
	  try{
		  for(int cnt=0;cnt<excelData[0].length;cnt++){
			  if(excelData[0][cnt].equals(colName)){
				  matchCnt++;
				  return cnt;
			  }
		  }
		  if(matchCnt==0){
			  System.out.println("Column - "+ colName + " not present in the Excel");
			  return matchCnt;
		  }
		  return matchCnt;
	  }catch(Exception e){
		  System.out.println("Exception occured - "+ e);
		  return matchCnt;
	  }	  
  }
  
  //Updating the flag after the Test Case execution
  public static void setFlag(String ExcelFile,String sheetName, int rowToUpdate,int flagCol){
	  try{
		  File file = new File(ExcelFile);	        	       	 
			FileInputStream inputStream = new FileInputStream(file);	 
			Workbook wb = new HSSFWorkbook(inputStream); 	 
			Sheet sheet = wb.getSheet(sheetName);

			Row row = sheet.getRow(rowToUpdate);
			Cell cell = row.getCell(flagCol);	    	
			cell.setCellValue("Y");

			inputStream.close();	 
			FileOutputStream outputStream = new FileOutputStream(file);	 
			wb.write(outputStream);	 
			outputStream.close();
	  }catch(Exception e){
		  
	  }
  }
}

package thirdPartyReq;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.testng.Assert;
import org.testng.annotations.Test;

public class convertCellToString {
  @Test
//Converting the cell value to string/numeric for HSSF
	public  static String cellTostringConvert(HSSFCell cell) {
		try{
			int type;
			Object result;
			type = cell.getCellType();
			switch(type) {
			case 0 :
				result = cell.getNumericCellValue();
				int value = (int)Double.parseDouble(result.toString());
				String pass_value = Integer.toString(value);
				return pass_value.trim();

			case 1 :
				result = cell.getStringCellValue();
				return result.toString().trim();

			case 3:
				if(!cell.getBooleanCellValue()){
					System.out.println("EMPTY CELL in --> ("+cell.getRowIndex()+","+cell.getColumnIndex()+") in the sheet name '"+cell.getSheet().getSheetName()+"'");
				}
				throw new RuntimeException("UNSUPPORTED CELL TYPE");
			}
		}catch(Exception e){
			System.out.println("The exception in cell to string is :- "+ e);
			Assert.fail("Exception occured in cell to string conversion");
		}
		return null;
	}
}

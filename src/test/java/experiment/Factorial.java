package experiment;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class Factorial {
 private final int n;
 private int result = 1;
 
 public Factorial(int n) {
   this.n = n;
 }

 public int getResult() {
   return result;
 }

 public Factorial compute() {
   if(n >= 1) {
     Factorial that = new Factorial(n-1);
     that.compute();
     result = n * that.getResult();
   }
 
   return this;
 }

 public static void main(String[] args)
   throws Exception {
   ScriptEngineManager mgr
     = new ScriptEngineManager();
   ScriptEngine engine
     = mgr.getEngineByName("JavaScript");

   engine.eval(
   "importClass(Packages.experiment.Factorial);" +
   "importClass(Packages.junit.framework.Assert);" +
   "" +
   "Assert.assertEquals(24, " +
   "  new Factorial(4).compute().getResult());" +
   "println('-ok-')");
 }
}
package experiment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class OracleDBconnection {

	public static void main(String[] args) {
		String query = "select * from demo";
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","system");
			Statement stmt=con.createStatement();  
			ResultSet rs=stmt.executeQuery(query);
			while(rs.next())  
				System.out.println(rs.getString(1)+ " " + rs.getString(2) );  

			//step5 close the connection object  
			con.close();

		}catch(Exception e){
			System.out.println("Exception - "+ e);
		}

	}

}

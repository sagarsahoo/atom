package experiment;

import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import utilityPackage.XMLParserFunctions;

public class XmlTagList {
	@Test
	public void f() {
		Document doc = XMLParserFunctions.parseXML("C:\\Users\\sagar.kumar.sahoo\\Desktop\\sampleRequest.xml");
		NodeList nodeList = doc.getChildNodes();
		if (doc.hasChildNodes()) {					 
			findChildTag(nodeList);		 
		}
	}

	public static void findChildTag(NodeList nodeList){	
		try{
			int cnt =0;
			Node tempNode;
			for( cnt = 0; cnt <nodeList.getLength();cnt ++ ){
				tempNode = nodeList.item(cnt);
				if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
					System.out.println("Node name - "+ tempNode.getNodeName());
					//Iterating Child nodes
					if(tempNode.hasChildNodes()){
						findChildTag(tempNode.getChildNodes());
					}/*else{
						System.out.println("Child Node - "+ tempNode.getNodeName());
					}*/
				}
				
			}
		}
		catch(Exception e){
			System.out.println("The Exception in finding value by xml tag is - "+e);
		}
	}
}
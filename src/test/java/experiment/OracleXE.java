package experiment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.testng.annotations.Test;

import oracle.jdbc.pool.OracleDataSource;

public class OracleXE {
	@Test
	public void f() {
		String query = "select * from demo";
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","system");
			Statement stmt=con.createStatement();  
			ResultSet rs=stmt.executeQuery(query);
			while(rs.next())  
				System.out.println(rs.getString(1)+ " " + rs.getString(2) );  

			//step5 close the connection object  
			con.close();

		}catch(Exception e){

		}

	}
}

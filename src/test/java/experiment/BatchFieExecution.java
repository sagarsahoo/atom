package experiment;

import java.io.IOException;

import org.testng.annotations.Test;

public class BatchFieExecution {
  @Test
  public void executeBatch(String batchFilePath,String batchFilename) throws IOException, InterruptedException {
	 /* String batchFilePath= "C:\\QC-rally\\ROS";
	  String batchFilename = "ROS-QC-Rally.bat";*/
	  
	  String batchFile = batchFilePath + "\\"+ batchFilename;
	  String windowProcess = "cmd /C start /wait " + batchFile ; 
	  Process p = Runtime.getRuntime().exec(windowProcess);
	 
	  System.out.println("Waiting for batch file to complete execution...");
      p.waitFor();
      System.out.println("Batch file execution done.");
      System.out.println("Back to the class - BatchFieExecution");
  }
}

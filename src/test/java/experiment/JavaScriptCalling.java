package experiment;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.testng.annotations.Test;

public class JavaScriptCalling {
	String jspath = "C:\\Users\\sagar.kumar.sahoo\\Desktop\\sample.html";

	/*@Test
	public void executeJavaScripCode() {
		try{			
			ScriptEngineManager factory = new ScriptEngineManager(); // create a script engine manager			
			ScriptEngine engine = factory.getEngineByName("JavaScript"); // create a JavaScript engine			 
			engine.eval("print('Welocme to java world')");// evaluate JavaScript code from String	
		}catch(Exception e){
			System.out.println("Exception is - "+ e);
		}
	}*/
	
	@Test
	public void executeJSFromFile(){
		try{
			ScriptEngineManager factory = new ScriptEngineManager(); // create a script engine manager			
			ScriptEngine engine = factory.getEngineByName("JavaScript"); // create a JavaScript engine
			//engine.eval(new java.io.FileReader("C:\\Users\\sagar.kumar.sahoo\\Desktop\\OpenUrl.html"));
			//String script = "function hello(name) { print('Hello, ' + name); }";
			String script="function getXPath( element )"
					+ "{var val=element.value;var xpath = ''; "
					+ "for ( ; element && element.nodeType == 1; "
					+ "element = element.parentNode ){"
					+ "var id = $(element.parentNode).children(element.tagName).index(element) + 1;"
					+ "id > 1 ? (id = '[' + id + ']') : (id = '');"
					+ "xpath = '/' + element.tagName.toLowerCase() + id + xpath;"
					+ "}"
					+ "return xpath;}";
			engine.eval(script);
			Invocable inv = (Invocable) engine;
			inv.invokeFunction("getXPath", "test" );
		}catch(Exception e){
			
		}
		
	}
	
}

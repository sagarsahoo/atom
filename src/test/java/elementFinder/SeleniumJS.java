package elementFinder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class SeleniumJS {
	@Test
	public void f() throws ScriptException, IOException, NoSuchMethodException {
		WebDriver _driver = new FirefoxDriver();
		_driver.get("http://www.w3schools.com/");
		//String script = "alert(\"test\")";
		JavascriptExecutor js = (JavascriptExecutor)_driver;
		//js.executeScript("C:\\Accenture\\Project\\selenium\\workspace\\Automation\\src\\test\\java\\elementFinder\\FunctionsJS.js");
		js.executeScript(showAlert("sagar"));
	}
	
	public static String showAlert(String test){
		String script = "alert(\""+test+"\")\";";
		return script;
	}
}

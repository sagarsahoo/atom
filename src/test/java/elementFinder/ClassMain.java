package elementFinder;

import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.script.*;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class ClassMain {

	public static void main(String[] args) throws ScriptException, IOException, NoSuchMethodException {
		
		
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");
		// read script file
		//engine.eval(Files.newBufferedReader(Paths.get("C:/Accenture/Project/selenium/workspace/javascript_tut/src/main/java/jquerypkg/FunctionsJS.js"), StandardCharsets.UTF_8));
		FileReader fr = new FileReader("C:/Accenture/Project/selenium/workspace/javascript_tut/src/main/java/jquerypkg/FunctionsJS.js");
		engine.eval(fr);
		Invocable inv = (Invocable) engine;
		// call function from script file
		inv.invokeFunction("Alert", "sagar");

	}

}

package webService;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.testng.annotations.Test;

public class HTTPConnection {
	private final String USER_AGENT = "Mozilla/5.0";
	@Test
	public void requestProcess() {
		try{
			HTTPConnection httpObject = new HTTPConnection();
			System.out.println("Testing 1 - Send Http GET request");
			httpObject.sendGet();

			System.out.println("\nTesting 2 - Send Http POST request");
			httpObject.sendPost();
		}catch(Exception e){
			System.out.println("Exception is - "+ e);
		}		
	}

	// HTTP GET request
	private void sendGet() throws Exception {
		String url = "http://www.google.com/search?q=mkyong";
		//String url = "https://www.soapui.org/rest-testing/getting-started.html";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();		
		con.setRequestMethod("GET"); // optional default is GET		
		con.setRequestProperty("User-Agent", USER_AGENT); //add request header
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		System.out.println(response.toString()); //print result

	}

	// HTTP POST request
	private void sendPost() throws Exception {
		String url = "https://selfsolve.apple.com/wcResults.do";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		System.out.println(response.toString());//print result

	}
}

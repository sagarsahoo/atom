package webService;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;

public class ResposeUsingJavaXML {
  public static void main(String[] args) {
	  try {
	      SOAPConnectionFactory sfc = SOAPConnectionFactory.newInstance();
	      SOAPConnection connection = sfc.createConnection();

	      MessageFactory mf = MessageFactory.newInstance();
	      SOAPMessage smsg = mf.createMessage();

	      SOAPHeader shead = smsg.getSOAPHeader();

	      SOAPBody sbody = smsg.getSOAPBody();
	      shead.detachNode();
	      QName bodyName = new QName("http://www.webserviceX.NET", "GetAtomicNumber", "web");
	      SOAPBodyElement bodyElement = sbody.addBodyElement(bodyName);
	      QName qn = new QName("ElementName");
	      SOAPElement quotation = bodyElement.addChildElement(qn);

	      quotation.addTextNode("iron");

	      System.out.println("\n Soap Request:\n");
	      smsg.writeTo(System.out);
	      System.out.println();

	      URL endpoint = new URL("http://www.webservicex.net/periodictable.asmx");
	      SOAPMessage response = connection.call(smsg, endpoint);

	    System.out.println("\n Soap Response:\n");

	     System.out.println(response.getContentDescription());


	    } catch (Exception ex) {
      ex.printStackTrace();
    }
}
}
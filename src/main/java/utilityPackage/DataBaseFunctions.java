package utilityPackage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DataBaseFunctions {


	public static String db_server_ip ;
	public static String db_server_port;
	public static String db_service_name;
	public static String db_username;
	public static String db_password;

	static String path = System.getProperty("user.dir");
	static String downloadDataFile = path + "\\TestData\\downloadDataSheet.xls";
	static String uploadFIle = path + "\\TestData\\ABCD_Scripting.xls";

	static Logger DataBaseFunctions_log = Logger.getLogger(DataBaseFunctions.class);


	/*public static String buildQuery(String sqlQuery,String condition,String value){
		try{
			String customisedQuery= null;
			customisedQuery = sqlQuery + " where " + condition + "'" + value + "'" ;
			return customisedQuery;
		}
		catch(Exception e){
			DataBaseFunctions_log.info("Error in building query - ", e);
			return null;
		}
	}*/





	@Test
	public static List<String> getResultArray(String query,String DBname) {
		try{
			List<String> resultList = new ArrayList<String>();
			Connection con = getDBConnection(DBname);   			//Function to get the connection
			Statement stmt=con.createStatement();  			//step3 create the statement object 
			ResultSet rs=stmt.executeQuery(query);  			//step4 execute query
			String result[][] = storeVariable(rs,query,DBname);
			stmt.close();
			con.close();
			for(int cnt=0;cnt<result.length;cnt++){
				resultList.add(result[0][cnt]);
			}
			return resultList;
		}
		catch(Exception e){
			DataBaseFunctions_log.error("Exception in get Result - ",e);
			return null;
		}  
	}

	//Getting result from SQL query 
	public static String getResult(String query,String DBname) {
		try{
			Connection con = getDBConnection(DBname);   			//Function to get the connection
			Statement stmt=con.createStatement();  			//step3 create the statement object 
			ResultSet rs=stmt.executeQuery(query);  			//step4 execute query
			String VariableArray[][] = storeVariable(rs,query,DBname);
			String result = VariableArray[0][0];
			stmt.close();
			con.close();
			return result;
		}
		catch(Exception e){
			DataBaseFunctions_log.error("Exception in get Result - ", e);
			return null;
		}  
	}


	//Download data to Excel  
	public static void downloadDataToExcel(String query,String DBname) {



		try {

			DataBaseFunctions_log.info("Writing method in excel -------------------");

			Connection con = getDBConnection(DBname);   			//Function to get the connection
			Statement stmt=con.createStatement();  			//step3 create the statement object 
			ResultSet rs=stmt.executeQuery(query);  			//step4 execute query


			writeDataToExcel(rs,query,DBname);
			stmt.close();
			con.close();


		} catch (SQLException e1) {
			e1.printStackTrace();

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}




	//Import data from excel to DB
	public static void excelKeywordScriptingToDB(String sheetToUpload,String testCaseName,String DBname){
		try{

			Connection excelToBD_con = getDBConnection(DBname); 
			excelToBD_con.setAutoCommit(false);


			//Deleting existing entries
			Statement delete_stmt = excelToBD_con.createStatement();
			String delete_query= "delete from abcdscripting where tcname=('"+testCaseName+"')" ;
			delete_stmt.execute(delete_query);
			excelToBD_con.commit();
			DataBaseFunctions_log.info("Existing entries for the testcaseName - "+testCaseName + " has been deleted.\n" );


			//Inserting null into first row
			Statement insertNUll =  excelToBD_con.createStatement();
			String insertQuery = "INSERT INTO abcdscripting VALUES(null,'"+testCaseName+"','N','TS_0',null,null,null,null,null,null,null,null,null,null)";
			insertNUll.execute(insertQuery);
			excelToBD_con.commit();


			PreparedStatement pstm = null ;
			FileInputStream input = new FileInputStream(uploadFIle);
			POIFSFileSystem fs = new POIFSFileSystem( input );
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			HSSFSheet sheet = wb.getSheet(sheetToUpload);

			Row row;
			for(int i=1; i<=sheet.getLastRowNum(); i++){
				row = sheet.getRow(i);

				int tcid = (int) row.getCell(0).getNumericCellValue();
				String tcname = row.getCell(1).getStringCellValue();
				String execflag = row.getCell(2).getStringCellValue();
				String tsid = row.getCell(3).getStringCellValue();
				String tsname = row.getCell(4).getStringCellValue();
				String keyword = row.getCell(5).getStringCellValue();
				String elementtype = row.getCell(6).getStringCellValue();
				String elementidentifiedby = row.getCell(7).getStringCellValue();
				String elementidentifier = row.getCell(8).getStringCellValue();
				String passvalues = row.getCell(9).getStringCellValue();
				String targetidentifiedby = row.getCell(10).getStringCellValue();
				String targetidentifier = row.getCell(11).getStringCellValue();
				String teststepsegment = row.getCell(12).getStringCellValue();
				String result = row.getCell(13).getStringCellValue();

				//Formatting the xpath expression
				if(elementidentifier.contains("'")){
					elementidentifier =elementidentifier.replace("'", "''");
				}

				if(targetidentifier.contains("'")){
					targetidentifier =targetidentifier.replace("'", "''");
				}


				String sql = "INSERT INTO abcdscripting VALUES('"+tcid+"','"+tcname+"','"+execflag+"','"+tsid+"','"+tsname+"','"+keyword+"','"+elementtype+"','"+elementidentifiedby+"','"+elementidentifier+"','"+passvalues+"','"+targetidentifiedby+"','"+targetidentifier+"','"+teststepsegment+"','"+result+"')";
				System.out.println("The SQL query is - "+ sql);
				pstm = (PreparedStatement) excelToBD_con.prepareStatement(sql);
				pstm.execute();
				DataBaseFunctions_log.info("Import rows "+i);

			} //End of iterator

			excelToBD_con.commit();
			pstm.close();
			excelToBD_con.close();
			input.close();
			DataBaseFunctions_log.info("Success import excel to abcdscripting table");

		}catch(SQLException ex){
			DataBaseFunctions_log.error(ex);
		}catch(IOException ioe){
			DataBaseFunctions_log.error(ioe);
		}
	}




	//    *************************************************************************************************************************************** 


	//Supporting function - Getting DB connection
	private static Connection getDBConnection(String databaseName) throws FileNotFoundException, SQLException{
		try{
			if(databaseName.equals("NA")){
				db_server_ip = RepositoryObject.IdentifierRepo(TestCaseConfiguration.testConfData[1], "database_server_ip");
				db_server_port = RepositoryObject.IdentifierRepo(TestCaseConfiguration.testConfData[1], "database_server_port");
				db_service_name = RepositoryObject.IdentifierRepo(TestCaseConfiguration.testConfData[1], "database_service_name");
				db_username = RepositoryObject.IdentifierRepo(TestCaseConfiguration.testConfData[1], "database_username");
				db_password = RepositoryObject.IdentifierRepo(TestCaseConfiguration.testConfData[1], "database_password");			  
			}
			else{
				db_server_ip = RepositoryObject.IdentifierRepo(TestCaseConfiguration.testConfData[1], databaseName+ "_database_server_ip");
				db_server_port = RepositoryObject.IdentifierRepo(TestCaseConfiguration.testConfData[1], databaseName+"_database_server_port");
				db_service_name = RepositoryObject.IdentifierRepo(TestCaseConfiguration.testConfData[1], databaseName+"_database_service_name");
				db_username = RepositoryObject.IdentifierRepo(TestCaseConfiguration.testConfData[1], databaseName+"_database_username");
				db_password = RepositoryObject.IdentifierRepo(TestCaseConfiguration.testConfData[1], databaseName+"_database_password");			  
			}
			int port = Integer.parseInt(db_server_port);
			String connString = "jdbc:oracle:thin:@" + db_server_ip + ":" + port + "/" + db_service_name;
			OracleDataSource ods = new OracleDataSource();
			ods.setURL(connString);
			ods.setUser(db_username);
			ods.setPassword(db_password);
			Connection con = ods.getConnection();
			return con;

		}
		catch(Exception e){		  
			DataBaseFunctions_log.error("Exception in connection - ",e);
			Assert.fail();
			return null;
		}	  	  	  
	}




	//Storing the query value in variable
	private static String[][] storeVariable(ResultSet resultSet,String Query,String DBname){
		try{

			ResultSetMetaData rsmd = resultSet.getMetaData();
			//Calling function to get number of rows
			int rowsCount = getNumberOfRows(Query,DBname);
			DataBaseFunctions_log.info("Number of rows fetched - "+ rowsCount);
			int columnCount = rsmd.getColumnCount();
			DataBaseFunctions_log.info("Number of columns - "+ columnCount);
			//Calling function to get column list
			String[] columnNames = getColumnNames(rsmd);
			String[][] variableArray = new String[rowsCount][columnCount];
			int rowNum_counter=0;
			while (resultSet.next()){
				for( int iterator=1; iterator <= columnCount; iterator++ ) { 
					// data type in database
					String columnType = rsmd.getColumnTypeName( iterator ); 
					//System.out.println("Column type is - "+ columnType);
					switch(columnType){

					case "VARCHAR2" :
						//System.out.print(resultSet.getString(columnNames[iterator-1]));
						variableArray[rowNum_counter][iterator-1] = resultSet.getString(columnNames[iterator-1]);
						break;

					case "NUMBER" :
						//System.out.print(resultSet.getInt(columnNames[iterator-1]));
						String str = Integer.toString(resultSet.getInt(columnNames[iterator-1]));
						variableArray[rowNum_counter][iterator-1] = str;
						break;

					} //End of Switch

				}	// End of for loop for column iterator	
				rowNum_counter++;

			} // End of result set

			return variableArray;	  
		}
		catch(Exception e){
			System.out.println("Exception in storing result in variable - "+ e);
			return null;
		}

	}



	//Writing data into Excel
	@SuppressWarnings("deprecation")
	private static void writeDataToExcel(ResultSet resultSet,String Query,String DBname){

		try{

			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("data_import");	    
			HSSFRow rowhead = sheet.createRow((short) 0);


			ResultSetMetaData rsmd = resultSet.getMetaData();


			int rowsCount = getNumberOfRows(Query,DBname);  //Calling function to get number of rows
			DataBaseFunctions_log.info("Number of rows fetched - "+ rowsCount);

			int columnCount = rsmd.getColumnCount(); // Getting number of columns
			DataBaseFunctions_log.info("Number of columns - "+ columnCount);

			//Calling function to get column list
			String[] columnNames = getColumnNames(rsmd);



			//writing column names in excel
			System.out.println(" writing column names in excel !!!!");
			for(int col_no=0; col_no<columnCount;col_no++){

				rowhead.createCell((short) col_no).setCellValue(columnNames[col_no]);
			}
			//Writing result in excel
			System.out.println("Writing data in excel");
			int i = 1;
			while (resultSet.next()){

				HSSFRow row = sheet.createRow((short) i);

				System.out.println("**************************************** ROW - "+ i);

				for( int iterator=1; iterator <= columnCount; iterator++ ) { 

					// data type in database
					String columnType = rsmd.getColumnTypeName( iterator ); 

					DataBaseFunctions_log.info( "Column Type : " + columnType );
					DataBaseFunctions_log.info("Writing value - "+ resultSet.getString(columnNames[iterator-1]));



					//switch case for column data type 
					switch(columnType){

					case "VARCHAR2" :
						row.createCell((short) (iterator-1)).setCellValue(resultSet.getString(columnNames[iterator-1]));
						break;

					case "NUMBER" :
						row.createCell((short) (iterator-1)).setCellValue(resultSet.getInt(columnNames[iterator-1]));
						break;

					}

				} // for count of columns

				i++;

			}

			String target_file = downloadDataFile;
			FileOutputStream fileOut = new FileOutputStream(target_file);

			workbook.write(fileOut);
			fileOut.close();

		}
		catch(Exception e){

		}

	}



	//Getting number of rows
	private static int getNumberOfRows(String Query,String DBname){

		int rows = 0;
		try{

			DataBaseFunctions_log.info("Query is - "+ Query);

			int position = Query.indexOf("from");
			DataBaseFunctions_log.info("Position is : "+position );

			String condition = Query.substring(position,Query.length());
			DataBaseFunctions_log.info("CONDITION IS - "+ condition);

			String queryCountRows = "Select count(*) " + condition;
			DataBaseFunctions_log.info("Rows count query - "+ queryCountRows);


			Connection rowsConnection = getDBConnection(DBname);  
			Statement stmt_rowCount=rowsConnection.createStatement();  			
			ResultSet rowsCount =stmt_rowCount.executeQuery(queryCountRows);


			//Getting row count
			int row_num=0;
			while (rowsCount.next()){
				rows = rowsCount.getInt("count(*)");
				DataBaseFunctions_log.info("rows returned from funtion - "+ rows);
				row_num++;				
			}

			stmt_rowCount.close();
			rowsConnection.close();
			return rows;
		}
		catch(Exception e ){
			DataBaseFunctions_log.error("The exception is - ",e);
			return 0;
		}
	}


	// Getting the column names from the result query
	private static String[] getColumnNames(ResultSetMetaData resultSetMD){
		try{

			int columns = resultSetMD.getColumnCount();
			String[] columnListArray = new String[columns];

			//Store the column names in a array of String
			for (int i = 1; i < columns + 1; i++) {
				String columnName = resultSetMD.getColumnName(i);
				columnListArray[i-1] = resultSetMD.getColumnName(i);

			}		  

			return columnListArray;
		}
		catch(Exception e){
			DataBaseFunctions_log.error("The exception in getting column names - ",e);
			return null;
		}
	}
	
	public static String sqlQueryString(List<String> UIresult){
		String dbQuery="";
		try{
			List<String> value = new ArrayList<String> ();
			dbQuery = ActionPerformer.element_identifiedBy;
			String matchingString= null;
			while(dbQuery.contains("UserInput_excelSheet")||dbQuery.contains("UIresult")){
				//Taking DB value from UserInput Excel Sheet
				if(dbQuery.contains("UserInput_excelSheet")){
					String ele_identifiedByColumnName = ActionPerformer.test_step + "_element_identifiedBy";
					DataBaseFunctions_log.info("Column to find in the UserInputExcelSheet  - "+ ele_identifiedByColumnName);
					value.add(UserInputDataSheet.getPassValues(ActionPerformer.testDataRow,ele_identifiedByColumnName,ActionPerformer.UserInputData));
					matchingString="UserInput_excelSheet";
					DataBaseFunctions_log.info("Value retrieved from UserInput Excelsheet for the query - "+ value.get(value.size() -1));
					dbQuery = DataBaseFunctions.queryFormation(ActionPerformer.element_identifiedBy, value,matchingString);
					DataBaseFunctions_log.info("Query formed by replacing UserInput_excelSheet values - "+ dbQuery );
				}
				//Getting value from UIresult
				else if(dbQuery.contains("UIresult")){
					matchingString="UIresult";
					dbQuery = DataBaseFunctions.queryFormation(ActionPerformer.element_identifiedBy, UIresult,matchingString);
					DataBaseFunctions_log.info("Query formed by replacing UIresult - "+ dbQuery );
				}
			}						
			DataBaseFunctions_log.info("Final Query formed is - " + dbQuery );

			/*String[][] allValue = DataBaseFunctions.getResultArray(dbQuery,ActionPerformer.element_identifier);
			for (int i = 0; i < allValue.length; i++) {
				dbResult.add(allValue[i][0]);
				DataBaseFunctions_log.info("Result of query is - "+ dbResult.get(i));
			}*/
		}catch(Exception e){
			DataBaseFunctions_log.error("Exception in creating SQL query - "+ e);
		}
		return dbQuery;
	}
	
	public static String queryFormation(String sqlQuery,List<String> result,String matchNode){
		String querybuild=null;
		try{
			switch(matchNode){
			//Finding for value in UserInput Excel sheet
			case "UserInput_excelSheet":
				querybuild = UserInputDataSheet.formDBQueryFromUserInputValues(sqlQuery,result);
				return querybuild;

			case "UIresult":
				//Finding for UIresult
				if(sqlQuery.contains("UIresult")&& (!sqlQuery.contains("UIresult("))){
					querybuild = sqlQuery.replace("UIresult", result.get(result.size()-1));
				}
				//FInding for index in UI result
				else if(sqlQuery.contains("UIresult(")){
					querybuild = JavaFunctions.getQuery(sqlQuery,result);
				}
				return querybuild;
				
			case "dbResult":
				break;
			}
			return querybuild;
		}
		catch(Exception e){
			DataBaseFunctions_log.error("The Exception in building Query is -  ",e);
			return querybuild;
		}
	}
}




package utilityPackage;


import org.apache.log4j.Logger;
import org.testng.annotations.Test;

public class Calculator {
	
	static  Logger calculatorLog = Logger.getLogger(Calculator.class);
	
  @Test  
  public static int getCalculatorResult(String tableIdentifiedBy,String tableIdentifier,String pass_values,String FirstCellPosition,String secondValue) {
	  
	  int[] operationParameters = new int[2];
	  int calculatedResult = 0;
	  
	  //First cell position has been provided as r,c
	  //Second cell position could be r,c/variable/value
	  
	  int FirstParameter = 0;
	  int SecondParameter = 0;
	  int FirstCellRowIndex = 0;
	  int FirstCellColumnIndex=0;
	  int SecondCellRowIndex=0;
	  int SecondCellColumnIndex=0;
	  
	  
	  try{
		  
		  //Condition to add from two cell positions
		  //Getting first parameter
		  String[] FirstCell_row_column = FirstCellPosition.split(",");
		  calculatorLog.info(FirstCell_row_column[0]);
		  calculatorLog.info(FirstCell_row_column[1]);
		  
		  FirstCellRowIndex = Integer.parseInt(FirstCell_row_column[0]);
		  FirstCellColumnIndex = Integer.parseInt(FirstCell_row_column[1]);
		  
		  FirstParameter = Integer.parseInt(WebTable.GetText(tableIdentifiedBy, tableIdentifier, FirstCellRowIndex, FirstCellColumnIndex));
		  calculatorLog.info("FirstParameter is - "+ FirstParameter);
		  
		  
		  //Getting second parameter
		  
		  //Checking whether the second parameter is from a cell
		  if(secondValue.contains(",")){
			  calculatorLog.info("Second value is from cell position");
			  String[] SecondCell_row_column = secondValue.split(",");
			  calculatorLog.info(SecondCell_row_column[0]);
			  calculatorLog.info(SecondCell_row_column[1]);
			  
			  SecondCellRowIndex = Integer.parseInt(SecondCell_row_column[0]);
			  SecondCellColumnIndex = Integer.parseInt(SecondCell_row_column[1]);
			  
			  SecondParameter = Integer.parseInt(WebTable.GetText(tableIdentifiedBy, tableIdentifier, SecondCellRowIndex, SecondCellColumnIndex));
			  calculatorLog.info("SecondParameter is - "+ SecondParameter);
		  }
		  else{
			  SecondParameter = Integer.parseInt(secondValue);
		  }
		  
		  operationParameters[0] = FirstParameter;
		  calculatorLog.info("operationParameters - First parameter - "+ operationParameters[0]);
		  operationParameters[1] = SecondParameter;
		  calculatorLog.info("operationParameters - Second parameter - "+ operationParameters[1]);
		  
		  
		  
		  //Going for the operations
		  calculatorLog.info("pass_values inside calculator is - "+ pass_values);
		  switch(pass_values){
		  
		  	case "add":
		  		calculatedResult=add(operationParameters);
			  break;
			  
		  	case "Row_series_add":
		  		int series_addResult = 0;
		  		calculatorLog.info("Inside the Row_series_add");	  		
		  		calculatorLog.info("FirstCellColumnIndex - "+ FirstCellRowIndex);
		  		calculatorLog.info("SecondCellColumnIndex - "+ SecondCellColumnIndex);
		  		for(int row_count=FirstCellColumnIndex;row_count<=SecondCellColumnIndex;row_count++){
		  			calculatorLog.info("Web Element - "+ WebTable.GetText(tableIdentifiedBy, tableIdentifier, SecondCellRowIndex, row_count));
		  			int next_value = Integer.parseInt(WebTable.GetText(tableIdentifiedBy, tableIdentifier, SecondCellRowIndex, row_count));
		  			calculatorLog.info("Next value is - " + next_value);
		  			series_addResult = series_addResult + next_value;
		  			calculatorLog.info("series_addResult - "+series_addResult);
		  		}
		  		calculatedResult=series_addResult;
		  		break;
		  		
			case "Column_series_add":
		  		int series_addResult_col = 0;
		  		calculatorLog.info("Inside the Column_series_add");
		  		calculatorLog.info("FirstCellRowIndex - "+ FirstCellRowIndex);
		  		calculatorLog.info("SecondCellRowIndex - "+ SecondCellColumnIndex);
		  		for(int row_count=FirstCellRowIndex;row_count<=SecondCellRowIndex;row_count++){
		  			int next_value = Integer.parseInt(WebTable.GetText(tableIdentifiedBy, tableIdentifier, row_count, SecondCellColumnIndex));
		  			calculatorLog.info("Next value is - " + next_value);
		  			series_addResult = series_addResult_col + next_value;
		  			calculatorLog.info("series_addResult - "+series_addResult_col);
		  		}
		  		calculatedResult=series_addResult_col;
		  		break;
		  		
			  
		  	case "subtract":
		  		calculatedResult=subtract(operationParameters);
		  		break;
		  		
		  	case "multiply":
		  		calculatedResult=multiply(operationParameters);
		  		break;
		  		
		  	case "divide":
		  		calculatedResult=divide(operationParameters);
		  		break;	
		  		
		  	default:
		  		calculatorLog.info("Operation in pass_values did not match in the switch case");
		  		calculatorLog.info("Please check the value passed in pass_values column in the Scripting sheet");
		  
		  }
		  calculatorLog.info("calculatedResult - "+ calculatedResult);
		  return calculatedResult;
	  }
	  catch(Exception e){
		  calculatorLog.error("Exception in the getValues mrthod in calculator - ",e);
		  return calculatedResult;
	  }
	  
	 
  }
  
  
  
  public static int add(int[] parameters){
	  int result = 0;
	  try{		  
		  result = parameters[0] + parameters[1];
		  return result;
	  }
	  catch(Exception e){
		  calculatorLog.error("Exception in add method inside Calculator - ",e);
		  return result;
	  }
  }
  
  
  public static int subtract(int[] parameters){
	  int result = 0;
	  try{		  
		  result = parameters[0] - parameters[1];
		  return result;
	  }
	  catch(Exception e){
		  calculatorLog.error("Exception in add method inside Calculator - ",e);
		  return result;
	  }
  }
  
  public static int multiply(int[] parameters){
	  int result = 0;
	  try{		  
		  result = parameters[0] * parameters[1];
		  return result;
	  }
	  catch(Exception e){
		  calculatorLog.error("Exception in add method inside Calculator - ",e);
		  return result;
	  }
  }
  
  public static int divide(int[] parameters){
	  int result = 0;
	  try{		  
		  result = parameters[0]/parameters[1];
		  return result;
	  }
	  catch(Exception e){
		  calculatorLog.error("Exception in add method inside Calculator - ",e);
		  return result;
	  }
  }
  
}

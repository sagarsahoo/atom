package utilityPackage;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;


public class BrokenLinks {
  @Test
  public static List<String> checkBrokenLinks(WebDriver driver) throws Exception {
	  
	  	List<String> statusLinksList = new ArrayList<String>();
	    List<WebElement> allImages = findAllLinks(driver);    
	    System.out.println("Total number of elements found " + allImages.size());
	    String linkStatus;
	    for( WebElement element : allImages){
	    try{
	    	linkStatus="URL: " + element.getAttribute("href")+ " returned " + isLinkBroken(new URL(element.getAttribute("href")));
		    System.out.println(linkStatus);
	    	statusLinksList.add(linkStatus);
	    }catch(Exception exp){ 
	    		System.out.println("At " + element.getAttribute("innerHTML") + " Exception occured -&gt; " + exp.getMessage());	    		
	    	}
	    }
	    return statusLinksList;
  }
  
  private static List<WebElement> findAllLinks(WebDriver driver)  
  {
	  List<WebElement> elementList = new ArrayList<WebElement>(); 
	  elementList.addAll(driver.findElements(By.tagName("a")));
	  elementList.addAll(driver.findElements(By.tagName("img")));
 
	  List<WebElement> finalList = new ArrayList<WebElement>(); 
	  for (WebElement element : elementList) 
	  {
		  if(element.getAttribute("href") != null)
		  { 
			  finalList.add(element); 
		  }		   
	  }	 
	  return finalList;
  }
 
	private static String isLinkBroken(URL url) throws Exception
	{
		String response = "";
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		try{
		    connection.connect();
		    response = connection.getResponseMessage();	         
		    connection.disconnect();
		    return response;
		} 
		catch(Exception exp) { 
			return exp.getMessage();
		}
	}
}

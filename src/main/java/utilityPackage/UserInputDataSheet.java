package utilityPackage;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

public class UserInputDataSheet {
	
	public static String[] userSheetInfo = new String[3];
	public static int testRow = 0;
	public static int destColumn_no = 0;
	
	
	static Logger UserInputDataSheet_log = Logger.getLogger(UserInputDataSheet.class);
	
	//For setting values for user Input data sheet
	public static void setValues(String workBookLocation,String WorkBookName,String SheetName){
		try{
			
			UserInputDataSheet_log.info("Inside Set value function of UserInputDataSheet Class for the sheet - "+SheetName );
			UserInputDataSheet_log.info("Workbooklocation"+workBookLocation+" WorkBookName - "+WorkBookName);
			
			userSheetInfo[0] = workBookLocation;
			userSheetInfo[1] = WorkBookName;
			userSheetInfo[2] = SheetName;
			
			UserInputDataSheet_log.info(userSheetInfo[0]+"*****"+userSheetInfo[1]+"******"+ userSheetInfo[2]);
			
		}
		catch(Exception e){
			UserInputDataSheet_log.error("Exception occured - "+e);
		}
	}
	
	
	// Function to retrieve the test data row for input from the user input Data sheet  
	@Test
	public static int getEffectiveRow(String sheet_name,String wb_loc,String UserInputFile,String[][] UserInputData){
		try{
			String columnName = "RESULT";
			int RESULTcolumn = ExcelFunctions.getColumnPosition(wb_loc, UserInputFile, sheet_name, columnName);
			UserInputDataSheet_log.info("The RESULT column found in - "+ RESULTcolumn);
			//Code for selecting the test data row
			UserInputDataSheet_log.info("UserInputData length - "+ UserInputData.length);
			for(int row_num=1;row_num< UserInputData.length;row_num++){
				
				if(UserInputData[row_num][RESULTcolumn].equalsIgnoreCase("NA")){
					UserInputDataSheet_log.info("Destination row is - "+ row_num);
					testRow=row_num;
					return testRow;
				}
			}
			
		}
		catch(Exception e){
			UserInputDataSheet_log.error("Exception occured in getEffectiveRow - "+e);
			return -1;
		}		
		return -1;	
	}
	
	
	//To get the position of the Field defined mainly the column
	//In this case RESULT column position is retrieved 
  public static String getPassValues(int testDataRow,String test_step,String[][] UserInputData) {
	  String pass_value = null;
	  
	  try{
		  
		  UserInputDataSheet_log.info("Test step inside getPassValues is - "+test_step );
		  int column_length = UserInputData[0].length;
		  //int destColumn_no = 0;
		  
		  for(destColumn_no = 0;destColumn_no<column_length;destColumn_no++){
			  UserInputDataSheet_log.info("Data to be compared - "+ UserInputData[0][destColumn_no]);
			  if(UserInputData[0][destColumn_no].equalsIgnoreCase(test_step)){
				  pass_value = UserInputData[testDataRow][destColumn_no];
				  UserInputDataSheet_log.info("PASSED VALUE - "+pass_value );
				  return pass_value;
			  }
		  }
		  
		
		  
	  }
	  catch(Exception e){
		  UserInputDataSheet_log.error("Exception in User Input Data function is - "+e);
		  return null;
	  }
	return pass_value;
	
  }
  
  
  //To map the result of the test run
  public static void mapResult(String status){
	  try{
		  UserInputDataSheet_log.info("status is -"+status);
		  UserInputDataSheet_log.info("Inside mapResult function");		  
		  UserInputDataSheet_log.info(userSheetInfo[0]+userSheetInfo[1]+userSheetInfo[2]);
		  boolean result=false;
		  if(status.equalsIgnoreCase("PASS")){
			  result = true;
		  }else{
			  result = false;
		  }
		  ExcelFunctions.writeToExcel(userSheetInfo[0], userSheetInfo[1], userSheetInfo[2], testRow, result);
		  
	  }
	  catch(Exception e){
		  UserInputDataSheet_log.error("Exception in result map function is - "+e);
		  
	  }
  }
  
  public static String formDBQueryFromUserInputValues(String sqlQuery, List userInputValues){
	  String newQuery=null;
	  try{
		  
		  
		  //multiple values separated by '|' will be stored in a array
		  String value = (String) userInputValues.get(0);
		  String[] queryValues = SplitString.stringSplit(value);
		  
		  for(int valueCnt=0;valueCnt<queryValues.length;valueCnt++){
			 //newQuery = sqlQuery.replace(queryValues[valueCnt],"UserInput_excelSheet");
			  newQuery = sqlQuery.replaceFirst("UserInput_excelSheet", queryValues[valueCnt]);
			  sqlQuery=newQuery;
			  System.out.println((valueCnt+1)+ " modified query is - "+ sqlQuery);
		  }
		  
		  return newQuery;
	  }
	  catch(Exception e){
		  return newQuery;
	  }
  }
  
  
}

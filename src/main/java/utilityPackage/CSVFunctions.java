package utilityPackage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class CSVFunctions {
	
	static Logger CSVFunctions_log = Logger.getLogger(CSVFunctions.class);
	@Test
	public static void readCSV(String csv,String splitter) {
		String csvFile = csv;
		String cvsSplitBy = splitter;
		BufferedReader br = null;
		String line = "";
		String quote = "\"";
		try {

			int line_no = 0;
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				// use comma as separator
				String[] country = line.split(cvsSplitBy);
				System.out.println("Record - " + (line_no+1));
				for(int element_no =0; element_no<country.length; element_no++){
					System.out.println("element "+ element_no + " is - "+ country[element_no].replace(quote, ""));
				}
				line_no++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("Done");
	}

	//Search a word in csv file
	public static void searchWordInCSV(){


	}


	//Write a record in a new CSV file
	public static void writeCSV(String csvFilePath,String recordtoInsert) throws IOException{
		try{
			String csv_file = csvFilePath;
			String record = recordtoInsert;
			CSVWriter writer = new CSVWriter(new FileWriter(csv_file));
			String [] recordElement_list = record.split(","); //Create record			
			writer.writeNext(recordElement_list); //Write the record to file			
			writer.close(); //close the writer
		}
		catch(Exception e){
			System.out.println("The exception in writeCSV method is - "+ e);
		}
	}




	//Append a record in an existing csv file
	public static void appendCSV(String csv,String recordtoAppend) throws IOException{
		try{
			String csv_file = csv;
			String record = recordtoAppend;
			CSVWriter writer = new CSVWriter(new FileWriter(csv_file, true));
			String [] recordElement_list = record.split(",");
			writer.writeNext(recordElement_list);
			writer.close();
		}
		catch(Exception e){
			System.out.println("The exception in appendCSV method is - "+ e);
		}  
	}




	//To validate whether a given word or string is present
	public static boolean validateWord(String csvFileLoc,String wordtoValidate){
		try{
			String csvFile = csvFileLoc;
			String word = wordtoValidate;
			String cvsSplitBy = ","; 
			BufferedReader br = null;
			String line = "";
			String quote = "\"";
			int match_count = 0;
			int line_no = 0;
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				String[] country = line.split(cvsSplitBy);
				for(int element_no =0; element_no<country.length; element_no++){
					if(country[element_no].replace(quote, "").equals(wordtoValidate)){
						System.out.println("match found");
						System.out.println(word+" present in line - "+ (line_no+1) + " position is - "+ (element_no+1));
						match_count++;
					}				
				}	 				
				line_no++;
			}			
			System.out.println("\nTotal number of matches - "+ match_count );
			if(match_count>0){
				return true;
			}
			return false;

		}
		catch(Exception e){
			System.out.println("Exception in validating word in the CSV file - "+ e);
			return false;
		}
	}



	//To validate a give record
	public static boolean validateRecord(String csvFileLoc,String recordtoValidate){

		try{
			boolean match_result=false;
			String csvFile = csvFileLoc;
			String record = recordtoValidate; //find "this","record","inserted"
			String cvsSplitBy = ","; 

			BufferedReader br = null;
			String line = "";
			String quote = "\"";

			match_result = false;
			String[] word_list = record.split(cvsSplitBy);    //Splitting the record and storing in array
			int target_record_ele_count = word_list.length;   // Target record count
			String first_element = word_list[0].replace(quote, ""); //FIrst element of target
			int line_no = 0;
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				String[] country = line.split(cvsSplitBy);
				System.out.println("Record - " + (line_no+1));
				System.out.println("Record length in target record - "+ target_record_ele_count);
				System.out.println("Record length in CSV file - "+ country.length);
				int match_count = 0;

				//target record length should be <= line length
				if(target_record_ele_count<=country.length){

					for(int element_no =0; element_no<country.length; element_no++){

						//Logic for first element match
						if(country[element_no].replace(quote, "").equalsIgnoreCase(first_element)){
							System.out.println("*********** First element match found");
							match_count++;
							//match_var.add(first_element);

							//logic for remaining elements
							for(int rem_ele=1; rem_ele<target_record_ele_count; rem_ele++){
								//System.out.println("Remaining elements - "+ rem_ele);
								if(country[element_no+rem_ele].replace(quote, "").equalsIgnoreCase(word_list[rem_ele])){
									System.out.println("Element no in CSV file - "+ (element_no+rem_ele)+ "   ###### " + "Element - "+ country[element_no+rem_ele].replace(quote, ""));
									System.out.println("Element no in target record - "+rem_ele + "   #### " + "Element - "+ word_list[rem_ele] );
									System.out.println("********* Element - "+(rem_ele+1) + " match found"+"\n");
									//match_var.add(word_list[rem_ele]);
									match_count++;
								}
							}
						}

					}

					/*System.out.println("matched upto - ");
					for(String ele:match_var ){
						System.out.print(ele+",");
					}
					System.out.println();*/

				} 
				line_no++;
				System.out.println("Target ele count - "+ target_record_ele_count);
				System.out.println("match count value - "+ match_count+ "\n");

				if(target_record_ele_count==match_count){
					System.out.println("\n%%%%%%%%%%%%%%%%%%%%%%%%%  MATCH FOUND  in RECORD number -" + (line_no) + " %%%%%%%%%%%%%%%%%");
					match_result= true;
					return match_result;
				}
			}			
			if(match_result==false){
				System.out.println("\n############################  SORRY  ------------------  match not found");
				return false;
			}
		}
		catch(Exception e){
			System.out.println("Exception in validating record in CSV file - "+ e);
			return false;
		}
		return false;
	}

}

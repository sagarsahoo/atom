package utilityPackage;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ProvideURL {
	static Logger ProvideURL_log = Logger.getLogger(ProvideURL.class);

	public static void baseUrl(WebDriver driver,String url_string){
		try{
			driver.get(url_string);
			ProvideURL_log.info("The url retrieved is - "+ url_string);	
			if (OpenBrowser.GetBrowserInfo().startsWith("IE") && driver.getTitle().startsWith("Certificate Error")){
				driver.get("javascript:document.getElementById('overridelink').click();");
			}
			
		}
		catch(Exception e){
			ProvideURL_log.error("Exception is - ",e);
			Assert.fail("Failure as exception occurred");
		}
	}
}

package utilityPackage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;


public class Listener  implements ITestListener, ISuiteListener, IInvokedMethodListener {
	
	
	static String path = System.getProperty("user.dir");
	static String workbook_loc = path +"\\TestData\\";
	private static  Logger log = Logger.getLogger(Listener.class);
	
	@Override
	 
	public void onStart(ISuite arg0) {
 
		Reporter.log("About to begin executing Suite " + arg0.getName(), true);
		//Calling method to move the log files into Backup location
		Reporter.log("Moving log files into Backup location");
		CommonMethod.MoveLogFiles();
		
		
 
	}
 
	// This belongs to ISuiteListener and will execute, once the Suite is finished
	@Override 
	public void onFinish(ISuite arg0) {
 
		Reporter.log("About to end executing Suite " + arg0.getName(), true);
		
		//Calling function to convert log file to .html format
		log.info("log folder - "+ CommonMethod.suiteMasterSheet);
		TextToHTML.htmlLog(CommonMethod.suiteMasterSheet);
		
		//Taking backup of testNG report
		 File ExistingReportFile = new File(path + "\\test-output\\emailable-report.html");
			String dateToAppend = TimeClass.getCurrentTimestamp("dd_MMM_yyyy");
			File backupReportFile = new File(path + "\\test-output\\emailable-report_"+ dateToAppend + ".html");			
			try {
				FileUtils.copyFile(ExistingReportFile, backupReportFile);
			} catch (IOException e) {
				log.error("Exception is - ",e);
			}
 
	}
 
	
	
	
	
	// This belongs to ITestListener and will execute before starting of Test set/batch 
	public void onStart(ITestContext arg0) {
 
		Reporter.log("About to begin executing Test " + arg0.getName(), true);
 
	}
	
	
 
	// This belongs to ITestListener and will execute, once the Test set/batch is finished
	public void onFinish(ITestContext arg0) {
 
		Reporter.log("Completed executing test " + arg0.getName(), true);
		//
 
	}
	
	
 
	// This belongs to ITestListener and will execute only when the test is pass
	public void onTestSuccess(ITestResult arg0) {
 
		// This is calling the printTestResults method
		
		
		log.info("Test case has been passed");
		log.info("Test CASE PASSED !!!!!!!!!!!!");
 
		/*try {
			printTestResults(arg0);
			logger.info("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP");
			logger.info("Test case has been passed");
			log.info("Test CASE PASSED !!!!!!!!!!!!");
			
			//Calling function to update the result to PASS in user Input data sheet
			String testResult = "PASS";
			log.info("Mapping the result into the user input sheet");
			UserInputDataSheet.mapResult(testResult);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
 
	}
 
	
	
	// This belongs to ITestListener and will execute only on the event of fail test
	public void onTestFailure(ITestResult result) {
 
		// This is calling the printTestResults method
		log.info("Test case has been failed");
		log.info("Test CASE FAILED !!!!!!!!!!!!");
		
		
		String testCaseName = result.getTestClass().getName();
		String status = "FAIL";
		
		//Calling function to update the result to FAIL in user Input data sheet

		if(UserInputDataSheet.userSheetInfo[1]==null){
			log.info("Cannot write FAIL in UserInput sheet as 'N' is provided against the test case in the TestExecution Mapping sheet");
		}
		else{
			log.info("Mapping the result into the user input sheet");			
			UserInputDataSheet.mapResult(status);
		}
		
		
		
		//BugZilla For defect logging
		/*
		try {
			printTestResults(result);
			
			//Checking for the status in the bugzilla defect list
			String testCaseBugPresent = BugZilla.testStatus(testCaseName);
			
			//To check wheteher bug is present in defect list or not
			if(testCaseBugPresent.equalsIgnoreCase("buggy")){
				logger.info("Bug present in defect list");
				logger.info("No change in defect list");		
			}
			else{
				
				logger.info("New bug");
				logger.info("Bug list should be updated");
				logger.info("Mapping the result in the BugzillaReport.xls file:-  !!!!!!!!!!!!");
				BugZilla.updateDefectList(testCaseName);
			}
				
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	}
	
 
	
	
	
 
	// This belongs to ITestListener and will execute before the main test start (@Test)
	public void onTestStart(ITestResult arg0) {
 
		log.info("The execution of the main test starts now");
 
	}
	
	
	
	
 
	// This belongs to ITestListener and will execute only if any of the main test(@Test) get skipped
	public void onTestSkipped(ITestResult arg0) {
 
		try {
			printTestResults(arg0);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.info("Test Case has been skipped !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
 
	}
 

 
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
 
	}
	
	
 
	// This is the method which will be executed in case of test pass or fail
	// This will provide the information on the test
 
	private void printTestResults(ITestResult result) throws FileNotFoundException {
 
		/*
		Reporter.log("Test Method resides in " + result.getTestClass().getName(), true);
 
		if (result.getParameters().length != 0) {
 
			String params = null;
 
			for (Object parameter : result.getParameters()) {
 
				params += parameter.toString() + ",";
 
			}
 
			Reporter.log("Test Method had the following parameters : " + params, true);
 
		}
 
		String status = null;
 
		switch (result.getStatus()) {
 
		case ITestResult.SUCCESS:
 
			status = "Pass";
 
			break;
 
		case ITestResult.FAILURE:
 
			status = "Failed";
 
			break;
 
		case ITestResult.SKIP:
 
			status = "Skipped";
 
		}
 
		Reporter.log("Test Status: " + status, true);
		
		
		//Calling the mapping method for result
		logger.info("Mapping the result in the MappingSheet.xsl file:-  !!!!!!!!!!!!");
		
		String testCaseName = result.getTestClass().getName();
		
		
		if(status.equalsIgnoreCase("Pass")){
			MappingSheet.testResult(testCaseName, status);
		}
		else{
			status="FAIL";
			MappingSheet.testResult(testCaseName, status);
		}
		*/

	}
 
	
	
	
	
	
	// This belongs to IInvokedMethodListener and will execute before every method including @Before @After @Test
	public void beforeInvocation(IInvokedMethod arg0, ITestResult arg1) {
 
		String textMsg = "About to begin executing following method : " + returnMethodName(arg0.getTestMethod());
 
		Reporter.log(textMsg, true);
 
	}
 
	
	
	
	// This belongs to IInvokedMethodListener and will execute after every method including @Before @After @Test
	public void afterInvocation(IInvokedMethod arg0, ITestResult arg1) {
 
		String textMsg = "Completed executing following method : " + returnMethodName(arg0.getTestMethod());
 
		Reporter.log(textMsg, true);
 
	}

	
	
	
	// This will return method names to the calling function
	private String returnMethodName(ITestNGMethod method) {
 
		return method.getRealClass().getSimpleName() + "." + method.getMethodName();
 
	}

}

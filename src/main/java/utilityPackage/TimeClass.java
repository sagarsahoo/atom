package utilityPackage;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

public class TimeClass {
	
	static Logger TimeClass_log = Logger.getLogger(TimeClass.class);
	
  @Test
  public static String getCurrentTimestamp(String format) {
	  try{
		  Date date = new Date();
	      SimpleDateFormat sdf = new SimpleDateFormat(format);
	      String formattedDate = sdf.format(date);
	      TimeClass_log.info("SimpleDateFormat - "+formattedDate);
	      
	      return formattedDate;
	  }
	  catch(Exception e){
		  TimeClass_log.error("Exception in getCurrentTime function is - "+ e);
		  return  null;
	  }
  }
}

package utilityPackage;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class WindowClass {

	static Logger log = Logger.getLogger(WindowClass.class);

	@Test
	public static Set<String> newWindow(WebDriver wd_driver) throws InterruptedException {

		try{
//			ActionPerformer.WaitForPageload();
			Thread.sleep(5000);
			wd_driver.getTitle();//TODO Since driver object not been used after getting a new window. So getting wrong count of window(s). As of now for temporary fix, reading the browser title.
			Set<String> windowHandler_array2 = wd_driver.getWindowHandles();
			String wind2 = (String) windowHandler_array2.toArray()[windowHandler_array2.size()-1];

			int size = windowHandler_array2.size();
			log.info("Size of array - " + size);

			for(int i=0 ; i<size ; i++ ){
				log.info("Window - " + i + "-" + (String) windowHandler_array2.toArray()[i]);
			}
			wd_driver.switchTo().window(wind2); // switch to new window
			WebElementActions.WaitForPageload();
			log.info("Switched to - " + wind2) ;
			wd_driver.manage().window().maximize();
			wd_driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			if (OpenBrowser.GetBrowserInfo().startsWith("IE") && wd_driver.getTitle().startsWith("Certificate Error")){
				wd_driver.get("javascript:document.getElementById('overridelink').click();");
			}
			return  windowHandler_array2;
		}
		catch(Exception e){
			log.info("Exception in window class - " + e);
		}
		return null;
	}

	public static void switchWindow(WebDriver wd_driver,Set<String> WinHandlerArray) {
		try{
			log.info("************************** Inside SwitchWindow function  ***************************");
			log.info("Size of Array - "+ WinHandlerArray.size());

			if(WinHandlerArray.size()==1){
				String target_window = (String) WinHandlerArray.toArray()[0];
				wd_driver.switchTo().window(target_window);
			}else{
				log.info("Closing window");
				wd_driver.close();
				String closed_window = (String) WinHandlerArray.toArray()[WinHandlerArray.size()-1];
				log.info("Window closed - " + closed_window);
				WinHandlerArray.remove(closed_window);
				log.info("Size of Array after window closed - "+ WinHandlerArray.size());

				for(int i=0 ; i<WinHandlerArray.size() ; i++ ){
					log.info("Window - " + i + "-" + (String) WinHandlerArray.toArray()[i]);
				}

				String target_window = (String) WinHandlerArray.toArray()[WinHandlerArray.size()-1];
				wd_driver.switchTo().window(target_window);
				log.info("Switched to - " + target_window );
			}
		}catch(Exception e){
			log.info("Exception occured while window close");

			String closed_window = (String) WinHandlerArray.toArray()[WinHandlerArray.size()-1];
			log.info("Window closed - " + closed_window);
			WinHandlerArray.remove(closed_window);
			log.info("Size of Array after window closed - "+ WinHandlerArray.size());


			String target_window =  (String) WinHandlerArray.toArray()[WinHandlerArray.size()-1];
			log.info("Switched to - " + target_window );
			wd_driver.switchTo().window(target_window);
		}

	}

	public static void switchFrame(WebDriver _driver,String framelocatedBy,String frame_value){
		try{
			WebElement frameElement =  WebElementActions.GetWebElement(framelocatedBy, frame_value);
			_driver.switchTo().frame(frameElement);
		}catch(Exception e){
			log.info("Exception in window class - " ,e); 
		}

	}

	public static void switchFrame_default(WebDriver _driver){
		_driver.switchTo().defaultContent();
	}



}

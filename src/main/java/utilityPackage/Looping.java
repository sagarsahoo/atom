package utilityPackage;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class Looping {
	static int listsize = 0;
	static int rowCnt=0;
	static int colCnt=0;
	
	static  Logger loopinglog = Logger.getLogger(Looping.class);
	
  @Test
  public static void normalLoop(WebDriver _driver,String test_case_name,String testcase_sheet, String wb_loc,String ScriptingFileName,String propertyFile,String[][] UserInputData,int testDataRow,String cellcategory,String stepRange, String item_list) {
	  try{
		  String[] itemlist_array = SplitString.stringSplit(item_list);
		  listsize = itemlist_array.length;
		  loopinglog.info("Number of times the loop will iterate - "+ listsize);
		  
		  //modifying the starting index of the test step
		  String newTestStepRange = modifyTestStepRange(stepRange);
		  
		  String[][] loopingSteps = ExcelFunctions.getTestSteps(testcase_sheet, wb_loc, ScriptingFileName, newTestStepRange);
		  
		  //calling the method for getting the loopValue- different data for each iteration index
		  getLoopValueIndex(loopingSteps);
		  
		  //Replacing the iterative value in the looping test steps 
		  for(int cnt=0;cnt<listsize;cnt++){
			  loopinglog.info("Iteration - "+ (cnt+1));
			  if(rowCnt==0 || colCnt==0){
				  loopinglog.info("loop value not found //////////");
			  }else{
				  loopingSteps[rowCnt][colCnt]=itemlist_array[cnt];
			  }
			  //_driver = ActionPerformer.performAction(loopingSteps, _driver, test_case_name, testcase_sheet, wb_loc, ScriptingFileName, propertyFile, UserInputData, testDataRow, cellcategory);//TODO
		  }
		  	  
	  }catch(Exception e){
		  loopinglog.error("Exception in looping condition - ",e);
	  }
  }
  
  
  //Function to get the position of LoopValue in the looping test steps 
  private static void getLoopValueIndex(String[][] loopingSteps){
	  try{
		  int match_cnt=0;
		  for(int i=0;i<loopingSteps.length;i++){
			  for(int j=0;j<loopingSteps[0].length;j++){
				  if(loopingSteps[i][j].equalsIgnoreCase("loop_value")){
					  rowCnt=i;
					  colCnt=j;
					  loopinglog.info("loop value found !!!   ROW - "+ rowCnt + " and COLUMN - "+ colCnt );
					  match_cnt++;
					  break;
				  }
			  }
			  if(match_cnt!=0){
				  break;
			  }
		  }
	  }catch(Exception e){
		  loopinglog.error("Exception in get LoopValue Index is - ",e);
	  }
	  
  }
  
  //
  public static String modifyTestStepRange(String stepRange){
	  try{
		  String modifiedTestStepRange=null;
		  String[] testStepRange = SplitString.stringSplit(stepRange);
		  String startingstepIndex = testStepRange[0].substring(3);
		  int index = Integer.parseInt(startingstepIndex);
		  String modifiedStartingTestStepIndex = "TS_"+ Integer.toString(index-1);
		  //Only 1 step
		  if(testStepRange.length<2){
			  modifiedTestStepRange = modifiedStartingTestStepIndex+"|" + stepRange;
		  }
		  //Group of steps
		  else{			  
			  modifiedTestStepRange = stepRange.replace(testStepRange[0], modifiedStartingTestStepIndex);			  
		  }
		  return modifiedTestStepRange;
	  }catch(Exception e){
		  loopinglog.error("Exception in modifying the test step range is - ",e);
		  return null;
	  }
  }
}

package utilityPackage;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

public class DateFormater {
	
	static Logger DateFormater_log = Logger.getLogger(DateFormater.class);
	
  @Test
  public static String convertDateToString(String datevalue,String Format) {
	  
	  try{
		  
		  String startDateString = datevalue;
		  	String dateFormat = Format;
		  	
			@SuppressWarnings("deprecation")
			Date date = new Date(startDateString);
		    SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		    String format = formatter.format(date);
		    DateFormater_log.info(format);
		    
		  return format;
	  }
	  catch(Exception e){
		  return null;
	  }
	  	
  }
  
}

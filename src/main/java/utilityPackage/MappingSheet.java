package utilityPackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class MappingSheet {
	
	static WebDriver driver;
	static String path = System.getProperty("user.dir");
	public static String sheet_wb_loc = path + "\\TestData\\" ;
	public static String sheet_wb_name = "MappingSheet.xls";
	public static String excel_sheet = "Sheet1";
	static String test_iteration = null;
	static String searchFlag="false";
	static int dest_col_num=0;
	
	//String result = "FAIL";                          // to be passed in the method 	
	//String target_testCase = "test_case_6";          // to be passed in the method
	
	static Logger MappingSheet_log = Logger.getLogger(MappingSheet.class);
	
	
  @Test
  public static void testResult(String test_case,String status) throws FileNotFoundException {
	  
	  try{
	  
		  String target_testCase=test_case;
		  String result = status;
		  
		  
		  //customizing the target_testCase name
		  
		  
		  //reding data from mapping sheet
		  String[][] data = readDataMapping(driver,sheet_wb_loc,sheet_wb_name,excel_sheet);
		  
	  
		 
		    
		    
		   	  File excel = new File(sheet_wb_loc + "\\" + sheet_wb_name);
			  FileInputStream fis =  new FileInputStream(excel);
			  HSSFWorkbook wb = new HSSFWorkbook (fis);
			  HSSFSheet ws = wb.getSheet(excel_sheet);
				
				for (int cnt =1 ; cnt<data.length ; cnt++){
					
					if(data[cnt][0].equalsIgnoreCase(target_testCase)){
						
						searchFlag="true";
						MappingSheet_log.info("Search Flag -----------------  TRUE");
						dest_col_num = ws.getRow(cnt).getLastCellNum();
						MappingSheet_log.info("Row number is :" + cnt);
						MappingSheet_log.info("Destination column number is:" + dest_col_num);
						
						test_iteration = "iteration" + "_" + dest_col_num;
						
						
						ExcelFunctions.writeCellByCondition(sheet_wb_loc, sheet_wb_name, excel_sheet, 0, dest_col_num,test_iteration,searchFlag);
						ExcelFunctions.writeCellByCondition(sheet_wb_loc, sheet_wb_name, excel_sheet, cnt, dest_col_num,result,searchFlag);
						
						
					}
				}
					
				if (searchFlag.equalsIgnoreCase("false")){
						
					MappingSheet_log.info("Search Flag -----------------  FALSE");
					int dest_row = ws.getLastRowNum();
					MappingSheet_log.info("Destination ROW is :"+ dest_row);
					test_iteration = "iteration_1";
					
					
					
					ExcelFunctions.writeCellByCondition(sheet_wb_loc, sheet_wb_name, excel_sheet, dest_row, 0,target_testCase,"false");
					
					
					ExcelFunctions.writeCellByCondition(sheet_wb_loc, sheet_wb_name, excel_sheet, 0, 1,test_iteration,"true");
					
					
					ExcelFunctions.writeCellByCondition(sheet_wb_loc, sheet_wb_name, excel_sheet, dest_row+1,1,result,"true");
				}
					
		    
		    
		}
	  catch(Exception e){
		  
		  MappingSheet_log.error("The Exception is :" ,e);
	  }
	  
	  
  }
  
  //
  public static String[][] readDataMapping(WebDriver driver,String sheet_wb_loc,String sheet_wb_name,String excel_sheet ) throws FileNotFoundException {
	  
	  try {
		  	
		  File excel = new File(sheet_wb_loc + "\\" + sheet_wb_name);
		    FileInputStream inputStr = new FileInputStream(excel);
		    HSSFWorkbook hssfWork = new HSSFWorkbook(inputStr) ;
		    HSSFSheet ws = hssfWork.getSheetAt(0);
		    Iterator rowItr = ws.rowIterator();
		    
		    int rowNum = ws.getLastRowNum() + 1;
			int colNum = ws.getRow(0).getLastCellNum();
			String[][] data = new String[rowNum][colNum];
		    
		    
		    for ( int i=0;i<rowNum; i++){
		    
		    	if ( rowItr.hasNext() ) {
		    		HSSFRow row = (HSSFRow) rowItr.next();
		    		//MappingSheet_log.info("ROW "+ i + ":-->");
		    		Iterator cellItr = row.cellIterator();
		    		
		    		for (int j=0;j<colNum; j++){
		    			
		    			if ( cellItr.hasNext() ) {
			    			HSSFCell cell = (HSSFCell) cellItr.next();
			    			//MappingSheet_log.info("CELL:-->"+cell.toString());
			    			cellToString cs = new cellToString();
			    			String value = cs.cellTostring_HSSF(cell);
			    			data[i][j]=value;
			    			//MappingSheet_log.info("value is :"+ value);
			    		}
		    		}
		    		
		    	}
		    }
		    
		    
		    //printing value in data
		    /*
		    for(int r=0;r<rowNum;r++){
		    	MappingSheet_log.info("Row:"+r+"-->");
		    	for (int c=0;c<colNum;c++){
		    		MappingSheet_log.info(data[r][c] + "\t");
		    	}
		    	MappingSheet_log.info();	
		    }
		    */
		    
		    return data;
		    
		    
		} catch (Exception e) {
			MappingSheet_log.error("Exception in readDataMapping -",e);
		}
	return null;
  }
  
  
  //
}

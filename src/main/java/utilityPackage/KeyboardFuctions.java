package utilityPackage;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class KeyboardFuctions {
	
	static Logger Keyboardlog = Logger.getLogger(KeyboardFuctions.class.getName());
	
  @Test
  public static void KeyboardKeys(String elementIdentifiedBy, String elementIdentifier, String keyStrokes) {
	  try{
		  
		  WebElement we = WebElementActions.GetWebElement(elementIdentifiedBy, elementIdentifier);
		  
		  switch(keyStrokes){
		  
		  case "tab" :
			  Keyboardlog.info("Pressing the TAB KEY of Keyboard");
			  we.sendKeys(Keys.TAB);
			  break;
			  
		  case "enter":
			  Keyboardlog.info("Pressing the ENTER KEY of Keyboard");
			  we.sendKeys(Keys.ENTER);
			  break;
			  
		  case "charSeq":
			  Keyboardlog.info("Pressing the a KEY of Keyboard");
			  we.sendKeys(Keys.chord("test"));
			  break;
			  
		  case "up-arrow":
			  Keyboardlog.info("Pressing the UP-ARROW KEY of Keyboard");
			  we.sendKeys(Keys.ARROW_UP);
			  break;
			  			  
		  case "down-arrow":
			  Keyboardlog.info("Pressing the DOWN-ARROW KEY of Keyboard");
			  we.sendKeys(Keys.ARROW_DOWN);
			  break;
			  
		  case "right-arrow":
			  Keyboardlog.info("Pressing the RIGHT-ARROW KEY of Keyboard");
			  we.sendKeys(Keys.ARROW_RIGHT);
			  break;
			  
		  case "left-arrow":
			  Keyboardlog.info("Pressing the LEFT-ARROW KEY of Keyboard");
			  we.sendKeys(Keys.ARROW_LEFT);
			  break;
			  
			  
		  default:
			  Keyboardlog.info("Please provide appropiate keyboard key value");
			  
		  }
		  
		  
	  }
	  catch(Exception e){
		  Keyboardlog.error("The Exception occured in KeyBoard Function is " ,e);
	  }
	  
  }

}

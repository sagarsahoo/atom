package utilityPackage;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class ElementProperty {


	static Logger ElementProperty_log = Logger.getLogger(ElementProperty.class);


	@Test
	public static String getAttribute(String element_identifiedBy,String element_identifier,String group_identifiedBy,String group_identifier,String attributeName) {

		String variable=null;

		try{
			switch(ActionPerformer.pass_values){

			case "text":
				variable = WebElementActions.GetWebElement(ActionPerformer.element_identifiedBy, ActionPerformer.element_identifier).getText();
				ElementProperty_log.info("Text is - "+ variable);
				return variable;

			case "tagname":
				variable = WebElementActions.GetWebElement(ActionPerformer.element_identifiedBy, ActionPerformer.element_identifier).getTagName();
				ElementProperty_log.info("TagName is - "+ variable);
				return variable;

			case "textSelected":
				WebElement group = WebElementActions.GetWebElement(ActionPerformer.target_identifiedBy,ActionPerformer.target_identifier);
				ElementProperty_log.info("Group_list -" + group.getText());
				List<WebElement> elements = group.findElements(ActionPerformer.elementFinder(ActionPerformer.element_identifiedBy,ActionPerformer.element_identifier));

				for (WebElement element : elements){
					if(element.isSelected()){
						ElementProperty_log.info("Element " + element.getText() + " is Selected");
						variable = element.getText();
					}
				}
				ElementProperty_log.info("Variable passed is - "+variable );
				return variable;

			case "value":
				variable = WebElementActions.GetWebElement(ActionPerformer.element_identifiedBy, ActionPerformer.element_identifier).getAttribute("value");
				ElementProperty_log.info("Text is - "+ variable);
				return variable;		

			default :
				variable = WebElementActions.GetWebElement(ActionPerformer.element_identifiedBy, ActionPerformer.element_identifier).getAttribute(ActionPerformer.pass_values);
				return variable;
			}

		}

		catch(Exception e){
			ElementProperty_log.error("Exception in storing value from test case output in the function storeVal- ",e);
			return null;
		}
	}
	
	//Without arguments
	public static String getAttribute() {

		String variable=null;

		try{
			switch(ActionPerformer.pass_values){

			case "text":
				variable = WebElementActions.GetWebElement(ActionPerformer.element_identifiedBy, ActionPerformer.element_identifier).getText();
				ElementProperty_log.info("Text is - "+ variable);
				return variable;

			case	"tagname":
				variable = WebElementActions.GetWebElement(ActionPerformer.element_identifiedBy, ActionPerformer.element_identifier).getTagName();
				ElementProperty_log.info("TagName is - "+ variable);
				return variable;

			case "textSelected":
				WebElement group = WebElementActions.GetWebElement(ActionPerformer.target_identifiedBy,ActionPerformer.target_identifier);
				ElementProperty_log.info("Group_list -" + group.getText());
				List<WebElement> elements = group.findElements(ActionPerformer.elementFinder(ActionPerformer.element_identifiedBy,ActionPerformer.element_identifier));

				for (WebElement element : elements){
					if(element.isSelected()){
						ElementProperty_log.info("Element " + element.getText() + " is Selected");
						variable = element.getText();
					}
				}
				ElementProperty_log.info("Variable passed is - "+variable );
				return variable;

			case "value":
				variable = WebElementActions.GetWebElement(ActionPerformer.element_identifiedBy, ActionPerformer.element_identifier).getAttribute("value");
				ElementProperty_log.info("Text is - "+ variable);
				return variable;		

			default :
				variable = WebElementActions.GetWebElement(ActionPerformer.element_identifiedBy, ActionPerformer.element_identifier).getAttribute(ActionPerformer.pass_values);
				return variable;
			}

		}

		catch(Exception e){
			ElementProperty_log.error("Exception in storing value from test case output in the function storeVal- ",e);
			return null;
		}
	}

	public static Double getCustomizedResultforNumber(String element_identifiedBy,String element_identifier,String UIresult,String pass_value){
		Double d = null;
		String Actual = null;
		try{
			if(!element_identifiedBy.equalsIgnoreCase("NA")){
				Actual = WebElementActions.GetWebElement(element_identifiedBy, element_identifier).getText();
			}else{
				Actual = UIresult;
			}
			NumberFormat format = NumberFormat.getInstance();//Default Locale
			Number number = format.parse(Actual);
			d = number.doubleValue();

		}catch(Exception e){
			ElementProperty_log.error("Exception occurred",e);
		}
		return d;
	}
}

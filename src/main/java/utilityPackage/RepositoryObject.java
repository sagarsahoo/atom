package utilityPackage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RepositoryObject {
	
	static Logger RepositoryObject_log = Logger.getLogger(RepositoryObject.class);
	
	
  @Test
  public static String IdentifierRepo(String objRepositoryFile,String repo_element) throws FileNotFoundException {
	  
	  try{
		  
		  Properties obj = new Properties(); 
		  FileInputStream objfile = new FileInputStream(System.getProperty("user.dir")+"\\objRepo\\"+objRepositoryFile);
		  
		  //loading the object.properties file
		  obj.load(objfile);
		  
		  String ele_identifier = obj.getProperty(repo_element);
		  RepositoryObject_log.info("element identifier in object Property file :"+ ele_identifier);
		  
		  return ele_identifier;
	  }
	  catch(Exception e){
		  RepositoryObject_log.error("Exception in Object Repository Function :",e);
		Assert.fail("Failure as exception occured");  
	  }
	return null;
	  
	  
  }
}

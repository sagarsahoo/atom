package utilityPackage;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ExceptionActions {
	static Logger log = Logger.getLogger(ExceptionActions.class);
	
  @Test
  public static void  performAfterExceptionOccurs(WebDriver driver) {
	  try{
		  	
		  	String scrShotTestCase = ActionPerformer.scrShotTestCase;
		  	String test_step = ActionPerformer.test_step;
		  	String propertyFile = TestCaseConfiguration.testConfData[1];
		  	String workbook_loc = CommonMethod.wb_loc;
		  	String excel_sheet = TestCaseConfiguration.testConfData[2];
		  	
		  	
			log.error(" **************************  Test case - "+scrShotTestCase+"******************************");
			log.error("Test case - "+scrShotTestCase + "failed");
			if(!driver.equals(null)){
				CheckPoint.screenshotException(driver,scrShotTestCase,test_step);
			}else{
				CheckPoint.windowsScreenShot("FAIL");
			}
			
			//Mapping result in UserInputFIle
			if(TestCaseConfiguration.getPresenceOfUserInputSheet().equalsIgnoreCase("Y")){
				log.error("Mapping the result into the user input sheet");
				String UserInputFileName = RepositoryObject.IdentifierRepo(propertyFile,"UserInputFile");
				UserInputDataSheet.setValues(workbook_loc, UserInputFileName, excel_sheet);
				UserInputDataSheet.getEffectiveRow(excel_sheet, workbook_loc, UserInputFileName, CommonMethod.UserInputData);
				//UserInputDataSheet.mapResult(Verify_list);
			}
			log.error("Driver title - "+ driver.getTitle());
			BrowserClass.closeBrowser();
	  }catch(Exception e){
		  log.error("Exception inside Exception class - ",e);
	  }
	  finally{
			Assert.fail("failed due to exception");
		}
  }
}

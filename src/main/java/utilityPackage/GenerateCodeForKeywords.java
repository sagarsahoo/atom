package utilityPackage;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.bag.SynchronizedSortedBag;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import selfitGui.PageScanner;

public class GenerateCodeForKeywords {
	static String keywordCode;
	static List<String> keywordRelatedVales = new ArrayList<String>();

	@Test
	public static String  getCode(List<String> KeywordValues) {
		
		keywordCode="";
		keywordCode = "log.info(\" Excecuting Test Step -"+KeywordValues.get(4)+ "!!! refering Keyword - " + KeywordValues.get(5) + "\");\n";

		switch(KeywordValues.get(5)){
		//To open the browser
		case "open_browser":
			//keywordCode =keywordCode +"ActionPerformer.assignExcelValues("+ row_no+")";
			keywordCode =keywordCode + "_driver = OpenBrowser.open_browser(\"" + KeywordValues.get(9)+"\""+ "," + "\""+KeywordValues.get(10) + "\");\n"+ "act = new Actions(_driver);\n";
			return keywordCode;

		case "get_url":
			keywordCode = keywordCode + "ProvideURL.baseUrl(_driver,\""+ KeywordValues.get(9) + "\");\n";
			return keywordCode;

		case "getTitle" :
			keywordCode=keywordCode + "UIresult.add(_driver.getTitle());\n";
			return keywordCode;
			


		case "wait" :
			keywordCode = keywordCode + "Thread.sleep(1000);";
			return keywordCode;

		case "pageRefresh":
			keywordCode=keywordCode +"_driver.navigate().refresh();";
			return keywordCode;

		case "navigate-back":
			keywordCode=keywordCode +"_driver.navigate().back();";
			return keywordCode;

		case "navigate-forward":
			keywordCode=keywordCode + "_driver.navigate().forward();";
			return keywordCode;

		case "enter_text":
			keywordCode = keywordCode + "WebElementActions.enterText(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\""+  KeywordValues.get(9) + "\");\n";			
			return keywordCode;

		case "EnterWithoutClear":
			keywordCode = keywordCode + "WebElementActions.enterTextWithoutClear(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\""+  KeywordValues.get(9) + "\");\n";
			return keywordCode;

		case "enter-text_conditional":
			keywordCode = keywordCode + "WebElementActions.enterTextIfNotPresent(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\""+  KeywordValues.get(9) + "\",\"" + KeywordValues.get(11)+ "\");\n";
			return keywordCode;

		case "text_clear":
			keywordCode = keywordCode + "WebElementActions.clearText(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\");\n";
			return keywordCode;

		case "click":
			keywordCode = keywordCode + "WebElementActions.Click(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\");\n";
			return keywordCode;

		case "NormalClick":
			keywordCode = keywordCode + "WebElementActions.NormalClick(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\");\n";
			return keywordCode;
			
		case "clickAndProceed":
			keywordCode = keywordCode + "WebElementActions.ClickAndProceed(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\");\n";
			return keywordCode;

		case "DoubleClick":
			keywordCode = keywordCode + "WebElementActions.doubleClick(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\");\n";
			return keywordCode;

		case "nav_section":
			keywordCode = keywordCode + "MouseActions.element_section(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\""+  KeywordValues.get(9) + "\",\"" + "\",\""+ KeywordValues.get(10) +"\",\"" + KeywordValues.get(11)+ "\");\n";
			return keywordCode;
			
		case "text_verify":
			keywordCode = keywordCode + "Verify_list = VerifyTheText.verifyExact(UIresult);\n";			
			break;

		case "verify_contains":
			keywordCode = keywordCode + "Verify_list = VerifyTheText.verifyContains(UIresult);\n";						
			break;

		case "VerifyRegularExpression":
			keywordCode = keywordCode + "Verify_list = VerifyTheText.regularExpressionVerify(UIresult);\n";
			break;
			
		case "build_query":
			keywordCode = keywordCode + "dbQuery = DataBaseFunctions.sqlQueryString(UIresult);\n";						
			break;

			
		case "alertBox":
			keywordCode = keywordCode + "AlertBoxHandler.alertBoxHandling(_driver,\""+  KeywordValues.get(9) + "\");\n";
			return keywordCode;

		case "select_Items":
			keywordCode = keywordCode + "SelectItems.itemsToSelect(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\""+  KeywordValues.get(9) + "\",\"" + "\",\""+ KeywordValues.get(10) +"\",\"" + KeywordValues.get(11)+ "\");\n";
			return keywordCode;

		case "Conditional_select_Items":
			keywordCode = keywordCode + "SelectItems.selecOtherValue(\""+ KeywordValues.get(10) + "\",\""+ KeywordValues.get(11) +"\",\");\n";
			return keywordCode;
			
		case "Click_cor_select_item" :
			keywordCode = keywordCode + "WebTable.ClickOnCorrSearch(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",Integer.parseInt(\""+  KeywordValues.get(9) + "\"),\"" + "\",\""+ KeywordValues.get(10) +"\",Integer.parseInt(\"" + KeywordValues.get(11)+ "\"));\n";
			return keywordCode;

		case "get_cor_rowIndex":
			keywordCode = keywordCode + "UIresult.add(String.valueOf(WebTable.GetRowIndex(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",Integer.parseInt(\""+  KeywordValues.get(10) + "\"),\"" + "\",\""+ KeywordValues.get(9) +"\");\n";
			return keywordCode;

		case "get_textByRowCol":
			keywordCode = keywordCode + "UIresult.add(WebTable.GetText(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",Integer.parseInt(\""+  KeywordValues.get(9) + "\"),\"" + "\",Integer.parseInt(\""+ KeywordValues.get(9) +"\"));\n";
			return keywordCode;
			
		case "check_Box" :
			keywordCode = keywordCode + "SelectItems.selectCheckBox(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\""+  KeywordValues.get(9) + "\");\n";
			return keywordCode;

		case "new_window" :
			keywordCode = keywordCode + "array_windows = WindowClass.newWindow(_driver);\n";
			return keywordCode;

		case "switch_window" :
			keywordCode = keywordCode + "WindowClass.switchWindow(_driver, array_windows);\n";
			return keywordCode;

		case "switch_frame" :
			keywordCode = keywordCode + "WindowClass.switchFrame(_driver,\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\");\n";
			return keywordCode;

		case "switch_frame_default" :
			keywordCode = keywordCode + "WindowClass.switchFrame_default(_driver);\n";
			return keywordCode;
			
		case "mouse_sliding" :
			keywordCode = keywordCode + "MouseActions.mouseSLider(_driver, \""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\");\n";
			return keywordCode;
			
		case "javascript" :
			keywordCode = keywordCode + "JavaFunctions.executeJavaScript(\""+ KeywordValues.get(8) + "\",\""+ KeywordValues.get(9) +"\");\n";
			return keywordCode;

		case "scrollDown" :
			keywordCode = keywordCode + "MouseActions.mouseScrollDown();\n";
			return keywordCode;

		case "scrollUp" :
			keywordCode = keywordCode + "MouseActions.mouseScrollUp();\n";
			return keywordCode;
			
		case "AutoIT_script" :
			keywordCode= keywordCode + "Runtime.getRuntime().exec(\""+ KeywordValues.get(9)+"\");\n";
			return keywordCode;
			
		case "ownCode" :
			keywordCode = keywordCode + "\n" + KeywordValues.get(9) + "\n"; 
			return keywordCode;


		case "getResult":
			keywordCode = keywordCode + "UIresult.add(ElementProperty.getAttribute(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\""+  KeywordValues.get(10) + "\",\"" + "\",\""+ KeywordValues.get(11) +"\",\"" + KeywordValues.get(9)+ "\");\n";
			return keywordCode;
			
		case "screenshot":
			keywordCode = keywordCode + "CheckPoint.windowsScreenShot();\n";
			return keywordCode;
			
		case "getSubStringByLength":
			keywordCode = keywordCode + "String subStringByLength = ElementProperty.getAttribute();\n";
			keywordCode = keywordCode + "UIresult.add(JavaFunctions.getstrinByLength(subStringByLength,\""+  KeywordValues.get(10) +"\",\""+  KeywordValues.get(11) + "\");\n";
			return keywordCode;

		case "getSubStringByValue"	:
			keywordCode = keywordCode+ "String subStringByValue = ElementProperty.getAttribute();\n";
			keywordCode = keywordCode + "UIresult.add(JavaFunctions.getstrinByLength(subStringByValue,\""+  KeywordValues.get(10) +"\",\""+  KeywordValues.get(11) + "\");\n";
			return keywordCode;

		case "seleniumScreenshot":
			keywordCode = keywordCode + "CheckPoint.screenShot(\"screenshot\");\n";
			return keywordCode;

		case "quit":
			keywordCode = keywordCode + "BrowserClass.closeBrowser();";
			return keywordCode;

		case "getList":
			keywordCode = keywordCode + "SelectItems.getList(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\");\n";
			return keywordCode;

		case "EnterByLabel":
			keywordCode = keywordCode + "LabelFunctions.EnterByLabel(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\");\n";
			return keywordCode;

		case "WaitForElementToBeClickable":
			keywordCode = keywordCode + "WebElementActions.ExplicitWaitElementClikable(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\");\n";
			return keywordCode;
			
		case "waitForPageToLoad":
			keywordCode = keywordCode + "WebElementActions.WaitForPageload();";
			return keywordCode;

		case "WaitForInvisibilityOfElement"	:
			keywordCode= keywordCode + "WebElementActions.ExplicitWaitInvisibilityOfElement(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",Integer.parseInt(\""+  KeywordValues.get(9) + "\"));\n";
			return keywordCode;
			
		case "waitforobject" :
			keywordCode = keywordCode + "WebElementActions.ExplicitWaitPresenceOfElement(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",30);\n";
			return keywordCode;
			
		case "WaitForFrameSwitchToIt":
			keywordCode = keywordCode + "WebElementActions.WaitForFrameSwitchToIt(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",30);\n";
			return keywordCode;

		case "WebTableClick":
			keywordCode = keywordCode + "WebTable.ClickTextProvided(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\""+  KeywordValues.get(9) + "\",\"" + KeywordValues.get(10)+ "\");\n";
			return keywordCode;

		case "WebTableCalculation":
			keywordCode = keywordCode + "actualList = WebTable.Calculation(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",Integer.parseInt(\""+  KeywordValues.get(9) + "\"));\n";
			return keywordCode;
			
		case "SelectRadioButton":
			keywordCode= keywordCode + "WebElementActions.SelectRadioButton(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\");\n";
			return keywordCode;
			
		case "ObjectExists":
			keywordCode =keywordCode +  "if(WebElementActions.ObjectExist(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\")){\n";
			keywordCode = keywordCode + "CheckPoint.screenShot(\"PASS\");\n}";
			keywordCode= keywordCode + "else{\n}";
			return keywordCode;
			
		case "drop_down" :
			keywordCode =keywordCode +  "SelectItems.SelectDropDown(_driver,\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\""+  KeywordValues.get(9) + "\",\"" + "\",\""+ KeywordValues.get(10) +"\",\"" + KeywordValues.get(11)+ "\");\n";
			return keywordCode;

		case "DropDownSelectByValue":
			keywordCode = keywordCode + "SelectItems.DropDown_SelectValue(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\""+  KeywordValues.get(9) + "\");\n";
			return keywordCode;

		case "DropDownSelectByIndex":
			keywordCode = keywordCode + "SelectItems.DrpDwn_SelectIndex(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\""+  KeywordValues.get(9) + "\");\n";
			return keywordCode;

		case"DropDownSelectByText":
			keywordCode= keywordCode + "SelectItems.DropDown_SelectText(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\""+  KeywordValues.get(9) + "\");\n";
			return keywordCode;
			
		case "WbTableClick":
			keywordCode = keywordCode + "WebTable.Click(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",Integer.parseInt(\""+  KeywordValues.get(9) + "\"),Integer.parseInt(\""+ KeywordValues.get(10) + "));\n";
			return keywordCode;

		case "WebTableGetText":
			keywordCode = keywordCode + "UIresult.add(WebTable.GetText(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",Integer.parseInt(\""+  KeywordValues.get(10) + "\"),Integer.parseInt(\""+ KeywordValues.get(11) + "));\n";
			return keywordCode;
			
		case "RunJavaScript":
			keywordCode = keywordCode + "WebElementActions.RunJavaScript(\""+ KeywordValues.get(7)+ "\");";
			return keywordCode;

		case "DropDownGetSelectedText":
			keywordCode = keywordCode + "UIresult.add(SelectItems.DropDown_GetSelectedText(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\");\n";
			return keywordCode;

		case "KeyboardKey":
			keywordCode = keywordCode + "KeyboardFuctions.KeyboardKeys(\""+ KeywordValues.get(7) + "\",\""+ KeywordValues.get(8) +"\",\""+  KeywordValues.get(9) + "\");\n";
			return keywordCode;
			
		/*case "simpleLoop":
			Looping.normalLoop(driver,test_case_name,excel_sheet, workbook_loc, workbook_name,propertyFile,UserInputData,testDataRow,cellcategory,element_identifiedBy, pass_values);
			row_no = (row_no+Looping.listsize)-1;
			System.out.println("row num to be referred - "+ row_no);
			return keywordCode;

		case "if-else":
			//Verifying the result
			Verify_list = VerifyTheText.funcVerifyContainsText(element_identifiedBy,element_identifier,pass_values,excel_sheet,"verify",propertyFile,UserInputData);
			IfElseStatements.ExecuteSteps(driver,test_case_name,excel_sheet, workbook_loc, workbook_name,propertyFile,UserInputData,testDataRow,cellcategory,target_identifiedBy,target_identifier, Verify_list);
			int lastStep = IfElseStatements.getLastStep(target_identifier);
			row_no = lastStep;
			return keywordCode;*/

		case "scan-page":
			keywordCode = keywordCode + "PageScanner.getWebElementsFRomExistingPage(_driver,\""+KeywordValues.get(9)+"\",CommonMethod.applicationFolder);\n";
			return keywordCode;
			
			
		case "UpperCase":
			keywordCode = keywordCode + "UIresult.add(JavaFunctions.ToUpperCase(\""+KeywordValues.get(9) + "\");\n";
			return keywordCode;
			
		case "LowerCase":
			keywordCode = keywordCode + "UIresult.add(JavaFunctions.ToLowerCase(\""+KeywordValues.get(9) + "\");\n";
			return keywordCode;
			
		case "Replace":
			keywordCode = keywordCode + "UIresult.add(JavaFunctions.replace(\""+ KeywordValues.get(9) + "\",\""+ KeywordValues.get(10) +"\",\""+  KeywordValues.get(11) + "\");\n";
			return keywordCode;
			
		case "brokenLinks":
			keywordCode = keywordCode + "BrokenLinks.checkBrokenLinks(driver);";
			return keywordCode;
		}
		return keywordCode;
	}//End of Switch
}


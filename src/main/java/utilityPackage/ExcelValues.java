package utilityPackage;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

public class ExcelValues extends CommonMethod{
	static  Logger log = Logger.getLogger(ExcelValues.class);
	static List<String> ObjectValues= new ArrayList<String>();
	static List<String> TargetObjectValues= new ArrayList<String>();
	public static int UIResult_getIndex;

	@Test
	public static List<String> getObjectlocators(String element_identifiedBy,String element_identifier,int RowOfTestData,String[][] DataFromUserInputSheet,List<String> UIresult,List<String> dbResult ) {
		try{
			ObjectValues.clear();
			UIResult_getIndex=0;
			if(element_identifiedBy.trim().equalsIgnoreCase("repo")){

				String obj_identifier = element_identifier;
				log.info("Element IdentifiedBy value in repository - "+ obj_identifier );
				String elementProperty[] = SplitString.stringSplit(RepositoryObject.IdentifierRepo(objRepositoryFile,obj_identifier));
				element_identifiedBy = elementProperty[0];
				element_identifier = elementProperty[1];
				ObjectValues.add(element_identifiedBy);
				ObjectValues.add(element_identifier);
				return ObjectValues;
			}else {
				/*if(element_identifiedBy.trim().equalsIgnoreCase("excel")){
					//Split the sheetName and Object name from element_indetifier
					ObjectValues.clear();
					List<String> ObjectCharacteristics= new ArrayList<String>();
					String ObjectLocation[] = SplitString.stringSplit(element_identifier);
					String ObjectSheetName = ObjectLocation[0];
					String ObjectName = ObjectLocation[1];
					ObjectCharacteristics.addAll(ExcelDataList.getExcelDataInListByRowValue(CommonMethod.ExcelrepositoryFile, ObjectSheetName, "Name", ObjectName));
					element_identifiedBy = ObjectCharacteristics.get(2);
					element_identifier = ObjectCharacteristics.get(3);
					ObjectValues.add(element_identifiedBy);
					ObjectValues.add(element_identifier);
					return ObjectValues;
				}*/
			}
			if(element_identifiedBy.equalsIgnoreCase("UserInput_excelSheet")){
				log.info("Getting the element_identifier from UserInput_ExcelSheet");
				String ele_identifiedByColumnName = ActionPerformer.test_step + "_element_identifiedBy";
				element_identifiedBy = UserInputDataSheet.getPassValues(RowOfTestData,ele_identifiedByColumnName,DataFromUserInputSheet);
				ObjectValues.add(element_identifiedBy);
			}
			
			if(element_identifiedBy.equalsIgnoreCase("getCustomizedResult")){//TODO
				log.info("Getting the element_identifier from UserInput_ExcelSheet");
				String ele_identifiedByColumnName = ActionPerformer.test_step + "_element_identifiedBy";
				element_identifiedBy = UserInputDataSheet.getPassValues(RowOfTestData,ele_identifiedByColumnName,DataFromUserInputSheet);
				ObjectValues.add(element_identifiedBy);
			}
			else{
				ObjectValues.add(element_identifiedBy);
			}

			//Checking for element_identifier
			if(element_identifier.contains("UIresult")){							
				if(element_identifier.contains("(")){
					UIResult_getIndex =Integer.parseInt(element_identifier.substring(element_identifier.indexOf("(")+1,element_identifier.indexOf(")")));
					log.info("The index of UIresult passed is - " + UIResult_getIndex);
					element_identifier=UIresult.get(UIResult_getIndex);
					ObjectValues.add(element_identifier);
				}
				else{
					UIResult_getIndex = (UIresult.size()-1);
					log.info("UIresult took the last index - "+ UIResult_getIndex);
					element_identifier=UIresult.get(UIResult_getIndex);
					ObjectValues.add(element_identifier);
				}							
			}
			if(element_identifier.equalsIgnoreCase("UserInput_excelSheet")){
				log.info("Getting the element_identifier from UserInput_ExcelSheet");
				String ele_identifierColumnName = ActionPerformer.test_step + "_element_identifier";
				element_identifier = UserInputDataSheet.getPassValues(testDataRow,ele_identifierColumnName,UserInputData);
				ObjectValues.add(element_identifier);
			}

			if(element_identifier.contains("UserInput_excelSheet")){
				log.info("Replacing element_identifier from UserInput_ExcelSheet");
				String ele_identifierColumnName = ActionPerformer.test_step + "_element_identifier";
				String value = UserInputDataSheet.getPassValues(testDataRow,ele_identifierColumnName,UserInputData);
				element_identifier = element_identifier.replace("UserInput_excelSheet", value);
				log.info("Updated value - "+element_identifier );
				ObjectValues.add(element_identifier);
			}else{
				ObjectValues.add(element_identifier);
			}

			//Assigning default values
			if(ObjectValues.size()==0){
				ObjectValues.add(element_identifiedBy);
				ObjectValues.add(element_identifier);
			}else{
				if(ObjectValues.get(0).toString().length()==0){
					ObjectValues.add(element_identifiedBy);
				}
				if(ObjectValues.get(1).toString().length()==0){
					ObjectValues.add(element_identifier);
				}
			}
			
			return ObjectValues;
		}catch(Exception e){
			log.error("Exception in getting the getObjectLocators - ",e);
			return ObjectValues;
		}
	}


	public static String  getPassedValues(String[][] scriptExcelData,String passValueFromExcel,int RowOfTestData,String[][] DataFromUserInputSheet,List<String> UIresult,List<String> dbResult){
		try{
			UIResult_getIndex =0;
			if(passValueFromExcel.contains("master_sheet")){
				//Calling the MasterShhet function to retrieve data from master sheet
				passValueFromExcel=MasterSheet.masterSheetData(masterSheetData,passValueFromExcel);
				return passValueFromExcel;
			}
			if(passValueFromExcel.contains("UIresult")){ 
				if(passValueFromExcel.contains("(")){
					UIResult_getIndex =Integer.parseInt(passValueFromExcel.substring(passValueFromExcel.indexOf("(")+1,passValueFromExcel.indexOf(")")));
					log.info("The index of UIresult passed is - " + UIResult_getIndex);
					passValueFromExcel=UIresult.get(UIResult_getIndex);
					return passValueFromExcel;
				}
				else{
					UIResult_getIndex = UIresult.size()-1;
					log.info("UIresult took the last index - "+ UIResult_getIndex);
					passValueFromExcel=UIresult.get(UIResult_getIndex);
					return passValueFromExcel;
				}							
			}
			else if(passValueFromExcel.contains("dbResult")){
				int dbResult_index = 0;
				if(passValueFromExcel.contains("dbResult(")){							
					dbResult_index =Integer.parseInt(passValueFromExcel.substring(passValueFromExcel.indexOf("(")+1,passValueFromExcel.indexOf(")")));
					log.info("The index of DBresult passed is - " + dbResult_index);
					passValueFromExcel=dbResult.get(dbResult_index);
					return passValueFromExcel;
				}
				else{
					dbResult_index = Integer.parseInt(dbResult.get(dbResult.size()-1));
					log.info("DBresult took the last index - "+ dbResult_index);
					passValueFromExcel=dbResult.get(dbResult_index);
					return passValueFromExcel;
				}
			}else if(passValueFromExcel.contains("currentTimeStamp")){
				String format = passValueFromExcel.substring(passValueFromExcel.indexOf("("),passValueFromExcel.indexOf(")"));
				if(format.length()==0 || format.equals("format")){					
					return TimeClass.getCurrentTimestamp("MM-dd-yyyy_h-mm-ss_a"); //Default format
				}else{
					return TimeClass.getCurrentTimestamp(format);
				}
			}
			//For random string
			else if(passValueFromExcel.contains("Random")){
				int lengthOfString = Integer.parseInt(passValueFromExcel.substring(passValueFromExcel.indexOf("(")+1, passValueFromExcel.indexOf(")")));
				if(passValueFromExcel.contains("RandomString")){			
					//pass_values = JavaFunctions.RandomString(Integer.parseInt(target_identifiedBy));
					passValueFromExcel = JavaFunctions.RandomString(lengthOfString);
					return passValueFromExcel;
				}else{
					if(passValueFromExcel.contains("RandomInteger")){
						passValueFromExcel = JavaFunctions.RandomInteger(lengthOfString);
						return passValueFromExcel;
					}
					else{
						if(passValueFromExcel.contains("RandomAlphaNumeric")){
							passValueFromExcel = JavaFunctions.RandomAlphaNumeric(lengthOfString);
							return passValueFromExcel;
						}
					}
				}
			}
			//Taking from UserInputSheet
			if(passValueFromExcel.equalsIgnoreCase("UserInput_excelSheet")){
				log.info("calling fuction to read from User Input Data Sheet");
				//call function to get user value
				log.info("Test data row is - "+testDataRow);
				passValueFromExcel = UserInputDataSheet.getPassValues(RowOfTestData,ActionPerformer.test_step,DataFromUserInputSheet);
				return passValueFromExcel;
			}
			//Return the set value
			return passValueFromExcel;
		}catch(Exception e){
			log.error("Exception in getting the pass_values - ",e);
			return passValueFromExcel;
		}
	}

	public static List<String> getTargetObjectLocators(String[][] scriptExcelData,String target_identifiedBy,String target_identifier,List<String> UIresult,List<String> dbResult){
		try{
			TargetObjectValues.clear();
			UIResult_getIndex=0;
			if(target_identifiedBy.equalsIgnoreCase("master_sheet")){
				//Calling the MasterShhet function to retrieve data from master sheet
				target_identifiedBy=MasterSheet.masterSheetData(masterSheetData,ActionPerformer.test_step);
				TargetObjectValues.add(target_identifiedBy);
			}else if(target_identifiedBy.contains("UserInput_excelSheet")){
				if(target_identifiedBy.contains("(")){
					log.info("Getting the element_identifier from UserInput_ExcelSheet having explicit step name");
					String stepName = target_identifiedBy.substring(target_identifiedBy.indexOf("(")+1,target_identifiedBy.indexOf(")"));
					target_identifiedBy = UserInputDataSheet.getPassValues(testDataRow,stepName,UserInputData);
					TargetObjectValues.add(target_identifiedBy);
				}else{
					log.info("Getting the element_identifier from UserInput_ExcelSheet");
					String target_identifiedByColumnName = ActionPerformer.test_step + "_target_identifiedBy";
					target_identifiedBy = UserInputDataSheet.getPassValues(testDataRow,target_identifiedByColumnName,UserInputData);
					TargetObjectValues.add(target_identifiedBy);
				}
				
			}else if(target_identifiedBy.contains("UIresult")){					
				if(target_identifiedBy.contains("(")){
					UIResult_getIndex =Integer.parseInt(target_identifiedBy.substring(target_identifiedBy.indexOf("(")+1,target_identifiedBy.indexOf(")")));
					log.info("The index of UIresult passed is - " + UIResult_getIndex);
					target_identifiedBy=UIresult.get(UIResult_getIndex);
					TargetObjectValues.add(target_identifiedBy);
				}
				else{
					UIResult_getIndex = Integer.parseInt(UIresult.get(UIresult.size()-1));
					log.info("UIresult took the last index - "+ UIResult_getIndex);
					target_identifiedBy=UIresult.get(UIResult_getIndex);
					TargetObjectValues.add(target_identifiedBy);
				}							
			}else{
				TargetObjectValues.add(target_identifiedBy);
			}


			//Checking for the target_identifier
			if(target_identifier.equalsIgnoreCase("master_sheet")){
				//Calling the MasterShhet function to retrieve data from master sheet
				target_identifier=MasterSheet.masterSheetData(masterSheetData,ActionPerformer.test_step);
				TargetObjectValues.add(target_identifier);
			}else{
				if(target_identifier.contains("UserInput_excelSheet")){
					if(target_identifier.contains("(")){
						log.info("Getting the element_identifier from UserInput_ExcelSheet having explicit step name");
						String stepName = target_identifier.substring(target_identifier.indexOf("(")+1,target_identifier.indexOf(")"));
						target_identifier = UserInputDataSheet.getPassValues(testDataRow,stepName,UserInputData);
						TargetObjectValues.add(target_identifier);
					}else{
						log.info("Getting the element_identifier from UserInput_ExcelSheet");
						String target_identifierColumnName = ActionPerformer.test_step + "_target_identifier";
						target_identifier = UserInputDataSheet.getPassValues(testDataRow,target_identifierColumnName,UserInputData);
						TargetObjectValues.add(target_identifier);
					}
					
				}if(target_identifier.contains("UIresult")){					
					if(target_identifier.contains("(")){
						UIResult_getIndex =Integer.parseInt(target_identifier.substring(target_identifier.indexOf("(")+1,target_identifier.indexOf(")")));
						log.info("The index of UIresult passed is - " + UIResult_getIndex);
						target_identifier=UIresult.get(UIResult_getIndex);
						TargetObjectValues.add(target_identifier);
					}
					else{
						UIResult_getIndex = Integer.parseInt(UIresult.get(UIresult.size()-1));
						log.info("UIresult took the last index - "+ UIResult_getIndex);
						target_identifier=UIresult.get(UIResult_getIndex);
						TargetObjectValues.add(target_identifier);
					}							
				}else{
					TargetObjectValues.add(target_identifier);
				}
			}

			//Assigning default values
			if(TargetObjectValues.size()==0){
				TargetObjectValues.add(target_identifiedBy);
				TargetObjectValues.add(target_identifier);
			}else{
				if(TargetObjectValues.get(0).toString().length()==0){
					TargetObjectValues.add(target_identifiedBy);
				}
				if(TargetObjectValues.get(1).toString().length()==0){
					TargetObjectValues.add(target_identifier);
				}
			}
			
			return TargetObjectValues;
		}catch(Exception e){
			return TargetObjectValues;
		}
	}
}

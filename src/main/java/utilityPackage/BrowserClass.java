package utilityPackage;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;

public class BrowserClass {
	
	static Logger Browser_log = Logger.getLogger(BrowserClass.class.getName());
	
	public static WebDriver openBrowser(WebDriver driver) {
		  
		Browser_log.info("Inside BrowserClass Class - openBrowser method");
		  
		  ProfilesIni profile = new ProfilesIni();
		  FirefoxProfile fp = profile.getProfile("default");
		  driver = new FirefoxDriver(fp);
		  driver.get("http://www.google.com");
		  driver.manage().window().maximize();
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  
		return driver;
		  
		 
	 }
	
	public static void closeBrowser(){
		Browser_log.info("Quitting browser - Inside closeBrowserFunction");
		if(ActionPerformer.driver!=null){
			ActionPerformer.driver.close();
			ActionPerformer.driver.quit();
			
		}
	}
	   
	  

}

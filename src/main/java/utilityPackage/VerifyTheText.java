package utilityPackage;

import java.util.List;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class VerifyTheText {

	static  Logger log = Logger.getLogger(VerifyTheText.class);
	//static String path = System.getProperty("user.dir");
	//static String workbook_loc ;
	//static String verifyResult = "";

	//Compare the Exact text
	public static boolean verifyExact(List<String> UIresultList){
		boolean VerifyResult=false;
		String verify_action = ActionPerformer.target_identifier;
		String expectedResult= ActionPerformer.pass_values;
		//String expectedResult = ExcelValues.getPassedValues(CommonMethod.scripting_data, ActionPerformer.pass_values, CommonMethod.testDataRow, CommonMethod.UserInputData);
		try{
			String actualResult = getActualResult(UIresultList);			
			log.info("Actual Result - "+ actualResult);
			log.info("Expected Result = "+expectedResult );
			if(verify_action.equalsIgnoreCase("Verify")){
				VerifyResult= expectedResult.trim().equals(actualResult.trim());
				//return VerifyResult;
			}else if(verify_action.equalsIgnoreCase("Assert")){
				Assert.assertEquals(actualResult.trim(), expectedResult.trim());
				VerifyResult=true;
			}
			writeResultToExcel(VerifyResult);
			return VerifyResult;
		}catch(Exception e){
			log.error("Getting Verify result - "+ e);
			if(verify_action.equalsIgnoreCase("Assert")){
				ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
			}
			return VerifyResult;
		}
	}
	
	//Compare the Exact text
		public static boolean verifyIgnoreCase(List<String> UIresultList){
			boolean VerifyResult=false;
			String verify_action = ActionPerformer.target_identifier;
			String expectedResult= ActionPerformer.pass_values;
			//String expectedResult = ExcelValues.getPassedValues(CommonMethod.scripting_data, ActionPerformer.pass_values, CommonMethod.testDataRow, CommonMethod.UserInputData);
			try{
				String actualResult = getActualResult(UIresultList);			
				log.info("Actual Result - "+ actualResult);
				log.info("Expected Result = "+expectedResult );
				if(verify_action.equalsIgnoreCase("Verify")){
					VerifyResult= expectedResult.trim().equalsIgnoreCase(actualResult.trim());
					return VerifyResult;
				}else if(verify_action.equalsIgnoreCase("Assert")){
					Assert.assertEquals(actualResult.trim(), expectedResult.trim());
					VerifyResult=true;
				}
				writeResultToExcel(VerifyResult);
				return VerifyResult;
			}catch(Exception e){
				log.error("Getting Verify result - "+ e);
				if(verify_action.equalsIgnoreCase("Assert")){
					ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
				}
				return VerifyResult;
			}
		}

	//Verify the regularExpression
	public static boolean regularExpressionVerify(List<String> UIresultList){
		boolean result=false;
		String verify_action = ActionPerformer.target_identifier;
		String expectedResult= ActionPerformer.pass_values;
		//String expectedResult = ExcelValues.getPassedValues(CommonMethod.scripting_data, ActionPerformer.pass_values, CommonMethod.testDataRow, CommonMethod.UserInputData);
		try{
			String actualResult = getActualResult(UIresultList);
			log.info("Actual Result - "+ actualResult);
			log.info("Expected Result = "+expectedResult );
			if(Pattern.matches(expectedResult,actualResult)){
				result=true;
			}else{
				result=false;
			}
			writeResultToExcel(result);
			return result;
		}catch(Exception e){
			log.error("Getting Verify result - "+ e);
			if(verify_action.equalsIgnoreCase("Assert")){
				ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
			}
			return result;
		}
	}

	//Verify the text contains
	public static boolean verifyContains(List<String> UIresultList){
		boolean VerifyResult=false;
		String verify_action = ActionPerformer.target_identifier;
		String expectedResult= ActionPerformer.pass_values;
		try{
			String actualResult = getActualResult(UIresultList);
			log.info("Actual Result - "+ actualResult);
			log.info("Expected Result = "+expectedResult );
			if(expectedResult.contains(actualResult)){
				VerifyResult=true;
			}else{
				VerifyResult=false;
			}			
			writeResultToExcel(VerifyResult);
			return VerifyResult;
		}catch(Exception e){
			log.error("Getting Verify result - "+ e);
			if(verify_action.equalsIgnoreCase("Assert")){
				ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
			}
			return VerifyResult;
		}
	}
	
	//Verify two values
	public static boolean verifyTwoValue(List<String> UIresultList){ //TODO
		boolean result=false;
		String verify_action = ActionPerformer.target_identifier;
		String expectedResult= ActionPerformer.pass_values;
		String actualResult = ActionPerformer.target_identifiedBy;
		try{
			log.info("Actual Result - "+ actualResult);
			log.info("Expected Result = "+expectedResult );
			if(verify_action.equalsIgnoreCase("Verify")){
				result= expectedResult.trim().equals(actualResult.trim());
			}else if(verify_action.equalsIgnoreCase("Assert")){
				Assert.assertEquals(actualResult.trim(), expectedResult.trim());
				result= true;
			}
			writeResultToExcel(result);
		}catch(Exception e){
			log.error("verifyTwoValue() - "+ e);
			if(verify_action.equalsIgnoreCase("Assert")){
				ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
			}
			return false;
		}
		return false;
	}
	
	//Getting the overall status of all the checkpoints
	public static boolean getAllResultStatus(List<Boolean> resultList,String action){
		try{
			int match_cnt=0;
			for(int cnt=0;cnt<resultList.size();cnt++){
				if(resultList.get(cnt)==true){
					match_cnt++;
				}
			}
			if(match_cnt==resultList.size()){
				if(action.equalsIgnoreCase("write")){
					writeResultToExcel(true);
				}
				return true;
			}
		}catch(Exception e){
			log.error("Getting Verify result - "+ e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
		return false;
	}

	//Asserting the alert message
	public static boolean assertAlertBoxText(){
		boolean assertTextVerify=false;
		try{			
			String actualValue="";
			String expectedValue=ActionPerformer.pass_values.trim();
			//String expectedValue = ExcelValues.getPassedValues(CommonMethod.scripting_data, ActionPerformer.pass_values, CommonMethod.testDataRow, CommonMethod.UserInputData);
			Alert alt = ActionPerformer.driver.switchTo().alert();
			actualValue = alt.getText().trim();
			log.info("Actual Text -"+actualValue);
			log.info("Expected Text - "+ expectedValue);
			if(actualValue.equals(expectedValue)){
				//Assert.assertEquals(pass_values.trim(),textRetrieved.trim());
				assertTextVerify= true;
			}else{
				assertTextVerify= false;
			}
			writeResultToExcel(assertTextVerify);
			return assertTextVerify;
		}catch(AssertionError e){
			log.info("Exception Inside assert text-"+ e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
		return false;
	}

	// Verify the text with the value from DB
	public static boolean DBresultVerify(){
		try{
			return true;
		}catch(Exception e){
			log.info("The Exception in the DB verify is - "+ e);
			return false;
		}
	}

	//Getting the actual result to verify
	private static String getActualResult(List<String> UIresultList){
		String result="";		
		try{
			if(ActionPerformer.element_identifier.contains("UIresult")){
				int UIIndex=0;
				if(ActionPerformer.element_identifier.contains("(")){
					UIIndex = Integer.parseInt(ActionPerformer.element_identifier.substring(ActionPerformer.element_identifier.indexOf("(") + 1,ActionPerformer.element_identifier.indexOf(")")));
				}else{
					UIIndex = UIresultList.size()-1;
				}
				result = UIresultList.get(UIIndex); //Getting the actual value from the result list
			}else{
				WebElementActions.ExplicitWaitPresenceOfElement(ActionPerformer.element_identifiedBy,ActionPerformer.element_identifier,30);
				WebElement we =  WebElementActions.GetWebElement(ActionPerformer.element_identifiedBy,ActionPerformer.element_identifier);
				log.info("Element found in UI - "+ we.getText());
				result= we.getText();
			}			
		}catch(Exception e){
			log.error("Exception in getting the Actual Result to verify -"+ e);
		}
		return result;
	}

	public static void writeResultToExcel(boolean verifyResult){
		try{
			CheckPoint.screenShot(verifyResult);
			ExcelFunctions.writeToExcel(CommonMethod.wb_loc, CommonMethod.wb_name, CommonMethod.scriptingSheet_name, Integer.parseInt(ActionPerformer.test_step_no.substring(3)), verifyResult);								
			if(TestCaseConfiguration.getPresenceOfUserInputSheet().equalsIgnoreCase("Y") && ActionPerformer.target_identifier.equalsIgnoreCase("assert")){
				log.info("Mapping the result into the user input sheet");
				String UserInputFileName = RepositoryObject.IdentifierRepo(CommonMethod.objRepositoryFile,"UserInputFile");
				UserInputDataSheet.setValues(CommonMethod.wb_loc, UserInputFileName, CommonMethod.scriptingSheet_name);
				UserInputDataSheet.getEffectiveRow(CommonMethod.scriptingSheet_name, CommonMethod.wb_loc, UserInputFileName, CommonMethod.UserInputData);
				if(verifyResult==true){
					UserInputDataSheet.mapResult("PASS");
				}
			}
		}catch(Exception e){
			log.error("Exception in writing the result into Excel - "+ e);
		}
	}

}



package utilityPackage;
/*
 * Purpose		: Utility class for Sikuli
 * Author		: Ezhilarasan.S
 * Created Date	: 21-August-2015
 * Modified	on	: 08-September-2015
 */

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.sikuli.script.App;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.KeyModifier;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;

public class SikuliUtil{

	static App a = null;
	static Region region = null;
//	static Screen screen = null;
	static String path = System.getProperty("user.dir");
	static Logger Log = Logger.getLogger(SikuliUtil.class.getName());
	static String sikuliImages = path+"\\TestData\\SikuliImage\\";
	final static int waitInSeconds = 60;//As of now wait is 60 seconds.
	final static int autowaitInSeconds = 60;//As of now wait is 60 seconds.
	static String target_loc= path+"\\TestReports\\ScreenShots\\";

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	public static boolean RegionObjectExists(String imageName){
		boolean value = false;
		try{
			region.setAutoWaitTimeout(3);//To avoid to wait till autowaitInSeconds 
			Pattern image = new Pattern(sikuliImages+imageName);
			if(region.find(image) != null){
				region.highlight(1);
				region.setAutoWaitTimeout(autowaitInSeconds);
				value = true;
			}

			//			if(region.exists(image) != null){
			//				value = true;
			//			}
		}catch(FindFailed e){
			region.setAutoWaitTimeout(autowaitInSeconds);
			//logger.info(e.getCause());
			if(e.getCause()==null){
				value = false;
				//				Log.log(Level.ERROR, imageName+" - Image is not present");
			}else{
				throw new RuntimeException("Some other exception occured!!! Instead of FindFailed exception.");
			}
		}catch(Exception e){
			region.setAutoWaitTimeout(autowaitInSeconds);
			throw e;
		}
		return value;
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	public static boolean ScreenObjectExists(String imageName){
		boolean value = false;
		Screen screen = new Screen();
		try{
			screen.setAutoWaitTimeout(3);
			Pattern image = new Pattern(sikuliImages+imageName);
			if(screen.find(image) != null){
				screen.setAutoWaitTimeout(autowaitInSeconds);
				value = true;
			}
		}catch(FindFailed e){
			screen.setAutoWaitTimeout(autowaitInSeconds);
			//logger.info(e.getCause());
			if(e.getCause()==null){
				value = false;
				//				Log.log(Level.ERROR, imageName+" - Image is not present");
			}else{
				throw new RuntimeException("Some other exception occured");
			}
		}catch(Exception e){
			screen.setAutoWaitTimeout(autowaitInSeconds);
			throw e;
		}
		return value;
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	public static Match RegionFind(String imagePath){
		try{
			Pattern image = new Pattern(sikuliImages+imagePath);
			return region.find(image);
		}catch(FindFailed e){
			Log.log(Level.ERROR, "'"+imagePath+"' IMAGE NOT FOUNG");
		}catch(Exception e){
			throw e;
		}
		return null;
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	public static void FocusRegionWindow(){
		try{
			Thread.sleep(3000);
			region = App.focusedWindow();//Identify the currently focused or the frontmost window and switch to it. Sikuli does not tell you, to which application this window belongs.
			region.highlight(1);
			region.setAutoWaitTimeout(autowaitInSeconds);
		} catch (Exception e) {
			Log.log(Level.ERROR, "Exception occured",e);
		}
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	public static String CopyToClipboard(){
		String text="";
		try{
			WebElementActions.Wait(1);
			region.type("c", Key.CTRL);//TODO
			WebElementActions.Wait(1);
			text = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor); 
			//			text = App.getClipboard().trim();
		}catch(Exception e){
			Log.log(Level.ERROR, "Exception occured",e);
		}
		return text;
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	public static Match ScreenFind(String imagePath){
		try{	
			Screen screen = new Screen();
			screen.setAutoWaitTimeout(waitInSeconds);
			Pattern image = new Pattern(sikuliImages+imagePath);
			return screen.find(image);
		}catch(FindFailed e){
			Log.log(Level.ERROR, "'"+imagePath+"' IMAGE NOT FOUNG");
		}catch(Exception e){
			throw e;
		}
		return null;
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	public static void RegionWaitForObject(String imageName){
		try{
			Match element = RegionFind(imageName);
			element.highlight(2);
			//			while(RegionObjectExists(imageName) != true){//Wait till the element is not null.
			//				logger.info();
			//				Log.log(Level.INFO, "Waiting for expected image to appear!!!!!");
			//			}
		}catch(Exception e){
			Log.log(Level.ERROR, "Exception occured",e);
		}
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	public static void ScreenWaitForObject(String imageName){
		try{
			Screen screen = new Screen();
			screen.setAutoWaitTimeout(waitInSeconds);
			Match element = ScreenFind(imageName);
			element.highlight(2);
		}catch(Exception e){
			Log.log(Level.ERROR, "Exception occured",e);
		}
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	public static boolean Click(String imageName){
		try{
			//			if(RegionObjectExists(imageName)){
			Match element = RegionFind(imageName);
			element.highlight(1);
			element.hover();
			element.click();
			return true;
			//			}else{
			//				Log.log(Level.ERROR, imageName+" is not exists");
			//				return false;
			//			}
		}catch(Exception e){
			Log.log(Level.ERROR, "Exception occured",e);
			return false;
		}
	}
	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	public static String GetText(String imagePath){
		try{
			//			Robot robot = new Robot();
			Click(imagePath);
			return CopyToClipboard();
			/*	String image = "CommentsSection.PNG";
			RegionFind(image).hover();
			RegionFind(image).click();*/

			//			
			//			int x = region.getLastMatch().getX() + 10;
			//			int y = region.getLastMatch().getY() + 10;
			//			//			robot.mousePress("");
			//			robot.keyPress(InputEvent.BUTTON1_MASK);

			//			region.type("a", Key.CTRL);
			//			region.type("c", Key.CTRL);

			//			expected  = region.find(image).right(100).text();

		}catch(Exception e){
			Log.log(Level.ERROR, "Exception occured",e);
		}
		return null;
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	public static boolean DoubleClick(String imageName){
		try{
			if(RegionObjectExists(imageName)){
				Match element  = RegionFind(imageName);
				element.hover();
				element.doubleClick();
				return true;
			}else{
				Log.log(Level.INFO, imageName+" is not exists");
				return false;
			}
		}catch(Exception e){
			Log.log(Level.ERROR, "Exception occured",e);
			return false;
		}
	}

	//	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	//	public static void FocusScreenWindow(){
	//		try{
	//			Thread.sleep(1000);
	//			r = App.focusedWindow();
	//			r.highlight(2);
	//		}catch (Exception e) {
	//			Log.log(Level.ERROR, "Exception occured",e);
	//		}
	//	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	public static String GetFileName(String imageName){
		return imageName.substring(imageName.lastIndexOf("/")+1, imageName.length());
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	public static boolean CompareDate(String actual, String expected){
		boolean returnValue= false;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
			Date expectedDate = sdf.parse(expected);
			Date actualDate = sdf.parse(actual);
			returnValue = expectedDate.equals(actualDate);
		}catch(Exception e){
			Log.log(Level.ERROR, "Exception occured",e);
		}
		return returnValue;
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	public static void SaveScreenshot(){
		try{
			String fileName ="Sikuli_"+WebElementActions.GetTimeStamp("ddMMMyy-hh_mm_ss_SSS");
			fileName = region.saveScreenCapture(target_loc,fileName);
			Log.log(Level.INFO, "Screenshot path - "+fileName);
		}catch(Exception e){
			Log.log(Level.ERROR, "Exception occured",e);
		}
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	@Deprecated
	public static void KeyboardStrokes(String keyStrokes){
		try{	
			Screen screen = new Screen();
			List<String> keyStroke= Arrays.asList(keyStrokes.split("\\|"));
			if(keyStroke.size()==2){
				String variableName = keyStroke.get(0).toUpperCase();// + letter.toUpperCase();
				Class clazz = KeyModifier.class;
				Field field = clazz.getField(variableName);
				//	     	int keyCode = field.getInt(null);
				//	        String keycode = field.toString();
				int keyCode = field.getInt(null);
				screen.type(keyStroke.get(1), keyCode);//KeyModifier only accept the integer as as parameters,nor Key wont accept it.
			}else if(keyStroke.size()==1){
				screen.type(keyStroke.get(0));
			}else{
				Log.log(Level.ERROR, "Invalid parameter(s)");
			}
			//	            screen.type("d".toString(),keycode.substring(keycode.indexOf("org"), keycode.length()));
			//				screen.type("d",Key.WIN);
			//	            robot.keyPress( keycode );
			//	            robot.keyPress(KeyEvent.VK_D);
			//	           
			//	            robot.keyRelease( keyCode );
			//				robot.keyPress(KeyEvent.VK_H);
			//				String c[]= keyStrokes.split(",");
			//				String az= "WIN";
			//				if(a.size()==2){
			//				r.type(c[1].toString(),Key.az);
			//				r.type(c[1].toString(),org.sikuli.api.robot.Key.);
			//				screen.type(c[1].toString(),c[0].toString());
			//				screen.type(a.get(1),a.get(0));
			//				}else if(a.size()==1){
			//				screen.type(a.get(0));
			//				}else{
			//				Log.log(Level.INFO, "Invalid paramaeter");
			//				}
		}catch(NoSuchFieldException e){
			Log.log(Level.ERROR, "Invalid parameter!!!Please pass the exact windows keywords i.e.,To press,Windows->WIN,Alt->alt etc..,");
		}catch(Exception e){
			Log.log(Level.ERROR, "Exception occured",e);
		}
	}

	//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Opera PMS reusable method starts from here*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	@Deprecated
	public static void compareDatePMS(String passvalue){//String confirmationNumber,String expectedFromDate, String expectedToDate
		try{
			List<String> str = Arrays.asList(passvalue.split(","));
			String confirmationNumber = str.get(0);
			String expectedFromDate= str.get(1);
			String expectedToDate= str.get(2);
			Thread.sleep(3000);
			KeyboardStrokes("alt|r");
			//			r.type("r",Key.ALT);
			WebElementActions.Wait(2);
			KeyboardStrokes("u");
			WebElementActions.Wait(3);
			//			r.type("u");
			for(int i=0;i<=4;i++){
				WebElementActions.Wait(3);
				region.type(Key.TAB);
				//				r.type(Key.TAB);//Move cursor to Conf/Cxl No Edit box
			}
			region.type(confirmationNumber);
			SaveScreenshot();
			WebElementActions.Wait(2);
			region.type(Key.ENTER);
			WebElementActions.Wait(8);
			if(!RegionObjectExists("ReservationNotFound.PNG")){
				Log.log(Level.INFO, "Reservation details displayed !!!!");
				region.type(Key.ENTER);//To pop out the reservation details
				WebElementActions.Wait(30);
				String actualFromDate = CopyToClipboard();
				region.type(Key.TAB);
				WebElementActions.Wait(2);
				region.type(Key.TAB);
				WebElementActions.Wait(2);
				String actualToDate = CopyToClipboard();
				SaveScreenshot();
				//Comparing From date 
				if(CompareDate(expectedFromDate,actualFromDate)){
					Log.log(Level.INFO,"Equal"+actualFromDate+" <--> "+expectedFromDate);//TODO
				}else{
					Log.log(Level.INFO,"NOT equal"+actualFromDate+" <--> "+expectedFromDate);//TODO
				}

				//Comparing to date
				if(CompareDate(expectedToDate ,actualToDate)){
					Log.log(Level.INFO,"Equal"+actualToDate+" <--> "+expectedToDate);//TODO
				}else{
					Log.log(Level.INFO,"NOT equal"+actualToDate+" <--> "+expectedToDate);//TODO
				}

				//Click close button
				Click("ReservationClose_BN.PNG");
				WebElementActions.Wait(5);
				Click("ReservationClose_BN.PNG");
			}else{
				SaveScreenshot();
				Log.log(Level.ERROR, "Reservation not found Popup displayed!! So ignoring it!!");
				Click("OK_BN.PNG");
			}
		}catch(Exception e){
			Log.log(Level.ERROR, "Exception occured",e);
		}
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	@Deprecated
	public static void VerifyConfirmationNumber(String confirmationNumber){
		try{
			//Instead clicking an image made use of windows shortcuts.
			WebElementActions.Wait(3);
			KeyboardStrokes("alt|r");
			//			r.type("r",Key.ALT);
			WebElementActions.Wait(2);
			KeyboardStrokes("u");
			WebElementActions.Wait(3);
			//			r.type("u");
			for(int i=0;i<=4;i++){
				WebElementActions.Wait(1);
				region.type(Key.TAB);
				//				r.type(Key.TAB);//Move cursor to Conf/Cxl No Edit box
			}
			region.type(confirmationNumber);
			WebElementActions.Wait(2);
			region.type(Key.ENTER);
			WebElementActions.Wait(5);
			if(!RegionObjectExists("ReservationNotFound.PNG")){
				SaveScreenshot();
				Log.log(Level.INFO, "Reservatoin details displayed !!!!");
			}else{
				SaveScreenshot();
				Log.log(Level.ERROR, "Reservation not found Popup displayed!! So ignoring it!!");
				RegionObjectExists("OK_BN.PNG");
				Click("OK_BN.PNG");
			}
			WebElementActions.Wait(5);
			Click("Traces_BN.PNG");

			for(int i=0;i<=5;i++){
				WebElementActions.Wait(3);
				region.type(Key.TAB);
			}
			for(int i=0;i<=6;i++){
				WebElementActions.Wait(3);
				region.type(Key.DOWN);	
			}
			WebElementActions.Wait(3);
			String s= GetText("CommentsSection.PNG");
			Log.log(Level.ERROR, s);



			//Close the Reservation popup
			//			Click("ReservationClose_BN.PNG");
			//			//Reservation Popup 
			//			if(DoubleClick(path+"UpdateReservation_BN.PNG")){
			//				if(ObjectExists(path+"UpdateReservation_PopUp.PNG")){
			//					logger.info("Reservation plan displayed");
			//				}else{
			//					logger.info("Reservation plan not displayed");
			//				}
			//			}
		}catch(Exception e){
			Log.log(Level.ERROR, "Exception occured",e);
		}
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	@Deprecated
	public static void Reinstate(String confirmationNumber){
		//Instead clicking an image made use of windows shortcuts.
		WebElementActions.Wait(3);
		KeyboardStrokes("alt|r");
		//			r.type("r",Key.ALT);
		WebElementActions.Wait(2);
		KeyboardStrokes("u");
		WebElementActions.Wait(3);
		//			r.type("u");
		for(int i=0;i<=4;i++){
			WebElementActions.Wait(3);
			region.type(Key.TAB);
			//				r.type(Key.TAB);//Move cursor to Conf/Cxl No Edit box
		}
		region.type(confirmationNumber);
		WebElementActions.Wait(2);
		region.type(Key.ENTER);
		WebElementActions.Wait(5);
		if(!RegionObjectExists("ReservationNotFound.PNG")){
			SaveScreenshot();
			Log.log(Level.INFO, "Reservatoin details displayed !!!!");
			if(RegionObjectExists("")){//TODO
				Log.log(Level.INFO, "Reinstate button is displayed");
			}else{
				Log.log(Level.ERROR, "Reinstate button is not displayed");
			}
		}else{
			SaveScreenshot();
			Log.log(Level.ERROR, "Reservation not found Popup displayed!!");
			Click("OK_BN.PNG");
		}
	}
	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	
	
	
	public static String GetCommentsForReservationNumber(String reservartionNumber){

		try{
			//Instead clicking an image made use of windows shortcuts.
			WebElementActions.Wait(3);
			KeyboardStrokes("alt|r");
			//			r.type("r",Key.ALT);
			WebElementActions.Wait(2);
			KeyboardStrokes("u");
			WebElementActions.Wait(3);
			//			r.type("u");
			for(int i=0;i<=4;i++){
				WebElementActions.Wait(1);
				region.type(Key.TAB);
				//				r.type(Key.TAB);//Move cursor to Conf/Cxl No Edit box
			}
			region.type(reservartionNumber);
//			ActionPerformer.Wait(2);
			region.type(Key.ENTER);
			WebElementActions.Wait(2);
			if(!RegionObjectExists("ReservationNotFound.PNG")){
				SaveScreenshot();
				Log.log(Level.INFO, "Reservatoin details displayed !!!!");
			}else{
				SaveScreenshot();
				Log.log(Level.ERROR, "Reservation not found Popup displayed!! So ignoring it!!");
				RegionObjectExists("OK_BN.PNG");
				Click("OK_BN.PNG");
			}
			WebElementActions.Wait(2);
			Click("Traces_BN.PNG");

			for(int i=0;i<=5;i++){
				WebElementActions.Wait(3);
				region.type(Key.TAB);
			}
			for(int i=0;i<=6;i++){
				WebElementActions.Wait(3);
				region.type(Key.DOWN);	
			}
			WebElementActions.Wait(3);
			String s = GetText("CommentsSection.PNG");
			Log.log(Level.INFO, s);
			return s;
			//Close the Reservation popup
			//			Click("ReservationClose_BN.PNG");
			//			//Reservation Popup 
			//			if(DoubleClick(path+"UpdateReservation_BN.PNG")){
			//				if(ObjectExists(path+"UpdateReservation_PopUp.PNG")){
			//					logger.info("Reservation plan displayed");
			//				}else{
			//					logger.info("Reservation plan not displayed");
			//				}
			//			}
		}catch(Exception e){
			Log.log(Level.ERROR, "Exception occured",e);
		}
		return null;
	
	}
}





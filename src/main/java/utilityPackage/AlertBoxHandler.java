package utilityPackage;


import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class AlertBoxHandler {

	static Logger log = Logger.getLogger(AlertBoxHandler.class.getName());

	public static void alertBoxHandling(WebDriver driver , String pass_values) throws InterruptedException, IOException{
		try{
			log.info("inside alert Box handler");
			WebElementActions.ExplicitWaitAlertIsPresent(20);
			
			if(driver.switchTo().alert() != null){
				Alert alt = driver.switchTo().alert();
				//Thread.sleep(300);
				log.info(alt.getText());
				if(pass_values.equalsIgnoreCase("YES")){
					alt.accept();
					log.info("Alert box accepted");
				}else{
					alt.dismiss();
					log.info("Alert box rejected");
				}
			}
			//driver.switchTo().window(ParentWindow);
		}catch(Exception e){
			log.error("Exception - inside alert Box handler",e);
			//WebDriver pvtDriver = null;
			//CheckPoint.screenshotException(pvtDriver);
			ExceptionActions.performAfterExceptionOccurs(driver);
		}
	}
}

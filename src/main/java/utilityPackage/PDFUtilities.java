package utilityPackage;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class PDFUtilities {
	
	static String projectPath = System.getProperty("user.dir");
	static  Logger PDFlog = Logger.getLogger(PDFUtilities.class);
	
  @Test
  private static String searchTextInPDF(PDFParser parser, String reqTextInPDF,String[] startEndPages) {
	  
	  String flag = "FAIL";
		
		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = null;

		try {
			
			/*URL url = new URL(strURL);
			BufferedInputStream file = new BufferedInputStream(url.openStream());
			PDFParser parser = new PDFParser(file);*/
		
			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdDoc = new PDDocument(cosDoc);
			
			//Starting and ending page in the PDF 
			if(startEndPages[0].equalsIgnoreCase("all") || startEndPages[0].equalsIgnoreCase("NA")){
				pdfStripper.setStartPage(1);
				int noOfPages = pdDoc.getNumberOfPages();
				PDFlog.info("Number of pages in the pdf file - "+ noOfPages);
				pdfStripper.setEndPage(noOfPages);
			}
			else{
				PDFlog.info("Starting page index - "+startEndPages[0] );
				pdfStripper.setStartPage(Integer.parseInt(startEndPages[0]));
				PDFlog.info("Ending page index - "+startEndPages[1] );
				pdfStripper.setEndPage(Integer.parseInt(startEndPages[1]));
				
			}
			
			parsedText = pdfStripper.getText(pdDoc);
			
		} catch (MalformedURLException e2) {
			PDFlog.error("URL string could not be parsed ",e2);
		} catch (IOException e) {
			PDFlog.error("URL string could not be parsed ",e);
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}
		
		PDFlog.info("++++++++++++++++++++++++ Reading PDF FILE");
		PDFlog.info(parsedText);
		PDFlog.info("+++++++++++++++++ Completed reading");

		if(parsedText.contains(reqTextInPDF)) {
			flag="PASS";
			PDFlog.info("!!!!!!!!!!!!!!!!!!!!!!!! required text is found - "+ reqTextInPDF);
		}
		
		return flag;
	  
  }

  //*************************************************************
  
  //Searching text from URL
  public static String searchPDFFromURL(WebDriver _driver,String searchText){
	  String result = "FAIL";
	  try{
		  String applURL = _driver.getCurrentUrl();
		  String startEndPage[] = {"all"};
		  
		  URL url = new URL(applURL);
			BufferedInputStream file = new BufferedInputStream(url.openStream());
			PDFParser parser = new PDFParser(file);
			result = searchTextInPDF(parser,searchText,startEndPage);
			return result;
	  }
	  catch(Exception e){
		  PDFlog.error("The Exception in searching text from PDF url - ",e);
		  return result;
	  }
	  
  }
  
  //Searching text from File
  public  static String searchPDFFromFile( String pdfFile,String propertyFile,String searchText,String pages) throws FileNotFoundException{
	  String result = "FAIL";
	  String startEndPage[] = SplitString.stringSplit(pages);
	  String applicationFolder = RepositoryObject.IdentifierRepo(propertyFile, "TestDataFolder");
	  String pdfFileLocation = projectPath + "//TestData//" + applicationFolder + "//" ;
	  
	  File file = new File(pdfFileLocation + pdfFile);
	  
	  try{
		  PDFParser parser = new PDFParser(new FileInputStream(file));
		  result = searchTextInPDF(parser,searchText,startEndPage);
		  return result;
	  }
	  catch(Exception e){
		  PDFlog.error("The Exception in searching text from PDF file - ",e);
		  return result;
	  }
	  
  }
  
}

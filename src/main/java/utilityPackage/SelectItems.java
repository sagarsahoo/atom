package utilityPackage;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;



public class SelectItems {
	
	
	//itemsToSelect()
	//itemsToDeselect()
	//selectCheckBox()
	//clickCorrepondingSelectItem()
	//SelectDropDown
	
	static Logger SelectItems_log = Logger.getLogger(SelectItems.class);
	
	
	public static void itemsToSelect(String element_identifiedBy, String element_identifier, String list_item, String group_identifiedBy, String group_identifier ){
		
		
		try{
			WebElementActions.ExplicitWaitPresenceOfElement(group_identifiedBy,group_identifier,30);
			SelectItems_log.info("Inside itemsToSelect function ");

			//Locating the group of the item list
			WebElement group =  WebElementActions.GetWebElement(group_identifiedBy,group_identifier);
			SelectItems_log.info("Group_list -" + group.getText());

			//Storing all the list of items
			List<WebElement> elements = group.findElements(ActionPerformer.elementFinder(element_identifiedBy,element_identifier));
			
			if(list_item.trim().equalsIgnoreCase("all")){
				SelectItems_log.info("Inside condition to select all");
				for (WebElement element : elements){
					SelectItems_log.info("element - "+ element.getText());
					if(element.isEnabled()){
						SelectItems_log.info("Element is enabled");
						element.click();
					}
					Thread.sleep(1000);
				}
			}
			else{				
				String[] itemlist_array = SplitString.stringSplit(list_item); // Splitting the passed values as per separator and storing in a variable
				for (int i = 0 ; i <itemlist_array.length ; i ++){
					SelectItems_log.info("list_item " + i + "=" + itemlist_array[i] );
					
					for (WebElement element : elements){						
						if(itemlist_array[i].trim().equalsIgnoreCase(element.getText().trim())){							
								SelectItems_log.info("click - " + element.getText() + "," + itemlist_array[i] );
								element.click();
								Thread.sleep(100);
								SelectItems_log.info("match Found");
								break;
						}						
					}										
				}	
				
			}//End of else	
		}	
		catch(Exception e){
			
			SelectItems_log.error(" Exception - Inside select items form list - " ,e);
			Assert.fail("Failure as exception occured");			
		}		
	}
	
	// For de-selecting items
	public static void itemsToDeselect(String element_identifiedBy, String element_identifier, String list_item, String group_identifiedBy, String group_identifier ){
		
		try{
			
			//Locating the group of the item list
			WebElement group =  WebElementActions.GetWebElement(group_identifiedBy,group_identifier);
			SelectItems_log.info("Group_list -" + group.getText());
			//Storing all the list of items
			List<WebElement> elements = group.findElements(ActionPerformer.elementFinder(element_identifiedBy,element_identifier));
			Select selectData=new Select(group);
			if(list_item.equalsIgnoreCase("all")){				
				selectData.deselectAll(); 			
			}			
			String[] itemlist_array = SplitString.stringSplit(list_item); // Splitting the passed values as per separator and storing in a variable			
			for (int i = 0 ; i <itemlist_array.length ; i ++){
				//SelectItems_log.info("list_item " + i + "=" + itemlist_array[i] );				
				for (WebElement element : elements){
					//SelectItems_log.info("element in the product list-" + element.getText());					
					if(itemlist_array[i].trim().equalsIgnoreCase(element.getText().trim())){						
						if(element.isSelected()){
							element.click();
							Thread.sleep(100);
						}						
						SelectItems_log.info("Deselected - " + element.getText() + "," + itemlist_array[i] );	
						break;
					}
				}								
			}			
		}
		
		catch(Exception e){
			
			SelectItems_log.error(" Exception - Inside de-select items form list - " ,e);
			Assert.fail("Failure as exception occured");
		}
	}

	//Selecting the Check Boxes
	public static void selectCheckBox(String elementIdentifiedBy,String elementIdentifier,String ONorOFF){
		try{
			WebElement ActionObject = WebElementActions.GetWebElement(elementIdentifiedBy, elementIdentifier);
			if(ONorOFF.equalsIgnoreCase("Y")){
				if(!ActionObject.isSelected()){
					ActionObject.click();
				}
				try{
					if(!ActionObject.isSelected()){
						ActionObject.click();
					}
				}catch(StaleElementReferenceException e){
					SelectItems_log.info("Page refreshed");
				}
			}else{
				if(ActionObject.isSelected()){
					ActionObject.click();
				}
				try{
					if(ActionObject.isSelected()){//TO avoid the StaleElementReferenceException exception
						ActionObject.click();
					}
				}catch(StaleElementReferenceException e){
					SelectItems_log.info("Page refreshed");
				}

			}
		}catch(Exception e){
			SelectItems_log.error("Exception occured is-",e);
			Assert.fail("Failure as exception occured");
		}
	}

	
	
public static void SelectDropDown(WebDriver driver,String element_identifiedBy, String element_identifier, String list_item, String group_identifiedBy, String group_identifier ){
		
		
		try{
			//Explicit wait provided
			WebElementActions.ExplicitWaitPresenceOfElement(group_identifiedBy,group_identifier,30);
			SelectItems_log.info("Inside Drop Down function ");

			//Locating the group of the item list
			WebElement group =  WebElementActions.GetWebElement(group_identifiedBy,group_identifier);
			//SelectItems_log.info("Group_list -" + group.getText());
						
			//Storing all the list of items			
			List<WebElement> elements = group.findElements(ActionPerformer.elementFinder(element_identifiedBy,element_identifier));
					
					for (WebElement element : elements){						
						if(list_item.trim().equalsIgnoreCase(element.getText().trim())){							
								SelectItems_log.info("click - " + element.getText() + "," + list_item );
								element.click();
								Thread.sleep(100);
								SelectItems_log.info("match Found");
								break;
						}						
					}	

			/*WebElement ActionObject = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
			Select DropDownObject = new Select(ActionObject);
			DropDownObject.selectByVisibleText(list_item);*/		
		
			
		}
		catch(Exception e){
			
			SelectItems_log.error(" Exception - Inside drop down by visible text - ", e);
			Assert.fail("Failure as exception occured");
			
		}		
	}


public static void SelectDropDownByValue(WebDriver driver,String element_identifiedBy, String element_identifier, String value, String group_identifiedBy, String group_identifier ){
	
	
	try{
		SelectItems_log.info("Inside Drop Down function by Value ");

		WebElement ActionObject = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
		Select DropDownObject = new Select(ActionObject);
		DropDownObject.selectByValue(value);	
	}
	catch(Exception e){
		
		SelectItems_log.error(" Exception - Inside drop down by value - ", e);
		Assert.fail("Failure as exception occured");
		
	}		
}

public static void SelectDropDownByIndex(WebDriver driver,String element_identifiedBy, String element_identifier, String index, String group_identifiedBy, String group_identifier ){
	
	
	try{
		SelectItems_log.info("Inside Drop Down function by Value ");

		WebElement ActionObject = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
		Select DropDownObject = new Select(ActionObject);
		int dropDownIndex = Integer.parseInt(index);
		DropDownObject.selectByIndex(dropDownIndex);;	
	}
	catch(Exception e){
		
		SelectItems_log.error(" Exception - Inside drop down by Index - ", e);
		Assert.fail("Failure as exception occured");
		
	}		
}



public static void getList(String ele_indentifiedBy, String ele_identifier){
	try{
		
		List<WebElement> Elements =  WebElementActions.GetWebElements("xpath",("(//table[@class='detailList'])[1]//label"));
		//WebElement we1 = driver.findElement(By.xpath("//@label=Campaign Type"))
		
		for(WebElement wb:Elements){
			SelectItems_log.info("element - "+ wb.getText());
		}
	}
	catch(Exception e){
		SelectItems_log.info("Exception is - "+ e);
	}
}

public static boolean DropDown_SelectValue(String element_identifiedBy, String element_identifier, String ValueToSelect){
	try{
		WebElementActions.ExplicitWaitPresenceOfElement(element_identifiedBy,element_identifier,30);
		WebElement ActionObject = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
		Select DropDownObject = new Select(ActionObject);
		DropDownObject.selectByValue(ValueToSelect);
		return true;
	}catch(NoSuchElementException e){
		SelectItems_log.info("There is no option with value: '"+ValueToSelect +"'" + "for the object ");
		return false;
	}catch(Exception e){
		SelectItems_log.info("Exception occured",e);
		return false;
	}
}

public static boolean DropDown_SelectText(String element_identifiedBy, String element_identifier, String TextToSelect){
	try{
		WebElementActions.ExplicitWaitPresenceOfElement(element_identifiedBy,element_identifier,30);
		WebElement ActionObject = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
		Select DropDownObject = new Select(ActionObject);
		DropDownObject.selectByVisibleText(TextToSelect);
		return true;
	}catch(NoSuchElementException e){
		SelectItems_log.info("There is no option with text: '"+ TextToSelect + "' for the object ");
		return false;
	}catch(Exception e){
		SelectItems_log.info("Exception occured",e);
		return false;
	}
}

public static boolean DrpDwn_SelectIndex(String element_identifiedBy, String element_identifier, String Index) {
	boolean ReturnValue = false;
	int IndexToSelect = Integer.parseInt(Index);	
	try {
		WebElementActions.ExplicitWaitPresenceOfElement(element_identifiedBy,element_identifier,30);
		WebElement ActionObject = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
		Select DropDownObject = new Select(ActionObject);
		DropDownObject.selectByIndex(IndexToSelect);
		ReturnValue = true;
	} catch (NoSuchElementException e) {
		SelectItems_log.info("There is no option with index: '"+ IndexToSelect + "' for the object ");
		ReturnValue = false;
	} catch (Exception e) {
		SelectItems_log.info("Exception occured", e);
		ReturnValue = false;
	}
	return ReturnValue;
}

public static boolean DrpDwn_SelectRandom(String element_identifiedBy, String element_identifier) {
	boolean ReturnValue = false;
	int totalOptionsSize=0;
	int IndexToSelect=0;
	try {
		WebElementActions.ExplicitWaitPresenceOfElement(element_identifiedBy,element_identifier,30);
		WebElement ActionObject = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
		Select DropDownObject = new Select(ActionObject);
		totalOptionsSize = DropDownObject.getOptions().size();
		IndexToSelect = Integer.parseInt(JavaFunctions.RandomInteger(totalOptionsSize));
		DropDownObject.selectByIndex(IndexToSelect);
		ReturnValue = true;
	} catch (NoSuchElementException e) {
		SelectItems_log.info("There is no option with index: '"+ IndexToSelect + "' for the object ");
		ReturnValue = false;
	} catch (Exception e) {
		SelectItems_log.info("Exception occured", e);
		ReturnValue = false;
	}
	return ReturnValue;
}

public static String DropDown_GetSelectedText(String element_identifiedBy, String element_identifier){
	String returnText="";
	try{
		WebElement ActionObject = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
		Select DropDownObject = new Select(ActionObject);
		returnText = DropDownObject.getFirstSelectedOption().getText();
	}catch(Exception e){
		SelectItems_log.info("Exception occured",e);
		return returnText;
	}
	return returnText;
}

//To select a different value which other than the selected
public static void selecOtherValue(String grp_identifiedby,String grp_idetifier){
	String selectedValue=null;
	try{
		
		WebElement ActionObject = WebElementActions.GetWebElement(grp_identifiedby,grp_idetifier);
		Select DropDownObject = new Select(ActionObject);
		selectedValue = DropDownObject.getFirstSelectedOption().getText();
		
		List <WebElement> getValues = DropDownObject.getOptions();
		for (WebElement element : getValues){
			if(!element.getText().equals(selectedValue)){
				element.click();
				break;
			}
		}
		
	}
	catch(Exception e){
		SelectItems_log.error("The exception in the selecOtherValue is -  ",e);
	}
}

}





package utilityPackage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import selfitGui.ExcelDataList;

public class WebElementActions {

	static Logger log = Logger.getLogger(WebElementActions.class);
	@Test
	//to find element location



	public static WebElement GetWebElement(String elementIdentifiedBy,String elementIdentifier){
		WebElement FoundObject = null;
		try{
			int numberOfObjects = ActionPerformer.driver.findElements(ActionPerformer.elementFinder(elementIdentifiedBy, elementIdentifier)).size();
			log.info("Number of elements searched in the page for the indentifier values - "+ numberOfObjects);
			FoundObject = ActionPerformer.driver.findElement(ActionPerformer.elementFinder(elementIdentifiedBy, elementIdentifier));
		}catch(NoSuchElementException exc){
			//			CheckPoint.screenShot(driver,"NoSuchElement","Exception","FAIL");
			log.error("OBJECT NOT FOUND "+elementIdentifiedBy+" as '"+elementIdentifier+"'");
			//			if(!(driver.findElements(By.tagName("iframe")).isEmpty()&& driver.findElements(By.tagName("frame")).isEmpty())){
			if(ActionPerformer.driver.findElements(By.tagName("iframe")).size()>0 || ActionPerformer.driver.findElements(By.tagName("frame")).size()>0){
				log.error("FRAME(s) ARE AVAILABLE!!!..Switch to corrrespoding frame and try it!!!");
			}
			CheckPoint.screenShot("FAIL");
		}catch(Exception e){
			//NewActionPerformer.driver.quit();
			log.info("Exception in getting web element -",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
		return FoundObject;
	}



	//Getting a list of webElements
	public static List<WebElement> GetWebElements(String elementIdentifiedBy,String elementIdentifier){
		List<WebElement> FoundObject = null;
		try{
			int numberOfObjects = ActionPerformer.driver.findElements(ActionPerformer.elementFinder(elementIdentifiedBy, elementIdentifier)).size();
			log.info("Number of elements searched in the page for the indentifier values - "+ numberOfObjects);
			FoundObject = ActionPerformer.driver.findElements(ActionPerformer.elementFinder(elementIdentifiedBy, elementIdentifier));
		}catch(Exception e){
			log.error("OBJECT NOT FOUND "+elementIdentifiedBy+" as '"+elementIdentifier+"'",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
		return FoundObject;
	}

	public static void ExplicitWaitPresenceOfElement(String elementIdentifiedBy,String elementIdentifier,int TimeForExplicitWait){
		try{
			ActionPerformer.wait = new WebDriverWait(ActionPerformer.driver, TimeForExplicitWait);
			ActionPerformer.wait.until(ExpectedConditions.presenceOfElementLocated(ActionPerformer.elementFinder(elementIdentifiedBy,elementIdentifier)));
		}catch(Exception e){
			log.error("Exception occurred",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	}

	public static void ExplicitWaitElementClikable(String elementIdentifiedBy,String elementIdentifier,int TimeoutInSeconds){
		try{
			ActionPerformer.wait = new WebDriverWait(ActionPerformer.driver, TimeoutInSeconds);
			ActionPerformer.wait.until(ExpectedConditions.elementToBeClickable(ActionPerformer.elementFinder(elementIdentifiedBy,elementIdentifier)));
		}catch(Exception e){
			//CheckPoint.screenShot(NewActionPerformer.driver,"Exception","Occured","FAIL");
			log.error("Exception occurred",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	}

	public static void ExplicitWaitAlertIsPresent(int TimeoutInSeconds){
		try{
			ActionPerformer.wait = new WebDriverWait(ActionPerformer.driver, TimeoutInSeconds);
			ActionPerformer.wait.until(ExpectedConditions.alertIsPresent());
		}catch(Exception e){
			//CheckPoint.screenShot(NewActionPerformer.driver,"Exception","Occured","FAIL");
			log.error("Exception occurred",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	}

	public static void ExplicitWaitInvisibilityOfElement(String elementIdentifiedBy,String elementIdentifier,int TimeForExplicitWait){
		try{
			ActionPerformer.wait = new WebDriverWait(ActionPerformer.driver, TimeForExplicitWait);
			ActionPerformer.wait.until(ExpectedConditions.invisibilityOfElementLocated(ActionPerformer.elementFinder(elementIdentifiedBy,elementIdentifier)));
		}catch(Exception e){
			//CheckPoint.screenShot(NewActionPerformer.driver,"Exception","Occured","FAIL");
			log.error("Exception occurred",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	}

	public static void WaitForFrameSwitchToIt(String elementIdentifiedBy,String elementIdentifier,int TimeForExplicitWait){
		try{
			ActionPerformer.wait = new WebDriverWait(ActionPerformer.driver, TimeForExplicitWait);
			ActionPerformer.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(ActionPerformer.elementFinder(elementIdentifiedBy,elementIdentifier)));
		}catch(Exception e){
			//CheckPoint.screenShot(NewActionPerformer.driver,"Exception","Occured","FAIL");
			log.error("Exception occurred",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	}

	public static void QuitBrowser(){
		if(ActionPerformer.driver!=null){
			try{
				ActionPerformer.driver.quit();
			}catch(Exception e){
				log.error("Exception occurred",e);
			}
		}
	}
	public static Object RunJavaScript(String JScript){
		try{
			return ((JavascriptExecutor) ActionPerformer.driver).executeScript(JScript);

		}catch(Exception e){
			log.error("Window has been closed");
			return "complete";
		}
	}	

	public static Object RunJavaScript(String JScript,WebElement ActionObject){
		try{
			return ((JavascriptExecutor) ActionPerformer.driver).executeScript(JScript,ActionObject);
		}catch(Exception e){
			return "Error";
		}
	}

	public static void WaitForPageload(){
		String BrowserStatus;
		do{
			log.debug("Browser is still loading...");
			Wait(1);
			try {
				BrowserStatus = (String) RunJavaScript("return document.readyState");
			}catch(Exception e){
				log.error("Exception occured - ",e);
				BrowserStatus = "complete";//If exception occurred to avoid indefinite loop making the status as 'complete'
			}
		}while(!(BrowserStatus.compareToIgnoreCase("complete")==0));
		BrowserStatus = null;
	}

	public static void Wait(int TimeOutInSecs){
		try {
			Thread.sleep(TimeOutInSecs * 1000);
		} catch (InterruptedException e) {
			log.error("Exception occured - ",e);
		}
	}

	public static boolean ObjectExist(String element_identifiedBy,String element_identifier){
		boolean result=false;
		if(ActionPerformer.driver.findElements(ActionPerformer.elementFinder(element_identifiedBy, element_identifier)).size()>0){
			result=true;
		}else{
			result=false;
		}
		CheckPoint.screenShot(result);
		VerifyTheText.writeResultToExcel(result);
		return result;
		//return ActionPerformer.driver.findElements(ActionPerformer.elementFinder(element_identifiedBy, element_identifier)).size()>0 ? true:false;
	}
	
	public static boolean ElementSelected(String element_identifiedBy,String element_identifier ){
		boolean result=false;
		result = GetWebElement(element_identifiedBy,element_identifier).isSelected();
		CheckPoint.screenShot(result);
		VerifyTheText.writeResultToExcel(result);
		return result;
		//return GetWebElement(element_identifiedBy,element_identifier).isSelected()?true:false;
	}

	public static void SelectRadioButton(String element_identifiedBy,String element_identifier){
		try{
			WebElement element = GetWebElement(element_identifiedBy,element_identifier);
			if(!element.isSelected()){
				element.click();
				if(!element.isSelected()){
					element.click();
				}
			}
			try{
				if(!element.isSelected()){
					WebElementActions.RunJavaScript("document.getElementById('"+element.getAttribute("id")+"').checked=true;");
				}
			}catch(StaleElementReferenceException ex){
				log.info("Link has been clicked on previous action, So WebPage got refreshed and navigated to another page.");//TO avoid StaleElementReferenceException exception

			}
		}catch(Exception e){
			log.error("Exception occured - ",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	}


	public static String GetTimeStamp(String DateFormat){
		String formattedDate = null ;
		try{
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat(DateFormat);
			formattedDate = sdf.format(date);
			return formattedDate;

		}catch(IllegalArgumentException e){
		}
		return formattedDate;
	}


	public boolean IsAlertPresent(){
		boolean ReturnValue = false;
		WebDriverWait WaitForAlert = new WebDriverWait(ActionPerformer.driver, 5);
		try{
			if(WaitForAlert.until(ExpectedConditions.alertIsPresent())!=null)
				ReturnValue =  true;
		}catch(TimeoutException e){
			ReturnValue =  false;
		}
		WaitForAlert = null;
		return ReturnValue;
	}

	public static void Click(String element_identifiedBy,String element_identifier){
		try{
			ActionPerformer.we = GetWebElement(element_identifiedBy,element_identifier);
			if(ActionPerformer.we.isEnabled() && ActionPerformer.we.isDisplayed()){
				if(OpenBrowser.GetBrowserInfo().startsWith("IE") && (ActionPerformer.we.getTagName().compareToIgnoreCase("a")==0 || 
						ActionPerformer.we.getTagName().compareToIgnoreCase("input")==0 || ActionPerformer.we.getTagName().compareToIgnoreCase("img")==0)){// && we.getAttribute("type").compareToIgnoreCase("checkbox")!=0
					ActionPerformer.we.sendKeys(Keys.ENTER);
				}else{
					ActionPerformer.we.click();
				}
				WaitForPageload();
			}else{
				log.error("Object is disabled");
			}
		}catch(Exception e){
			log.error("Exception occurred",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	}	


	public static void NormalClick(String element_identifiedBy,String element_identifier){
		try{
			ActionPerformer.we = GetWebElement(element_identifiedBy,element_identifier);
			if(OpenBrowser.GetBrowserInfo().startsWith("IE")){
				ActionPerformer.act.moveToElement(ActionPerformer.we).build().perform();
			}
			ActionPerformer.we.click();
			Thread.sleep(1000);
		}catch(Exception e){
			log.error("Exception occurred",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	}
	
	public static void ClickAndProceed(String element_identifiedBy,String element_identifier){
		try{
			ActionPerformer.we = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
			if(OpenBrowser.GetBrowserInfo().startsWith("IE") && (ActionPerformer.we.getTagName().compareToIgnoreCase("a")==0 || ActionPerformer.we.getTagName().compareToIgnoreCase("input")==0 || ActionPerformer.we.getTagName().compareToIgnoreCase("img")==0)){// && we.getAttribute("type").compareToIgnoreCase("checkbox")!=0
				ActionPerformer.we.sendKeys(Keys.ENTER);
			}else{
				ActionPerformer.we.click();
			}
		}catch(Exception e){
			log.error("Exception occurred",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	}
	
	public static void doubleClick(String element_identifiedBy,String element_identifier){
		try{
			ActionPerformer.act.moveToElement(WebElementActions.GetWebElement(element_identifiedBy,element_identifier)).doubleClick().build().perform();
			WebElementActions.WaitForPageload();
		}catch(Exception e){
			log.error("Exception occurred",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	}

	private static WebElement elementIfVisible(WebElement element) {
		return element.isDisplayed() ? element : null;
	}
	
	public static void enterText(String element_identifiedBy, String element_identifier,String pass_values){
		try{
			WebElementActions.ExplicitWaitPresenceOfElement(element_identifiedBy,element_identifier,30);
			ActionPerformer.we = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
			//ActionPerformer.we.clear();
			ConditionalEnterText.enterText(ActionPerformer.we, pass_values);
		}catch(Exception e){
			log.error("Exception occurred",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	}
	
	public static void enterTextWithoutClear(String element_identifiedBy, String element_identifier,String pass_values){
		try{
			WebElementActions.ExplicitWaitPresenceOfElement(element_identifiedBy,element_identifier,30);
			ActionPerformer.we = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
			ActionPerformer.we.clear();
			ConditionalEnterText.enterText(ActionPerformer.we, pass_values);
		}catch(Exception e){
			log.error("Exception occurred",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	}
	
	public static void enterTextIfNotPresent(String element_identifiedBy, String element_identifier,String pass_values,String target_identifier ){
		try{
			WebElementActions.ExplicitWaitPresenceOfElement(element_identifiedBy,element_identifier,30);
			ActionPerformer.we = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
			ConditionalEnterText.enterTextVerify(ActionPerformer.we, target_identifier, pass_values);
		}catch(Exception e){
			log.error("Exception occurred",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	}
	
	public static void clearText(String element_identifiedBy, String element_identifier){
		try{
			WebElementActions.ExplicitWaitPresenceOfElement(element_identifiedBy,element_identifier,30);
			ActionPerformer.we = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
			ActionPerformer.we.clear();
		}catch(Exception e){
			log.error("Exception occurred",e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	}
	
	public static List<String> getObjectIdentifiers(String objectIdentifiedBy,String objectIdentifier){
		List<String> ObjectValues = new ArrayList<String>();
		try{
			if(objectIdentifiedBy.trim().equalsIgnoreCase("excel")){
				//Split the sheetName and Object name from element_indetifier
				ObjectValues.clear();
				List<String> ObjectCharacteristics= new ArrayList<String>();
				String ObjectLocation[] = SplitString.stringSplit(objectIdentifier);
				String ObjectSheetName = ObjectLocation[0];
				String ObjectName = ObjectLocation[1];
				ObjectCharacteristics.addAll(ExcelDataList.getExcelDataInListByRowValue(CommonMethod.ExcelrepositoryFile, ObjectSheetName, "Name", ObjectName));
				objectIdentifiedBy = ObjectCharacteristics.get(2);
				objectIdentifier = ObjectCharacteristics.get(3);
				ObjectValues.add(objectIdentifiedBy);
				ObjectValues.add(objectIdentifier);
			}
			return ObjectValues;
		}catch(Exception e){
			log.error("Exception is - "+ e);
			ObjectValues.add(objectIdentifiedBy);
			ObjectValues.add(objectIdentifier);
			return ObjectValues;
		}
	}
	
	

}

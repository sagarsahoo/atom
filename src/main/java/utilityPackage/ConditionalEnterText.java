package utilityPackage;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class ConditionalEnterText {

	static Logger ConditionalEnterText_log = Logger.getLogger(ConditionalEnterText.class);

	//Enter the text after verifying the text already present
	public static void enterTextVerify(WebElement we,String elementToVerify,String pass_values){
		try{
			String textPresent = we.getText();
			ConditionalEnterText_log.info("Text Present is :"+ textPresent);
			if(textPresent.equalsIgnoreCase(elementToVerify)){
				ConditionalEnterText_log.info("Already text present");
			}
			else{
				we.clear();
				we.sendKeys(pass_values);
			}
		}
		catch(Exception e){
			ConditionalEnterText_log.error("The exception is",e);
			Assert.fail("Quittig as exception occured");
		}
	}

	//Simply enter the text
	public static void enterText(WebElement we,String pass_values){
		try{
			we.sendKeys(pass_values);
		}
		catch(Exception e){
			ConditionalEnterText_log.error("The exception is - ",e);
			Assert.fail("Quittig as exception occured");
		}
	}
}

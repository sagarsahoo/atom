package utilityPackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelFunctions {

	static Logger ExcelFunctions_log = Logger.getLogger(ExcelFunctions.class);
	static WebDriver wd_driver;
	//Reading data from excel files
	public static String[][] readKeywordFunc(WebDriver driver,String excel_sheet,String wb_loc, String wb_name) throws IOException {

		try{
			ExcelFunctions_log.info("Reading the sheet - "+ excel_sheet + " in the workbook - "+ wb_name);
			String fileExtensionName = wb_name.substring(wb_name.indexOf("."));			
			ExcelFunctions_log.info("The file Extension is - " + fileExtensionName);
			if(fileExtensionName.equals(".xls")){
				//Creation of Excel file
				File excel = new File(wb_loc + "\\" + wb_name);
				FileInputStream fis =  new FileInputStream(excel);

				HSSFWorkbook wb = new HSSFWorkbook (fis);
				HSSFSheet ws = wb.getSheet(excel_sheet);
				FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
				//Counting Number of rows and columns
				int rowNum = ws.getLastRowNum() + 1;
				int colNum = ws.getRow(0).getLastCellNum();
				String[][] data = new String[rowNum][colNum];

				//keeping the excel values in an 2D variable
				for (int i=0 ;i < rowNum ; i++){
					HSSFRow row = ws.getRow(i);
					for(int j=0 ; j < colNum ; j++){
						HSSFCell cell = row.getCell(j);
						//HSSFCell cell=row.getCell(j, org.apache.poi.ss.usermodel.Row.CREATE_NULL_AS_BLANK );
						switch (evaluator.evaluateInCell(cell).getCellType())
						{
						case Cell.CELL_TYPE_NUMERIC:
							if (DateUtil.isCellDateFormatted(cell)) 
							{
								Date date = cell.getDateCellValue();
								DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
								String value = df.format(date);
								data[i][j] = value.trim();
								//								ExcelFunctions_log.info(value);
							} else{
								String value = cellToString.cellTostring_HSSF(cell);
								data[i][j] = value.trim();
								//								ExcelFunctions_log.info(value);
							}
							break;
						case Cell.CELL_TYPE_STRING:
							String value = cellToString.cellTostring_HSSF(cell);
							data[i][j] = value.trim();
							//							ExcelFunctions_log.info(value);
							break;
						case Cell.CELL_TYPE_FORMULA:
							ExcelFunctions_log.info("Its a formula");
							break;
						case Cell.CELL_TYPE_BLANK:
							//if(!cell.getBooleanCellValue()){
							ExcelFunctions_log.error("EMPTY CELL in --> ("+cell.getRowIndex()+","+cell.getColumnIndex()+") in the sheet name '"+cell.getSheet().getSheetName()+"'"+" of "+wb_name);
							//	                        	   }
							throw new RuntimeException("UNSUPPORTED CELL TYPE");
						}
					}	
					//					ExcelFunctions_log.info("\n");	
				}

				ExcelFunctions_log.info("Read the file !!!!!!!!!!!");
				return data;
			}else{

				if(fileExtensionName.equals(".xlsx")){

					//Creation of Excel file
					File excel = new File(wb_loc + "\\" + wb_name);
					FileInputStream fis =  new FileInputStream(excel);

					XSSFWorkbook wb = new XSSFWorkbook (fis);
					XSSFSheet ws = wb.getSheet(excel_sheet);

					//Counting Number of rows and columns
					int rowNum = ws.getLastRowNum() + 1;
					int colNum = ws.getRow(0).getLastCellNum();
					String[][] data = new String[rowNum][colNum];


					//keeping the excel values in an 2D variable
					for (int i=0 ;i < rowNum ; i++){
						XSSFRow row = ws.getRow(i);
						for(int j=0 ; j < colNum ; j++){
							XSSFCell cell = row.getCell(j);	
							cellToString cs = new cellToString();
							String value = cellToString.cellTostring_XSSF(cell);
							data[i][j] = value;
							//System.out.print(value + "\t");
						}	
						//ExcelFunctions_log.info();	
					}
					return data;
				}
			}
		}catch(Exception e){
			ExcelFunctions_log.error("Exception in read excel - ",e);
			Assert.fail("Failure as exception occured");
		}
		return null;
	}


	//Search result in an excel file
	public static boolean SearchResult(WebDriver driver,String excel_sheet,String wb_loc, String wb_name,String searchPattern){
		boolean result = false;
		int match_cnt=0;
		try{

			String[][] excelData = ExcelFunctions.readKeywordFunc(driver, excel_sheet, wb_loc, wb_name);
			int[] countRowCol = ExcelFunctions.getRowColumn(excel_sheet, wb_loc, wb_name);
			for(int row_cnt=1;row_cnt<countRowCol[0];row_cnt++){
				for(int col_cnt=0;col_cnt<countRowCol[1];col_cnt++){
					if(excelData[row_cnt][col_cnt].equals(searchPattern)){
						match_cnt++;
						result=true;
						break;
					}
				}if(match_cnt>0){
					break;
				}
			}

			return result;
		}
		catch(Exception e){
			ExcelFunctions_log.error("Exception in search pattern in  excel - ",e);
			return false;
		}
	}


	//Writing the STATUS into excel file for PASS/FAIL
	public static void writeToExcel(String filePath,String fileName,String sheetName,int row_no,boolean result) throws IOException{
		try{
			String statusResult="";
			if(result==true){
				statusResult="PASS";
			}else{
				statusResult="FAIL";
			}
			File file =    new File(filePath+"\\"+fileName);	   //Create a object of File class to open xlsx file     	       			
			FileInputStream inputStream = new FileInputStream(file); //Create an object of FileInputStream class to read excel file 
			Workbook wb = new HSSFWorkbook(inputStream);			
			if(wb.getSheet(sheetName)==null){ //creating sheet if not present
				ExcelFunctions_log.info("Sheet not present");
				wb.createSheet(sheetName);
			}			   
			Sheet sheet = wb.getSheet(sheetName); //Read excel sheet by sheet name 
			int columnCount = sheet.getRow(row_no).getLastCellNum()-1;
			ExcelFunctions_log.info("COLUMN -"+ columnCount);
			ExcelFunctions_log.info("ROW -"+row_no);
			Row row = sheet.getRow(row_no);
			Cell cell = row.getCell(columnCount);
			if (statusResult.equalsIgnoreCase("PASS")){
				cell.setCellValue("PASS");
			}
			else{
				cell.setCellValue("FAIL");

			}			
			inputStream.close(); //Close input stream			
			FileOutputStream outputStream = new FileOutputStream(file); //Create an object of FileOutputStream class to create write data in excel file 			
			wb.write(outputStream); //write data in the excel file			
			outputStream.close();//close output stream
		}
		catch(Exception e){

			ExcelFunctions_log.error("The exception in write to excel is :",e);
			Assert.fail("Failure as Exception occured");
		}
	}


	//Writing to a cell in excel by searching a flag value
	public static void writeCellByCondition(String filePath,String fileName,String sheetName,int row_no,int columnCount,String msg,String searchFlag) throws IOException{

		try{
			File file = new File(filePath+"\\"+fileName);	        	       	 
			FileInputStream inputStream = new FileInputStream(file);	 
			Workbook wb = null;	 
			wb = new HSSFWorkbook(inputStream); 	 
			Sheet sheet = wb.getSheet(sheetName);
			if(row_no == sheet.getLastRowNum()){
				if(searchFlag=="false"){
					ExcelFunctions_log.info("inside if condition");	        	
					Row row = sheet.createRow(row_no+1);
					row_no=sheet.getLastRowNum();
				}		        	
			}

			Row row = sheet.getRow(row_no);	        	    
			Cell cell = row.createCell(columnCount);	    	
			cell.setCellValue(msg);
			inputStream.close();	 
			FileOutputStream outputStream = new FileOutputStream(file);	 
			wb.write(outputStream);	 
			outputStream.close();
		}
		catch(Exception e){
			ExcelFunctions_log.error("Exception is :",e);
			Assert.fail("Failure as exception occured");
		}
	}


	//Get cell position
	public static int getColumnPosition(String filePath,String fileName,String sheetName,String columnName){

		try{

			ExcelFunctions_log.info("Inside the function getCellPosition");

			File file = new File(filePath+"\\"+fileName);
			FileInputStream inputStream = new FileInputStream(file);	 
			Workbook wb = new HSSFWorkbook(inputStream); 	 
			Sheet sheet = wb.getSheet(sheetName);

			int row_num=0;
			int colNum = sheet.getRow(0).getLastCellNum();
			ExcelFunctions_log.info("Total number of Columns - "+colNum);

			HSSFRow row = (HSSFRow) sheet.getRow(row_num);
			for(int j=0 ; j < colNum ; j++){
				HSSFCell cell = row.getCell(j);
				cellToString cs = new cellToString();
				String value = cs.cellTostring_HSSF(cell);
				//ExcelFunctions_log.info("value is - "+value);
				if(value.equalsIgnoreCase(columnName)){
					ExcelFunctions_log.info("Column - "+value + " found" + " in "+ "columnNo -"+ j );
					return j;
				}
			}
			return 0;
		}
		catch(Exception e){
			ExcelFunctions_log.error("Exception in getCellPosition is - ",e);
			Assert.fail("Failure as exception occured");
		}
		return 0;
	}



	public static int[] getRowColumn(String excel_sheet,String wb_loc, String wb_name) throws IOException{

		try{
			ExcelFunctions_log.info("Inside method to find Row and column count");

			int[] rc ={'0','0'};
			//Creation of Excel file
			File excel = new File(wb_loc + "\\" + wb_name);
			FileInputStream fis =  new FileInputStream(excel);

			HSSFWorkbook wb = new HSSFWorkbook (fis);
			HSSFSheet ws = wb.getSheet(excel_sheet);

			//Counting Number of rows and columns
			int rowNum = ws.getLastRowNum() + 1;
			int colNum = ws.getRow(0).getLastCellNum();

			rc[0] = rowNum;
			rc[1] = colNum;
			return rc;
		}
		catch(Exception e){
			ExcelFunctions_log.error("The Exception is - ",e);
			Assert.fail("Failure as exception occured");
			return null;
		}	

	}


	//Getting row as per cell value to insert value from checkpoint
	public static int getRow(String sheet_name,String wb_loc,String ScriptingFile,String[][] ScriptingData,String columnName,String columnValue){
		try{

			int testRow=0;
			int RESULTcolumn = ExcelFunctions.getColumnPosition(wb_loc, ScriptingFile, sheet_name, columnName);
			ExcelFunctions_log.info("The column found is - "+ RESULTcolumn);


			//Code for selecting the test data row
			for(int row_num=1;;row_num++){

				if(ScriptingData[row_num][RESULTcolumn].equalsIgnoreCase(columnValue)){
					ExcelFunctions_log.info("Destination row is - "+ row_num);
					testRow=row_num;
					return testRow;
				}
			}

		}
		catch(Exception e){
			ExcelFunctions_log.error("Exception occured in getEffectiveRow - ",e);
			Assert.fail("Failure as exception occured");
			return 0;
		}

	}




	//Writing the output of the test case in to the input sheet of different test case
	public static void storeValue(List<String> valueList,String valueToWrite,String testStepColumnName){
		try{
			String value=null;
			//Getting value to write
			if(valueToWrite.contains("UIresult(")){
				String index = valueToWrite.substring(valueToWrite.indexOf("("),valueToWrite.indexOf(")"));
				if(index.length()>0){
					value = valueList.get(Integer.parseInt(index));
				}else{
					value = valueList.get(valueList.size()-1);
				}
			}
			//int row_no = UserInputDataSheet.getEffectiveRow( sheet_name, wb_loc, UserInputFileName, UserInputData);
			int row_no = CommonMethod.testDataRow;
			int col_no = ExcelFunctions.getColumnPosition(CommonMethod.wb_loc, CommonMethod.UserInputFile, CommonMethod.scriptingSheet_name, testStepColumnName);

			ExcelFunctions_log.info("Row number is - "+ row_no);
			ExcelFunctions_log.info("Column no is - "+ col_no);

			File file = new File(CommonMethod.wb_loc+"\\"+CommonMethod.UserInputFile);	        	       	 
			FileInputStream inputStream = new FileInputStream(file);	 
			Workbook wb = null;	 
			wb = new HSSFWorkbook(inputStream); 	 
			Sheet sheet = wb.getSheet(CommonMethod.scriptingSheet_name);

			Row row = sheet.getRow(row_no);
			Cell cell = row.getCell(col_no); 
			cell.setCellValue(value);

			inputStream.close();	 
			FileOutputStream outputStream = new FileOutputStream(file);	 
			wb.write(outputStream);	 
			outputStream.close();
			
			CommonMethod.UserInputData = ExcelFunctions.readKeywordFunc(ActionPerformer.driver, CommonMethod.scriptingSheet_name, CommonMethod.wb_loc, CommonMethod.wb_name);
		}
		catch(Exception e){
			ExcelFunctions_log.error("The exception in Store value from DB is - ",e);
			Assert.fail("Failure as exception occured");
		}

	}


	//Create 2D variable which will hold the test steps of another test case
	public static String[][] getTestSteps(String testcase_sheet,String wb_loc,String ScriptingFileName,String testStepRange){
		try{

			String EntireData[][] = readKeywordFunc(wd_driver, testcase_sheet,wb_loc,ScriptingFileName);
			//String selectiveData[][] = readSelectiveSteps(EntireData,startTestStep,endTestStep);
			String selectiveDataArray[][] = readSelectiveSteps(EntireData,testStepRange);

			return selectiveDataArray;
		}
		catch(Exception e){
			ExcelFunctions_log.error("Exception occured in getting variable for different test steps - ",e);
			Assert.fail("Failure as exception occured");
			return null;
		}
	}


	//Read test steps from Excel file
	private static String[][] readSelectiveSteps(String[][] dataVariable,String TestStepRange){
		try{


			String stepRange[] = SplitString.stringSplit(TestStepRange);
			String startTestStep = stepRange[0];
			String endTestStep = stepRange[1];


			//Getting the starting row 
			int startRow = Integer.parseInt(startTestStep.substring(3));
			ExcelFunctions_log.info("Starting row - "+ startRow);
			int endRow = Integer.parseInt(endTestStep.substring(3));
			ExcelFunctions_log.info("End row - "+endRow );

			//Getting number of rows
			int rows_count=0;
			for(int row=1;row<dataVariable.length;row++){			
				int testStep = Integer.parseInt((dataVariable[row][3]).substring(3));
				ExcelFunctions_log.info("testStep - "+ testStep);
				if(dataVariable[row][2].equalsIgnoreCase("Y") && testStep >=startRow && testStep<=endRow){
					rows_count++;
				}
			}

			ExcelFunctions_log.info("Number of rows - "+ rows_count);
			int noOfColumns = dataVariable[0].length;

			String[][] selectiveData = new String[rows_count][noOfColumns];

			int row_iterator=0;
			for(int row=1;row<dataVariable.length;row++){			
				int testStep = Integer.parseInt((dataVariable[row][3]).substring(3));			
				if(dataVariable[row][2].equalsIgnoreCase("Y") && testStep >=startRow && testStep<=endRow){
					for(int col_counter=0;col_counter<noOfColumns;col_counter++){
						selectiveData[row_iterator][col_counter] = dataVariable[row][col_counter];
						System.out.print(selectiveData[row_iterator][col_counter]);
						System.out.print(",");
					}
					row_iterator++;
					ExcelFunctions_log.info("\n");
				}
			}


			return selectiveData;


		}
		catch(Exception e){
			ExcelFunctions_log.error("Exception occured in readSelective steps is - ",e);
			Assert.fail("Failure as exception occured");
			return null;
		}
	}




	//Updating the scripting data with the value inserted in the same sheet
	private static String[][] updateScriptingData(String[][] ScriptingData,int rowNo,int columnNo,String value){
		try{

			ScriptingData[rowNo][columnNo]=value;

			return ScriptingData;
		}
		catch(Exception e){
			ExcelFunctions_log.error("The Exception in updating Excel data is - ",e);
			Assert.fail("Failure as exception occured");
			return null;
		}
	}


	//Insert record in Excel file
	public static void insertRow(File ExecutionMappingFile,String SheetName, int lastRow, String[] insertValues){
		try{

			File mappingFile = ExecutionMappingFile;
			FileInputStream inputStream = new FileInputStream(mappingFile);
			Workbook wb = new HSSFWorkbook(inputStream);
			Sheet sh = wb.getSheet(SheetName);

			sh.createRow(lastRow);
			Row row = sh.getRow(lastRow);

			int no_valuesToInsert = insertValues.length;
			for(int i=0;i<no_valuesToInsert;i++){
				Cell cell = row.createCell(i);
				cell.setCellValue(insertValues[i]);
			}

			inputStream.close();
			FileOutputStream outputStream = new FileOutputStream(mappingFile);	 
			wb.write(outputStream);	 
			outputStream.close();

		}
		catch(Exception e){
			ExcelFunctions_log.error("The Exception in inserting row in Excel file is - ",e);
		}
	}


}

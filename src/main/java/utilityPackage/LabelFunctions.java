package utilityPackage;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class LabelFunctions {
	private static  Logger log = Logger.getLogger(LabelFunctions.class);
  @Test
  public static void EnterByLabel(String labelName,String FieldType,String value) {
	  
	  try{
		  String labelxpath=null;
		  
		  if(FieldType.contains(")")){
			  log.info("Matched word");
			  labelxpath =  " (((//*[contains(text(),'"+ labelName+ "')]//ancestor::td)[2]/following-sibling::td)[1]";
		  }
		  else{
			  labelxpath = "(//*[text()='"+ labelName+ "']//ancestor::td)[2]/following-sibling::td[1]";
//			  labelxpath = "((//*[contains(text(),'"+ labelName+ "')]//ancestor::td)[2]/following-sibling::td)[1]";
		  }

		  log.info("label xpath - "+ labelxpath);
		  
		  String correspondingElemet = labelxpath+ "//"+ FieldType;
		  
		  log.info("correspondingElemet - "+ correspondingElemet);
		  WebElementActions.GetWebElement("xpath", correspondingElemet).sendKeys(value);
	  }
	  catch(Exception e){
		  log.info("Exception is - "+ e);
	  }
  }
}

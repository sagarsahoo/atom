package utilityPackage;

import org.testng.ITestResult;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReporting {
	
	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	ExtentTest test;
  @Test
  public void startReporting(String testName,String description ) {
	  test = extent.createTest(testName, description);
  }
  
  public void showResult(Boolean stepResult,String description){

		if(stepResult == false)
		{
			test.log(Status.FAIL, MarkupHelper.createLabel(""+" Automation Test Suite  FAILED due to below issues:", ExtentColor.RED));
			test.fail(description);
		}
		else if(stepResult == true)
		{
			test.log(Status.PASS, MarkupHelper.createLabel(""+" Automation Test Suite Executed Successfully", ExtentColor.GREEN));
		}
		else
		{
			test.log(Status.SKIP, MarkupHelper.createLabel(""+" Automation Test Suite SKIPPED", ExtentColor.ORANGE));
			test.skip(description);
		}
	  
  }
}

package utilityPackage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import selfitGui.PageScanner;


public class ActionPerformer extends CommonMethod {

	//public static WebDriver driver;
	public static String scrShotTestCase;
	public static WebDriverWait wait;
	static WebElement we;
	static boolean Verify_list=false;
	static boolean allResults = false;
	static ArrayList<Boolean> resultList =  new ArrayList<Boolean>();
	static Set<String> array_windows = null;
	static List<String> UIresult = new ArrayList<String>();
	static List<Double> actualList = new ArrayList<Double>();
	static List<String> xmlValues = new ArrayList<String>();
	static List<String> jsonValues = new ArrayList<String>();
	static List<String> ObjectValuesList = new ArrayList<String>();
	static List<String> TargetObjectValuesList = new ArrayList<String>();
	static HashMap<String,String> actualValues = new HashMap<String,String>();
	static Double CustomizeResult;
	public static String test_step;
	static String dbQuery;
	static List<String>  dbResult = new ArrayList<String>();
	static int dbResultIndex;
	static Actions act;
	static String test_step_no;
	static List<Integer> calculatedResult = new ArrayList<Integer>();
	public static String tc_id,test_case,exec_flag,refKeyword,ele_type,element_identifiedBy,element_identifier,pass_values,target_identifiedBy,target_identifier,cellCategory;
	public static int sl_no=0;

	static  Logger log = Logger.getLogger(ActionPerformer.class);

	@SuppressWarnings("finally")
	public static WebDriver performAction(String[][] excel_data, String test_case_name,String excel_sheet,String[][] UserInputData,int testDataRow,String cellcategory) throws InterruptedException, IOException{
		try{

			log.info("Action Performer CLass");
			log.info("Inside performAction method");

			//Assigning value to test case variable for screenshot filename
			scrShotTestCase = test_case_name;

			//Iterating the test steps
			for(int row_no = 1 ; row_no < excel_data.length ; row_no ++){
				assignExcelValues(row_no,UIresult,dbResult); //Assigning values to the variables

				//For matching Test case name
				if (test_case.equalsIgnoreCase(test_case_name)){					
					if(exec_flag.equalsIgnoreCase("Y") && cellCategory.equalsIgnoreCase(cellcategory)){ //For the test case execution status is Y
						log.info("------------------------------");
						log.info(cellcategory + " : " + "Test STEP : "+  test_step_no + " :Test Step - "+ test_step);
						log.info("test_case_name - "+ test_case_name + " exec_flag - "+ exec_flag +" cellCategory- "+ cellCategory );
						log.info("refkeyword - "+ refKeyword);
						
						

						switch(refKeyword){

						//To open the browser
						case "open_browser":
							//Open Browser 
							driver = OpenBrowser.open_browser(pass_values,target_identifiedBy);
							act = new Actions(driver);
							break;

							//To pass the url into the browser
						case "get_url":
							ProvideURL.baseUrl(driver,pass_values);
							break;

						case "getTitle" :
							log.info("URL received is - "+ driver.getCurrentUrl());
							log.info("Title of page is - "+ driver.getTitle());
							UIresult.add(driver.getTitle());
							log.info("Page title stored in the index - " + (UIresult.size()-1));
							break;


						case "wait" :
							Thread.sleep(1000);
							break;

						case "pageRefresh":
							driver.navigate().refresh();
							break;

						case "navigate-back":
							driver.navigate().back();
							break;

						case "navigate-forward":
							driver.navigate().forward();
							break;

						case "find_frames" :							
							List<WebElement> ele = driver.findElements(By.tagName("iframe"));
							log.info("Number of frames in a page :" + ele.size());
							log.info("Number of frames in a page :" + ele.size());
							for(WebElement el : ele){
								//Returns the Id of a frame.
								log.info("Frame Id :" + el.getAttribute("id"));
								log.info("Frame Id :" + el.getAttribute("id"));
								//Returns the Name of a frame.
								log.info("Frame name :" + el.getAttribute("name"));
							}

							//log.info(driver.switchTo().activeElement());
							break;

						case "enter_text":
							WebElementActions.enterText(element_identifiedBy, element_identifier, pass_values);
							break;
							
						case "EnterWithoutClear":
							WebElementActions.enterTextWithoutClear(element_identifiedBy, element_identifier, pass_values);
							break;

						case "enter-text_conditional":
							WebElementActions.enterTextIfNotPresent(element_identifiedBy, element_identifier, pass_values, target_identifier);
							break;

						case "text_clear":
							WebElementActions.clearText(element_identifiedBy, element_identifier);
							break;

						case "click":
							WebElementActions.Click(element_identifiedBy, element_identifier);
							act.click().build().perform();
							break;

						case "NormalClick":
							WebElementActions.NormalClick(element_identifiedBy, element_identifier);
							break;

						case "clickAndProceed":
							WebElementActions.ClickAndProceed(element_identifiedBy, element_identifier);
							break;

						case "DoubleClick":
							WebElementActions.doubleClick(element_identifiedBy, element_identifier);
							//act.moveToElement(WebElementActions.GetWebElement(element_identifiedBy,element_identifier)).doubleClick().build().perform();
							break;

						case "nav_section":							
							//Navigating to a particular section
							MouseActions.element_section(element_identifiedBy,element_identifier,pass_values,target_identifiedBy,target_identifier);
							break;

						case "text_verify":
							Verify_list= VerifyTheText.verifyExact(UIresult);
							resultList.add(Verify_list);
							break;
							
						case "verifyTwoValue":
							Verify_list = VerifyTheText.verifyTwoValue(UIresult);
							resultList.add(Verify_list);
							break;
							
						case "verifyIgnoreCase":
							Verify_list= VerifyTheText.verifyIgnoreCase(UIresult);
							resultList.add(Verify_list);
							break;


						case "verify_contains":
							Verify_list = VerifyTheText.verifyContains(UIresult);
							resultList.add(Verify_list);
							break;

						case "VerifyRegularExpression":
							Verify_list = VerifyTheText.regularExpressionVerify(UIresult);
							resultList.add(Verify_list);
							break;


						case "build_query":
							dbQuery = DataBaseFunctions.sqlQueryString(UIresult);						
							break;
							
						case "getDBresult":
							dbResult.add(DataBaseFunctions.getResult(dbQuery, pass_values));
							break;
							
						case "getDBresultArray":
							dbResult.addAll(DataBaseFunctions.getResultArray(dbQuery, pass_values));
							break;

						case "VerifyWithDB":
							//dbResult.add(DataBaseFunctions.getResult(dbQuery));
							dbResultIndex =Integer.parseInt(pass_values.substring(8, 9));
							Verify_list =VerifyTheText.DBresultVerify();
							/*
							 dbResultIndex =Integer.parseInt(pass_values.substring(8, 9));
							 if(element_identifiedBy.equalsIgnoreCase("UIresult")){

								Verify_list = VerifyTheText.funcVerifyText(element_identifiedBy,UIresult.get(UIResult_getIndex),dbResult.get(dbResultIndex),excel_sheet,target_identifier,objRepositoryFile,UserInputData);
							}
							else{
								Verify_list = VerifyTheText.funcVerifyText(element_identifiedBy,element_identifier,dbResult.get(dbResultIndex),excel_sheet,target_identifier,objRepositoryFile,UserInputData);
							}*/	
							break;

						case "writeValueFromDB":
							//pass_values - query to fetch data to be compared/verify
							//element_identifiedBy - UserInput Sheet where data from DB to be stored
							//element_identifier - UserInput File Name
							//target_identifiedBy - Column of UserInputData Sheet under which value to be stored

							//log.info("Storing value from DB to excel file - ");
							//dbResultIndex =Integer.parseInt(pass_values.substring(8, 9));							
							//String variableFromDB = dbResult.get(dbResultIndex);
							//log.info("variableFromDB value is - "+ variableFromDB);
							ExcelFunctions.storeValue(dbResult,pass_values,target_identifiedBy);
							//UserInputData = ExcelFunctions.readKeywordFunc(driver,excel_sheet, wb_loc, RepositoryObject.IdentifierRepo(objRepositoryFile,"UserInputFile"));
							break;


						case "alertTextAssert" :
							Verify_list = VerifyTheText.assertAlertBoxText();
							break;

						case "alertBox":
							//String ParentWindow = driver.getWindowHandle();
							AlertBoxHandler.alertBoxHandling(driver, pass_values);
							break;

						case "select_Items":
							if(pass_values.equalsIgnoreCase("UIresult")){
								pass_values = UIresult.get(0);
							}
							SelectItems.itemsToSelect(element_identifiedBy,element_identifier,pass_values,target_identifiedBy,target_identifier);
							break;

						case "Conditional_select_Items":
							SelectItems.selecOtherValue(target_identifiedBy, target_identifier);
							break;

						case "Click_cor_select_item" :
							//SelectItems.clickCorrepondingSelectItem(driver, element_identifiedBy, element_identifier, pass_values, target_identifiedBy, target_identifier);
							//ebTable.ClickOnCorrSearch(element_identifiedBy, element_identifier, Integer.parseInt(pass_values), target_identifiedBy,Integer.parseInt(target_identifier));
							if(!WebTable.ClickOnCorrSearch(element_identifiedBy, element_identifier, Integer.parseInt(pass_values), target_identifiedBy,Integer.parseInt(target_identifier))){

								log.info("Returned false");
								throw new RuntimeException("FAIL");
							}
							break;

						case "get_cor_rowIndex":

							UIresult.add(String.valueOf(WebTable.GetRowIndex(element_identifiedBy, element_identifier, Integer.parseInt(target_identifiedBy), pass_values)));
							log.info("selected row index is "+ UIresult.get(UIresult.size()-1));

							break;

						case "get_textByRowCol":
							UIresult.add(WebTable.GetText(element_identifiedBy, element_identifier, Integer.parseInt(pass_values), Integer.parseInt(target_identifiedBy)));
							break;

						case "deselect_Items":
							SelectItems.itemsToDeselect(element_identifiedBy,element_identifier,pass_values,target_identifiedBy,target_identifier);
							break;

						case "check_Box" :
							SelectItems.selectCheckBox(element_identifiedBy,element_identifier,pass_values);
							break;

						case "new_window" :
							Thread.sleep(2000);
							array_windows = WindowClass.newWindow(driver);
							break;

						case "switch_window" :
							WindowClass.switchWindow(driver, array_windows);
							break;

						case "switch_frame" :
							WindowClass.switchFrame(driver, element_identifiedBy,element_identifier);
							break;

						case "switch_frame_default" :
							WindowClass.switchFrame_default(driver);
							break;

						case "mouse_sliding" :
							MouseActions.mouseSLider(driver, element_identifiedBy, element_identifier);
							break;

						case "raedCSV" :
							CSVFunctions.readCSV(element_identifier,pass_values);
							break;

						case "writeCSV" :
							CSVFunctions.writeCSV(element_identifier, pass_values);
							break;

						case "appendCSV" :
							CSVFunctions.appendCSV(element_identifier, pass_values);
							break;

						case "validateWordCSV" :
							Verify_list = CSVFunctions.validateWord(element_identifier, pass_values);
							ExcelFunctions.writeToExcel(wb_loc, wb_name, excel_sheet, row_no, Verify_list);
							break;

						case "validateRecordCSV":
							Verify_list = CSVFunctions.validateRecord(element_identifier, pass_values);
							ExcelFunctions.writeToExcel(wb_loc, wb_name, excel_sheet, row_no, Verify_list);
							break;

						case "XML_listTags" :
							//element_identifier - represent the fileType(file/url)
							//pass_values - represent the filepath upto fileName/url value
							XMLParserFunctions.listXmlValuesByTag(element_identifier, pass_values);
							break;

						case "XML_findValuesByTagName" :
							//element_identifier - represent the fileType(file/url)
							//element_identifiedBy -- represent the tagName
							//pass_values - represent the filepath upto fileName/url value
							UIresult.add(XMLParserFunctions.findXmlValuesByTag(element_identifier, pass_values, element_identifiedBy,target_identifiedBy));
							break;

						case "XML_findValuesByAttribute" :
							//element_identifier - represent the fileType(file/url)
							//element_identifiedBy -- represent the tagName
							//pass_values - represent the filepath upto fileName/url value
							//target_identifiedBy - represent the attribute name
							Verify_list=XMLParserFunctions.findXMLValueByAttribute(element_identifier, pass_values, element_identifiedBy, target_identifiedBy);
							break;

						case "XML_getValueByTagName":
							xmlValues.addAll(XMLParserFunctions.getValueByTagName(element_identifier, pass_values, element_identifiedBy));
							for(int i=0;i<xmlValues.size();i++){
								log.info("XML VALUES - "+ xmlValues.get(i));
							}
							break;

						case "JSON_getValueByTagName":
							jsonValues.addAll(JSONObjects.getJSONvalueByTagName(element_identifier, pass_values, element_identifiedBy));
							for(int i=0;i<jsonValues.size();i++){
								log.info("JSON VALUES - "+ jsonValues.get(i));
							}
							break;


						case "JSON_findTag":
							/*Verify_list = JSONObjects.findJSONtag(element_identifier, pass_values, element_identifiedBy);
							CheckPoint.windowsScreenShot(Verify_list);
							ExcelFunctions.writeToExcel(wb_loc, wb_name, excel_sheet, row_no, Verify_list);								
							if(TestCaseConfiguration.getPresenceOfUserInputSheet().equalsIgnoreCase("Y") && target_identifier.equalsIgnoreCase("assert")){
								log.info("Mapping the result into the user input sheet");
								String UserInputFileName = RepositoryObject.IdentifierRepo(objRepositoryFile,"UserInputFile");
								UserInputDataSheet.setValues(wb_loc, UserInputFileName, excel_sheet);
								UserInputDataSheet.getEffectiveRow(excel_sheet, wb_loc, UserInputFileName, UserInputData);
								if(Verify_list.equalsIgnoreCase("PASS")){
									UserInputDataSheet.mapResult(Verify_list);
								}
							}*/
							break;

						case "JSON_finfTagValue":
							/*Verify_list = JSONObjects.findJSONElementValue(element_identifier, pass_values, element_identifiedBy, target_identifiedBy);
							CheckPoint.windowsScreenShot(Verify_list);
							ExcelFunctions.writeToExcel(wb_loc, wb_name, excel_sheet, row_no, Verify_list);								
							if(TestCaseConfiguration.getPresenceOfUserInputSheet().equalsIgnoreCase("Y") && target_identifier.equalsIgnoreCase("assert")){
								log.info("Mapping the result into the user input sheet");
								String UserInputFileName = RepositoryObject.IdentifierRepo(objRepositoryFile,"UserInputFile");
								UserInputDataSheet.setValues(wb_loc, UserInputFileName, excel_sheet);
								UserInputDataSheet.getEffectiveRow(excel_sheet, wb_loc, UserInputFileName, UserInputData);
								if(Verify_list.equalsIgnoreCase("PASS")){
									UserInputDataSheet.mapResult(Verify_list);
								}
							}*/
							break;

						case "javascript" :
							JavaFunctions.executeJavaScript(driver,pass_values);
							break;
							
						case "javascriptOnObject" :
							JavaFunctions.executeJavaScriptOnObject(driver,pass_values, element_identifiedBy,element_identifier);
							break;

						case "scrollDown" :
							JavascriptExecutor scrlDwn = (JavascriptExecutor)driver;
							scrlDwn.executeScript("window.scrollBy(0,250)", "");
							//scrlDwn.executeScript("scroll(0, 250);");
							break;

						case "scrollUp" :
							JavascriptExecutor scrlUp = (JavascriptExecutor)driver;
							scrlUp.executeScript("window.scrollBy(250,0)", "");
							break;


						case "TestCaseAsStep" :

							//target_identifiedBy - test sheet of the called test case
							//pass_values - test case name
							//element_identifiedBy - Which segment the target test steps are 
							//element_identifier - ExecutionMapping Sheet name

							if(target_identifiedBy.equalsIgnoreCase("NA") && element_identifier.equalsIgnoreCase("NA")){
								target_identifiedBy = pass_values; //Sheet name is equal to the TC name
								element_identifier = CommonMethod.applicationFolder+"_suite";
							}
							String[][] excel_data_calledTestCase= ExcelFunctions.readKeywordFunc(driver,target_identifiedBy,wb_loc,wb_name);
							String called_UserInputFile = RepositoryObject.IdentifierRepo(objRepositoryFile, "UserInputFile");
							log.info("called_UserInputFIle : "+ called_UserInputFile);

							String [] testConfigurationData = TestCaseConfiguration.getTestCaseInfo(pass_values, element_identifier);
							String isUserInputSheetPresent = testConfigurationData[3];
							log.info("IS User INPUT File present :"+ isUserInputSheetPresent);

							String[][] calledUserInputData = null; 
							int calledTestDataRow = 0;

							if(isUserInputSheetPresent.equalsIgnoreCase("Y")){
								calledUserInputData = ExcelFunctions.readKeywordFunc(driver,target_identifiedBy,wb_loc,called_UserInputFile);
								calledTestDataRow = UserInputDataSheet.getEffectiveRow(target_identifiedBy,wb_loc,called_UserInputFile,calledUserInputData);
							}else{			
								calledUserInputData = null; 
								calledTestDataRow = 0;
							}						
							driver = performAction(excel_data_calledTestCase,pass_values,target_identifiedBy,calledUserInputData,calledTestDataRow,element_identifiedBy);	
							//ExcelFunctions.writeToExcel(wb_loc, called_UserInputFile, target_identifiedBy, calledTestDataRow, "used");
							break;
							
						case "callTestCase":
							String calledTestCase = CallTestCase.callTestMethod(pass_values, element_identifiedBy);
							break;

						case "TestStepRange":
							//target_identifiedBy -  scripting test sheet of the called test case
							//target_identifier - Test Step range separated by comma
							//element_identifiedBy - Which segment the target test steps are 
							//element_identifier - ExecutionMapping Sheet name
							//pass_values - test case name

							if(target_identifiedBy.equalsIgnoreCase("NA") && element_identifier.equalsIgnoreCase("NA")){
								target_identifiedBy = pass_values; //Sheet name is equal to the TC name
								element_identifier = CommonMethod.applicationFolder+"_suite";
							}

							String[][] selectiveTestSteps = ExcelFunctions.getTestSteps( target_identifiedBy, wb_loc, wb_name, target_identifier);
							String selectiveUserInputFile = RepositoryObject.IdentifierRepo(objRepositoryFile, "UserInputFile");
							String [] testConfigurationDataStepRange = TestCaseConfiguration.getTestCaseInfo(pass_values,element_identifier);
							String isUserInputSheetPresentStepRange = testConfigurationDataStepRange[3];
							String[][] selectiveUserInputData=null;
							int selectiveTestDataRow=0;
							if(isUserInputSheetPresentStepRange.equalsIgnoreCase("Y")){
								selectiveUserInputData = ExcelFunctions.readKeywordFunc(driver,target_identifiedBy,wb_loc,selectiveUserInputFile);
								selectiveTestDataRow = UserInputDataSheet.getEffectiveRow(target_identifiedBy,wb_loc,selectiveUserInputFile,selectiveUserInputData);
							}else{
								selectiveUserInputData=null;
								selectiveTestDataRow=0;
							}
							driver = performAction(selectiveTestSteps,pass_values,target_identifiedBy,selectiveUserInputData,selectiveTestDataRow,element_identifiedBy);
							break;

						case "AutoIT_script" :
							Runtime.getRuntime().exec(pass_values);
							break;
							
						case "OwnCode" :
							String customCode = pass_values; //TODO
							break;

						case "getResult":
							UIresult.add(ElementProperty.getAttribute(element_identifiedBy, element_identifier, target_identifiedBy, target_identifier,pass_values));
							log.info("Result stored is - "+ UIresult.get(UIresult.size()-1) + " in the index - " + (UIresult.size()-1));
							break;

						case "writeValueFromUI":
							//element_identifiedBy - UserInput Sheet where data from UI to be stored
							//element_identifier - UserInput File Name
							//target_identifiedBy - Column of UserInputData Sheet under which value to be stored
							//String targetUserInputData[][] = ExcelFunctions.readKeywordFunc( driver,element_identifiedBy, wb_loc, element_identifier);
							//ExcelFunctions.storeValue(element_identifiedBy, wb_loc, element_identifier, targetUserInputData, target_identifiedBy, UIresult.get(UIResult_getIndex));
							ExcelFunctions.storeValue(UIresult,pass_values,target_identifiedBy);
							//UserInputData = ExcelFunctions.readKeywordFunc(driver, excel_sheet, wb_loc, pass_values);
							break;

						case "autoFillSelect":
							log.info("Going to select from AutoFill");
							AutoFill.selectTextInAutoComplete(element_identifiedBy, element_identifier, target_identifiedBy, target_identifier, pass_values);
							break;

						case "autoFillSearch":
							log.info("Going to search from AutoFill");
							Verify_list= AutoFill.VerifyTextInAutoComplete(element_identifiedBy, element_identifier, target_identifiedBy, target_identifier, pass_values);
							break;

						case "screenshot":
							CheckPoint.windowsScreenShot();
							break;

						case "selectDate":
							//target_identifiedBy - date format
							//pass_values - date to enter
							String dateValue = DateFormater.convertDateToString(pass_values, target_identifiedBy);
							WebElementActions.ExplicitWaitPresenceOfElement(target_identifiedBy,target_identifier,30);
							we = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
							ConditionalEnterText.enterText(we, dateValue);
							break;

						case "getSubStringByLength":
							String subStringByLength = ElementProperty.getAttribute(element_identifiedBy, element_identifier,target_identifiedBy, target_identifier,pass_values);
							log.info("subStringByLength - "+ subStringByLength);
							UIresult.add(JavaFunctions.getstrinByLength(subStringByLength, target_identifiedBy, target_identifier));
							log.info("UIresult from javaFunctions to get SubString by Length is - "+ UIresult.get(UIresult.size()-1) + "stored in - "+ (UIresult.size()-1));
							break;

						case "getSubStringByValue"	:
							String subStringByValue = ElementProperty.getAttribute(element_identifiedBy, element_identifier, target_identifiedBy, target_identifier,pass_values);
							UIresult.add(JavaFunctions.getStringByValue(subStringByValue,target_identifiedBy,target_identifier));
							log.info("UIresult from javaFunctions to get SubString by Value is - "+ UIresult.get(UIresult.size()-1)  + "stored in - "+ (UIresult.size()-1));
							break;

						case "seleniumScreenshot":
							CheckPoint.screenShot("selenium-screenshot");
							break;

						case "quit":
							BrowserClass.closeBrowser();
							break;

						case "getList":
							SelectItems.getList(element_identifiedBy, element_identifier);
							break;

						case "EnterByLabel":
							LabelFunctions.EnterByLabel(element_identifiedBy,element_identifier,pass_values);
							break;

						case "WaitForElementToBeClickable":
							WebElementActions.ExplicitWaitElementClikable(element_identifiedBy,element_identifier,30);
							break;
							
						case "waitForPageToLoad":
							WebElementActions.WaitForPageload();
							break;

						case "WaitForInvisibilityOfElement"	:
							WebElementActions.ExplicitWaitInvisibilityOfElement(element_identifiedBy, element_identifier, Integer.parseInt(pass_values));
							break;
							
						case "waitforobject" :
							WebElementActions.ExplicitWaitPresenceOfElement(element_identifiedBy, element_identifier, 30);
							break;
							
						case "WaitForFrameSwitchToIt":
							WebElementActions.WaitForFrameSwitchToIt(element_identifiedBy, element_identifier, 30);
							break;

						case "WebTableClick":
							WebTable.ClickTextProvided(element_identifiedBy, element_identifier, pass_values,target_identifiedBy);
							break;

						case "WebTableCalculation":
							actualList = WebTable.Calculation(element_identifiedBy, element_identifier, Integer.parseInt(pass_values));
							break;

						case "SearchTextInWebTable":
							//Provide the xpath of the webtable and provide the textToFind in the pass_values
							Verify_list = WebTable.SearchTextInWebTable(element_identifiedBy, element_identifier, pass_values);
							log.info("The result of search from WebTable is - "+ Verify_list);
							CheckPoint.screenShot(Verify_list);
							ExcelFunctions.writeToExcel(wb_loc, wb_name, excel_sheet, row_no, Verify_list);								
							if(TestCaseConfiguration.getPresenceOfUserInputSheet().equalsIgnoreCase("Y") && target_identifier.equalsIgnoreCase("assert")){
								log.info("Mapping the result into the user input sheet");
								String UserInputFileName = RepositoryObject.IdentifierRepo(objRepositoryFile,"UserInputFile");
								UserInputDataSheet.setValues(wb_loc, UserInputFileName, excel_sheet);
								UserInputDataSheet.getEffectiveRow(excel_sheet, wb_loc, UserInputFileName, UserInputData);
								if(Verify_list==true){
									String result="PASS";
									UserInputDataSheet.mapResult(result);
								}
							}
							break;

						case "GetCustomizedResult":
							CustomizeResult = ElementProperty.getCustomizedResultforNumber(element_identifiedBy, element_identifier, UIresult.get(ExcelValues.UIResult_getIndex), pass_values);
							//CustomizeResult = ElementProperty.getCustomizedResultforNumber(element_identifiedBy, element_identifier, UIresult.get(UIresult.size()), pass_values);
							//UIresult.add("");
							break;
							
						case "CheckForObjectAndSendMail":
							if(WebElementActions.ObjectExist(element_identifiedBy, element_identifier)){
								log.info("OBJECT IS PRESENT IN DOM");
								CheckPoint.screenShot("PASS");
							}else{
								CheckPoint.screenShot("FAIL");
								//For Email part
								if(pass_values.equalsIgnoreCase("YES")){
									driver.quit();
									EmailFunctions.SendMail(driver,target_identifiedBy,target_identifier);
								}
								log.error("OBJECT NOT SPRESENT IN DOM");
							}
							break;

						case "SelectRadioButton":
							WebElementActions.SelectRadioButton(element_identifiedBy, element_identifier);
							break;

						case "calculator":
							if(target_identifier.contains("calculatedResult")){
								int var_index=Integer.parseInt(target_identifier.substring(17, 18));
								log.info("Index of calculatedResult to be accessed - "+ var_index);
								calculatedResult.add(Calculator.getCalculatorResult(element_identifiedBy, element_identifier, pass_values, target_identifiedBy, Integer.toString(calculatedResult.get(var_index))));
							}else{
								log.info("Value/Variable has been provided - ");
								int operationResult = Calculator.getCalculatorResult(element_identifiedBy, element_identifier, pass_values, target_identifiedBy, target_identifier);
								log.info("Value of calculatedResult returned by method - " + operationResult);
								calculatedResult.add(operationResult);
							}
							log.info("Size of the CalculatedResult List - "+calculatedResult.size());
							for(int i=0;i<calculatedResult.size();i++){
								log.info("calculatedResult is - " +calculatedResult.get(i) );
							}														
							break;


						case "Calculation":
							switch(element_identifiedBy){

							case "ADD":
								UIresult.add(String.valueOf((Integer.parseInt(pass_values) + Integer.parseInt(element_identifier))));
								break;
							case "SUBTRACT":
								UIresult.add(String.valueOf((Integer.parseInt(pass_values) - Integer.parseInt(element_identifier))));
								break;
							case "DIVIDE":
								break;
							case "MULTIPLY":
								UIresult.add(String.valueOf((Integer.parseInt(pass_values) * Integer.parseInt(element_identifier))));
								break;	
							}

							break;
						case "checkObjectExists":
							if(WebElementActions.ObjectExist(element_identifiedBy, element_identifier)){
								log.info("OBJECT IS PRESENT IN DOM");
								CheckPoint.screenShot("PASS");
							}else{
								CheckPoint.screenShot("FAIL");
								driver.quit();
								Assert.fail("OBJECT NOT FOUNG");
							}
							break;
						case "WaitForWindowCount":
							final int seconds = Integer.parseInt(pass_values);
							new WebDriverWait(driver, 60).until(new ExpectedCondition<Boolean>(){
								public Boolean apply(WebDriver driver) {                        
									return (driver.getWindowHandles().size() == seconds);
								}});
							break;

						case "CheckExistingText":
							boolean condition = false;
							List<String>ColText = WebTable.GetAllTextInColumn(element_identifiedBy, element_identifier, Integer.parseInt(pass_values));
							for(String text:ColText){
								if(text.compareTo(target_identifiedBy)==0){
									condition = true;
								}
								break;
							}
							if(condition){


								//Already activity is available so select the radio button and click on the modify button and modify the existing. 
							}else{
								//Click the create new button.
							}
							break;


						case "drop_down" :
							SelectItems.SelectDropDown(driver,element_identifiedBy,element_identifier,pass_values,target_identifiedBy,target_identifier);
							break;

						case "DropDownSelectByValue":
							SelectItems.DropDown_SelectValue(element_identifiedBy, element_identifier, pass_values);
							break;

						case "DropDownSelectByIndex":
							SelectItems.DrpDwn_SelectIndex(element_identifiedBy, element_identifier, pass_values);
							break;

						case"DropDownSelectByText":
							SelectItems.DropDown_SelectText(element_identifiedBy, element_identifier, pass_values);
							break;
							
						case "DropDownSelectRandom":
							SelectItems.DrpDwn_SelectRandom(element_identifiedBy, element_identifier);
							break;

						case "CheckForMapping"://TODO
							if(WebElementActions.ObjectExist(element_identifiedBy, element_identifier)){
								UIresult.add(WebTable.GetText(target_identifiedBy, target_identifier, 1, 2));
							}else{
								log.fatal("Mapping not available!!So stopping test run");
								Assert.fail();
							}
							break;

						case "WbTableClick":
							WebTable.Click(element_identifiedBy, element_identifier, Integer.parseInt(pass_values),  Integer.parseInt(target_identifiedBy));
							break;

						case "WebTableGetText":
							UIresult.add(WebTable.GetText(element_identifiedBy, element_identifier, Integer.parseInt(target_identifiedBy), Integer.parseInt(target_identifier)));
							break;


						case"ClickIfEnabled":

							WebElement ActionObject = WebElementActions.GetWebElement(element_identifiedBy, element_identifier);
							if(ActionObject.isEnabled()){
								ActionObject.click();
							}
							break;
						case "RunJavaScript":
							WebElementActions.RunJavaScript(pass_values);
							break;
							
						case "DropDownGetSelectedText":
							UIresult.add(SelectItems.DropDown_GetSelectedText(element_identifiedBy, element_identifier));
							break;

						case "DropDownCompareSelectedText":
							UIresult.add(SelectItems.DropDown_GetSelectedText(element_identifiedBy, element_identifier));
							break;

						case "KeyboardKey":
							KeyboardFuctions.KeyboardKeys(element_identifiedBy, element_identifier, pass_values);
							break;

						case "WebTableColTextVerification":
							List<String> values = WebTable.GetAllTextInColumn(element_identifiedBy, element_identifier, Integer.parseInt(target_identifiedBy));
							int flag=0;
							for(int i=0;i<values.size();i++){

								if(values.get(i).equalsIgnoreCase(pass_values)){
									log.info("Expected-"+values.get(i)+" Actual-"+pass_values+" PASS");
									flag=0;
									break;
								}else{

									flag = 1;

								}

							}
							// to validate after complete check with all the values 
							if (flag==1)
							{
								log.info("FAIL");
								Assert.fail("FAIL");
							}
							break;

						case "GetTimeStamp":
							UIresult.add(TimeClass.getCurrentTimestamp(pass_values));
							break;


						case "VerifyTextInPDF" :
							//
							/*String pdfsearchResult="FAIL";
							if(element_identifiedBy.equalsIgnoreCase("URL")){
								pdfsearchResult = PDFUtilities.searchPDFFromURL(driver,pass_values);
								log.info("The result in the pdf validation from URL is - "+ pdfsearchResult);
							}
							else if(element_identifiedBy.equalsIgnoreCase("File")){
								pdfsearchResult = PDFUtilities.searchPDFFromFile(element_identifier,objRepositoryFile,pass_values,target_identifiedBy);
								log.info("The result in the pdf validation from FILE is - "+ pdfsearchResult);
							}
							else{
								log.info("Please provide appropiate value in the element_identifiedBy column - (URL/File)");
							}							
							//Checking the result and writing
							log.info("PDFresult is - "+ pdfsearchResult);
							//CheckPoint.windowsScreenShot(test_case,excel_data[row_no][4],pdfsearchResult);
							ExcelFunctions.writeToExcel(wb_loc, wb_name, excel_sheet, row_no, pdfsearchResult);								
							if(TestCaseConfiguration.getPresenceOfUserInputSheet().equalsIgnoreCase("Y") && target_identifier.equalsIgnoreCase("assert")){
								log.info("Mapping the result into the user input sheet");
								String UserInputFileName = RepositoryObject.IdentifierRepo(objRepositoryFile,"UserInputFile");
								UserInputDataSheet.setValues(wb_loc, UserInputFileName, excel_sheet);
								UserInputDataSheet.getEffectiveRow(excel_sheet, wb_loc, UserInputFileName, UserInputData);
								if(pdfsearchResult.equalsIgnoreCase("PASS")){
									UserInputDataSheet.mapResult(pdfsearchResult);
								}
							}*/
							break;

						case "batchExecution":
							ThirdPartyTools.executeWindowBatch(element_identifiedBy,pass_values);
							break;

						case "simpleLoop":
							Looping.normalLoop(driver,test_case_name,excel_sheet, wb_loc, wb_name,objRepositoryFile,UserInputData,testDataRow,cellcategory,element_identifiedBy, pass_values);
							row_no = (row_no+Looping.listsize)-1;
							System.out.println("row num to be referred - "+ row_no);
							break;

						/*case "if-else":
							//Verifying the result
							Verify_list = VerifyTheText.verifyExact(UIresult);
							IfElseStatements.ExecuteSteps(driver,test_case_name,excel_sheet, wb_loc, wb_name,objRepositoryFile,UserInputData,testDataRow,cellcategory,target_identifiedBy,target_identifier, Verify_list);
							int lastStep = IfElseStatements.getLastStep(target_identifier);
							row_no = lastStep;
							break;*/
							
						case "if-else":
							IfElseStatements.ifElseStatements();
							break;

						case "scan-page":
							PageScanner.getWebElementsFRomExistingPage(driver,pass_values,CommonMethod.applicationFolder);
							break;
							
						case "UpperCase":
							UIresult.add(JavaFunctions.ToUpperCase(pass_values));
							break;
							
						case "LowerCase":
							UIresult.add(JavaFunctions.ToLowerCase(pass_values));
							break;
							
						case "Replace":
							UIresult.add(JavaFunctions.replace(pass_values, target_identifiedBy, target_identifier));
							break;
							
						case "brokenLinks":
							BrokenLinks.checkBrokenLinks(driver);
							break;
							
						case "Email-send":
							EmailFunctions.SendMail(driver, element_identifiedBy, element_identifier);
							break;
							
						case "checkElementSelection":
							Verify_list= WebElementActions.ElementSelected(element_identifiedBy, element_identifier);
							resultList.add(Verify_list);
							break;
							
						case "checkAllResultStatus":
							VerifyTheText.getAllResultStatus(resultList, "write");
							break;
							
							

							//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Sikuli Utility functions*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
						case "sikuli_focusRegionWindow":
							SikuliUtil.FocusRegionWindow();
							break;

						case "sikuli_click":
							SikuliUtil.Click(element_identifier);
							break;

						case "sikuli_doubleclick":
							SikuliUtil.DoubleClick(element_identifier);
							break;

						case "sikuli_regionwaitforobject":
							SikuliUtil.RegionWaitForObject(element_identifier);
							break;

						case "sikuli_screenwaitforobject":
							SikuliUtil.ScreenWaitForObject(element_identifier);
							break;

						case "sikuli_objectexists":
							SikuliUtil.RegionObjectExists(element_identifier);
							break;

						case "sikuli_keyboardstrokes":
							SikuliUtil.KeyboardStrokes(element_identifier);
							break;

						case "sikuli_comparedatePMS":
							SikuliUtil.compareDatePMS(pass_values);
							break;

						case "GetReservationCommentsAndCompare":
							String text = SikuliUtil.GetCommentsForReservationNumber(pass_values);
							String[] split = text.split("\n");
							if(split[0].compareTo("Defect. Hotel Services / Beach / Food Issue")==0){
								log.info("PASS-Comment has been matched with the PMS");
							}else{
								log.info("FAIL-Comment has not been matched with the PMS");
							}
							break;

						case "VerifyConfirmationNumber":
							if(pass_values.equalsIgnoreCase("FROMDB")){
								pass_values = dbResult.get(0);
							}
							SikuliUtil.VerifyConfirmationNumber(pass_values);
							break;

						default:
							log.info("Keyword dit not match");
							log.error("No matching Keyword");

						}  //end of switch case
					}  // end of if TEST_STEP
				}   // end of if TEST_CASE
			}   // end of for loop		
			return driver;
		}catch(Exception e){
			try{
				ExceptionActions.performAfterExceptionOccurs(driver);
			}catch(Exception catchException){
				log.error("Exception Inside catch block of Action Performer - ", catchException);
			}finally{
				return driver;
			}	
		}
	}

	//**************************************************************************************************************************************************

	//Delaration and Initializations
	
	//Get all the argument values
	public static void getArgumentValues(List<String> ListUIResults,List<String> ListDBResults){
		actualValues.clear();
		//ObjectValuesList.addAll(ExcelValues.getObjectlocators(excel_sheet, cellcategory, testDataRow, UserInputData));// old code
		ObjectValuesList.addAll(ExcelValues.getObjectlocators(element_identifiedBy, element_identifier, testDataRow, UserInputData,ListUIResults,ListDBResults));
		TargetObjectValuesList.addAll(ExcelValues.getTargetObjectLocators(CommonMethod.scripting_data, target_identifiedBy, target_identifier,ListUIResults,ListDBResults));

		element_identifiedBy = ObjectValuesList.get(0);
		element_identifier = ObjectValuesList.get(1);
		pass_values = ExcelValues.getPassedValues(CommonMethod.scripting_data, pass_values, testDataRow, UserInputData,ListUIResults,ListDBResults);						
		target_identifiedBy = TargetObjectValuesList.get(0);
		target_identifier = TargetObjectValuesList.get(1);
		
		actualValues.put("element_identifiedBy", ObjectValuesList.get(0));
		actualValues.put("element_identifier", ObjectValuesList.get(1));
		actualValues.put("pass_values", pass_values);
		actualValues.put("target_identifiedBy", TargetObjectValuesList.get(0));
		actualValues.put("target_identifier", TargetObjectValuesList.get(1));
	}

	//Finding element
	public static By elementFinder(String var_identifiedBy, String var_identifier){
		try{
			List<String> ObjectIdentifiers = new ArrayList<String>();
			ObjectIdentifiers.addAll(WebElementActions.getObjectIdentifiers(var_identifiedBy,var_identifier)); // Getting the object Identifiers
			var_identifiedBy = ObjectIdentifiers.get(0);
			var_identifier = ObjectIdentifiers.get(1);

			switch(var_identifiedBy){

			case "id":
				return By.id(var_identifier);

			case "className":
				return By.className(var_identifier);

			case "linkText":
				return By.linkText(var_identifier);

			case "partialLinkText":
				return By.partialLinkText(var_identifier);

			case "name":
				return By.name(var_identifier);

			case "xpath":
				String identifier = null;
				if(var_identifier.contains("UIresult")){
					if(var_identifier.equals("UIresult")){
						identifier = UIresult.get(Integer.parseInt(UIresult.get(UIresult.size()-1)));
						var_identifier = var_identifier.replace(var_identifier, identifier);
						return  By.xpath(var_identifier);
					}
					else {
						identifier = JavaFunctions.getQuery(var_identifier,UIresult);
						return  By.xpath(identifier);
					}					
				}
				return By.xpath(var_identifier);
				
			case "custom-xpath":
				return By.xpath(var_identifier);

			case "TagName":
				return By.tagName(var_identifier);

			case "css":
				return By.cssSelector(var_identifier);

			case "title":
				String[] title_attributes = var_identifier.split(",");
				int index = 1;
				if(title_attributes.length>1){
					index = Integer.parseInt(title_attributes[1]);
				}
				String titlie_xpath = "(//*[@title='"+ title_attributes[0] +  "'])[" + index + "]" ;
				log.info("Title Xpath -"+ titlie_xpath);
				return By.xpath(titlie_xpath);

			case "value":
				String[] value_attributes = var_identifier.split(",");
				int value_index = 1;
				if(value_attributes.length>1){
					value_index = Integer.parseInt(value_attributes[1]);
				}
				String value_xpath = "(//*[@value='"+ value_attributes[0] +  "'])[" + value_index + "]" ;
				log.info("Value Xpath -"+ value_xpath);
				return By.xpath(value_xpath);


			default:
				//Check for the regular expression
				String regularExpression = var_identifiedBy.substring(var_identifiedBy.length()-3);
				if(regularExpression.equalsIgnoreCase("rex")){
					log.info("Indentifier having regular expression");
					String locator_identifier = var_identifiedBy.substring(0, var_identifiedBy.length()-4);
					log.info("identifier - "+ locator_identifier);

					//Finding regular Expression condition
					String[] regularConditionArray = SplitString.stringSplit(var_identifier);
					String regularCondition = regularConditionArray[0];
					String regularExpIdentifier = regularConditionArray[1];
					log.info("//*["+regularCondition+"(@" + locator_identifier + ",'" +regularExpIdentifier + "')]");
					return By.xpath("//*["+regularCondition+"(@" + locator_identifier + ",'" +regularExpIdentifier + "')]") ;
				}
			}
		}catch(Exception e){
			log.error("Check the Identifior ",e);
			ExceptionActions.performAfterExceptionOccurs(driver);
			//Assert.fail("failed due to exception");
		}
		return null;
	}

	//Assigning values to the variables
	public static void assignExcelValues(int row_no,List<String> uiList,List<String> dbList){
		ObjectValuesList.clear();
		TargetObjectValuesList.clear();
		tc_id = CommonMethod.scripting_data[row_no][0];
		test_case = CommonMethod.scripting_data[row_no][1];
		exec_flag = CommonMethod.scripting_data[row_no][2];
		test_step_no = CommonMethod.scripting_data[row_no][3];
		test_step = CommonMethod.scripting_data[row_no][4];
		refKeyword = CommonMethod.scripting_data[row_no][5];
		ele_type = CommonMethod.scripting_data[row_no][6];
		element_identifiedBy = CommonMethod.scripting_data[row_no][7];
		element_identifier = CommonMethod.scripting_data[row_no][8];
		pass_values = CommonMethod.scripting_data[row_no][9];
		target_identifiedBy = CommonMethod.scripting_data[row_no][10];
		target_identifier = CommonMethod.scripting_data[row_no][11];
		sl_no = Integer.parseInt(CommonMethod.scripting_data[row_no][0]);
		cellCategory = CommonMethod.scripting_data[row_no][12];
		
		getArgumentValues(uiList,dbList);
	}

}


package utilityPackage;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;


public class WebTable {

	static Logger Log = Logger.getLogger(WebTable.class);

	public static String GetText(String tableIdentifiedBy, String tableIdentifier, int rowIndex ,int colIndex){
		try{
			if(tableIdentifiedBy.equals("xpath")){
				tableIdentifier = tableIdentifier + "/tbody/tr["+rowIndex+"]/td["+colIndex+"]";
				return  WebElementActions.GetWebElement(tableIdentifiedBy, tableIdentifier).getText();
			}else{
				tableIdentifier = "//table[@"+tableIdentifiedBy+"='"+ tableIdentifier + "']" + "/tbody/tr["+rowIndex+"]/td["+colIndex+"]";
				return  WebElementActions.GetWebElement(tableIdentifiedBy, tableIdentifier).getText();
			}
			
		}catch(Exception e){
			Log.error("Exception occured ", e);
			return null;
		}
	}

	public static void ClickTextProvided(String tableIdentifiedBy, String tableIdentifier, String ClickableText,String TDorLink){
		try{
			tableIdentifier = tableIdentifier + "/tbody/tr/td[text()=starts-with(., '"+ClickableText+"')]";
			if(TDorLink.equalsIgnoreCase("link")|| TDorLink.equalsIgnoreCase("Y")){
				tableIdentifier = tableIdentifier + "/*";
			}
			WebElementActions.GetWebElement(tableIdentifiedBy, tableIdentifier).click();
		}catch(Exception e){
			Log.error("Exception occured ", e);
		}
	}

	private static int GetColumnCount(String tableIdentifiedBy, String tableIdentifier){
		try{
			tableIdentifier = tableIdentifier+"/tbody/tr[1]/td";
			return WebElementActions.GetWebElements(tableIdentifiedBy, tableIdentifier).size();
		}catch(Exception e){
			Log.error("Exception occured ", e);
			return -1;
		}
	}

	private static int GetRowCount(String tableIdentifiedBy, String tableIdentifier){
		try{
			tableIdentifier = tableIdentifier+"/tbody/tr";
			return WebElementActions.GetWebElements(tableIdentifiedBy, tableIdentifier).size();
		}catch(Exception e){
			Log.error("Exception occured ", e);
			return -1;
		}
	}

	public static List<Double> Calculation( String tableIdentifiedBy, String tableIdentifier, int colIndexStartsFrom){
		List<Double> eachElement = new ArrayList<Double>();
		List<Double> returnValue = new ArrayList<Double>();
		try{
			int colCount = GetColumnCount(tableIdentifiedBy, tableIdentifier);
			int rowCount = GetRowCount( tableIdentifiedBy, tableIdentifier);
			for(int CurrentRow=1;CurrentRow<=rowCount;CurrentRow++){
				int colIndexStartsFromTemp = colIndexStartsFrom;
				Double sum = 0d;
				for(colIndexStartsFromTemp=colIndexStartsFrom; colIndexStartsFromTemp<=colCount; colIndexStartsFromTemp++){
					String text = GetText(tableIdentifiedBy, tableIdentifier, CurrentRow, colIndexStartsFromTemp);
					if(!text.isEmpty()){
						NumberFormat format = NumberFormat.getInstance();//Default Locale
						Number number = format.parse(text);
						eachElement.add(number.doubleValue());
						//						eachElement.add(Double.parseDouble(text.replaceAll(",", "")));
					}
				}
				DecimalFormat df = new DecimalFormat("#.00");//Restricting the decimal to 2 Digit
				for(int eachElementIndex=0;eachElementIndex<eachElement.size();eachElementIndex++){
					sum = Double.parseDouble(df.format(sum+eachElement.get(eachElementIndex)));
					//										sum= (sum*100+eachElement.get(eachElementIndex)*100)/100;
				}
				eachElement.removeAll(eachElement);
				returnValue.add(sum);
			}
		}catch(Exception e){
			Log.error("Exception occured", e);
		}
		return returnValue;
	}

	public static int GetRowIndex( String tableIdentifiedBy, String tableIdentifier,int ColoumnIndex, String searchString){
		try{
			WebElement Object = WebElementActions.GetWebElement(tableIdentifiedBy, tableIdentifier);
			String actualText=null;
			List<WebElement> Cells = Object.findElements(By.xpath("tbody/tr/td["+ColoumnIndex+"]"));
			for (int CurrentRow=0;CurrentRow<Cells.size();CurrentRow++){
				actualText = Cells.get(CurrentRow).getText();
				if (searchString.trim().compareTo(actualText.trim())==0){
					return CurrentRow + 1;
				}
			}
			return -1;
		}catch(Exception e){
			Log.error("Exception occured", e);
			return -1; 
		}
	}

	public static int GetColumnIndex( String tableIdentifiedBy, String tableIdentifier,int RowIndex, String searchString){
		try{
			WebElement Object = WebElementActions.GetWebElement(tableIdentifiedBy, tableIdentifier);
			String actualText=null;
			List<WebElement> Cells = Object.findElements(By.xpath("tbody/tr["+RowIndex+"]/td"));
			for (int CurrentRow=0;CurrentRow<Cells.size();CurrentRow++){
				actualText = Cells.get(CurrentRow).getText();
				if (searchString.trim().compareTo(actualText.trim())==0){
					return CurrentRow + 1;
				}
			}
			return -1;
		}catch(Exception e){
			Log.error("Exception occured", e);
			return -1;
		}
	}

	public static boolean ClickOnCorrSearch(String tableIdentifiedBy, String tableIdentifier, int WhichColToSearch, String SearchText,int InWhichColToClick){
		try{
			String Xpath ="";
			int row = GetRowIndex(tableIdentifiedBy, tableIdentifier, WhichColToSearch, SearchText);
			if(row!=-1){
				Xpath = tableIdentifier+"/tbody/tr["+row+"]/td["+InWhichColToClick+"]/*";
				
				WebElementActions.NormalClick("xpath", Xpath);
				WebElement element = WebElementActions.GetWebElement(tableIdentifiedBy, Xpath);
				if(element == null){
					Xpath = tableIdentifier+"/tbody/tr["+row+"]/td["+InWhichColToClick+"]";
					WebElementActions.Click(tableIdentifiedBy, Xpath);
				}
				
//				try{
//					if(!element.isSelected()){
//						ActionPerformer.RunJavaScript("document.getElementsByName("+element.getAttribute("id")+")["+row+"].checked=true;");
//					}
//				}catch(StaleElementReferenceException ex){
//					Log.info("Link has been clicked on previous action, So WebPage got refreshed and navigated to another page.");//TO avoid StaleElementReferenceException exception
//				}
				return true;
			}else{
				Log.error("Search String '"+SearchText+"' is not found in the table");
				return false;
			}
		}catch(Exception e){
			Log.error("Exception occured", e);
			return false;
		}
	}

	public static void Click(String element_identifiedBy,String element_identifier, int Row, int Column){
		try{
			element_identifier = element_identifier + "/tbody/tr["+Row+"]/td["+Column+"]/*";
			WebElementActions.Click(element_identifiedBy, element_identifier);
		}catch(Exception e){
			Log.error("Exception occured", e);
		}
	}
	
	public static List<String> GetAllTextInColumn(String element_identifiedBy,String element_identifier, int colIndex){	
		try{
			WebElement ActionObject = WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
			String CellText = "";
			List<String> TableText = new ArrayList<String>();
			List<WebElement> Cells = ActionObject.findElements(By.xpath("tbody/tr/td["+colIndex+"]"));
			for (int CurrentRow=0;CurrentRow<Cells.size();CurrentRow++){
				CellText = Cells.get(CurrentRow).getText();
				TableText.add(CellText);
			}
			return TableText;
		}catch(Exception e){
			Log.error("Exception occured", e);
			return null;
		}
	}
	
	
	//Search text in Webtable
	public static boolean SearchTextInWebTable(String element_identifiedBy,String element_identifier, String textToSearch ){
		boolean result = false;
		List<WebElement> WebTableElements = null;
		List<String> WebTableData = new ArrayList<String>();
		
		try{
			WebTableElements = WebElementActions.GetWebElements(element_identifiedBy, element_identifier);
			for(WebElement element : WebTableElements){
				WebTableData.add(element.getText());
			}
			
			for(int cnt=0;cnt<WebTableData.size();cnt++){
				if(WebTableData.get(cnt).equals(textToSearch)){
					Log.info("Match Found");
					return true;
				}
			}
		}
		catch(Exception e){
			Log.error("The Exception in SearchTextInWebTable method is - ",e);
			return false;			
		}
		return result;
	}
	
	
	
	//Get the row and column index for a string
	public static int[] GetCellPosition(String element_identifiedBy,String element_identifier, String textToSearch){
		try{
			//getting all the cell values inside the webtable
			WebElementActions.GetWebElements(element_identifiedBy, element_identifier);
		}
		catch(Exception e){
			
		}
		return null;
		
	}
	
}
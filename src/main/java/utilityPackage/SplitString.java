package utilityPackage;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SplitString {

	static Logger SplitString_log = Logger.getLogger(SplitString.class);

	@Test
	public static String[] stringSplit(String pass_values) {
		try{
			String[] item_array = null;
			if (pass_values!= null) {
				String SplitChars = "\\|";
				//SplitString_log.info("Index of Slip character - " + CommaSepStr.indexOf(SplitChars));
				if (pass_values.indexOf("|") >= 0) {
					item_array = pass_values.split(SplitChars);
				}else{
					item_array = pass_values.split("\n");
				}
			}
			return item_array;
		}catch(Exception e){
			SplitString_log.error("The Exception in split functions -",e);
			Assert.fail("Failure as exception occured");
		}
		return null;
	}
}

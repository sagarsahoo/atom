package utilityPackage;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MouseActions {

	static Logger MouseActions_log = Logger.getLogger(MouseActions.class);

	//Sliding the mouse
	public static void mouseSLider(WebDriver driver, String var_identifiedBy,String var_identifier){
		WebElement slider =  WebElementActions.GetWebElement(var_identifiedBy, var_identifier);
		//Using Action Class
		Actions move = new Actions(driver);
		Action action = move.dragAndDropBy(slider, 100, 0).build();
		action.perform();
	}



	// Function to click on fly-over Main MENU items 
	public static void element_section(String var_identifiedBy, String var_identifier, String act_values,String t_identifiedBy,String t_identifier) throws InterruptedException{
		WebElementActions.ExplicitWaitPresenceOfElement(var_identifiedBy,var_identifier,30);
		WebElement menu =  WebElementActions.GetWebElement(var_identifiedBy,var_identifier);
		Actions builder = new Actions(ActionPerformer.driver);//Initiate mouse action using Actions class
		builder.moveToElement(menu).build().perform();// move the mouse to the earlier identified menu option
		Thread.sleep(1000);
		subMenuFunc(t_identifiedBy,t_identifier);

	}


	//Function to select the sub-menu items 
	public static void subMenuFunc(String submenu_idenfiedBy, String subMenu_identifier){
		WebElementActions.ExplicitWaitPresenceOfElement(submenu_idenfiedBy,subMenu_identifier,5);
		WebElement subMenuOption =  WebElementActions.GetWebElement(submenu_idenfiedBy,subMenu_identifier);
		subMenuOption.click();
	}
	
	public static void mouseScrollDown(){
		try{
			JavascriptExecutor scrlDwn = (JavascriptExecutor)ActionPerformer.driver;
			scrlDwn.executeScript("window.scrollBy(0,250)", "");
		}catch(Exception e){
			MouseActions_log.error("Exception in scroll down - "+ e);
		}
	}

	public static void mouseScrollUp(){
		try{
			JavascriptExecutor scrlUp = (JavascriptExecutor)ActionPerformer.driver;
			scrlUp.executeScript("window.scrollBy(250,0)", "");
		}catch(Exception e){
			MouseActions_log.error("Exception in scroll down - "+ e);
		}
	}


}

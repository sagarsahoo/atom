package utilityPackage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLParserFunctions {


	//listXMLValuesBytag ------------------- (Both file/url) ,list all the tags and its corresponding values
	//findXmlValuesByTag ------------------- (Both file/url) ,Provide the tag in the function and it will display the corresponding value
	//findXMLValueByAttribute -------------- (Both file/url) ,Provide the Attribute in the function and it will display the corresponding value


	private static String path = System.getProperty("user.dir");
	private static String writeXMLlFilePath = path +"\\TestData\\XML_files\\urlXML_" + TimeClass.getCurrentTimestamp("MM-dd-yyyy_h-mm-ss_a") + ".xml";
	private static String xmlFilePath = path + "\\TestData\\XML_files\\";
	static String match_found = "false";
	static int match_cntr = 0;
	static int tagName_cnt=0;
	static int findAttributeMatchCounter=0;
	static List<String> ListTagValues = new ArrayList<String>();
	//static String XMLvalueRetrievedByTagName = null;

	static Logger XMLParserFunctions_log = Logger.getLogger(XMLParserFunctions.class);


	//Listing the values and tag names
	@Test
	public static void listXmlValuesByTag(String fileType,String xmlFileName){	  
		Document doc;	  
		try {		  
			String pathType = fileType;		  
			//For file
			if(pathType.equalsIgnoreCase("file")){			 
				doc= parseXML(xmlFilePath+ xmlFileName);
				if (doc.hasChildNodes()) {					 
					printNode(doc.getChildNodes());
				}  			  
			}		  		  
			//For url
			if(pathType.equalsIgnoreCase("url")){			  
				writeURLXMLToFile(xmlFileName);
				doc = parseXML(writeXMLlFilePath);
				if (doc.hasChildNodes()) {					 
					printNode(doc.getChildNodes());			 
				}   
			} 		 
		} 
		catch (Exception e) {
			XMLParserFunctions_log.info(e.getMessage());
		}
	}


	// For finding the value by tagName
	public static String findXmlValuesByTag(String fileType,String xmlFileName,String tagName,String expResult) {
		String result = "";
		try{		  

			String pathType = fileType;
			Document doc;
			//For fileType parsing
			if(pathType.equalsIgnoreCase("file")){			  
				doc = parseXML(xmlFilePath+ xmlFileName);                // File is passed by user
				NodeList nodeList = doc.getChildNodes();			  
				if (doc.hasChildNodes()) {					 
					result = findValueByTag(nodeList,tagName,expResult);
					XMLParserFunctions_log.info("##################################");
					XMLParserFunctions_log.info("The value of result to be returned is - "+ result);
					return result;			 
				}			  
			}		  		  
			//For url
			if(pathType.equalsIgnoreCase("url")){			  
				writeURLXMLToFile(xmlFileName);              // Reading from url and witing to a xml file 
				doc = parseXML(writeXMLlFilePath);           // File path defined SystePath
				NodeList nodeList = doc.getChildNodes();			  
				if (doc.hasChildNodes()) {					 
					result = findValueByTag(nodeList,tagName,expResult);
					XMLParserFunctions_log.info("***************** Result passed by the findValueByTag method is - "+ result);
					XMLParserFunctions_log.info("#####################################");
					return result;			 
				}				  
			}
		}
		catch(Exception e){
			XMLParserFunctions_log.info("Exception is - "+ e);
		}
		return result;

	}

	//Getting the value as per the given tag
	public static List<String> getValueByTagName(String fileType,String xmlFileName,String tagName){
		try{
			ListTagValues.clear();
			Document doc;
			//For fileType parsing
			if(fileType.equalsIgnoreCase("file")){			  
				doc = parseXML(xmlFilePath+ xmlFileName);                // File is passed by user
				NodeList nodeList = doc.getChildNodes();			  
				if (doc.hasChildNodes()) {					 
					getValue(nodeList,tagName);
					XMLParserFunctions_log.info("##################################");
					XMLParserFunctions_log.info("The value of result to be returned is - "+ ListTagValues);
					return ListTagValues;			 
				}			  
			}		  
			return ListTagValues;
		}catch(Exception e){
			XMLParserFunctions_log.info("Exception in getting value by TagName - "+ e);
			return ListTagValues;
		}
	}


	//For finding values by tag attributes
	public static boolean findXMLValueByAttribute(String fileType,String xmlFileName,String tagAttributes,String expectedResult){
		boolean matchResult=false;
		try{

			Document doc; 
			String[] tagAndAttribute = new String[2];
			tagAndAttribute = SplitString.stringSplit(tagAttributes);
			String tagName = tagAndAttribute[0];
			String attributeName = tagAndAttribute[1];

			if(fileType.equalsIgnoreCase("file")){

				doc = parseXML(xmlFilePath+ xmlFileName);                // File is passed by user
				NodeList nodeList = doc.getChildNodes();
				matchResult = findAttributeValue(nodeList,tagName,attributeName,expectedResult);
			}

			if(fileType.equalsIgnoreCase("url")){
				writeURLXMLToFile(xmlFileName);
				doc = parseXML(writeXMLlFilePath);                // url is passed by user
				NodeList nodeList = doc.getChildNodes();
				matchResult = findAttributeValue(nodeList,tagName,attributeName,expectedResult);
			}

			if(matchResult==true){
				matchResult=true;
			}else{
				matchResult=false;
			}
			return matchResult;
		}
		catch(Exception e){
			XMLParserFunctions_log.info("Exception in Finding value by attribute - "+ e);
			return false;
		}
	}


	// ******************************************************************************************************************************

	//To find the value of the attribute
	private static boolean findAttributeValue(NodeList listOfNodes, String tagName,String Attribute,String resultExpected ){
		try{

			XMLParserFunctions_log.info("Inside findAttribute Value");

			String tagnameToFind = tagName;
			NodeList nodeList = listOfNodes;
			boolean result = false;
			int elementNodeTypeCounter = 0;
			Node node ;

			//We need tagName and corresponding Attribute name to find


			for (int cnt = 0;cnt<nodeList.getLength();cnt ++){
				XMLParserFunctions_log.info("Count is - "+ cnt);
				Node tempNode =  nodeList.item(cnt);

				if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
					XMLParserFunctions_log.info("Node - "+ tempNode.getNodeName());

					if(tempNode.getNodeName().equalsIgnoreCase(tagName)){
						XMLParserFunctions_log.info("%%%%%%%%%%%%%%%%%%%%%%%%%% tag found - "+ tagName);

						if(tempNode.hasAttributes()){
							XMLParserFunctions_log.info("Tag has attributes");
							NamedNodeMap nodeMap = tempNode.getAttributes(); 
							for (int i = 0; i < nodeMap.getLength(); i++) {
								node = nodeMap.item(i);
								XMLParserFunctions_log.info("Attribute inside the matched tag - "+ node + " Has value -- "+ node.getNodeName());
								XMLParserFunctions_log.info("Actual Value - "+node.getNodeName());
								XMLParserFunctions_log.info("Expected value - "+Attribute);
								XMLParserFunctions_log.info("///////////////");
								if(node.getNodeName().equalsIgnoreCase(Attribute)){
									XMLParserFunctions_log.info("*********************************************** Attribute - "+ Attribute+ " found ******************");
									XMLParserFunctions_log.info("############################################### Attribute  value : " + node.getNodeValue());
									if(node.getNodeValue().equals(resultExpected)){
										result=true;
										findAttributeMatchCounter++;
										return result;
									}									
								}			 
							} 						  
						}					  					  
					}


					if(tempNode.hasChildNodes()){
						XMLParserFunctions_log.info("Node - "+tempNode.getNodeName() + " has child nodes");
						XMLParserFunctions_log.info("no of child elements - "+ tempNode.getChildNodes().getLength());

						for(int chld_node_cntr = 0;chld_node_cntr<tempNode.getChildNodes().getLength();chld_node_cntr++){						  
							Node child_node = tempNode.getChildNodes().item(chld_node_cntr);
							if(child_node.getNodeType() == Node.ELEMENT_NODE){
								XMLParserFunctions_log.info("Child node - "+child_node.getNodeName());
								elementNodeTypeCounter++;
							}						  
						}
						XMLParserFunctions_log.info("no of child elements of element type - "+ elementNodeTypeCounter);
						if(elementNodeTypeCounter>0){
							XMLParserFunctions_log.info("calling iterative function");
							if(result==true){
								return result;
							}else{
								result = findAttributeValue(tempNode.getChildNodes(),tagName,Attribute,resultExpected);
							}
							//result = findAttributeValue(tempNode.getChildNodes(),tagName,Attribute);

						}


					}

				}	  

			}

			if(findAttributeMatchCounter>0){
				XMLParserFunctions_log.info("!!!!!!!!!!!!!!!! Match counter > 0 , returning true !!!!!!!!!!!!!!!!!!!!!!");
				return true;			 
			}else{
				XMLParserFunctions_log.info("!!!!!!!!!!!!!!!! Match counter = 0 , returning false !!!!!!!!!!!!!!!!!!!!!!");
				return false;
			}

		}
		catch(Exception e){
			XMLParserFunctions_log.error("Exception in sub function findAttributeValue is - "+ e);
			return false;
		}
	}








	//Function to parse the xml file 
	public static Document parseXML(String xmlFilePath){
		try{		  
			File file = new File(xmlFilePath);			 
			DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();	 
			Document doc = dBuilder.parse(file);		 
			XMLParserFunctions_log.info("Root element is :" + doc.getDocumentElement().getNodeName());			
			return doc;		  
		}
		catch(Exception e){
			XMLParserFunctions_log.info("Exception in parseXML function - "+ e);
		}
		return null;

	}


	//Writing to a file after raeding from url
	public static void writeURLXMLToFile(String xmlFilePath){
		try{

			//StringBuilder responseBuilder = new StringBuilder();
			StringBuffer responseBuilder = new StringBuffer();
			HttpURLConnection httpConn =null;
			BufferedReader rd=null;		  
			// Create a URLConnection object for a URL
			URL url = new URL(xmlFilePath);
			URLConnection conn = null;
			conn = url.openConnection();
			//HttpURLConnection httpConn;
			httpConn = (HttpURLConnection)conn;			 
			rd = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
			String line;
			while ((line = rd.readLine()) != null)
			{
				responseBuilder.append(line + '\n');
				//XMLParserFunctions_log.info("ResponseBuilder- "+responseBuilder);
			}
			BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(writeXMLlFilePath)));
			bwr.write(responseBuilder.toString());
			bwr.flush();
			bwr.close();

		}
		catch(Exception e){
			XMLParserFunctions_log.info("The Exception in read XML from URL - "+ e);
		}
	}



	//Function to list down the tag names along with values
	private static void printNode(NodeList nodeList) {	  
		for (int count = 0; count < nodeList.getLength(); count++) {	 
			Node tempNode = nodeList.item(count);	 
			// make sure it's element node.
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {	 
				// get node name and value
				XMLParserFunctions_log.info("\nNode Name =" + tempNode.getNodeName() + " [OPEN]");
				XMLParserFunctions_log.info("Node Value =" + tempNode.getTextContent());			
				//FInding the attributes			
				if (tempNode.hasAttributes()) {	 
					// get attributes names and values
					NamedNodeMap nodeMap = tempNode.getAttributes();	 
					for (int i = 0; i < nodeMap.getLength(); i++) {	 
						Node node = nodeMap.item(i);
						XMLParserFunctions_log.info("attr name : " + node.getNodeName());
						XMLParserFunctions_log.info("attr value : " + node.getNodeValue());	 
					}
				}
				//For Child nodes inside a node
				if (tempNode.hasChildNodes()) {	 
					// loop again if has child nodes
					printNode(tempNode.getChildNodes());	 
				}	 
				XMLParserFunctions_log.info("Node Name =" + tempNode.getNodeName() + " [CLOSE]");	 
			}
		}	 
	}



	//Function to find the value corresponding to a tag
	/* private static boolean findValueByTag(NodeList nodeList ,String tagnameToFind ){

	  try{
		  		int cnt =0;
		  		match_found = "false";

					for( cnt = 0; cnt <nodeList.getLength();cnt ++ ){

						Node tempNode = nodeList.item(cnt);

						//Enter the condition if Node type is ELEMENT
						if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

							//Iterate if the match_found is false
							if(match_found.equalsIgnoreCase("false")){

								//XMLParserFunctions_log.info("Node iterated - "+tempNode.getNodeName());

								if(tempNode.getNodeName().equalsIgnoreCase(tagnameToFind)){

									XMLParserFunctions_log.info("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Tag found   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
									XMLParserFunctions_log.info("Tag Name - "+ tagnameToFind);
									XMLParserFunctions_log.info("Tag Value - "+ tempNode.getTextContent()+"\n");
									//match_found = "true";
									match_cntr ++;
									//break;
								}

								//Iterating Child nodes
								if(tempNode.hasChildNodes()){
									findValueByTag(tempNode.getChildNodes(),tagnameToFind);
								}



								if(cnt == nodeList.getLength()-1){
									if(match_cntr == 0){
										XMLParserFunctions_log.info("match_cntr value is - "+ match_cntr);
										XMLParserFunctions_log.info("\nXXXXXXXXXXXXXXXXXXXX  match not found XXXXXXXXXXXXXXXXXX");
										match_found = "false";
										match_cntr=0;

										XMLParserFunctions_log.info("false will be returned");
										return false;

									}
									if(match_cntr > 0){
										XMLParserFunctions_log.info("match_cntr value is - "+ match_cntr);
										XMLParserFunctions_log.info("Total numbers of matched items - "+ match_cntr+ "\n");
										match_found = "false";
										match_cntr=0;

										XMLParserFunctions_log.info("true will be returned");
										return true;
									}
								}

							}// End of match_found condition loop	

						}//End of node type ELEMENT condition


					}//End of counter

					//Initializing match_count to 0




	  }
	  catch(Exception e){
		  XMLParserFunctions_log.info("The Exception in finding value by xml tag is - "+e);
	  }
	return false;
  }

	 */

	public static String findValueByTag(NodeList nodeList ,String tagnameToFind,String ExpectedResult){	
		try{
			int cnt =0;
			match_found = "false";
			for( cnt = 0; cnt <nodeList.getLength();cnt ++ ){				
				Node tempNode = nodeList.item(cnt);				
				//Enter the condition if Node type is ELEMENT
				if (tempNode.getNodeType() == Node.ELEMENT_NODE) {					
					//Iterate if the match_found is false
					if(match_found.equalsIgnoreCase("false")){						
						//XMLParserFunctions_log.info("Node iterated - "+tempNode.getNodeName());						
						if(tempNode.getNodeName().equalsIgnoreCase(tagnameToFind)){							
							XMLParserFunctions_log.info("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Tag found   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
							XMLParserFunctions_log.info("Tag Name - "+ tagnameToFind);
							XMLParserFunctions_log.info("Tag Value - "+ tempNode.getTextContent()+"\n");
							XMLParserFunctions_log.info("Expected value passed by user in the target_identified field- "+ ExpectedResult+"\n");
							if(tempNode.getTextContent().equals(ExpectedResult)){
								match_found = "true";
								match_cntr ++;
								XMLParserFunctions_log.info("The value of match_found in the method - findValueByTag = "+ match_found);
								return match_found;
							}							
						}						
						//Iterating Child nodes
						if(tempNode.hasChildNodes()){
							findValueByTag(tempNode.getChildNodes(),tagnameToFind,ExpectedResult);
						}
					}
				}
			}
			return match_found;
		}

		catch(Exception e){
			XMLParserFunctions_log.info("The Exception in finding value by xml tag is - "+e);
			return "false";
		}
	}

	//Get value from tagname
	public static List<String> getValue(NodeList nodeList ,String tagnameToFind){

		try{
			int cnt =0;
			tagName_cnt = 0;
			for( cnt = 0; cnt <nodeList.getLength();cnt ++ ){				
				Node tempNode = nodeList.item(cnt);				
				//Enter the condition if Node type is ELEMENT
				if (tempNode.getNodeType() == Node.ELEMENT_NODE) {					
					//Iterate if the match_found is false
					if(tagName_cnt>=0){						
						//XMLParserFunctions_log.info("Node iterated - "+tempNode.getNodeName());						
						if(tempNode.getNodeName().equalsIgnoreCase(tagnameToFind)){							
							XMLParserFunctions_log.info("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Tag found   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
							XMLParserFunctions_log.info("Tag Name - "+ tagnameToFind);
							XMLParserFunctions_log.info("Tag Value - "+ tempNode.getTextContent()+"\n");
							tagName_cnt++;
							ListTagValues.add(tempNode.getTextContent());
						}						
						//Iterating Child nodes
						if(tempNode.hasChildNodes()){
							getValue(tempNode.getChildNodes(),tagnameToFind);
						}
					}
				}
			}
			return ListTagValues;
		}catch(Exception e){
			return ListTagValues;
		}
	}


}



package utilityPackage;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.apache.log4j.Logger;


public class CheckPoint {

	static String path = System.getProperty("user.dir");
	static String target_loc;
	static String currentFile;
	static Logger checkPoint_log = Logger.getLogger(CheckPoint.class.getName());


	//Take screenshot when test case is pass/fail
	@Test
	public static void screenShot(String status){

		try{
 
			String proprtiesFile= TestCaseConfiguration.testConfData[1];
			String screenShotFolder = RepositoryObject.IdentifierRepo(proprtiesFile, "TestDataFolder");
			target_loc= path+"\\TestReports\\ScreenShots\\"+ screenShotFolder + "\\";

			checkPoint_log.info("Capture screenshot for pass/fail");

			String currentTime = TimeClass.getCurrentTimestamp("MM-dd-yyyy_h-mm-ss_a");

			String file_name = status + "_"+ActionPerformer.test_case + "_" + ActionPerformer.test_step +"_" + currentTime + ".png";
			File srcFile  = ((TakesScreenshot)ActionPerformer.driver).getScreenshotAs(OutputType.FILE);
			currentFile = target_loc+file_name;
			checkPoint_log.info("Screenshot - "+currentFile);
			FileUtils.copyFile(srcFile, new File(currentFile));
		}catch(Exception e){

			checkPoint_log.info("Screen shot cannot be captured due to exception");
			checkPoint_log.info("Exception in SCREENSHOT - "+ e);
			//Assert.fail("Exception occurred in screenshot");
		}

	}
	
	public static void screenShot(boolean status){

		try{
 
			String proprtiesFile= TestCaseConfiguration.testConfData[1];
			String screenShotFolder = RepositoryObject.IdentifierRepo(proprtiesFile, "TestDataFolder");
			target_loc= path+"\\TestReports\\ScreenShots\\"+ screenShotFolder + "\\";

			checkPoint_log.info("Capture screenshot for pass/fail");

			String currentTime = TimeClass.getCurrentTimestamp("MM-dd-yyyy_h-mm-ss_a");

			String file_name = status + "_"+ActionPerformer.test_case + "_" + ActionPerformer.test_step +"_" + currentTime + ".png";
			File srcFile  = ((TakesScreenshot)ActionPerformer.driver).getScreenshotAs(OutputType.FILE);
			currentFile = target_loc+file_name;
			checkPoint_log.info("Screenshot - "+currentFile);
			FileUtils.copyFile(srcFile, new File(currentFile));
		}catch(Exception e){

			checkPoint_log.info("Screen shot cannot be captured due to exception");
			checkPoint_log.info("Exception in SCREENSHOT - "+ e);
			//Assert.fail("Exception occurred in screenshot");
		}

	}

	//Take a screenshot on exception
	public static void screenshotException(WebDriver scrShot_driver,String testCaseName,String test_step){ 
		try{

			String proprtiesFile= TestCaseConfiguration.testConfData[1];
			String screenShotFolder = RepositoryObject.IdentifierRepo(proprtiesFile, "TestDataFolder");
			target_loc= path+"\\TestReports\\ScreenShots\\"+ screenShotFolder + "\\";

			checkPoint_log.info("Capturing screenshot for Exception ---");

			String currentTime = TimeClass.getCurrentTimestamp("MM-dd-yyyy_h-mm-ss_a");
			String file_name = "Exception"+ "_" + testCaseName +"_" +test_step + "_" + currentTime + ".png";
			File srcFile = ((TakesScreenshot)scrShot_driver).getScreenshotAs(OutputType.FILE);
			currentFile = target_loc+file_name;
			FileUtils.copyFile(srcFile, new File(currentFile));
			checkPoint_log.info("Screen shot captured");
		}catch(Exception e){
			checkPoint_log.error("Screen shot cannot be captured due to exception");
			checkPoint_log.error("Exception in SCREENSHOT - ",e);

		}
	}


	//Taking Windows screen shot result of boolean type
	public static void windowsScreenShot() {
		try {

			String proprtiesFile= TestCaseConfiguration.testConfData[1];
			String screenShotFolder = RepositoryObject.IdentifierRepo(proprtiesFile, "TestDataFolder");
			target_loc= path+"\\TestReports\\ScreenShots\\"+ screenShotFolder + "\\";

			Robot robot = new Robot();
			String format = "png";
			String fileName = target_loc  + "_" + ActionPerformer.test_case + "_" + ActionPerformer.test_step + "_" + TimeClass.getCurrentTimestamp("MM-dd-yyyy_h-mm-ss_a")+ "." + format;

			Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
			BufferedImage screenFullImage = robot.createScreenCapture(screenRect);
			ImageIO.write(screenFullImage, format, new File(fileName));

			checkPoint_log.info("A full screenshot saved!");
		} catch (AWTException | IOException ex) {
			checkPoint_log.error(ex);
		}
	}

	// Taking Windows screen shot result of String type
	public static void windowsScreenShot(String result) {
		try {

			String proprtiesFile= TestCaseConfiguration.testConfData[1];
			String screenShotFolder = RepositoryObject.IdentifierRepo(proprtiesFile, "TestDataFolder");
			target_loc= path+"\\TestReports\\ScreenShots\\"+ screenShotFolder + "\\";

			Robot robot = new Robot();
			String format = "png";
			String fileName = target_loc+result  + "_" + ActionPerformer.test_case + "_" + ActionPerformer.test_step + "_" + TimeClass.getCurrentTimestamp("MM-dd-yyyy_h-mm-ss_a")+ "." + format;

			Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
			BufferedImage screenFullImage = robot.createScreenCapture(screenRect);
			ImageIO.write(screenFullImage, format, new File(fileName));

			checkPoint_log.info("A full screenshot saved!");
		} catch (AWTException | IOException ex) {
			System.err.println(ex);
		}
	}




}

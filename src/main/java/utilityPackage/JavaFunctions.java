package utilityPackage;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class JavaFunctions {

	static Logger JavaFunctions_log = Logger.getLogger(JavaFunctions.class);
	static String query = null;

	@Test
	public static String getstrinByLength(String actaulString, String startIndex, String length) {
		try{
			String originalString = actaulString ;
			JavaFunctions_log.info("Original String is - "+ originalString);
			int index = Integer.parseInt(startIndex);
			int lengthOfString = Integer.parseInt(length);
			String requiredString = originalString.substring(index, (index+lengthOfString));
			JavaFunctions_log.info("Required string is - "+ requiredString);
			return requiredString;
		}
		catch(Exception e){
			JavaFunctions_log.error("Exception in substring - ",e);
			return null;
		}  
	}

	@Test
	public static String getStringByValue(String actualString,String startingText,String endingTextORLength){
		try{
			JavaFunctions_log.info("Original String is - "+ actualString);
			int endString;
			int startingIndex=0;
			if(startingText.equalsIgnoreCase("NA")){
				startingIndex=0;
			}else{
				startingIndex = actualString.indexOf(startingText)+startingText.length();
			}
			
			try{
				endString = Integer.parseInt(endingTextORLength)+startingIndex;
			}catch(NumberFormatException e){
				endString = actualString.indexOf(endingTextORLength);
			}
			String requiredString = actualString.substring((startingIndex), (endString)).trim();
			JavaFunctions_log.info("Required String is - "+ requiredString);
			return requiredString;
		}
		catch(Exception e){
			JavaFunctions_log.error("Exception in the getting string by value - ",e);
			return null;
		}
	}

	//Getting the statement by replacing UIResult values
	public static String getQuery(String identifier,List<String> resultsFromUI){
		query=identifier;
		JavaFunctions_log.info("Original Query - "+ query);

		try{

			while(query.contains("UIresult")){
				int UIResultStartIndex = query.indexOf("UIresult");
				JavaFunctions_log.info("Index of UIresult - "+ UIResultStartIndex);
				int EndBracketIndex = query.indexOf(")");
				JavaFunctions_log.info("End Bracket index - "+ EndBracketIndex);

				String UIResultIndex = query.substring((UIResultStartIndex+9),EndBracketIndex );
				int indexofUIresult = Integer.parseInt(UIResultIndex);
				JavaFunctions_log.info("UI Result index is - "+indexofUIresult );

				String srcToReplace = query.substring((UIResultStartIndex),EndBracketIndex+1 );
				JavaFunctions_log.info("Source to replace - "+ srcToReplace);

				String replacing_value = resultsFromUI.get(indexofUIresult);
				query = query.replaceAll("UIresult"+"\\("+UIResultIndex + "\\)",replacing_value);
				//String newQuery = query.replaceAll(srcToReplace,"get");

				JavaFunctions_log.info("new query is  - "+ query);
			}	  
		}
		catch(Exception e){
			JavaFunctions_log.info("The Exception in getQuery is -"+ e);
		}
		JavaFunctions_log.error("!!!!!!!!!!!!!!!!!!!!!!!!! - Final query is - "+ query);
		return query;
	}


	//Getting Ramdom strings
	public static String RandomString(int length){
		StringBuffer buffer = new StringBuffer();
		String letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		for (int i=0; i<length; i++){
			int index = (int)(Math.random()*letters.length());
			buffer.append(letters.charAt(index));
		}
		return buffer.toString();
	}

	public static String RandomInteger(int length){
		
			//return Integer.parseInt(RandomStringUtils.random(length,false,true));
			return RandomStringUtils.random(length,false,true); //Return type changed to string - to support length >9, integer will be treated till 9
	}

	public static String RandomAlphaNumeric(int length){
		StringBuffer buffer = new StringBuffer();
		String letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		for (int i=0; i<length; i++){
			int index = (int)(Math.random()*letters.length());
			buffer.append(letters.charAt(index));
		}
		return buffer.toString();
	}

	//Convert to upperCase
	public static String ToUpperCase(String Value){
		Value.toUpperCase();
		return Value;
	}
	
	public static String ToLowerCase(String Value){
		Value.toLowerCase();
		return Value;
	}
	
	public static String replace(String SourceString,String toReplace, String ReplaceWith){
		SourceString.replaceAll(toReplace, ReplaceWith);
		return SourceString;
	}
	
	public static void executeJavaScript(WebDriver _driver,String pass_values){
		try{
			JavascriptExecutor jse = (JavascriptExecutor)ActionPerformer.driver;
			jse.executeScript(pass_values,"");
		}catch(Exception e){
			
		}
	}
	
	public static void executeJavaScriptOnObject(WebDriver _driver,String pass_values, String element_identifiedBy,String element_identifier){
		try{
			WebElement javaWE = WebElementActions.GetWebElement(element_identifiedBy, element_identifier);
			JavascriptExecutor jse = (JavascriptExecutor)ActionPerformer.driver;
			jse.executeScript(pass_values,javaWE);
		}catch(Exception e){
			
		}
	}


}

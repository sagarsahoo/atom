package utilityPackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CommonMethod {

	static String path = System.getProperty("user.dir"); 
	public static WebDriver driver;	 
	public static String objRepositoryFile ;	 
	public static String wb_loc ;
	public static File ExcelrepositoryFile;
	public static String testLogs ;
	public static String wb_name;
	public static String  scriptingSheet_name;
	public static String UserInputSheetPresent;
	public static String UserInputFile;
	public static String[][] UserInputData;
	public static String[][] scripting_data;
	public static String[][] masterSheetData;
	public static String [][] selectiveTestSteps;
	public static int testDataRow;
	public static String test_case;				//Define by user - test case name as mentioned in Scripting sheet
	public static String suiteMasterSheet ;
	public static String applicationFolder;
	public static String ExecutionEnvironment;

	static Logger logger = Logger.getLogger(CommonMethod.class.getName());

	public CommonMethod(){//Default constructor
	}

	public CommonMethod(String test_case, String suiteMasterSheet){
		try{
			CommonMethod.test_case = test_case;
			CommonMethod.suiteMasterSheet = suiteMasterSheet;

			testLogs = path+"\\TestReports\\Logs\\"+ suiteMasterSheet +"\\";
			MoveLogFiles();
			System.setProperty("logfilename",testLogs+  Thread.currentThread().getStackTrace()[2].getClassName().concat("_").concat(TimeClass.getCurrentTimestamp("MM-dd-yyyy_h-mm-ss_a")) );
			DOMConfigurator.configure(path + "\\log4j.xml");

			logger.info("Test case configurations!!!!!!!!!!!!!!!!!!!!!!!!!");
			String[] testConfigurationData = new String[5];
			testConfigurationData = TestCaseConfiguration.getTestCaseInfo(test_case, suiteMasterSheet);

			objRepositoryFile= testConfigurationData[1];
			scriptingSheet_name= testConfigurationData[2];
			UserInputSheetPresent = testConfigurationData[3];
			
			//Getting the Execution Environment
			ExecutionEnvironment = RepositoryObject.IdentifierRepo(objRepositoryFile, "ExecutionEnv");

			//Taking the application folder name under which scripts are present and reports will be generated
			applicationFolder = RepositoryObject.IdentifierRepo(objRepositoryFile, "TestDataFolder");
			wb_loc=path + "\\TestData\\"+ applicationFolder ;
			ExcelrepositoryFile = new File(path+ "\\TestData\\"+ applicationFolder +"\\ObjectProperty.xls");

			String masterSheet_name = "Master_sheet";
			wb_name = RepositoryObject.IdentifierRepo(objRepositoryFile, "UserScriptingFile");
			masterSheetData = ExcelFunctions.readKeywordFunc(driver,masterSheet_name,wb_loc,wb_name);
			

			//Reading User Scripting fileName from object repository
			if(UserInputSheetPresent.equalsIgnoreCase("Y")){
				//Reading User Input File name from object repository
				UserInputFile = RepositoryObject.IdentifierRepo(objRepositoryFile, "UserInputFile");
				logger.info("User Input Excel file is -"+UserInputFile);

				//Storing User data in 2-D string
				UserInputData = ExcelFunctions.readKeywordFunc(driver,scriptingSheet_name,wb_loc,UserInputFile);

				//setting user input sheet info
				UserInputDataSheet.setValues(wb_loc, UserInputFile, scriptingSheet_name);

				//Getting the effective row to take the user data
				testDataRow = UserInputDataSheet.getEffectiveRow(scriptingSheet_name,wb_loc,UserInputFile,UserInputData);
				logger.info("Row to take user data - "+testDataRow);

				//Skipping the test case if no data exists
				if(testDataRow==(-1)){ 
					logger.info("Skipping Test case");
					logger.info("No New data available");
					throw new SkipException("No new Test Data Available");		
				}
			}else{
				UserInputData=null;
			}
			//Reading Scripting data from DB
			if(scriptingSheet_name.equalsIgnoreCase("DB")){
				logger.info("Getting the scripting information from DB");
				String DatabaseName = null;
				String scritingDataQuery = "select * from abcdscripting where tcname= ('"+test_case+"') order by substr(tsid,3) asc" ;
				logger.info("Query is - "+scritingDataQuery );
				//scripting_data = DataBaseFunctions.getResultArray(scritingDataQuery,DatabaseName);

				for (int i=0;i<scripting_data.length;i++){
					for(int j=0;j<scripting_data[0].length;j++){
						logger.info(scripting_data[i][j]);
						logger.info(",");
					}
				}
			}else{
				scripting_data= ExcelFunctions.readKeywordFunc(driver,scriptingSheet_name,wb_loc,wb_name);
			}
		}catch(Exception e){
			logger.error("Exception in main test cases - ",e);
			Assert.fail();
		}


	}

	
	public void BeforetestFunction() {
		try{
			//UserInputData = ExcelFunctions.readKeywordFunc(driver,scriptingSheet_name,wb_loc,UserInputFile);
			logger.info("******************** WELCOME TO "+ test_case + "TEST CASE ********************");
			//driver = ActionPerformer.performAction(scripting_data,driver,test_case,scriptingSheet_name,wb_loc,wb_name,objRepositoryFile,UserInputData,testDataRow,"BT"); //TODO
			driver = ActionPerformer.performAction(scripting_data,test_case,scriptingSheet_name,UserInputData,testDataRow,"BT");
		}catch(Exception e){
			logger.error("Exception in Before test cases - ",e);
			Assert.fail("Failed as exception occured");
			driver.close();
			driver.quit();
		}	
	}

	
	public void test(){
		try{
			logger.info("Inside "+ test_case + " @Test method");
			logger.info("Inside userSearch Function passing to ActionPerformer"); 		  
			driver = ActionPerformer.performAction(scripting_data,test_case,scriptingSheet_name,UserInputData,testDataRow,"BT");
		}catch(Exception e){
			logger.error("Exception in main test cases - ",e);
			Assert.fail("Failed as exception occured");
			driver.close();
			driver.quit();
		}
	}

	
	public void BrowserQuitFunc(){
		try{
			logger.info("Inside BrowserQuitFunc()");		
			//driver = ActionPerformer.performAction(scripting_data,driver,test_case,scriptingSheet_name,wb_loc,wb_name,objRepositoryFile,UserInputData,testDataRow,"AT"); //TODO
			//driver = ActionPerformer.performAction(scripting_data,test_case,scriptingSheet_name,UserInputData,testDataRow,"BT");
		}catch(Exception e){
			logger.info("Exception occured in After Method - "+ e);
			if(driver==null){
				logger.info("Driver already closed");
			}else{
				logger.info("Closing the browser - ");
				BrowserClass.closeBrowser();
			}
		}
		logger.info("-------------------------- TEST CASE END -----------------------------");
	}


	public static void MoveLogFiles(){
		try{

			File folder = new File(testLogs);
			File[] listOfFiles = folder.listFiles();
			for(File singleFile:listOfFiles){
				if(singleFile.renameTo(singleFile))
					FileUtils.moveFileToDirectory(singleFile, new File(path+"\\TestReports\\LogBackup\\"),false);
			}
		}catch(Exception e){
			logger.error("Exception occurred - ",e);
		}
	}

	//Function to get the existing java files from the package
	public static  List<String> getJavaFiles(String javaFileLoc,List<String> javaFiles){
		try{

			File folder = new File(javaFileLoc);
			File[] listOfFiles = folder.listFiles();
			String className=null;
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					className = listOfFiles[i].getName().substring(0,listOfFiles[i].getName().indexOf(".") );
					logger.info("className - "+ className);
					javaFiles.add(className);
				}
			}
			return javaFiles;
		}
		catch(Exception e){
			logger.info("Exception is - ");
			return javaFiles;
		}

	}

	// Function to check whether test cases in the Execution mapping sheet tagged as Y have corresponding java file inside the package
	// If java file not present it will create a class file
	public static void checkJavaFiles(String mappingSheetLocation,List<String> javaFiles,String SuiteSheet,String packageName){
		try{

			WebDriver driver = null;
			logger.info("Execution Mapping Sheet location - "+ mappingSheetLocation);
			String executionMappingData[][] = ExcelFunctions.readKeywordFunc(driver, SuiteSheet, mappingSheetLocation, "TestExecutionMapping.xls");

			String TC_name = null;
			int match_cnt = 0;
			for (int j=1;j<executionMappingData.length;j++){
				logger.info("Test Case name - "+ executionMappingData[j][1]);
				TC_name=executionMappingData[j][1];
				match_cnt = 0;
				if(executionMappingData[j][6].equalsIgnoreCase("Y")){
					logger.info("Checking whether java file is present for test case- "+TC_name );
					for(int filecnt=0;filecnt<javaFiles.size();filecnt++){
						logger.info("         Checking with the file-"+ javaFiles.get(filecnt));
						if(TC_name.equals(javaFiles.get(filecnt))){
							logger.info("match found");
							//Check for the testcase name and suiteMasterSheet
							checkClassFile(packageName,TC_name,SuiteSheet);
							System.out.println("Checked Class file !!!!!!!!!!!!!!!");
							match_cnt++;
							break;
						}
					}
					if(match_cnt==0){
						logger.info("File not found");
						logger.info("Calling function to create java file");
						//go for class creation
						createClassFile(packageName,TC_name,SuiteSheet);
					}
				}
			}

		}
		catch(Exception e){
			logger.info("Exception occured in checkJavaFiles - ",e );
		}

	}

	//Create class file
	public static void createClassFile(String NameOfPackage, String ClasFileName,String suiteSheetName)  throws ClassNotFoundException, IOException  {
		try{

			String packageName = NameOfPackage;
			String targetFileName = ClasFileName;

			File source = new File(path + "\\src\\main\\java\\utilityPackage\\TestCaseTemplate.java");
			String tagetLoc = path + "\\src\\main\\java\\" + packageName + "\\";
			File target = new File(tagetLoc+targetFileName+".java");

			String targetFile = path + "\\src\\main\\java\\" + packageName + "\\" + targetFileName + ".java";
			FileUtils.copyFile(source, target);

			System.out.println("copying of file from Java program is completed");

			//replacing the test case and the execution sheet name
			Path path_var = Paths.get(targetFile);
			Charset charset = StandardCharsets.UTF_8;

			String content = new String(Files.readAllBytes(path_var), charset);
			content = content.replaceAll("package utilityPackage", "package "+ NameOfPackage);
			content = content.replaceAll("public class TestCaseTemplate", "public class "+ ClasFileName);
			content = content.replaceAll("test_case = .*", "test_case = \""+ targetFileName+ "\";");
			content = content.replaceAll("suiteMasterSheet = .*", "suiteMasterSheet = \""+ suiteSheetName + "\";");
			Files.write(path_var, content.getBytes(charset));
		}
		catch(Exception e){
			logger.error("The Exception in create class file is",e);
		}
	}

	// Check the content of class file - check the testCase name and the suiteSheet name
	//if it is not exact with that the information provided in the execution mapping sheet it will replace 
	public static void checkClassFile(String packageName,String targetFileName,String suiteSheetName){
		try{

			String targetFile = path + "\\src\\main\\java\\" + packageName + "\\" + targetFileName + ".java";

			//replacing the test case and the execution sheet name
			Path path_var = Paths.get(targetFile);
			Charset charset = StandardCharsets.UTF_8;

			String content = new String(Files.readAllBytes(path_var), charset);
			content = content.replaceAll("test_case = .*", "test_case = \""+ targetFileName+ "\";");
			content = content.replaceAll("suiteMasterSheet = .*", "suiteMasterSheet = \""+ suiteSheetName + "\";");
			Files.write(path_var, content.getBytes(charset));

		}
		catch(Exception e){
			logger.error("The exception in checking class file is - ",e);
		}
	}

	//Mapping test cases from scripting sheet to execution mapping sheet
	public static void mapTestCases(File MappingFile,String ExecutionMappingSheet,String applicationName){
		try{

			WebDriver driver=null;
			int match_cnt=0;

			String ScriptingFileName = path + "\\TestData\\" + applicationName + "\\" + applicationName + "_scripting.xls";
			FileInputStream ScriptingFile = new FileInputStream(ScriptingFileName);
			HSSFWorkbook workbook = new HSSFWorkbook(ScriptingFile);

			// for each sheet in the the scripting file
			int numSheetsInScripting = workbook.getNumberOfSheets();
			logger.info("No. of Sheets in the Scripting file is - "+ numSheetsInScripting );

			for (int i = 0; i < numSheetsInScripting; i++) {

				String sheetName = workbook.getSheetName(i);
				if(!sheetName.equals("Master_sheet")){

					match_cnt=0;

					String[][] ExecutionMappingData = ExcelFunctions.readKeywordFunc(driver, ExecutionMappingSheet,  path + "\\TestData", "TestExecutionMapping.xls");
					int Mappingrowcol[] =  ExcelFunctions.getRowColumn(ExecutionMappingSheet, path + "\\TestData", "TestExecutionMapping.xls");

					String[][] ScriptingtestCaseDetails = ExcelFunctions.readKeywordFunc(driver, sheetName,  path + "\\TestData\\"+applicationName , applicationName + "_scripting.xls");
					int scriptingFileRowCol[] = ExcelFunctions.getRowColumn(sheetName, path + "\\TestData\\"+applicationName, applicationName + "_scripting.xls");
					String ScriptingTestCaseName = ScriptingtestCaseDetails[1][1];
					String className = applicationName+ "."+ ScriptingTestCaseName;
					String RepositoryFile = applicationName + "." + "properties";
					//Search for the UserInput_excelSheet in the scripting file to set the UserInputFlag
					String UserInputFlag = null;
					boolean UserInputFlag_value = ExcelFunctions.SearchResult(driver, sheetName, path + "\\TestData\\"+applicationName, applicationName + "_scripting.xls", "UserInput_excelSheet");
					if(UserInputFlag_value){
						UserInputFlag="Y";
					}
					else{
						UserInputFlag="N";
					}
					String ExecFag = "Y";
					String testID = ScriptingtestCaseDetails[1][0];
					String[] insertValuesInMapping = {testID,ScriptingTestCaseName,className,RepositoryFile,sheetName,UserInputFlag,ExecFag};
					if(Mappingrowcol[0]>1){
						for(int TC_cnt =1;TC_cnt<Mappingrowcol[0];TC_cnt++){
							if(ExecutionMappingData[TC_cnt][1].equals(ScriptingTestCaseName)){
								match_cnt++;
								break;
							}
						}
						if(match_cnt==0){
							ExcelFunctions.insertRow(MappingFile,ExecutionMappingSheet,Mappingrowcol[0],insertValuesInMapping);
						}
					}
					else{
						//inserting row in ExecutionMapping Sheet
						ExcelFunctions.insertRow(MappingFile,ExecutionMappingSheet,Mappingrowcol[0],insertValuesInMapping);
					}		                	

				} 	                
			}


		}
		catch(Exception e){

		}
	}

}

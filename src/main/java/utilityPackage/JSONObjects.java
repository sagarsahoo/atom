package utilityPackage;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

public class JSONObjects {
	
	static List<String> objectNameList = new ArrayList<String>();
	static List<String> objectValueList = new ArrayList<String>();
	static String projectPath = System.getProperty("user.dir");
	static int match_objcnt=0;
	static int match_valuecnt=0;
	static String providedObjectName =null;
	static String expectedValueToCompare = null;
	
	
	
	//Getting the JSON value by tagName
	public static List<String> getJSONvalueByTagName(String fileType,String fileName,String ObjectNamePassed){
		try{
			providedObjectName = ObjectNamePassed;
			JSONParser jsonParser = new JSONParser();
	        File file = new File(projectPath+ "\\TestData\\XML_files\\"+ fileName);
	        Object object = jsonParser.parse(new FileReader(file));
	        getValueByTag((JSONObject) object);
			return objectValueList;
		}catch(Exception e){
			System.out.println("");
			return objectValueList;
		}
	}
	
  //Matching the Element Value with the provided Element-Name & Element-Value
  @Test
  public static String findJSONElementValue(String fileType,String fileName,String ObjectNamePassed,String expectedValue) {
	  String elementMatchResult="FAIL";
	  try {
		  providedObjectName = ObjectNamePassed;
		  expectedValueToCompare=expectedValue;
		  
	        JSONParser jsonParser = new JSONParser();
	        File file = new File(projectPath+ "\\TestData\\XML_files\\"+ fileName);
	        Object object = jsonParser.parse(new FileReader(file));
	        parseJson((JSONObject) object);
	        elementMatchResult = getResultforMatchValue();
	        return elementMatchResult;
	    } catch (Exception ex) {
	        ex.printStackTrace();
	        return elementMatchResult;
	    }
	}
  
  //Matching the Element Name
  public static String findJSONtag(String fileType,String fileName,String ObjectNamePassed){
	  String elementValueMatchResult="FAIL";
	  try{
		  providedObjectName = ObjectNamePassed;
		  
	        JSONParser jsonParser = new JSONParser();
	        File file = new File(projectPath+ "\\TestData\\XML_files\\"+ fileName);
	        Object object = jsonParser.parse(new FileReader(file));
	        parseJson((JSONObject) object);
	        elementValueMatchResult = getResultforMatchElement();
	        return elementValueMatchResult;		  
	  }catch(Exception ex){
		  ex.printStackTrace();
		  return elementValueMatchResult;
	  }
  }
  
  
  //************************************************************************************************************************************************************************
  
  private static void getArray(Object object2) throws ParseException {

	    JSONArray jsonArr = (JSONArray) object2;
	    for (int k = 0; k < jsonArr.size(); k++) {
	        if (jsonArr.get(k) instanceof JSONObject) {
	            parseJson((JSONObject) jsonArr.get(k));
	        } else {
	            System.out.println(jsonArr.get(k));	            
	        }
	    }
	}

	private static void parseJson(JSONObject jsonObject) throws ParseException {
	    boolean objresult=false;
		Set<Object> set = jsonObject.keySet();
	    Iterator<Object> iterator = set.iterator();
	    while (iterator.hasNext()) {
	        Object obj = iterator.next();
	        if (jsonObject.get(obj) instanceof JSONArray) {
	            System.out.println(obj.toString());
	            getArray(jsonObject.get(obj));
	        } else {
	            if (jsonObject.get(obj) instanceof JSONObject) {
	                parseJson((JSONObject) jsonObject.get(obj));
	            } else {
	                System.out.println(obj.toString() + "\t" + jsonObject.get(obj));
	                objectNameList.add(obj.toString());
	                objresult=findElement(obj.toString(),providedObjectName);
	                if(objresult){
	                	matchObjectValue(jsonObject.get(obj).toString(),expectedValueToCompare);
	                }
	            }
	        }
	    }
	}
	
	//Getting the value for the matched tag
	private static List<String> getValueByTag(JSONObject jsonObject){
		try{
			boolean objresult=false;
			Set<Object> set = jsonObject.keySet();
		    Iterator<Object> iterator = set.iterator();
		    while (iterator.hasNext()) {
		        Object obj = iterator.next();
		        if (jsonObject.get(obj) instanceof JSONArray) {
		            System.out.println(obj.toString());
		            getArray(jsonObject.get(obj));
		        } else {
		            if (jsonObject.get(obj) instanceof JSONObject) {
		            	getValueByTag((JSONObject) jsonObject.get(obj));
		            } else {
		                System.out.println(obj.toString() + "\t" + jsonObject.get(obj));
		                objectNameList.add(obj.toString());
		                objresult=findElement(obj.toString(),providedObjectName);
		                if(objresult){
		                	objectValueList.add(jsonObject.get(obj).toString());
		                	return objectValueList;
		                }
		            }
		        }
		    }
		    return objectValueList;
		}catch(Exception e){
			System.out.println("Exception in getting the value by tag in jSON is - "+ e);
			return objectValueList;
		}
	}
	
	//Finding the element
	private static boolean findElement(String actualObject,String expectedObject){
		boolean objectMatchResult=false;
		try{
			/*System.out.println("Actaul object - "+ actualObject);
			System.out.println("Expected Object - "+ expectedObject);*/
			if(actualObject.equals(expectedObject)){
				System.out.println("Actaul object - "+ actualObject);
				System.out.println("Expected Object - "+ expectedObject);
				System.out.println("$$$$$$$$$$$$$$$$$$$ Object  MATCHED $$$$$$$$$$$$$$$$$$$ ");
				match_objcnt++;
				objectMatchResult=true;
			}
			return objectMatchResult;
		}catch(Exception e){
			System.out.println("Exception occured during finding the element - "+ e);
			return objectMatchResult;
		}
		
	}
	
	//Matching the value
	private static void matchObjectValue(String actualValue,String expectedValue){
		try{
			System.out.println("Actual value retrieved - "+ actualValue );
			System.out.println("Expected value provided - "+ expectedValue);
			if(actualValue.equals(expectedValue)){				
				System.out.println("$$$$$$$$$$$$$$$$$$$$$  Value matched $$$$$$$$$$$$$$$$$$$$");
				match_valuecnt++;
			}
		}catch(Exception e){
			System.out.println("Exception occured during matching the value - "+ e);
		}
		
	}
	
	//Display result for match Value
	private static String getResultforMatchValue(){
        System.out.println("Number of objects - "+ objectNameList.size());
        String elementValueResult="FAIL";
        if(match_objcnt>0){
        	System.out.println("\n ################## OBJECT FOUND SUCCESSFULLY ##########################");
        	if(match_valuecnt>0){
        		System.out.println("\n ########### Value matched successfully #####################");
        		elementValueResult="PASS";
        	}
        	else{
        		System.out.println("!!!!!!!!!!! Value not matched !!!!!!!!!!!!!!!");
        	}
        	return elementValueResult;
        }
        else{
        	System.out.println("!!!!!!!!!! Object NOT FOUND !!!!!!!!!!!!!!");
        	return elementValueResult;
        }
	}
	
	//Display result for match Element
	private static String getResultforMatchElement(){
        System.out.println("Number of objects - "+ objectNameList.size());
        String elementResult="FAIL";
        if(match_objcnt>0){
        	System.out.println("\n ################## OBJECT FOUND SUCCESSFULLY ##########################");
        	elementResult="PASS";
        }
        else{
        	System.out.println("!!!!!!!!!! Object NOT FOUND !!!!!!!!!!!!!!");
        }
        return elementResult;
	}
}

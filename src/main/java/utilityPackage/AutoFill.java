package utilityPackage;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class AutoFill {
	
	static WebDriverWait wt;
	static Logger autofill_log = Logger.getLogger(AutoFill.class);
	
  @Test
  public static void selectTextInAutoComplete(String elementIndentifiedBy,String elementIdentifier,String targetIdentifiedBy,String targetIdentifier,String text) {
	  
	  try {
		  
		  	String[] textList = SplitString.stringSplit(text);
		  	String textToEnter = textList[0];
		  	autofill_log.info("Text to enter - "+textToEnter );
		  	String textToSelect = textList[1];
		  	autofill_log.info("Text to select - "+textToSelect );
		  	
		  	autofill_log.info("Length of textList- "+textList.length );

		  	if(textList.length==0){
		  		textToSelect=textToEnter;
		  	}
		  	
			WebElement autoOptions = WebElementActions.GetWebElement(elementIndentifiedBy, elementIdentifier);
			autofill_log.info("WebElement stored");
			List<WebElement> optionsToSelect = autoOptions.findElements(ActionPerformer.elementFinder(targetIdentifiedBy, targetIdentifier));
			for(WebElement option : optionsToSelect){
				autofill_log.info("Element is - "+ option.getText());
		        if(option.getText().equals(textToSelect.trim())) {
		        	autofill_log.info("Trying to select: "+textToSelect);
		            option.click();
		            break;
		        }
		    }
			
		} catch (NoSuchElementException e) {
			autofill_log.error(e.getStackTrace());
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
		catch (Exception e) {
			autofill_log.info(e.getStackTrace());
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
  }
  
  //Verify whether the string is present in autoFill
public static boolean VerifyTextInAutoComplete(String elementIndentifiedBy,String elementIdentifier,String targetIdentifiedBy,String targetIdentifier,String text) {
	  
	boolean result=false;
	  try {
		  
		  	
		  	String[] textList = SplitString.stringSplit(text);
		  	String textToEnter = textList[0];
		  	autofill_log.info("Text to enter - "+textToEnter );
		  	String textToSelect = textList[1];
		  	autofill_log.info("Text to select - "+textToSelect );
		  	
		  	autofill_log.info("Length of textList- "+textList.length );

		  	if(textList.length==0){
		  		textToSelect=textToEnter;
		  	}
		  	
			WebElement autoOptions = WebElementActions.GetWebElement(elementIndentifiedBy, elementIdentifier);
			autofill_log.info("WebElement stored");
			List<WebElement> optionsToSelect = autoOptions.findElements(ActionPerformer.elementFinder(targetIdentifiedBy, targetIdentifier));
			for(WebElement option : optionsToSelect){
				autofill_log.info("Element is - "+ option.getText());
		        if(option.getText().equals(textToSelect.trim())) {
		        	autofill_log.info("Text found "+textToSelect);
		            result = true;
		            CheckPoint.screenShot(result);
		            return result;
		        }
		    }
			
		} catch (NoSuchElementException e) {
			autofill_log.error(e.getStackTrace());
			CheckPoint.screenShot(result);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
		catch (Exception e) {
			autofill_log.error(e.getStackTrace());
			CheckPoint.screenShot(result);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
	  //ExceptionActions.performAfterExceptionOccurs(NewActionPerformer.driver);
	  return result;
	  
  }
  
}

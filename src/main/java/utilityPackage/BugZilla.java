package utilityPackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

//Check whether BUG has been logged against the  test case 

public class BugZilla {
	
	static private WebDriver _driver;
	static String excel_sheet="report";
	static String wb_loc="D://Selenium//MavenSeleniumProject//TestData";
	static String wb_name="BugzillaReport.xls";
	static String var_status = "notBuggy";
	static Logger logger = Logger.getLogger(BugZilla.class.getName());
	
  @Test
  public static String testStatus(String test_case) throws IOException {
	  
	  logger.info("To find status of the test case from bugzilla");
	  String[][] bugReport = ExcelFunctions.readKeywordFunc(_driver, excel_sheet, wb_loc, wb_name);
	  
	  //String test_case="bugzilla_report";
	  
	  for (int row_num=1 ; row_num <bugReport.length ; row_num++){

		  if(bugReport[row_num][1].trim().equalsIgnoreCase(test_case)){
			  //logger.info("Test case inside function ---"+ test_case);
			  //logger.info("Test case in excel --------"+bugReport[row_num][1].trim());
			  var_status="buggy";
			  break;
		  }
	  }
	  
	  logger.info("Inside function testStatus --- The status of the report is :"+ var_status);
	  
	  //_driver.quit();
	  
	return var_status;
	  
  }
  
  
  public static void updateDefectList(String testCase) throws IOException {
	  
		 String path = System.getProperty("user.dir");
		 String sheet_wb_loc = path + "\\TestData\\" ;
		 String sheet_wb_name = "BugzillaReport.xls";
		 String excel_sheet = "report";
		 String target_testCase = testCase;
		 
		 String[][] data = ExcelFunctions.readKeywordFunc(_driver,excel_sheet, sheet_wb_loc, sheet_wb_name);
		
		 
		 File excel = new File(sheet_wb_loc + "\\" + sheet_wb_name);
		  FileInputStream fis =  new FileInputStream(excel);
		  HSSFWorkbook wb = new HSSFWorkbook (fis);
		  HSSFSheet ws = wb.getSheet(excel_sheet);
		 
		  int dest_row = ws.getLastRowNum();
		  
		  ExcelFunctions.writeCellByCondition(sheet_wb_loc, sheet_wb_name, excel_sheet, dest_row, 0,target_testCase,"false");
		  ExcelFunctions.writeCellByCondition(sheet_wb_loc, sheet_wb_name, excel_sheet, dest_row+1,1,"New BUG","true");
  }
  
  //Code to report a bug when test case fails
  public static void reportBug(){
	  
  }
}

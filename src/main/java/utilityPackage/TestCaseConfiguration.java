package utilityPackage;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class TestCaseConfiguration {
	
	static String path = System.getProperty("user.dir");
	static String MappingFilePath = path +"\\TestData";
	public static String MappingFileName = "TestExecutionMapping.xls";
	public static String[][] MappingData;
	public static String testConfData[] = new String[5];
	static WebDriver _driver;
	
	static Logger TestCaseConfiguration_log = Logger.getLogger(TestCaseConfiguration.class);
	
	
  @Test
  public static String[] getTestCaseInfo(String testCase,String sheetName) throws IOException {
	  try{
		  
		  
		  String testcaseFieldName="TestCaseName";
		  String classField = "className";
		  String repositoryFieldName = "RepositoryFile";
		  String ExcelSheetFieldName = "ExcelSheetName";
		  String UserInputFieldName = "UserInputFilePresent";
		  String execFlagField = "Exec_flag";
		  
		  
		  int testCaseNameColumnNo = ExcelFunctions.getColumnPosition(MappingFilePath, MappingFileName, sheetName, testcaseFieldName);
		  int classColumnNo= ExcelFunctions.getColumnPosition(MappingFilePath, MappingFileName, sheetName, classField);
		  int repositoryColumnNo = ExcelFunctions.getColumnPosition(MappingFilePath, MappingFileName, sheetName, repositoryFieldName);
		  int ExcelColumnNo = ExcelFunctions.getColumnPosition(MappingFilePath, MappingFileName, sheetName, ExcelSheetFieldName);
		  
		  int userInputColumnNo= ExcelFunctions.getColumnPosition(MappingFilePath, MappingFileName, sheetName, UserInputFieldName);
		  int execFlagColumnNo= ExcelFunctions.getColumnPosition(MappingFilePath, MappingFileName, sheetName, execFlagField);
		  
		  
		  
		  MappingData = ExcelFunctions.readKeywordFunc(_driver, sheetName, MappingFilePath, MappingFileName);
		  
		  
		  for(int row=0;row<MappingData.length;row++){
			  if(MappingData[row][testCaseNameColumnNo].equalsIgnoreCase(testCase)){
				  TestCaseConfiguration_log.info("Row found for test case " +testCase + " is - "+ row);
				  
				  testConfData[0]=MappingData[row][classColumnNo];
				  TestCaseConfiguration_log.info("test_case - "+ testConfData[0] ) ;
				  
				  testConfData[1]=MappingData[row][repositoryColumnNo];
				  TestCaseConfiguration_log.info("Repository - "+ testConfData[1] ) ;
				  
				  testConfData[2] =MappingData[row][ExcelColumnNo];
				  TestCaseConfiguration_log.info("ExcelSheet - "+ testConfData[2] ) ;
				  
				  testConfData[3]=MappingData[row][userInputColumnNo];
				  TestCaseConfiguration_log.info("UserInputSheetPresent - "+ testConfData[3] ) ;
				  
				  testConfData[4]=MappingData[row][execFlagColumnNo];
				  TestCaseConfiguration_log.info("ExecutionFlag - "+ testConfData[4] ) ;
				  break;
			  }
		  }
		  
		  return testConfData;

	  }
	  catch(Exception e){
		  TestCaseConfiguration_log.error("Exception in retrieving the object repository file is - ",e);
		  return null;
	  }  	  
  }
  
  
  //Getting the presence of UserInput Sheet
  public static String getPresenceOfUserInputSheet(){

	  try{
		  String isUserInputSheetPresent =  testConfData[3];
		  TestCaseConfiguration_log.info("value of isUserInputSheetPresent - "+ isUserInputSheetPresent);
		  return isUserInputSheetPresent;
		  
	  }
	  catch(Exception e){
		  TestCaseConfiguration_log.error("Exception - ",e);
		  return null;
	  }
  }
  
  
}

package utilityPackage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

public class TextToHTML {
	
	static String path = System.getProperty("user.dir");
	static Logger TextToHTML_log = Logger.getLogger(TextToHTML.class);
	static String souceFileLoc;
	
  @Test
  public static void htmlLog(String logFilePath) {
	  
	  try {
		  
		  souceFileLoc = path + "\\TestReports\\Logs\\" +logFilePath+"\\";
		  TextToHTML_log.info("Source log File locatuon path - " + souceFileLoc);
		  
		  
		  TextToHTML_log.info("converting .log file to .html file");
		  
		  //Picking up the latest log file
		  File folder = new File(souceFileLoc);
		  File[] listOfFiles = folder.listFiles();
		  
		  List<String> results = new ArrayList<String>();
		  
		  for (File file : listOfFiles) {
			    if (file.isFile()) {
			    	
			        results.add(file.getName());
			    }
			}
		  
		  TextToHTML_log.info("Number of files - "+results.size()+"\n");
		  
		  
		  
		  //Selecting .log file type
		  for (int i=0;i<results.size();i++){
			  TextToHTML_log.info("File -"+(i+1) + " is - "+ results.get(i));
			  
			  String file_extensionLog = results.get(i).substring(results.get(i).lastIndexOf("."));
			  //TextToHTML_log.info("file extension is - "+ file_extension);
			  
			  if(file_extensionLog.equalsIgnoreCase(".log")||file_extensionLog.equalsIgnoreCase(".errorlog") ){
				  TextToHTML_log.info("\nfile found - "+ results.get(i));
				  
				  String source_logfile = results.get(i);
				  TextToHTML_log.info("Target log file is - "+source_logfile );
				  String target_htmlfilename = results.get(i).substring(0, results.get(i).lastIndexOf("."));
				  TextToHTML_log.info("Target html file is - "+target_htmlfilename+ ".html" );
				  
				  //converting file to html
				  BufferedReader txtfile = new BufferedReader(new FileReader(souceFileLoc+ source_logfile));
		          OutputStream htmlfile= new FileOutputStream(new File(souceFileLoc+target_htmlfilename+ ".html"));
		          PrintStream printhtml = new PrintStream(htmlfile);

		          ArrayList<String> txtbyLine = new ArrayList<String>();
		          
		          String temp = "";
		          String txtfiledata = "";

		          String htmlheader="<html><head>";
		          htmlheader+="<title>Equivalent HTML</title>";
		          htmlheader+="</head><body>";
		          String htmlfooter="</body></html>";
		          int linenum = 0 ;

		          while ((txtfiledata = txtfile.readLine())!= null)
		             {	      
		        	  txtbyLine.add(linenum, txtfiledata);
		                  linenum++;
		              }
		          
		          for(int i1=0;i1<linenum;i1++)
		              {
		                  if(i1 == 0)
		                  {
		                      temp = htmlheader + txtbyLine.get(0) ;
		                      txtbyLine.add(0, temp +"<br>");
		                      //TextToHTML_log.info("Line 0 - "+ txtbyLine[0]);
		                	  
		                  }
		                  if(linenum == i1+1)
		                  {
		                      temp = "<br>"+txtbyLine.get(i1) + htmlfooter ;
		                      txtbyLine.add(i1,  temp + "<br>");
		                  }
		                  printhtml.println(txtbyLine.get(i1));
		                  printhtml.println("<br>");
		              }

		      printhtml.close();
		      htmlfile.close();
		      txtfile.close();

				  
			  }
			  
		  }		  
  }

  catch (Exception e) {
	  TextToHTML_log.error("Exception occured for converting TExt to HTML format - ",e);
  }
		

	}
	  
  }

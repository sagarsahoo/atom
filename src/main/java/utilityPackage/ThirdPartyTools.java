package utilityPackage;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

public class ThirdPartyTools {
	
	static Logger ThirdPartylog = Logger.getLogger(ThirdPartyTools.class.getName());
	static String ProjectPath = System.getProperty("user.dir");
	
  @Test
  public static void executeWindowBatch(String batchFilePath,String batchFilename) {
	  try{
		  //String batchFilePath = ProjectPath + "\\TestData\\" + CommonMethod.applicationFolder + "\\";
		  String batchFile = batchFilePath + "\\"+ batchFilename;
		  String windowProcess = "cmd /C start" + " " + batchFile ;
		  //String windowProcess = "cmd /C start" +  batchFilePath + "\\"+ batchFilename;
		  
		  //Process p = Runtime.getRuntime().exec("cmd /C start C:/Accenture/Project/build.bat");
		  Process p = Runtime.getRuntime().exec(windowProcess);
		  //p.wait();
		  ThirdPartylog.info("batch file executed");
	  }catch(Exception e){
		  ThirdPartylog.error("Error in executing the batch file - ",e);
	  }
  }
}

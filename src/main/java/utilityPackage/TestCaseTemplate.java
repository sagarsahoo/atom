package utilityPackage;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import utilityPackage.CommonMethod;

@Listeners(utilityPackage.Listener.class)
@Test
public class TestCaseTemplate {
	String test_case = "sample_TC"; // Test case name as mentioned in Execution mapping sheet and scripting sheet
	String suiteMasterSheet = "sample_Suite"; // Sheet name in the Execution Mapping Sheet

	@Test
	public void SmokeTestCase(){
		CommonMethod CM = new CommonMethod(test_case,suiteMasterSheet);
		CM.BeforetestFunction();
		CM.test();
		CM.BrowserQuitFunc();
	}
}

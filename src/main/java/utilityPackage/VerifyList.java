package utilityPackage;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class VerifyList {
	
	static Logger VerifyList_log = Logger.getLogger(VerifyList.class);
	
	
	// To verify the list of items present
	public static String funcVerifyList_attach(String element_identifiedBy,String element_identifier,String pass_values,String excel_sheet,String check_action){
		
		//Inside parent try block
		try{			
			
			String[] itemlist_array = SplitString.stringSplit(pass_values);
			WebElement we =  WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
			String[] group_list = SplitString.stringSplit(we.getText());			
			Thread.sleep(100);
			
			// Inside 1st parralel child try block - verify 
			try{
				
				VerifyList_log.info("Inside the verify function");
				
				int matched_list = 0;
				
				
				for (int i = 0 ; i <itemlist_array.length ; i++){
					VerifyList_log.info("Searching for ITEM - " + i + " - " +  itemlist_array[i]);
					for (int j = 0 ; j < group_list.length ; j++){
						VerifyList_log.info("Item in the group list - "  + group_list[j]);
						if(itemlist_array[i].trim().equalsIgnoreCase(group_list[j].trim())){
							VerifyList_log.info("Group - " + itemlist_array[i] + " - has been attached");
							matched_list ++;
							VerifyList_log.info("Matched list counter - " +matched_list );
						}						
					}					
				} 
				
				
				VerifyList_log.info("No of items in the array - " + itemlist_array.length);
				VerifyList_log.info("No of matched_items - " + matched_list);
				
				// pass case condition
				if (matched_list == itemlist_array.length){
					VerifyList_log.info("All matched items found");
					return "PASS";
				}
				// Failed case
				else{
					
					VerifyList_log.info("All matched items not found");
					return "FAIL";
				}
				
				
			}
			// Inside catch of 1st child try block
			catch(Exception e){
				
				VerifyList_log.error("The Exception in VerifyList is - " + e);
				VerifyList_log.error("Failure as exception occured");
			}
			
			
			
			//Inside try of 2nd child try block- assert
			try{
				
				VerifyList_log.info("Inside the verify function");
				
				int matched_list = 0;
				
				
				for (int i = 0 ; i <itemlist_array.length ; i++){
					VerifyList_log.info("Searching for ITEM - " + i + " - " +  itemlist_array[i]);
					for (int j = 0 ; j < group_list.length ; j++){
						VerifyList_log.info("Item in the group list - "  + group_list[j]);
						if(itemlist_array[i].trim().equalsIgnoreCase(group_list[j].trim())){
							VerifyList_log.info("Group - " + itemlist_array[i] + " - has been attached");
							matched_list ++;
							VerifyList_log.info("Matched list counter - " +matched_list );
						}						
					}					
				} 
				
				
				VerifyList_log.info("No of items in the array - " + itemlist_array.length);
				VerifyList_log.info("No of matched_items - " + matched_list);
				
				// pass case condition
				if (matched_list == itemlist_array.length){
					VerifyList_log.info("All matched items found");
					return "PASS";
				}
				
			}
			// Inside catch of 2nd child try block - assert
			catch(AssertionError  e){
				
				VerifyList_log.info("The Exception in Assert List is - " + e);
				Assert.fail("Failure as exception occurred");
				//driver.quit();
				return "FAIL";
			}
				
			
		}
		
		// Inside catch of parent try block
		catch(Exception e){
			
			VerifyList_log.info("The Exception in VerifyList is - " + e);
			Assert.fail("Failure as exception occured");
			//driver.quit();
		}
		
		
		
		return "FAIL";
	
	}
	
	/*public static String funcVerifyList_selected(String element_identifiedBy,String element_identifier,String pass_values,String excel_sheet,String check_action){
		
		try{
			
			String[] itemlist_array = SplitString.stringSplit(pass_values);
			int matched_list = 0;
			Thread.sleep(100);
			
			WebElement group =  WebElementActions.GetWebElement(element_identifiedBy,element_identifier);
			List<WebElement> elements = group.findElements(ActionPerformer.elementFinder(element_identifiedBy,element_identifier));
			
			
			for (int i = 0 ; i <itemlist_array.length ; i ++){
				VerifyList_log.info("list_item in excel to check for selection" + i + "=" + itemlist_array[i] );
				
				for (WebElement element : elements){
					VerifyList_log.info("element in the product list-" + element.getText());
					
					if(itemlist_array[i].trim().equalsIgnoreCase(element.getText().trim())){
						
							if(element.isSelected()){
								
								VerifyList_log.info("**************  Element is selected - " + element.getText());
								matched_list ++;
							}
					}						
				}										
			}	
			
			
			VerifyList_log.info("No of items in the excel for selection - " + itemlist_array.length);
			VerifyList_log.info("No of matched_items selected - " + matched_list);
			
			if (matched_list == itemlist_array.length){
				VerifyList_log.info("All matched items selected");
				return "PASS";
			}
			
			
		}
		catch(Exception e){
			
			VerifyList_log.error("Exception in Verify list Selected - " + e);
			Assert.fail("Failure as exception occured");
		}
		
		
		
		return "FAIL";
		
	}
	
*/
}

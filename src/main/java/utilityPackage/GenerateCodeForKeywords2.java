package utilityPackage;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.bag.SynchronizedSortedBag;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import selfitGui.PageScanner;

public class GenerateCodeForKeywords2 {
	static String keywordCode;
	static List<String> keywordRelatedVales = new ArrayList<String>();

	@Test
	public static String  getCode(int row_no) {
		
		keywordCode="";
		keywordCode = "log.info(\"  #### Step No - "+ GenerateScript.KeywordValueList.get(3) + " ########### Excecuting Test Step -"+GenerateScript.KeywordValueList.get(4)+ " ############ refering Keyword - " + GenerateScript.KeywordValueList.get(5) + "\");\n";
		keywordCode =keywordCode +"ActionPerformer.assignExcelValues("+ row_no+",UIresult,dbResult);\n";

		switch(GenerateScript.KeywordValueList.get(5)){
		//To open the browser
		case "open_browser":			
			keywordCode =keywordCode + "driver = OpenBrowser.open_browser(pass_values,target_identifiedBy);\n"+ "act = new Actions(driver);\n";
			break;

		case "get_url":			
			keywordCode = keywordCode + "ProvideURL.baseUrl(driver,pass_values);\n";
			break;

		case "getTitle" :			
			keywordCode=keywordCode + "UIresult.add(driver.getTitle());\n";
			break;


		case "wait" :			
			keywordCode = keywordCode + "Thread.sleep(1000);";
			break;

		case "pageRefresh":			
			keywordCode=keywordCode +"_driver.navigate().refresh();";
			break;

		case "navigate-back":			
			keywordCode=keywordCode +"_driver.navigate().back();";
			break;

		case "navigate-forward":			
			keywordCode=keywordCode + "_driver.navigate().forward();";
			break;

		case "enter_text":			
			keywordCode = keywordCode + "WebElementActions.enterText(element_identifiedBy, element_identifier, pass_values);\n";			
			break;
			
		case "custom-xpath":
			//String customxpath = GenerateScript.KeywordValueList.get(9).replaceAll("[\u0000-\u001f]", "");
			keywordCode = keywordCode + "customXpath=" + GenerateScript.KeywordValueList.get(9).replaceAll("[\u0000-\u001f]", "") + ";\n";
			break;

		case "EnterWithoutClear":			
			keywordCode = keywordCode + "WebElementActions.enterTextWithoutClear(element_identifiedBy, element_identifier, pass_values);\n";
			break;

		case "enter-text_conditional":			
			keywordCode = keywordCode + "WebElementActions.enterTextIfNotPresent(element_identifiedBy, element_identifier, pass_values, target_identifier);\n";
			break;

		case "text_clear":			
			keywordCode = keywordCode + "WebElementActions.clearText(element_identifiedBy, element_identifier);\n";
			break;

		case "click":			
			keywordCode = keywordCode + "WebElementActions.Click(element_identifiedBy, element_identifier);\n";
			break;

		case "NormalClick":			
			keywordCode = keywordCode + "WebElementActions.NormalClick(element_identifiedBy, element_identifier);\n";
			break;
			
		case "clickAndProceed":			
			keywordCode = keywordCode + "WebElementActions.ClickAndProceed(element_identifiedBy, element_identifier);\n";
			break;

		case "DoubleClick":			
			keywordCode = keywordCode + "WebElementActions.doubleClick(element_identifiedBy, element_identifier);\n";
			break;

		case "nav_section":			
			keywordCode = keywordCode + "MouseActions.element_section(element_identifiedBy,element_identifier,pass_values,target_identifiedBy,target_identifier);\n";
			break;
			
		case "text_verify":			
			keywordCode = keywordCode + "Verify_list= VerifyTheText.verifyExact(UIresult);\n";
			keywordCode = keywordCode +"resultList.add(Verify_list);\n";			
			break;
			
		case "verifyTwoValue":		
			keywordCode = keywordCode + "Verify_list = VerifyTheText.verifyTwoValue(UIresult);\n";
			keywordCode = keywordCode +"resultList.add(Verify_list);\n";			
			break;
			
		case "verifyIgnoreCase":
			keywordCode = keywordCode + "Verify_list= VerifyTheText.verifyIgnoreCase(UIresult);\n";
			keywordCode = keywordCode +"resultList.add(Verify_list);\n";
			break;


		case "verify_contains":			
			keywordCode = keywordCode + "Verify_list = VerifyTheText.verifyContains(UIresult);\n";
			keywordCode = keywordCode +"resultList.add(Verify_list);\n";
			break;

		case "VerifyRegularExpression":			
			keywordCode = keywordCode + "Verify_list = VerifyTheText.regularExpressionVerify(UIresult);\n";
			keywordCode = keywordCode +"resultList.add(Verify_list);\n";
			break;
			
		case "build_query":			
			keywordCode = keywordCode + "dbQuery = DataBaseFunctions.sqlQueryString(UIresult);\n";						
			break;
			
		case "getDBresult":
			keywordCode = keywordCode + "dbResult.add(DataBaseFunctions.getResult(dbQuery, pass_values));\n";
			break;
			
		case "getDBresultArray":
			keywordCode = keywordCode + "dbResult.addAll(DataBaseFunctions.getResultArray(dbQuery, pass_values));\n";
			break;

			
		case "alertBox":			
			keywordCode = keywordCode + "AlertBoxHandler.alertBoxHandling(driver, pass_values);\n";
			break;
			
		case "alertTextAssert" :			
			keywordCode = keywordCode + "Verify_list = VerifyTheText.assertAlertBoxText();\n";
			keywordCode = keywordCode +"resultList.add(Verify_list);\n";
			break;

		case "select_Items":			
			keywordCode = keywordCode + "SelectItems.itemsToSelect(element_identifiedBy,element_identifier,pass_values,target_identifiedBy,target_identifier);\n";
			break;
			
		case "deselect_Items":			
			keywordCode = keywordCode +"SelectItems.itemsToDeselect(element_identifiedBy,element_identifier,pass_values,target_identifiedBy,target_identifier);\n";
			break;

		case "Conditional_select_Items":			
			keywordCode = keywordCode + "SelectItems.selecOtherValue(target_identifiedBy, target_identifier);\n";
			break;
			
		case "Click_cor_select_item" :			
			keywordCode = keywordCode + "WebTable.ClickOnCorrSearch(element_identifiedBy, element_identifier, Integer.parseInt(pass_values), target_identifiedBy,Integer.parseInt(target_identifier));\n";
			break;

		case "get_cor_rowIndex":			
			keywordCode = keywordCode + "UIresult.add(String.valueOf(WebTable.GetRowIndex(element_identifiedBy, element_identifier, Integer.parseInt(target_identifiedBy), pass_values)));\n";
			break;

		case "get_textByRowCol":			
			keywordCode = keywordCode + "UIresult.add(WebTable.GetText(element_identifiedBy, element_identifier, Integer.parseInt(pass_values), Integer.parseInt(target_identifiedBy)));\n";
			break;
			
		case "check_Box" :			
			keywordCode = keywordCode + "SelectItems.selectCheckBox(element_identifiedBy,element_identifier,pass_values);\n";
			break;

		case "new_window" :			
			keywordCode = keywordCode + "array_windows = WindowClass.newWindow(driver);\n";
			break;

		case "switch_window" :			
			keywordCode = keywordCode + "WindowClass.switchWindow(driver, array_windows);\n";
			break;

		case "switch_frame" :			
			keywordCode = keywordCode + "WindowClass.switchFrame(driver, element_identifiedBy,element_identifier);\n";
			break;

		case "switch_frame_default" :			
			keywordCode = keywordCode + "WindowClass.switchFrame_default(driver);\n";
			break;
			
		case "mouse_sliding" :			
			keywordCode = keywordCode + "MouseActions.mouseSLider(driver, element_identifiedBy, element_identifier);\n";
			break;
			
		case "javascript" :
			keywordCode = keywordCode +"JavaFunctions.executeJavaScript(driver,pass_values);\n";
			break;
			
		case "javascriptOnObject" :
			keywordCode = keywordCode +"JavaFunctions.executeJavaScriptOnObject(driver,pass_values, element_identifiedBy,element_identifier);\n";
			break;

		case "scrollDown" :			
			keywordCode = keywordCode + "MouseActions.mouseScrollDown();\n";
			break;

		case "scrollUp" :			
			keywordCode = keywordCode + "MouseActions.mouseScrollUp();\n";
			break;
			
		case "AutoIT_script" :			
			keywordCode= keywordCode + "Runtime.getRuntime().exec(pass_values);\n";
			break;
			
		case "ownCode" :			
			String customCode = GenerateScript.KeywordValueList.get(9).replaceAll("[\u0000-\u001f]", "");
			keywordCode = keywordCode + customCode+"\n";
			break;


		case "getResult":			
			keywordCode = keywordCode + "UIresult.add(ElementProperty.getAttribute(element_identifiedBy, element_identifier, target_identifiedBy, target_identifier,pass_values));\n";
			break;
			
		case "screenshot":			
			keywordCode = keywordCode + "CheckPoint.windowsScreenShot();\n";
			break;
			
		case "getSubStringByLength":			
			keywordCode = keywordCode + "subStringByLength = ElementProperty.getAttribute(element_identifiedBy, element_identifier,target_identifiedBy, target_identifier,pass_values);\n";
			keywordCode = keywordCode + "UIresult.add(JavaFunctions.getstrinByLength(subStringByLength, target_identifiedBy, target_identifier));\n";
			break;

		case "getSubStringByValue"	:			
			keywordCode = keywordCode+ "subStringByValue = ElementProperty.getAttribute(element_identifiedBy, element_identifier, target_identifiedBy, target_identifier,pass_values);\n";
			keywordCode = keywordCode + "UIresult.add(JavaFunctions.getStringByValue(subStringByValue,target_identifiedBy,target_identifier));\n";
			break;

		case "seleniumScreenshot":			
			keywordCode = keywordCode + "CheckPoint.screenShot(\"screenshot\");\n";
			break;

		case "quit":		
			keywordCode = keywordCode + "BrowserClass.closeBrowser();";
			break;

		case "getList":		
			keywordCode = keywordCode + "SelectItems.getList(element_identifiedBy, element_identifier);\n";
			break;

		case "EnterByLabel":			
			keywordCode = keywordCode + "LabelFunctions.EnterByLabel(element_identifiedBy,element_identifier,pass_values);\n";
			break;

		case "WaitForElementToBeClickable":			
			keywordCode = keywordCode + "WebElementActions.ExplicitWaitElementClikable(element_identifiedBy,element_identifier,30);\n";
			break;
			
		case "waitForPageToLoad":			
			keywordCode = keywordCode + "WebElementActions.WaitForPageload();";
			break;

		case "WaitForInvisibilityOfElement"	:			
			keywordCode= keywordCode + "WebElementActions.ExplicitWaitInvisibilityOfElement(element_identifiedBy, element_identifier, Integer.parseInt(pass_values));\n";
			break;
			
		case "waitforobject" :			
			keywordCode = keywordCode + "WebElementActions.ExplicitWaitPresenceOfElement(element_identifiedBy, element_identifier, 30);\n";
			break;
			
		case "WaitForFrameSwitchToIt":			
			keywordCode = keywordCode + "WebElementActions.WaitForFrameSwitchToIt(element_identifiedBy, element_identifier, 30);\n";
			break;

		case "WebTableClick":			
			keywordCode = keywordCode + "WebTable.ClickTextProvided(element_identifiedBy, element_identifier, pass_values,target_identifiedBy);\n";
			break;

		case "WebTableCalculation":			
			keywordCode = keywordCode + "actualList = WebTable.Calculation(element_identifiedBy, element_identifier, Integer.parseInt(pass_values));\n";
			break;
			
		case "SelectRadioButton":			
			keywordCode= keywordCode + "WebElementActions.SelectRadioButton(element_identifiedBy, element_identifier);\n";
			break;
			
		case "checkObjectExists":			
			keywordCode =keywordCode +  "Verify_list= WebElementActions.ObjectExist(element_identifiedBy, element_identifier);\n";
			keywordCode = keywordCode +"resultList.add(Verify_list);\n";
			break;
			
		case "drop_down" :			
			keywordCode =keywordCode +  "SelectItems.SelectDropDown(driver,element_identifiedBy,element_identifier,pass_values,target_identifiedBy,target_identifier);\n";
			break;

		case "DropDownSelectByValue":			
			keywordCode = keywordCode + "SelectItems.DropDown_SelectValue(element_identifiedBy, element_identifier, pass_values);\n";
			break;

		case "DropDownSelectByIndex":			
			keywordCode = keywordCode + "SelectItems.DrpDwn_SelectIndex(element_identifiedBy, element_identifier, pass_values);\n";
			break;

		case"DropDownSelectByText":			
			keywordCode= keywordCode + "SelectItems.DropDown_SelectText(element_identifiedBy, element_identifier, pass_values);\n";
			break;
			
		case "DropDownSelectRandom":			
			keywordCode= keywordCode +"SelectItems.DrpDwn_SelectRandom(element_identifiedBy, element_identifier);\n";
			break;
			
		case "WbTableClick":		
			keywordCode = keywordCode + "WebTable.Click(element_identifiedBy, element_identifier, Integer.parseInt(pass_values),  Integer.parseInt(target_identifiedBy));\n";
			break;

		case "WebTableGetText":			
			keywordCode = keywordCode + "UIresult.add(WebTable.GetText(element_identifiedBy, element_identifier, Integer.parseInt(target_identifiedBy), Integer.parseInt(target_identifier)));\n";
			break;
			
		case "RunJavaScript":			
			keywordCode = keywordCode + "WebElementActions.RunJavaScript(pass_values);";
			break;

		case "DropDownGetSelectedText":			
			keywordCode = keywordCode + "UIresult.add(SelectItems.DropDown_GetSelectedText(element_identifiedBy, element_identifier));\n";
			break;

		case "KeyboardKey":			
			keywordCode = keywordCode + "KeyboardFuctions.KeyboardKeys(element_identifiedBy, element_identifier, pass_values);\n";
			break;
			
		case "startLoop":
			keywordCode = keywordCode + "for(int cnt=0;cnt<Integer.parseInt(pass_values);cnt++){\n";
			break;
			
		case "endLoop":
			keywordCode = keywordCode + "}\n";
			break;

		/*case "if-else":
			//Verifying the result
			Verify_list = VerifyTheText.funcVerifyContainsText(element_identifiedBy,element_identifier,pass_values,excel_sheet,"verify",propertyFile,UserInputData);
			IfElseStatements.ExecuteSteps(driver,test_case_name,excel_sheet, workbook_loc, workbook_name,propertyFile,UserInputData,testDataRow,cellcategory,target_identifiedBy,target_identifier, Verify_list);
			int lastStep = IfElseStatements.getLastStep(target_identifier);
			row_no = lastStep;
			break;*/
			
		case "GetTimeStamp":			
			keywordCode = keywordCode +"UIresult.add(TimeClass.getCurrentTimestamp(pass_values));\n";
			break;
			
		case "batchExecution":			
			keywordCode = keywordCode +"ThirdPartyTools.executeWindowBatch(element_identifiedBy,pass_values);\n";
			break;

		case "scan-page":			
			keywordCode = keywordCode + "PageScanner.getWebElementsFRomExistingPage(driver,pass_values,CommonMethod.applicationFolder);\n";
			break;
			
			
		case "UpperCase":			
			keywordCode = keywordCode + "UIresult.add(JavaFunctions.ToUpperCase(pass_values));\n";
			break;
			
		case "LowerCase":			
			keywordCode = keywordCode + "UIresult.add(JavaFunctions.ToLowerCase(pass_values));\n";
			break;
			
		case "Replace":			
			keywordCode = keywordCode + "UIresult.add(JavaFunctions.replace(pass_values, target_identifiedBy, target_identifier));\n";
			break;
			
		case "brokenLinks":
			keywordCode = keywordCode + "BrokenLinks.checkBrokenLinks(driver);\n";
			break;
			
		case "raedCSV" :
			keywordCode = keywordCode +"CSVFunctions.readCSV(element_identifier,pass_values);\n";
			break;

		case "writeCSV" :
			keywordCode = keywordCode +"CSVFunctions.writeCSV(element_identifier, pass_values);\n";
			break;

		case "appendCSV" :
			keywordCode = keywordCode +"CSVFunctions.appendCSV(element_identifier, pass_values);\n";
			break;

		case "validateWordCSV" :
			keywordCode = keywordCode+"Verify_list = CSVFunctions.validateWord(element_identifier, pass_values);\n";
			keywordCode = keywordCode +"resultList.add(Verify_list);\n";
			//ExcelFunctions.writeToExcel(wb_loc, wb_name, excel_sheet, row_no, Verify_list);
			break;

		case "validateRecordCSV":
			keywordCode = keywordCode+"Verify_list = CSVFunctions.validateRecord(element_identifier, pass_values);\n";
			keywordCode = keywordCode +"resultList.add(Verify_list);\n";
			//ExcelFunctions.writeToExcel(wb_loc, wb_name, excel_sheet, row_no, Verify_list);
			break;
			
		case "XML_listTags" :
			keywordCode = keywordCode+"XMLParserFunctions.listXmlValuesByTag(element_identifier, pass_values);\n";
			break;

		case "XML_findValuesByTagName" :
			keywordCode = keywordCode+"UIresult.add(XMLParserFunctions.findXmlValuesByTag(element_identifier, pass_values, element_identifiedBy,target_identifiedBy));\n";
			break;

		case "XML_findValuesByAttribute" :
			keywordCode = keywordCode+"Verify_list=XMLParserFunctions.findXMLValueByAttribute(element_identifier, pass_values, element_identifiedBy, target_identifiedBy);\n";
			keywordCode = keywordCode +"resultList.add(Verify_list);\n";
			break;
			
		case "XML_getValueByTagName":
			keywordCode = keywordCode+"xmlValues.addAll(XMLParserFunctions.getValueByTagName(element_identifier, pass_values, element_identifiedBy));\n";
			break;
			
		case "TestCaseAsStep":
			String calledTestCase = CallTestCase.callTestMethod(GenerateScript.KeywordValueList.get(9), GenerateScript.KeywordValueList.get(7));
			keywordCode = keywordCode+ calledTestCase + ";\n";
			break;
			
		case "start-if":
			keywordCode = "ActionPerformer.assignExcelValues("+ row_no+",UIresult,dbResult);\n";
			keywordCode = keywordCode + "if(IfElseStatements.getConditionStatus(pass_values,resultList,UIresult,allResults)){\n";
			return keywordCode;
			
		case "end-if":
			keywordCode = "}\n";
			return keywordCode;
			
		case "start-else":
			keywordCode ="else {\n";
			return keywordCode;
			
		case "end-else":
			keywordCode = "}\n";
			return keywordCode;
			
		case "checkElementSelection":
			keywordCode = keywordCode + "Verify_list= WebElementActions.ElementSelected(element_identifiedBy, element_identifier);\n";
			keywordCode = keywordCode +"resultList.add(Verify_list);\n";			
			break;
			
		case "checkAllResultStatus":
			keywordCode = keywordCode +"VerifyTheText.getAllResultStatus(resultList, \"write\");\n";
			break;
			
		case "writeValueFromUI":
			keywordCode = keywordCode + "ExcelFunctions.storeValue(UIresult,pass_values,target_identifiedBy);";
			break;
			
		case "writeValueFromDB":
			keywordCode = keywordCode + "ExcelFunctions.storeValue(dbResult,pass_values,target_identifiedBy);";
			break;
		}
		keywordCode = keywordCode + "log.info(\"=========================================================\");\n\n";
		return keywordCode;
	}//End of Switch
}


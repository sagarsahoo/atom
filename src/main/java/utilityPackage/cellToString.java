package utilityPackage;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.testng.Assert;

public class cellToString {

	static Logger cellToString_log = Logger.getLogger(cellToString.class.getName());

	//Converting the cell value to string/numeric for HSSF
	public  static String cellTostring_HSSF(HSSFCell cell) {
		try{
			int type;
			Object result;
			type = cell.getCellType();
			switch(type) {
			case 0 :
				result = cell.getNumericCellValue();
				int value = (int)Double.parseDouble(result.toString());
				String pass_value = Integer.toString(value);
				return pass_value.trim();

			case 1 :
				result = cell.getStringCellValue();
				return result.toString().trim();

			case 3:
				if(!cell.getBooleanCellValue()){
					cellToString_log.error("EMPTY CELL in --> ("+cell.getRowIndex()+","+cell.getColumnIndex()+") in the sheet name '"+cell.getSheet().getSheetName()+"'");
				}
				throw new RuntimeException("UNSUPPORTED CELL TYPE");
			}
		}catch(Exception e){
			cellToString_log.error("The exception in cell to string is :- ", e);
			Assert.fail("Exception occured in cell to string conversion");
		}
		return null;
	}

	public  static String cellTostring_XSSF(XSSFCell cell) {
try{
		int type;
		Object result;
		type = cell.getCellType();
		switch(type) {
		case 0 :
			result = cell.getNumericCellValue();
			int value = (int)Double.parseDouble(result.toString());
			String pass_value = Integer.toString(value);
			return pass_value;
		case 1 :
			result = cell.getStringCellValue();
			return result.toString();
		case 3 :
			if(!cell.getBooleanCellValue()){
				cellToString_log.error("EMPTY CELL in --> ("+cell.getRowIndex()+","+cell.getColumnIndex()+") in the sheet name '"+cell.getSheet().getSheetName()+"'");
			}
			throw new RuntimeException("no cell type");
		}
}catch(Exception e){
	cellToString_log.error("The exception in cell to string is :- ", e);
	Assert.fail("Exception occured in cell to string conversion");
}
return null;
	}
}

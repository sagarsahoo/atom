package utilityPackage;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class IfElseStatements {

	static  Logger ifelselog = Logger.getLogger(IfElseStatements.class);
	static int lastRowNum = 0;

	@Test 
	//Get the decision of the condition
	public static boolean getConditionStatus(String typeOfAction,List<Boolean> previousResultList,List<String> UiValueList,boolean allResult){ //ifPresent,ifselected,getValue
		try{
			if(typeOfAction.contains("ifPresent")){
				return WebElementActions.ObjectExist(ActionPerformer.element_identifiedBy, ActionPerformer.element_identifier);
			}else if(typeOfAction.contains("ifselected")){
				return WebElementActions.ElementSelected(ActionPerformer.element_identifiedBy, ActionPerformer.element_identifier);
			}else if(typeOfAction.contains("previousStoreValue")){
				//TODO
				String previousValueStored = UiValueList.get(UiValueList.size()-1);
				return getAdditionalConditionStatus(previousValueStored);
			}else if(typeOfAction.contains("true")|| typeOfAction.contains("false")){ //For the previous result
				//Get the index to be referred
				String index = typeOfAction.substring(typeOfAction.indexOf("("),typeOfAction.indexOf(")"));
				if(index.length()>0){
					return previousResultList.get(Integer.parseInt(index));
				}else{
					return previousResultList.get(previousResultList.size()-1);
				}			  
			}else if(typeOfAction.contains("allResult")){
				return VerifyTheText.getAllResultStatus(previousResultList, "NA");
			}else{
				String previousValueStored = UiValueList.get(UiValueList.size()-1);
				return getAdditionalConditionStatus(previousValueStored);
			}
		}catch(Exception e){
			ifelselog.error("Exception in getConditionStatus() - "+ e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
		return false;
	}
	
	//Getting the status as per the addition condition provided
	private static boolean getAdditionalConditionStatus(String value1){
		try{
			String operator;
			String value2;
			if(value1.contains("==")||value1.contains("!=")){
				operator = ActionPerformer.target_identifiedBy.substring(0,2);
				value2 = ActionPerformer.target_identifiedBy.substring(2).trim();
			}else{
				operator = ActionPerformer.target_identifiedBy.substring(0,1);
				value2 = ActionPerformer.target_identifiedBy.substring(1).trim();
			}			
			switch(operator){
			case "<":
				return Float.parseFloat(value1) <  Float.parseFloat(value2);
				
			case ">":
				return Float.parseFloat(value1) >  Float.parseFloat(value2);
				
			case "==":
				return Float.parseFloat(value1) ==  Float.parseFloat(value2);
				
			case "!=":
				return Float.parseFloat(value1) !=  Float.parseFloat(value2);
			}
		}catch(Exception e){
			ifelselog.error("Exception in getConditionStatus() - "+ e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
		return false;
	}

	//
	public static void ExecuteSteps(WebDriver _driver,String test_case_name,String testcase_sheet, String wb_loc,String ScriptingFileName,String propertyFile,String[][] UserInputData,int testDataRow,String cellcategory,String ifSteps,String elseSteps,boolean result) {
		try{
			if(result==true){
				//Getting the if statements
				String ifStatementsRange = Looping.modifyTestStepRange(ifSteps);
				String[][] stepRangeDataForIf = ExcelFunctions.getTestSteps(testcase_sheet, wb_loc, ScriptingFileName, ifStatementsRange);

				// _driver = ActionPerformer.performAction(stepRangeDataForIf, _driver, test_case_name, testcase_sheet, wb_loc, ScriptingFileName, propertyFile, UserInputData, testDataRow, cellcategory);//TODO
			}
			else{
				//Getting the if statements
				String elseStatementsRange = Looping.modifyTestStepRange(ifSteps);
				String[][] stepRangeDataForElse = ExcelFunctions.getTestSteps(testcase_sheet, wb_loc, ScriptingFileName, elseStatementsRange);

				// _driver = ActionPerformer.performAction(stepRangeDataForElse, _driver, test_case_name, testcase_sheet, wb_loc, ScriptingFileName, propertyFile, UserInputData, testDataRow, cellcategory);//TODO
			}
		}catch(Exception e){
			ifelselog.error("Exception in ifelse test execution is - "+ e);
			ExceptionActions.performAfterExceptionOccurs(_driver);
		}
	}

	//To get the last index of the test step
	public static int getLastStep(String elseStepRange){
		int row=0;
		String elseEndStep=null;
		try{
			String[] elseSteps = SplitString.stringSplit(elseStepRange);
			if(elseSteps.length<2){
				elseEndStep=elseSteps[0];
			}else{
				elseEndStep = elseSteps[1];
			}

			String endStepIndex = elseEndStep.substring(3);
			row = Integer.parseInt(endStepIndex);
			ifelselog.info("Last step of the else condition - "+ row);
			return row;  
		}catch(Exception e){
			ifelselog.error("Exception in getting last step for the else condition - "+ e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
			return row;  
		}
	}


	public static String ifElseStatements(){
		String conditionalStatements=null;
		int ifStartRow;
		int elseStartRow;
		try{
			//If-statements
			if(ActionPerformer.target_identifiedBy.contains("|")){
				String[] getIfRows = SplitString.stringSplit(ActionPerformer.target_identifiedBy);
				ifStartRow = Integer.parseInt(getIfRows[0]);
				int ifEndRow = Integer.parseInt(getIfRows[1]);
				conditionalStatements = conditionalStatements + "if(IfElseStatements.getConditionStatus(pass_values)){\n";
				for(int cnt =ifStartRow;cnt<=ifEndRow;cnt++){
					conditionalStatements = conditionalStatements + GenerateCodeForKeywords2.getCode(cnt);
				}
			}else{
				conditionalStatements = conditionalStatements + "if(IfElseStatements.getConditionStatus(pass_values)){\n";
				ifStartRow = Integer.parseInt(ActionPerformer.target_identifiedBy);
				conditionalStatements = conditionalStatements + GenerateCodeForKeywords2.getCode(ifStartRow);
			}
			conditionalStatements = conditionalStatements + "\n}";		  

			//Else-statements
			if(ActionPerformer.target_identifier.contains("|")){
				String[] getElseRows = SplitString.stringSplit(ActionPerformer.target_identifier);
				elseStartRow = Integer.parseInt(getElseRows[0]);
				int elseEndRow = Integer.parseInt(getElseRows[1]);
				conditionalStatements = conditionalStatements + "else {\n";
				for(int cnt =elseStartRow;cnt<=elseEndRow;cnt++){
					conditionalStatements = conditionalStatements + GenerateCodeForKeywords2.getCode(cnt);
				}
			}else{
				conditionalStatements = conditionalStatements + "else {\n";
				elseStartRow = Integer.parseInt(ActionPerformer.target_identifier);
				conditionalStatements = conditionalStatements + GenerateCodeForKeywords2.getCode(elseStartRow);
			}
			conditionalStatements = conditionalStatements + "\n}";

			//GenerateScript.toInsert = GenerateScript.toInsert.concat(conditionalStatements);
		}catch(Exception e){
			ifelselog.error("Exception in getConditionStatus() - "+ e);
			ExceptionActions.performAfterExceptionOccurs(ActionPerformer.driver);
		}
		return conditionalStatements;
	}
}

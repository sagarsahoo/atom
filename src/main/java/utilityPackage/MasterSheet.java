package utilityPackage;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MasterSheet extends CommonMethod{

	static Logger log = Logger.getLogger(MasterSheet.class.getName());


	@Test
	public static String masterSheetData(String[][] masterSheet_data,String parameterName) {

		try{
			String pass_values=null;
			log.info("ParameterName - "+parameterName );
			String[] parameterList = SplitString.stringSplit(parameterName);
			String parameterValue = parameterList[1];
			int EnvCellLocation = ExcelFunctions.getColumnPosition(wb_loc, wb_name, "Master_sheet", "Env");
			int parameterHeaderLocation = ExcelFunctions.getColumnPosition(wb_loc, wb_name, "Master_sheet", "parameter");
			int valueForParameter = ExcelFunctions.getColumnPosition(wb_loc, wb_name, "Master_sheet", "value");

			if(parameterList.length>0){
				//String env = parameterList[0];
				String env = CommonMethod.ExecutionEnvironment;
				for (int master_row=1 ; master_row < masterSheet_data.length ; master_row ++){
					log.info("master data traversed ref_testStep- "+masterSheet_data[master_row][parameterHeaderLocation]);
					if(masterSheet_data[master_row][parameterHeaderLocation].equalsIgnoreCase(parameterValue) && masterSheet_data[master_row][EnvCellLocation].equalsIgnoreCase(env)){
						pass_values=masterSheet_data[master_row][valueForParameter];
						log.info("value in master sheet - "+pass_values );
						break;
					}
				}
				return pass_values;
			}

			/*else{
				for (int master_row=1 ; master_row < masterSheet_data.length ; master_row ++){
					log.info("master data traversed ref_testStep- "+masterSheet_data[master_row][parameterHeaderLocation]);
					if(masterSheet_data[master_row][parameterHeaderLocation].equalsIgnoreCase(parameterEnvAndName)){
						pass_values=masterSheet_data[master_row][valueForParameter];
						log.info("value in master sheet - "+pass_values );
						break;
					}
				}
				return pass_values;
			}*/
		}
		catch(Exception e){

			log.error("Exception in master fetch data :",e);
			Assert.fail("Failure as Exception Occured");
		}
		return null;
	}
}

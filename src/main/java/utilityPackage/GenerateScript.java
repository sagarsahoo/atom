package utilityPackage;
import selfitGui.*;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.record.CommonObjectDataSubRecord;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class GenerateScript {

	static String projectPath = System.getProperty("user.dir");
	static File javaFile;
	static String className;
	static String toInsert;
	static FileWriter writer;
	static String toWrite="";
	static List<String> KeywordValueList = new ArrayList<String>();
	static int flagBT=0;
	static int flagT =0;
	static int flagAT=0;
	static String tc_id,test_case,exec_flag,test_step_no,test_step,refKeyword,ele_type,element_identifiedBy,
	element_identifier,pass_values,target_identifiedBy,target_identifier,cellCategory;
	static int sl_no;

	@Test
	public static void scriptGeneration() {		
		creteJavaFile(TestCreation.TCSelected+".java");//Create java file	
		createJavaFileFormat(TestCreation.ProjectSelected);//Creating format		
		insertCommandByKeywords();//read keyword and insert		
		endJavaFileFormat();//End format
		//clearContextInJavaClass(); //CLearing the file context
		JOptionPane.showMessageDialog(null, "Script Generated Successfully!");

	}
	
	

	// *************************************************************************************************************************

	private static void creteJavaFile(String classFileName){
		try{
			className = classFileName.substring(0, classFileName.indexOf("."));
			System.out.println("Class name is - "+ className);
			javaFile = new File(projectPath + "\\src\\main\\java\\"+TestCreation.ProjectSelected + "\\"+ classFileName );
			if(javaFile.exists()){
				FileUtils.deleteQuietly(javaFile);
			}else{
				Boolean bool = javaFile.createNewFile();
				System.out.println("java file created - "+ bool);
			}
		}catch(Exception e){
			System.out.println("Exception in creating java file"+e);
		}
	}

	private static void clearContextInJavaClass(){
		try{
			PrintWriter pw = new PrintWriter(javaFile);
			pw.close();
		}catch(Exception e){
			System.out.println("Exception in clearing context in java file - "+ e);
		}
	}

	private static void createJavaFileFormat(String packageName){
		try{
			//Write Content
			writer = new FileWriter(javaFile);
			includePackageInScript(packageName); //Importing the packages
			toInsert = toInsert.concat("@Listeners(utilityPackage.Listener.class)\n");
			toInsert = toInsert.concat("public class "+ className + " extends ActionPerformer "+"{\n");
			includeVariableDeclare(); //Variable declarations
			toInsert = toInsert.concat("\n@BeforeTest\n");
			toInsert = toInsert.concat("public static void beforeTestScenario(){\n");
			toInsert = toInsert.concat("try{\n");
			toInsert = toInsert.concat("CommonMethod CM = new CommonMethod(\""+ TestCreation.TCSelected + "\",\"" + TestCreation.ProjectSelected + "_suite\");\n");

		}catch(Exception e){
			System.out.println("Exception in writing into the file");
		}
	}

	private static void insertCommandByKeywords(){
		try{
			//Get the scripting excel data in 2D variable
			CommonMethod CM = new CommonMethod(TestCreation.TCSelected,TestCreation.ProjectSelected+ "_suite"); //Need to take the parameters from SELFIT
			String code = "";
			//Iterating the test steps
			for(int row_no = 1 ; row_no < CommonMethod.scripting_data.length ; row_no ++){
				assignExcelValues(row_no); //Assigning values to the variables
				if(exec_flag.equalsIgnoreCase("Y") && cellCategory.equalsIgnoreCase("BT")){ //TODO
					//assignExcelValues(row_no); 
					addValuesToList();
					code = GenerateCodeForKeywords2.getCode(row_no);
					toInsert = toInsert.concat(code);
				}else if(exec_flag.equalsIgnoreCase("Y") && cellCategory.equalsIgnoreCase("T")){
					endOfBT(); //ending the before method
					//assignExcelValues(); //TODO
					addValuesToList();
					code = GenerateCodeForKeywords2.getCode(row_no);
					toInsert = toInsert.concat(code);					
				}else if(exec_flag.equalsIgnoreCase("Y") && cellCategory.equalsIgnoreCase("AT")){
					//TODO
				}
			}
		}catch(Exception e){
			System.out.println("Exception in insrting commands as per the keywords - "+ e);
		}
	}
	
	

	private static void endJavaFileFormat(){
		try{
			FileWriter writer = new FileWriter(javaFile);		
			//toInsert = toInsert.concat("\n}"); //end of method
			if(flagBT==0){
				closeBTwithT();
			}else if(flagT==0){			
				endOfMethod();
			}else if(flagAT==0){
				endOfBT();
			}
			toInsert = toInsert.concat("\n}"); //end of class
			writer.write(toInsert);		  
			writer.close();

		}catch(Exception e){
			System.out.println("Exception occured in ending the format is - "+ e);
		}
	}
	
	private static void endOfBT(){
		if(flagBT==0){
			endOfMethod(); //Ending of BT section
			createMethod("Test"); //Declaring T section
			//endOfMethod();//CLosing try method
			flagBT++;
		}
	}
	
	private static void closeBTwithT(){
		endOfMethod(); //Ending of BT section
		createMethod("Test"); //Declaring T section
		endOfMethod();//CLosing try method
	}
	
	private static void createMethod(String methodTag){
		toInsert = toInsert.concat("\n@"+ methodTag+"\n"); //declare tag
		toInsert = toInsert.concat("public static void "+ methodTag+"scenario(){\ntry{\n"); //declare method name
		toInsert = toInsert.concat("CommonMethod CM = new CommonMethod(\""+ TestCreation.TCSelected + "\",\"" + TestCreation.ProjectSelected + "_suite\");\n");
	}
	
	private static void endOfMethod(){
		toInsert = toInsert.concat("\n}\n"); //end of Try 
		toInsert = toInsert.concat("catch(Exception e){\n	System.out.println(\"Exception occured is - \"+ e);\n\n}"); //catch and end of catch
		toInsert = toInsert.concat("\n}\n"); //end of method
	}
	
	private static void addValuesToList(){
		try{
			KeywordValueList.clear();
			KeywordValueList.add(tc_id);
			KeywordValueList.add(test_case);
			KeywordValueList.add(exec_flag);
			KeywordValueList.add(test_step_no);
			KeywordValueList.add(test_step);
			KeywordValueList.add(refKeyword);
			KeywordValueList.add(ele_type);
			KeywordValueList.add(element_identifiedBy);
			KeywordValueList.add(element_identifier);
			KeywordValueList.add(pass_values);
			KeywordValueList.add(target_identifiedBy);
			KeywordValueList.add(target_identifier);
			KeywordValueList.add(Integer.toString(sl_no));
			KeywordValueList.add(cellCategory);			
		}catch(Exception e){
			System.out.println("Exception in adding values to the keyword list - "+ e);
		}
	}
	
	private static void assignExcelValues(int row_no){
		/*ActionPerformer.ObjectValuesList.addAll(ExcelValues.getObjectlocators(ActionPerformer.element_identifiedBy,ActionPerformer.element_identifier, CommonMethod.testDataRow, CommonMethod.UserInputData));
		ActionPerformer.TargetObjectValuesList.addAll(ExcelValues.getTargetObjectLocators(CommonMethod.scripting_data, ActionPerformer.target_identifiedBy, ActionPerformer.target_identifier));
		ActionPerformer.element_identifiedBy = ActionPerformer.ObjectValuesList.get(0);
		ActionPerformer.element_identifier = ActionPerformer.ObjectValuesList.get(1);
		ActionPerformer.pass_values = ExcelValues.getPassedValues(CommonMethod.scripting_data, ActionPerformer.pass_values, CommonMethod.testDataRow, CommonMethod.UserInputData);						
		ActionPerformer.target_identifiedBy = ActionPerformer.TargetObjectValuesList.get(0);
		ActionPerformer.target_identifier = ActionPerformer.TargetObjectValuesList.get(1);*/
		
		
		tc_id = CommonMethod.scripting_data[row_no][0];
		test_case = CommonMethod.scripting_data[row_no][1];
		exec_flag = CommonMethod.scripting_data[row_no][2];
		test_step_no = CommonMethod.scripting_data[row_no][3];
		test_step = CommonMethod.scripting_data[row_no][4];
		refKeyword = CommonMethod.scripting_data[row_no][5];
		ele_type = CommonMethod.scripting_data[row_no][6];
		element_identifiedBy = CommonMethod.scripting_data[row_no][7];
		element_identifier = CommonMethod.scripting_data[row_no][8];
		pass_values = CommonMethod.scripting_data[row_no][9];
		target_identifiedBy = CommonMethod.scripting_data[row_no][10];
		target_identifier = CommonMethod.scripting_data[row_no][11];
		sl_no = Integer.parseInt(CommonMethod.scripting_data[row_no][0]);
		cellCategory = CommonMethod.scripting_data[row_no][12];
	}

	
	private static void includePackageInScript(String packageName){
		toInsert = toWrite.concat("package " + packageName + ";");			
		toInsert = toInsert.concat("\nimport org.testng.annotations.*;");
		toInsert = toInsert.concat("\nimport utilityPackage.*;");
		toInsert = toInsert.concat("\nimport selfitGui.*;");
		toInsert = toInsert.concat("\nimport java.util.ArrayList;");
		toInsert = toInsert.concat("\nimport java.util.List;");
		toInsert = toInsert.concat("\nimport org.openqa.selenium.*;");
		toInsert = toInsert.concat("\nimport org.openqa.selenium.interactions.Actions;\n");
		toInsert = toInsert.concat("\nimport org.openqa.selenium.support.ui.WebDriverWait;\n");
		toInsert = toInsert.concat("import org.apache.log4j.Logger;\n");
	}
	
	private static void  includeVariableDeclare(){
		//toInsert = toInsert.concat("public static WebDriver _driver;\n");
		toInsert = toInsert.concat("public static Actions act;\n");
		toInsert = toInsert.concat("static WebElement we;\n");
		toInsert = toInsert.concat("public static WebDriverWait wait;\n");
		toInsert = toInsert.concat("static List<String> UIresult = new ArrayList<String> ();\n");
		toInsert = toInsert.concat("static List<String> dbResult = new ArrayList<String> ();\n");
		toInsert = toInsert.concat("static List<String> xmlValues = new ArrayList<String>();\n");
		toInsert = toInsert.concat("static boolean Verify_list = false;\n");
		toInsert = toInsert.concat("static boolean allResults = false;\n");
		toInsert = toInsert.concat("static ArrayList<Boolean> resultList =  new ArrayList<Boolean>();\n");
		toInsert = toInsert.concat("static String dbQuery,subStringByLength,subStringByValue,customXpath;\n");
		toInsert = toInsert.concat("static  Logger log = Logger.getLogger("+TestCreation.TCSelected+".class);\n");
		//toInsert = toInsert.concat("CommonMethod CM = new CommonMethod(\""+ TestCreation.TCSelected + "\",\"" + TestCreation.ProjectSelected + "_suite\");\n");
	}
}

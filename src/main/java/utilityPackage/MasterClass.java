package utilityPackage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.TestNG;


public class MasterClass {
	
	
	
	static Logger logger = Logger.getLogger(MasterClass.class.getName());
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		 try{
			 
			 String applicationName[] = args;
			 String SuiteSheet = applicationName[0] + "_suite" ;
			 String TestDataFolder = applicationName[0];
			 String Projectpath = System.getProperty("user.dir");	
			 String mappingSheetLocation = Projectpath + "\\TestData\\";	
			 String ClassFileLocation = Projectpath + "\\src\\main\\java\\"+ applicationName[0] + "\\";
				
				
				List<String> javaFiles = new ArrayList<String> ();
				List<String> testCaseNames = new ArrayList<String> ();
				String[][] suiteMappingData;
				String xmlFile = SuiteSheet+ "TestNG.xml";
				
				String packageName=applicationName[0];
				System.out.println("packageNmae - "+ applicationName[0]);
				
				//Mapping test cases into Execution mapping sheet
				File ExecutionMappingFile = new File(Projectpath + "\\TestData\\TestExecutionMapping.xls");
			  	CommonMethod.mapTestCases(ExecutionMappingFile,SuiteSheet,TestDataFolder);
				
			  	
				//Getting list of java files in the suite package  
				  javaFiles = CommonMethod.getJavaFiles(ClassFileLocation,javaFiles);
				  
				  //Getting the test cases from ExecutionMappingSheeet flagged as Y and creating class file if not present
				  CommonMethod.checkJavaFiles(mappingSheetLocation,javaFiles,SuiteSheet,packageName);
				  
				 
				  
				  //Creation of xml file
				  String sourceFile = "excel";	  
				  CreateTestNGxml.getFromExcel(sourceFile,SuiteSheet);
				  logger.info("XML created");
				  
				 /* //Execute the class files
				  Thread.sleep(10000); //Waiting for the projet to be refreshed
				  TestNG runner=new TestNG();
				  List<String> suitefiles=new ArrayList<String>();
				  suitefiles.add(Projectpath+ "\\" + xmlFile);
				  runner.setTestSuites(suitefiles);
				  runner.run();*/

		  }
		  catch(Exception e){
			  logger.error("The exception in MasterClasCreation  is - ",e);
		  }
		  
		  	
	  }

	}
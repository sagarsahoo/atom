package utilityPackage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

public class OpenBrowser {
	protected static WebDriver wd_driver;
	static String path = System.getProperty("user.dir");
	static Logger log = Logger.getLogger(OpenBrowser.class);

	public static WebDriver open_browser(String browser_name,String profilenameForFirefirefox) throws IOException{
		try{
			
			
			log.info("Opening the Browser");
			//For FireFox browser
			if(browser_name.equalsIgnoreCase("FF")){
				try{
					ProfilesIni profile = new ProfilesIni();
					FirefoxProfile fp = profile.getProfile(profilenameForFirefirefox);
					if(fp==null){
						throw new RuntimeException();
					}
					wd_driver = new FirefoxDriver(fp);
				}catch(Exception e){
					log.info("Profile name '"+profilenameForFirefirefox+"' is not found, So launching the customized profile");
					File FirefoxProfilePath = new File(path+"/TestResources/SeleniumFirefoxProfile");
					FirefoxProfile profile = new FirefoxProfile(FirefoxProfilePath);
					wd_driver = new FirefoxDriver(profile);
					//If any exception occurred launch browser with default profile.
				}
			}else if(browser_name.equalsIgnoreCase("Chrome")){//For chrome browser
				System.setProperty("webdriver.chrome.driver", path+"\\TestResources\\ChromeDriver\\chromedriver.exe");
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				ChromeOptions options = new ChromeOptions();
				options.addArguments("test-type");
				options.addArguments("start-maximized");
				capabilities.setCapability("chrome.binary",path+"\\TestResources\\ChromeDriver\\chromedriver.exe");
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				wd_driver = new ChromeDriver(capabilities);
			}else if (browser_name.equalsIgnoreCase("IE")){
				System.setProperty("webdriver.ie.driver", path+"\\TestResources\\InternetExplorer\\IEDriverServer.exe");
				DesiredCapabilities caps = DesiredCapabilities.internetExplorer(); 
				caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true); 
				caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				wd_driver = new InternetExplorerDriver(caps);
			}
			ActionPerformer.driver=wd_driver;  // Assigning driver to the actionperformer driver
			wd_driver.manage().window().maximize();
			wd_driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			wd_driver.manage().deleteAllCookies();

		}catch(Exception e){
			log.error("The Exception is Open Browser is - ",e);
			Assert.fail("Failure as exception occured");
			wd_driver.quit();
			return null;
		}
		return wd_driver;
	}

	// Opening browser from config file
	public static WebDriver open_browser_config(WebDriver wd_driver,String propertyFile) throws IOException{
		try{
			String browser = RepositoryObject.IdentifierRepo(propertyFile,"browser_name");
			log.info("Browser name is :" + browser);
			switch(browser){
			case "IE":
				log.info("Opening browser"+ browser);
				System.setProperty("webdriver.ie.driver", path+"\\TestResources\\InternetExplorer\\IEDriverServer.exe");
				DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
				wd_driver.manage().window().maximize();
			}
		}catch(Exception e){
			log.error("Exception occured - ",e);
		}
		return wd_driver;
	}

	//Open browser using GRID concept
	public static RemoteWebDriver openBrowserGrid(RemoteWebDriver grid_driver,String grid_Hostname,String grid_PortNumber,String grid_browser,String url){
		try{
			DesiredCapabilities dr=null;
			if(grid_browser.equals("firefox")){
				dr=DesiredCapabilities.firefox();
				dr.setBrowserName("firefox");
				dr.setPlatform(Platform.WINDOWS);
			}else{
				dr=DesiredCapabilities.internetExplorer();
				dr.setBrowserName("iexplore");
				dr.setPlatform(Platform.WINDOWS);
			}
			//String user_url = "http://" + grid_Hostname + ":" + grid_PortNumber + 
			grid_driver =new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), dr);
			return grid_driver;
		}
		catch(Exception e){
			log.error("The Exception in the OpenBrowserGrid class is - ",e);
		}
		return grid_driver;
	}
	public static String GetBrowserInfo(){

		int version;
		String returValue = "";
		String browswerName= "";
		String browserAgent = (String) ((JavascriptExecutor) wd_driver).executeScript("return navigator.userAgent");

		if((version=browserAgent.indexOf("MSIE 9.0"))!=-1){
			returValue = "9";	//browserAgent.substring(version+5,version+6);
			browswerName="IE";
		}else if((version=browserAgent.indexOf("MSIE 7.0"))!=-1){
			returValue = "7";	//browserAgent.substring(version+5,version+6);
			browswerName="IE";
		}else if((version=browserAgent.indexOf("MSIE 8.0"))!=-1){
			returValue = "8";	//browserAgent.substring(version+5,version+6);
			browswerName="IE";
		}else if((version=browserAgent.indexOf("rv:11"))!=-1){
			returValue = "11";	//browserAgent.substring(version+3,version+5);
			browswerName="IE";
		}else if((version=browserAgent.indexOf("Chrome"))!=-1){
			returValue =  browserAgent.substring(version+7,version+9);
			browswerName = "Chrome";
		}else if((version=browserAgent.indexOf("Firefox"))!=-1){
			returValue = browserAgent.substring(version+8);
			browswerName = "Firefox";
		}
		return browswerName+returValue;		
	}
	
	// for adding proxy
	/*
	 FirefoxProfile profile = new FirefoxProfile(); 
	 profile.addAdditionalPreference("network.proxy.http", "your-office-proxy"); 
	profile.addAdditionalPreference("network.proxy.http_port", "your-office-proxy-port"); 
	WebDriver driver = new FirefoxDriver(profile);
	SECOND:
	Proxy proxy = new Proxy();
  	proxy.setHttpProxy("localhost:8090");
  	proxy.setFtpProxy("localhost:8090");
  	proxy.setSslProxy("localhost:8090");
  	DesiredCapabilities capabilities = new DesiredCapabilities();
  	capabilities.setCapability(CapabilityType.PROXY, proxy);
  	driver = new FirefoxDriver(capabilities);
  	this.setDriver(driver);
  	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 */

}



package utilityPackage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class EmailFunctions {
  @Test
  public static void SendMail(WebDriver driver,String toAndCC, String EmailContent){
		try{
			String[] Receptant = toAndCC.split(";");
			String[] to = Receptant[0].split("\\|");
			String[] cc = Receptant[1].split("\\|");
			
			String userName = null, password = null;
			WebElement we = null;

			driver = OpenBrowser.open_browser("FF", "default");
			ProvideURL.baseUrl(driver,"https://email.accenture.com/");

			if(WebElementActions.ObjectExist("id", "userNameInput")){
				we = WebElementActions.GetWebElement("id", "userNameInput");
				ConditionalEnterText.enterText(we,userName);
			}

			if(WebElementActions.ObjectExist("id", "passwordInput")){
				we = WebElementActions.GetWebElement("id", "passwordInput");
				ConditionalEnterText.enterText(we,password);
			}

			if(WebElementActions.ObjectExist("id", "submitButton")){
				we = WebElementActions.GetWebElement("id", "submitButton");
				we.click();
			}
			
			WebElementActions.ExplicitWaitInvisibilityOfElement("xpath", "//div[@id='mfaArea']//img", 15);
			if(WebElementActions.ObjectExist("xpath", "//*[@id='O365_MainLink_NavMenu']")){//Check for the outlook version if it's 2013 change it to 2013 Light version

				WebElementActions.GetWebElement("xpath", "//*[@id='O365_MainLink_Settings']").click();
				WebElementActions.GetWebElement("xpath", "//button[@aria-label='Options']").click();
				
				WebElementActions.GetWebElement("xpath", "//*[text()='General']").click();
				WebElementActions.GetWebElement("xpath", "//*[text()='Light version']").click();
				WebElementActions.GetWebElement("xpath", "(//button[@role='checkbox'])[last()]/span[1]").click();
				WebElementActions.GetWebElement("xpath", "//button[@title='Save']").click();

				//Logout
				WebElementActions.GetWebElement("xpath", "//button[@autoid='_ho2_0']").click();
				WebElementActions.GetWebElement("xpath", "//*[text()='Sign out']/../..").click();
//				Thread.sleep(300*1000);
				driver.get("https://email.accenture.com/");

				if(WebElementActions.ObjectExist("id", "userNameInput")){
					we = WebElementActions.GetWebElement("id", "userNameInput");
					ConditionalEnterText.enterText(we,userName);
				}
				if(WebElementActions.ObjectExist("id", "passwordInput")){
					we = WebElementActions.GetWebElement("id", "passwordInput");
					ConditionalEnterText.enterText(we,password);
				}
				if(WebElementActions.ObjectExist("id", "submitButton")){
					we = WebElementActions.GetWebElement("id", "submitButton");
					we.click();
				}
			}//if loop ends here
			
			WebElementActions.ExplicitWaitInvisibilityOfElement("id", "cssloadingSpinnerContainer", 15);
			WebElementActions.ExplicitWaitPresenceOfElement("id", "lnkHdrnewmsg", 15);

			WebElementActions.GetWebElement("id", "lnkHdrnewmsg").click();//New Button

			we = WebElementActions.GetWebElement("id","txtsbj");
			ConditionalEnterText.enterText(we, "sample subject");

			for(String CC:cc){
				we = WebElementActions.GetWebElement("id", "txtcc");
				ConditionalEnterText.enterText(we,CC);
				WebElementActions.GetWebElement("id", "lnkHdrchecknames").click();
			}
			for(String To:to){
				we = WebElementActions.GetWebElement("id", "txtto");
				ConditionalEnterText.enterText(we,To);
				WebElementActions.GetWebElement("id", "lnkHdrchecknames").click();
			}
			we = WebElementActions.GetWebElement("name", "txtbdy");
			ConditionalEnterText.enterText(we,"SUBJECT");

			WebElementActions.GetWebElement("id", "lnkHdrattachfile").click();
			WebElementActions.GetWebElement("id", "attach").sendKeys(CheckPoint.currentFile);//Set path for the file
			WebElementActions.GetWebElement("id", "attachbtn").click();//Attach button
			WebElementActions.GetWebElement("id", "lnkHdrdone").click();
			WebElementActions.GetWebElement("id", "lnkHdrsend").click();

		}catch(Exception e){

		} finally {
			if(driver!=null){
				driver.quit();
			}
		}
	}
	
}

package ownExp;
import utilityPackage.*;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.TestNG;
import org.testng.annotations.Test;

public class MasterClassownExp {
	
	
	static String applicationName = "ownExp";
	static String SuiteSheet = applicationName + "_suite" ;
	static String TestDataFolder = applicationName;
	
	static String Projectpath = System.getProperty("user.dir");	
	static String mappingSheetLocation = Projectpath + "\\TestData\\";	
	static String ClassFileLocation = Projectpath + "\\src\\main\\java\\"+ applicationName + "\\";
	
	
	public static List<String> javaFiles = new ArrayList<String> ();
	public static List<String> testCaseNames = new ArrayList<String> ();
	static String[][] suiteMappingData;
	
	static Logger logger = Logger.getLogger(MasterClass.class.getName());
	
	static String xmlFile = SuiteSheet+ "TestNG.xml";
	
  @Test
  public void MasterClassCreation() throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException {
	  
	  try{
			
			String packageName=applicationName;
			System.out.println("packageNmae - "+ applicationName);
			
			//Mapping test cases into Execution mapping sheet
			File ExecutionMappingFile = new File(Projectpath + "\\TestData\\TestExecutionMapping.xls");
		  	CommonMethod.mapTestCases(ExecutionMappingFile,SuiteSheet,TestDataFolder);
			
		  	
			//Getting list of java files in the suite package  
			  javaFiles = CommonMethod.getJavaFiles(ClassFileLocation,javaFiles);
			  
			  //Getting the test cases from ExecutionMappingSheeet flagged as Y and creating class file if not present
			  CommonMethod.checkJavaFiles(mappingSheetLocation,javaFiles,SuiteSheet,packageName);
			  
			 
			  
			  //Creation of xml file
			  String sourceFile = "excel";	  
			  CreateTestNGxml.getFromExcel(sourceFile,SuiteSheet);
			  logger.info("XML created");
			  
			  //Execute the class files
		/*	  Thread.sleep(5000); //Waiting for the projet to be refreshed
			  TestNG runner=new TestNG();
			  List<String> suitefiles=new ArrayList<String>();
			  suitefiles.add(Projectpath+ "\\" + xmlFile);
			  runner.setTestSuites(suitefiles);
			  runner.run();*/

	  }
	  catch(Exception e){
		  logger.error("The exception in MasterClasCreation  is - ",e);
	  }
	  
	  	
  }

}





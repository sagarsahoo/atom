package ownExp;
import org.testng.annotations.*;
import utilityPackage.*;
import selfitGui.*;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.log4j.Logger;
@Listeners(utilityPackage.Listener.class)
public class ScriptGen extends ActionPerformer {
public static Actions act;
static WebElement we;
public static WebDriverWait wait;
static List<String> UIresult = new ArrayList<String> ();
static List<String> dbResult = new ArrayList<String> ();
static List<String> xmlValues = new ArrayList<String>();
static boolean Verify_list = false;
static boolean allResults = false;
static ArrayList<Boolean> resultList =  new ArrayList<Boolean>();
static String dbQuery;
static  Logger log = Logger.getLogger(ScriptGen.class);

@BeforeTest
public static void beforeTestScenario(){
try{
CommonMethod CM = new CommonMethod("ScriptGen","ownExp_suite");
log.info("  #### Step No - TS_1 ########### Excecuting Test Step -Open FF ############ refering Keyword - open_browser");
ActionPerformer.assignExcelValues(1,UIresult,dbResult);
driver = OpenBrowser.open_browser(pass_values,target_identifiedBy);
act = new Actions(driver);
log.info("=========================================================");


}
catch(Exception e){
	System.out.println("Exception occured is - "+ e);

}
}

@Test
public static void Testscenario(){
try{
CommonMethod CM = new CommonMethod("ScriptGen","ownExp_suite");
log.info("  #### Step No - TS_2 ########### Excecuting Test Step -go to goibibo site ############ refering Keyword - get_url");
ActionPerformer.assignExcelValues(2,UIresult,dbResult);
ProvideURL.baseUrl(driver,pass_values);
log.info("=========================================================");

log.info("  #### Step No - TS_3 ########### Excecuting Test Step -OneWay trip ############ refering Keyword - click");
ActionPerformer.assignExcelValues(3,UIresult,dbResult);
WebElementActions.Click(element_identifiedBy, element_identifier);
log.info("=========================================================");

log.info("  #### Step No - TS_4 ########### Excecuting Test Step -gettripVAlue ############ refering Keyword - getResult");
ActionPerformer.assignExcelValues(4,UIresult,dbResult);
UIresult.add(ElementProperty.getAttribute(element_identifiedBy, element_identifier, target_identifiedBy, target_identifier,pass_values));
log.info("=========================================================");

log.info("  #### Step No - TS_5 ########### Excecuting Test Step -verifyValues ############ refering Keyword - verifyTwoValue");
ActionPerformer.assignExcelValues(5,UIresult,dbResult);
Verify_list = VerifyTheText.verifyTwoValue(UIresult);
resultList.add(Verify_list);
log.info("=========================================================");


}
catch(Exception e){
	System.out.println("Exception occured is - "+ e);

}
}

}
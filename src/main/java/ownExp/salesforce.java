package ownExp;
import org.testng.annotations.*;
import utilityPackage.*;
import selfitGui.*;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.log4j.Logger;
@Listeners(utilityPackage.Listener.class)
public class salesforce extends ActionPerformer {
public static Actions act;
static WebElement we;
public static WebDriverWait wait;
static List<String> UIresult = new ArrayList<String> ();
static List<String> dbResult = new ArrayList<String> ();
static List<String> xmlValues = new ArrayList<String>();
static boolean Verify_list = false;
static boolean allResults = false;
static ArrayList<Boolean> resultList =  new ArrayList<Boolean>();
static String dbQuery;
static  Logger log = Logger.getLogger(salesforce.class);

@BeforeTest
public static void beforeTestScenario(){
try{
CommonMethod CM = new CommonMethod("salesforce","ownExp_suite");
log.info("  #### Step No - TS_1 ########### Excecuting Test Step -chrome Browser ############ refering Keyword - open_browser");
ActionPerformer.assignExcelValues(1,UIresult,dbResult);
driver = OpenBrowser.open_browser(pass_values,target_identifiedBy);
act = new Actions(driver);
log.info("=========================================================");

log.info("  #### Step No - TS_2 ########### Excecuting Test Step -provide salesforce url ############ refering Keyword - get_url");
ActionPerformer.assignExcelValues(2,UIresult,dbResult);
ProvideURL.baseUrl(driver,pass_values);
log.info("=========================================================");


}
catch(Exception e){
	System.out.println("Exception occured is - "+ e);

}
}

@Test
public static void Testscenario(){
try{
CommonMethod CM = new CommonMethod("salesforce","ownExp_suite");
log.info("  #### Step No - TS_3 ########### Excecuting Test Step -verify ############ refering Keyword - text_verify");
ActionPerformer.assignExcelValues(3,UIresult,dbResult);
Verify_list= VerifyTheText.verifyExact(UIresult);
resultList.add(Verify_list);
log.info("=========================================================");

if(IfElseStatements.getConditionStatus(pass_values,resultList,UIresult,allResults)){
log.info("  #### Step No - TS_5 ########### Excecuting Test Step -username ############ refering Keyword - enter_text");
ActionPerformer.assignExcelValues(5,UIresult,dbResult);
WebElementActions.enterText(element_identifiedBy, element_identifier, pass_values);
log.info("=========================================================");

log.info("  #### Step No - TS_6 ########### Excecuting Test Step -password ############ refering Keyword - enter_text");
ActionPerformer.assignExcelValues(6,UIresult,dbResult);
WebElementActions.enterText(element_identifiedBy, element_identifier, pass_values);
log.info("=========================================================");

log.info("  #### Step No - TS_7 ########### Excecuting Test Step -ownCOdeAdd ############ refering Keyword - ownCode");
ActionPerformer.assignExcelValues(7,UIresult,dbResult);
WebElement invisibleelement1= driver.findElement(By.xpath(".//*[@id='label_traditionalRegistration_TC']/i"));JavascriptExecutor executor1 = (JavascriptExecutor)driver;executor1.executeScript("arguments[0].click();", invisibleelement1);
log.info("=========================================================");

log.info("  #### Step No - TS_8 ########### Excecuting Test Step -calling TC ############ refering Keyword - TestCaseAsStep");
ActionPerformer.assignExcelValues(8,UIresult,dbResult);
ScriptGen.beforeTestScenario();
log.info("=========================================================");

}
else {
log.info("  #### Step No - TS_11 ########### Excecuting Test Step -testclick ############ refering Keyword - click");
ActionPerformer.assignExcelValues(11,UIresult,dbResult);
WebElementActions.Click(element_identifiedBy, element_identifier);
log.info("=========================================================");

}
log.info("  #### Step No - TS_13 ########### Excecuting Test Step -starting loop ############ refering Keyword - startLoop");
ActionPerformer.assignExcelValues(13,UIresult,dbResult);
for(int cnt=0;cnt<Integer.parseInt(pass_values);cnt++){
log.info("=========================================================");

log.info("  #### Step No - TS_14 ########### Excecuting Test Step -test click ############ refering Keyword - click");
ActionPerformer.assignExcelValues(14,UIresult,dbResult);
WebElementActions.Click(element_identifiedBy, element_identifier);
log.info("=========================================================");

log.info("  #### Step No - TS_15 ########### Excecuting Test Step -end loop ############ refering Keyword - endLoop");
ActionPerformer.assignExcelValues(15,UIresult,dbResult);
}
log.info("=========================================================");


}
catch(Exception e){
	System.out.println("Exception occured is - "+ e);

}
}

}
package ownExp;
import org.testng.annotations.*;
import utilityPackage.*;
import selfitGui.*;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.log4j.Logger;
@Listeners(utilityPackage.Listener.class)
public class webtable extends ActionPerformer {
public static Actions act;
static WebElement we;
public static WebDriverWait wait;
static List<String> UIresult = new ArrayList<String> ();
static List<String> dbResult = new ArrayList<String> ();
static List<String> xmlValues = new ArrayList<String>();
static boolean Verify_list = false;
static boolean allResults = false;
static ArrayList<Boolean> resultList =  new ArrayList<Boolean>();
static String dbQuery,subStringByLength,subStringByValue,customXpath;
static  Logger log = Logger.getLogger(webtable.class);

@BeforeTest
public static void beforeTestScenario(){
try{
CommonMethod CM = new CommonMethod("webtable","ownExp_suite");
log.info("  #### Step No - TS_1 ########### Excecuting Test Step -open chrome browser ############ refering Keyword - open_browser");
ActionPerformer.assignExcelValues(1,UIresult,dbResult);
driver = OpenBrowser.open_browser(pass_values,target_identifiedBy);
act = new Actions(driver);
log.info("=========================================================");

log.info("  #### Step No - TS_2 ########### Excecuting Test Step -provide application url ############ refering Keyword - get_url");
ActionPerformer.assignExcelValues(2,UIresult,dbResult);
ProvideURL.baseUrl(driver,pass_values);
log.info("=========================================================");

log.info("  #### Step No - TS_3 ########### Excecuting Test Step -password ############ refering Keyword - enter_text");
ActionPerformer.assignExcelValues(3,UIresult,dbResult);
WebElementActions.enterText(element_identifiedBy, element_identifier, pass_values);
log.info("=========================================================");

log.info("  #### Step No - TS_4 ########### Excecuting Test Step -to enter value test ############ refering Keyword - enter_text");
ActionPerformer.assignExcelValues(4,UIresult,dbResult);
WebElementActions.enterText(element_identifiedBy, element_identifier, pass_values);
log.info("=========================================================");

log.info("  #### Step No - TS_5 ########### Excecuting Test Step -enter 2 ############ refering Keyword - enter_text");
ActionPerformer.assignExcelValues(5,UIresult,dbResult);
WebElementActions.enterText(element_identifiedBy, element_identifier, pass_values);
log.info("=========================================================");

log.info("  #### Step No - TS_6 ########### Excecuting Test Step -to click ############ refering Keyword - click");
ActionPerformer.assignExcelValues(6,UIresult,dbResult);
WebElementActions.Click(element_identifiedBy, element_identifier);
log.info("=========================================================");


}
catch(Exception e){
	System.out.println("Exception occured is - "+ e);

}
}

@Test
public static void Testscenario(){
try{
CommonMethod CM = new CommonMethod("webtable","ownExp_suite");

}
catch(Exception e){
	System.out.println("Exception occured is - "+ e);

}
}

}
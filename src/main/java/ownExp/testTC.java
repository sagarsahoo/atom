package ownExp;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import utilityPackage.CommonMethod;

@Listeners(utilityPackage.Listener.class)
@Test
public class testTC {
	String test_case = "testTC";
	String suiteMasterSheet = "ownExp_suite";

	@Test
	public void SmokeTestCase(){
		CommonMethod CM = new CommonMethod(test_case,suiteMasterSheet);
		CM.BeforetestFunction();
		CM.test();
		CM.BrowserQuitFunc();
	}
}

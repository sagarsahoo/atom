package ownExp;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import utilityPackage.CommonMethod;

@Listeners(utilityPackage.Listener.class)
@Test
public class goibibo {
	String test_case = "goibibo";
	String suiteMasterSheet = "ownExp_suite";

	@Test
	public void SmokeTestCase(){
		CommonMethod CM = new CommonMethod(test_case,suiteMasterSheet);
		CM.BeforetestFunction();
		CM.test();
		CM.BrowserQuitFunc();
	}
}

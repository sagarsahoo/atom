package selfitGui;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.testng.annotations.Test;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;

public class FileActions {
  @Test
  public static void editSingleTestNGXML(String NameOfPackage,String TCname) {
	  try{
		  
		  //Detele existing XML file
		  File XMLFile = new File(Initialization.projectPath +  "//SingleTest.xml");
		  if(XMLFile.exists()){
			  XMLFile.delete();
		  }
		  
		  DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder builder = factory.newDocumentBuilder();
	        Document document = builder.newDocument();
	        Element rootElement = document.createElement("suite");
	        document.appendChild(rootElement);
	        rootElement.setAttribute("name","Test suite "+ NameOfPackage);
       	rootElement.setAttribute("preserve-order", "true");
        
        
        Element Listeners = document.createElement("listeners");
	      Element Listener = document.createElement("listener");
        rootElement.appendChild(Listeners);
        Listeners.appendChild(Listener);
        Listener.setAttribute("class-name", "utilityPackage.Listener");
      
      
      Element Test = document.createElement("test");
		rootElement.appendChild(Test);
		Test.setAttribute("name", TCname);
		
		Element Classes = document.createElement("classes");
		Element Class = document.createElement("class");
		Test.appendChild(Classes);
		Classes.appendChild(Class);
		Class.setAttribute("name", NameOfPackage+ "." + TCname);
      
      
      
      TransformerFactory tFactory = TransformerFactory.newInstance();
      Transformer transformer = tFactory.newTransformer();
      //Add indentation to output
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
      transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
      transformer.setOutputProperty(OutputKeys.METHOD, "xml");
      DOMImplementation domImpl = document.getImplementation();
      
      //DocumentType doctype = domImpl.createDocumentType("doctype","-//Oberon//YOUR PUBLIC DOCTYPE//EN","http://testng.org/testng-1.0.dtd");
      DocumentType doctype = domImpl.createDocumentType("doctype","SYSTEM","http://testng.org/testng-1.0.dtd");
      
      //transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, doctype.getPublicId());
      transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());

      DOMSource source = new DOMSource(document);
      StreamResult result = new StreamResult(new File(Initialization.projectPath +  "//SingleTest.xml"));
      //StreamResult result = new StreamResult(System.out);
      transformer.transform(source, result);

             
		  
	  }catch (ParserConfigurationException e) {
	    	System.out.println("ParserConfigurationException " + e.getMessage());
	    } catch (TransformerConfigurationException e) {
	    	System.out.println("TransformerConfigurationException "+ e.getMessage());
	    } catch (TransformerException e) {
	    	System.out.println("TransformerException " + e.getMessage());
	    }    
  }
  
  
  
}

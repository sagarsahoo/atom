package selfitGui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.Test;

import utilityPackage.cellToString;

public class ExcelDataList {
  @Test
  public static List<String> getExcelDataInListByColName(File ExcelFile,String SheetName,String columnName) {
	  List<String> columnList = new ArrayList<String>();
	  try{
		  FileInputStream fis = new FileInputStream(ExcelFile);
		  HSSFWorkbook wb = new HSSFWorkbook(fis);
		  Sheet sh = wb.getSheet(SheetName);
		  //Get the column index as per the name
		  int indexOfColumn = getColumnIndex(ExcelFile,sh,columnName);
		  int noRows = (sh.getLastRowNum()+1);
		  
		  //Getting the list
		  for(int rowcnt=1;rowcnt<noRows;rowcnt++){
			  HSSFRow row = (HSSFRow) sh.getRow(rowcnt);
			   HSSFCell cell = row.getCell(indexOfColumn);
			   if(cell==null){
				   System.out.println("Cell is empty");
				   return columnList;
			   }else{
				   String value = cellToString.cellTostring_HSSF(cell);
				   System.out.println(rowcnt+  " Value in the "+ columnName + " is" + value );
				   columnList.add(value);
			   }
			    
		  }
		  
		  return columnList;
	  }catch(Exception e){
		  System.out.println("Exceptiong in getting Excel column in list - "+ e);
		  return null;
	  }
  }
  
  public static List<String> getExcelDataInListByColIndex(File ExcelFile,String SheetName,int columnIndex) {
	  List<String> columnList = new ArrayList<String>();
	  //columnList=null;
	  try{
		  FileInputStream fis = new FileInputStream(ExcelFile);
		  HSSFWorkbook wb = new HSSFWorkbook(fis);
		  Sheet sh = wb.getSheet(SheetName);
		  int noRows = (sh.getLastRowNum()+1);
		  System.out.println("Number of rows in the Excel - "+noRows );
		  //Will ignore the header row  and store the data
		  for(int rowcnt=1;rowcnt<noRows;rowcnt++){
			  HSSFRow row = (HSSFRow) sh.getRow(rowcnt);
			   HSSFCell cell = row.getCell(columnIndex);
			   if(cell==null){
				   System.out.println("Cell is empty");
			   }else{
				   String value = cellToString.cellTostring_HSSF(cell);
				   columnList.add(value);
			   }
			    
		  }
		  
		  return columnList;
	  }catch(Exception e){
		  System.out.println("Exceptiong in getting Excel column in list - "+ e);
		  return null;
	  }
  }
  
  //Get the values in List for the particular row as per the row value provided
  public static List<String> getExcelDataInListByRowIndex(File ExcelFile,String SheetName,int rownum) {
	  List<String> columnList = new ArrayList<String>();
	  
	  try{
		  FileInputStream fis = new FileInputStream(ExcelFile);
		  HSSFWorkbook wb = new HSSFWorkbook(fis);
		  Sheet sh = wb.getSheet(SheetName);
		  
		  //Getting the column index
		  Row targetRow = sh.getRow(rownum);
		  int numCols = targetRow.getLastCellNum();
		  HSSFCell cell;
		  for(int colNo=0;colNo<numCols;colNo++){
			  cell = (HSSFCell) targetRow.getCell(colNo);
			  if(cell==null){
				   System.out.println("Cell is empty");
			   }else{
				   String value = cellToString.cellTostring_HSSF(cell);
				   columnList.add(value);
			   }
		  }
		  
		  return columnList;
	  }catch(Exception e){
		  System.out.println("Exceptiong in getting Excel column in list - "+ e);
		  return null;
	  }
  }
  
//Get the values in List for the particular row as per the row value provided
  public static List<String> getExcelDataInListByRowValue(File ExcelFile,String SheetName,String colName,String rowValueofCol) {
	  List<String> columnList = new ArrayList<String>();
	  
	  try{
		  FileInputStream fis = new FileInputStream(ExcelFile);
		  HSSFWorkbook wb = new HSSFWorkbook(fis);
		  Sheet sh = wb.getSheet(SheetName);
		  
		  //Getting the target Row Index
		  int targetRowIndex = getRownumByColumnValue(ExcelFile,SheetName,colName,rowValueofCol);
		  
		  Row targetRow = sh.getRow(targetRowIndex);
		  int numCols = targetRow.getLastCellNum();
		  HSSFCell cell;
		  for(int colNo=0;colNo<numCols;colNo++){
			  cell = (HSSFCell) targetRow.getCell(colNo);
			  if(cell==null){
				   System.out.println("Cell is empty");
			   }else{
				   String value = cellToString.cellTostring_HSSF(cell);
				   columnList.add(value);
			   }
		  }		  
		  return columnList;
	  }catch(Exception e){
		  System.out.println("Exceptiong in getting Excel column in list - "+ e);
		  return null;
	  }
  }
  
  //Get the row number as per value for a particular column
  public static int getRownumByColumnValue(File ExcelFile,String SheetName,String colName, String value){
	  int rownum=0;
	  try{
		  FileInputStream fis = new FileInputStream(ExcelFile);
		  HSSFWorkbook wb = new HSSFWorkbook(fis);
		  Sheet sh = wb.getSheet(SheetName);		  
		  //getting column index
		  int colIndex = getColumnIndex(ExcelFile,sh,colName);
		  int totalRows = getTotalRows(ExcelFile,SheetName);
		  HSSFRow row ;
		  HSSFCell cell;
		  String cellValue;
		  for(int cnt =1;cnt<totalRows;cnt++){
			  row = (HSSFRow) sh.getRow(cnt);
			  cell = row.getCell(colIndex);
			  cellValue = cellToString.cellTostring_HSSF(cell);
			  if(cellValue.equals(value)){
				  System.out.println("Match found at - "+ cnt + " - row");
				  return cnt;
			  }
		  }
		  return rownum;
	  }catch(Exception e){
		  System.out.println("Exception in getting row number as per column value is - "+ e);
		  return rownum;
	  }
  }
  
  //Get row num as per the column-value list
  public static int getRowNumByColValueList(File ExcelFile,String SheetName,List<String> colNameList, List<String> valueList){
	  int rownum=0;
	  try{
		  FileInputStream fis = new FileInputStream(ExcelFile);
		  HSSFWorkbook wb = new HSSFWorkbook(fis);
		  Sheet sh = wb.getSheet(SheetName);		  
		  //getting column index
		  int colIndex ;
		  HSSFRow row ;
		  HSSFCell cell;
		  String cellValue;
		  int matchCnt=0;
		  int totalRows = getTotalRows(ExcelFile,SheetName);		 
		  for(int cnt =1;cnt<totalRows;cnt++){
			  row = (HSSFRow) sh.getRow(cnt);
			  matchCnt=0;
			  for(int colCnt=0;colCnt<colNameList.size();colCnt++){
				  colIndex = getColumnIndex(ExcelFile,sh,colNameList.get(colCnt));
				  cell = row.getCell(colIndex);
				  cellValue = cellToString.cellTostring_HSSF(cell);
				  if(cellValue.equals(valueList.get(colCnt))){
					  matchCnt++;
					  rownum=cnt;
					  colCnt++;
				  }
			  }			  
		  }
		  if(matchCnt==colNameList.size()){
			  return rownum;
		  }
		  return rownum;
	  }catch(Exception e){
		  
		  return rownum;
	  }
  }
  
  //Get Column index as per the name
  public static int getColumnIndex(File ExcelFile, Sheet excelSheet,String ColumnName){
	  int index=0;
	  try{
		  FileInputStream fis = new FileInputStream(ExcelFile);
		  HSSFWorkbook excelWorkBook = new HSSFWorkbook(fis);
		  int columnHeaderRowIndex =0;
		  int totalColumns = excelSheet.getRow(columnHeaderRowIndex).getLastCellNum();
		  
		  index=(totalColumns+1);
		  HSSFRow row = (HSSFRow) excelSheet.getRow(columnHeaderRowIndex);
		   
		  for(int colCnt=0;colCnt<totalColumns;colCnt++){
			  HSSFCell cell = row.getCell(colCnt);
			  if(cell==null){
				   System.out.println("Cell is empty");
			   }else{
				   String value = cellToString.cellTostring_HSSF(cell);
				   //System.out.println("Column selected - "+ value);
				   if(value.equals(ColumnName)){
					   System.out.println("Column found  in the index - "+colCnt );
					   return colCnt;
				   }
			   }
		  }
		  
		return index;  
	  }catch(Exception e){
		  System.out.println("The exception in getting column index is - "+ e);
		  return index;
	  }
  }
  
  
  public static int getTotalRows(File ExcelFile, String SheetName ){
	  int noRows=0;
	  try{
		  FileInputStream fis = new FileInputStream(ExcelFile);
		  HSSFWorkbook wb = new HSSFWorkbook(fis);
		  Sheet sh = wb.getSheet(SheetName);
		  noRows = (sh.getLastRowNum()+1);
		  return noRows;
	  }catch(Exception e){
		  System.out.println("Exception in getting excel totalRows - "+ e);
		  return noRows;
	  }
  }
  
  public static int getTotalColumns(File ExcelFile, String SheetName ){
	  int noColumns=0;
	  try{
		  FileInputStream fis = new FileInputStream(ExcelFile);
		  HSSFWorkbook wb = new HSSFWorkbook(fis);
		  Sheet sh = wb.getSheet(SheetName);
		  noColumns = sh.getRow(0).getLastCellNum();
		  return noColumns;
	  }catch(Exception e){
		  System.out.println("Exception in getting excel totalRows - "+ e);
		  return noColumns;
	  }
  }
  
  //Getting the sheets in the Excel file
  public static List<String> getSheetsInExcel(File ExcelFile){
	  List<String> SheetNames = new ArrayList<String>();
	  String sht = null;
	  try{
		  FileInputStream fis = new FileInputStream(ExcelFile);
		  HSSFWorkbook wb = new HSSFWorkbook(fis);
		  int NumSheets = wb.getNumberOfSheets();
		  for(int cnt=0;cnt<NumSheets;cnt++){
			  sht = wb.getSheetName(cnt).toString();
			  System.out.println("Sheet at index - "+ cnt + " is - "+ sht);
			  SheetNames.add(sht);
		  }
		  return SheetNames;
	  }catch(Exception e){
		  System.out.println("Exception in getting shhets from excel is - "+ e);
		  return null;
	  }
  }
  
  //Get the data as per excel column filter condition
  public static List<String> getDataByColumnFilter(File ExcelFile,String sheetName,String colFilter, String filterValue,String colValueToget){
	  List<String> selectedObjectsOnType = new ArrayList<String>();
	  try{		  
		  HSSFRow row;
		  HSSFCell cell;
		  String cellValue;
		  String objectName;
		  FileInputStream fis = new FileInputStream(ExcelFile);
		  HSSFWorkbook wb = new HSSFWorkbook(fis);
		  Sheet sh = wb.getSheet(sheetName);
		  
		  int totalRows = ExcelDataList.getTotalRows(ExcelFile, sheetName);
		  int colFilterIndex = ExcelDataList.getColumnIndex(ExcelFile, sh, colFilter);
		  int IndexColValueToGet = ExcelDataList.getColumnIndex(ExcelFile, sh, colValueToget);
		  
		  //Iterate all the rows in the sheet		 
		  for(int rowcnt=1;rowcnt<totalRows;rowcnt++){
			  row = (HSSFRow) sh.getRow(rowcnt);
			  cellValue = row.getCell(colFilterIndex).getStringCellValue();
			  if(cellValue.equals(filterValue)){
				  objectName = row.getCell(IndexColValueToGet).getStringCellValue();
				  System.out.println("Object name - "+ objectName);
				  selectedObjectsOnType.add(objectName);
			  }
		  }
		  return selectedObjectsOnType;
		  
	  }catch(Exception e){
		  System.out.println("Exception in geting the selected data as per the column filteration condition is - "+ e);
		  return selectedObjectsOnType;
	  }
  }
  
  //Write into Excel file as per provided row and column index
  public static void writeToExcel(File ExcelFile,String sheetName,int rowNum,int columnNum,String valueToWrite){
	  try{	        	       
		  FileInputStream inputStream = new FileInputStream(ExcelFile);
		  Workbook wb = new HSSFWorkbook(inputStream);   
		  Sheet sheet = wb.getSheet(sheetName);
		  Row row = sheet.getRow(rowNum);
		  Cell cell = row.getCell(columnNum);

		  cell.setCellValue(valueToWrite);

		  inputStream.close();
		  FileOutputStream outputStream = new FileOutputStream(ExcelFile);
		  wb.write(outputStream);
		  outputStream.close();

	  }catch(Exception e){
		  System.out.println("Exception in writing to excel is - "+ e);
	  }
  }
  
  
  
  //Storing the Excel data in 2-D variable
  public static String[][] getSheetData(File ExcelFile, String SheetName){
	  try{
		    FileInputStream fis =  new FileInputStream(ExcelFile);
			HSSFWorkbook wb = new HSSFWorkbook (fis);
			HSSFSheet ws = wb.getSheet(SheetName);
			FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
			//Counting Number of rows and columns
			int rowNum = ExcelDataList.getTotalRows(ExcelFile, SheetName);
			int colNum = ExcelDataList.getTotalColumns(ExcelFile, SheetName);
			String[][] data = new String[rowNum][colNum];
			
			for (int i=0 ;i < rowNum ; i++){
				HSSFRow row = ws.getRow(i);
				for(int j=0 ; j < colNum ; j++){
					HSSFCell cell = row.getCell(j);
					String value = cellToString.cellTostring_HSSF(cell);
					data[i][j] = value.trim();
					//System.out.println("value - "+ data[i][j]);
				}
			}
			return data;
		  
	  }catch(Exception e){
		  System.out.println("Exception in getting excel data - "+ e);
		  return null;
	  }
  }
  
  //Inserting Keyword into the Excel file
  public static void insertRowIntoExcel(File scriptingFile,String ExcelSheetName,List<String> DataList,int rowNumtoInsert){
	  try{
		  FileInputStream fis = new FileInputStream(scriptingFile);
		  HSSFWorkbook wb = new HSSFWorkbook (fis);
		  HSSFSheet ws = wb.getSheet(ExcelSheetName);
		  HSSFCell cell;
		  int targetRowToInsert;
		  if(rowNumtoInsert>0){
			  targetRowToInsert = (rowNumtoInsert)+1;
			  ws.shiftRows(targetRowToInsert, ws.getLastRowNum(), 1, true, false);
		  }else{
			  targetRowToInsert = (ws.getLastRowNum()+1);
		  }		  		  
		  Row row = ws.createRow(targetRowToInsert);
		  for(int itr=0;itr<DataList.size();itr++){
			  cell = (HSSFCell) row.createCell(itr);
			  cell.setCellValue(DataList.get(itr));
		  }		  
		  fis.close();
		  FileOutputStream outputStream = new FileOutputStream(scriptingFile);
		  wb.write(outputStream);
		  outputStream.close();
		  
	  }catch(Exception e){
		  System.out.println("Exception in inserting row into Excel - "+ e);
	  }
  }
  
  //Getting value as per ROW and COLUMN number
  public static String getValueByRowAndCol(File scriptingFile,String ExcelSheetName,int rowNUm, int colNum){
	  String valueRetrieved;
	  try{
		  FileInputStream fis = new FileInputStream(scriptingFile);
		  HSSFWorkbook wb = new HSSFWorkbook (fis);
		  HSSFSheet ws = wb.getSheet(ExcelSheetName);
		  HSSFCell cellValue;
		  Row excelRow = ws.getRow(rowNUm);
		  cellValue = (HSSFCell) excelRow.getCell(colNum);
		  valueRetrieved= cellToString.cellTostring_HSSF(cellValue);
		  return valueRetrieved;
		  
	  }catch(Exception e){
		
		  return null;
	  }
  }
  
  //Getting Sheet of the Excel
  public static HSSFSheet getSheet(File scriptingFile,String ExcelSheetName){
	  HSSFSheet ws;
	  try{
		  FileInputStream fis = new FileInputStream(scriptingFile);
		  HSSFWorkbook wb = new HSSFWorkbook (fis);
		  ws = wb.getSheet(ExcelSheetName);
		  return ws;
	  }catch(Exception e){
		 
		  return null;
	  }
  }
  
  
  //Update the row as per the given rowNumber
  public static void updateRowByRowNum(File ExcelFile,String sheetName,int rownum,List<String> valuesToUpdate){
	  try{
		  FileInputStream fis = new FileInputStream(ExcelFile);
		  HSSFWorkbook wb = new HSSFWorkbook (fis);
		  HSSFSheet ws = wb.getSheet(sheetName);
		  
		  Row rowselected = ws.getRow(rownum);
		  for(int cnt=0;cnt<valuesToUpdate.size();cnt++){
			  rowselected.getCell(cnt).setCellValue(valuesToUpdate.get(cnt));
		  }
		  
		  FileOutputStream outputStream = new FileOutputStream(ExcelFile);
		  wb.write(outputStream);
		  outputStream.close();
	  }catch(Exception e){
		  System.out.println("Exception in updating row in Excel - "+ e);
	  }
	  
  }
  
  //Inserting Header for Test Data
  public static void insertHeaderInTestData(File ExcelFile,String sheetName,List<String> headerList){
	  try{
		  if(headerList.size()>0){
			  FileInputStream fis = new FileInputStream(ExcelFile);
			  HSSFWorkbook wb = new HSSFWorkbook (fis);
			  HSSFSheet ws = wb.getSheet(sheetName);
			  
			  int totalCol = ExcelDataList.getTotalColumns(ExcelFile, sheetName);
			  Row header = ws.getRow(0);
			  for(int cnt=0;cnt<headerList.size();cnt++){
				  header.createCell(totalCol+cnt).setCellValue(headerList.get(cnt));
			  }
			  FileOutputStream outputStream = new FileOutputStream(ExcelFile);
			  wb.write(outputStream);
			  outputStream.close();  
		  }
	  }catch(Exception e){
		  System.out.println("Exception in inserting header is - "+ e);
	  }
  }
  
}

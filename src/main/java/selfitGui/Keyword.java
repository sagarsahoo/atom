package selfitGui;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import utilityPackage.ExcelFunctions;
import utilityPackage.SplitString;

public class Keyword extends JFrame {

	public static String TestCaseID ;
	public static String TestCaseName;
	public static String executionFlag;
	public static String testStepDesc;
	public static String TestStepNum;
	public static String keyword;
	public static String elementType;
	public static String elementfindBy;
	public static String elementfindValue;
	public static String userValues;
	public static String targetElementFindBy;
	public static String targetFindValue;
	public static String testStepSegment;
	public static String Result;

	private static JPanel contentPane;
	static Keyword keywordFrame;
	private static JTextField textField_TS_desc;

	private JLabel lblTS_desc;
	private JLabel lbl_ExecFlag;
	private JLabel lbl_segment;
	private static JComboBox comboBox_segment;
	private static JRadioButton rdbtn_Exec_Y;
	private static ButtonGroup group = new ButtonGroup();
	private static JRadioButton rdbtn_Exec_N;

	public static JTextField[] textField_srcLocatorValue = new JTextField[5];
	public static JComboBox[] comboBox_uservalues = new JComboBox[3];
	static List<String> dropDownItemList = new ArrayList<String>();
	static int textboxcounter;
	static int comboboxcounter;
	static int textAreacounter;
	static int userAccessTextBoxValues;
	static int userAccessComboBoxValues;
	private static String ObjectTypeSelected;	
	private static boolean userDecision;
	private static int ObjectCnt;
	static int chooseObjIndex;
	static int choose_objectCnt;
	public static List<String> elementsToInsert = new ArrayList<String>();

	public static List<String> getTextBoxValuesList =new ArrayList<String>();
	public static List<String> getComboBoxValuesList =new ArrayList<String>();
	static List<String> existingStepDetails = new ArrayList<String>();

	private static JLabel lbl_Field;
	private static JTextField textFieldAsPerKeyword;
	private static JLabel lblNameAsPerKeyword;
	private static JTextArea txtAreaOwnCode;
	private static JScrollPane scrollPaneOwnCode;
	private static Button choose_Object;



	//define Frame
	public static void getKeywordFrame(){
		keywordFrame = new Keyword();
		keywordFrame.setVisible(true);
		keywordFrame.setLocationRelativeTo(null);
		keywordFrame.setTitle("Step-details");
		keywordFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}

	/**
	 * Create the frame.
	 */
	public Keyword() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 583, 522);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		//Initializing and resetting all the variables
		variableInitialization();

		//Display default Objects on selection
		displayDefaultObjects();

		//Displaying objects as per Keywords
		displayKeywordRelatedObjects(TestCreation.selectedChildKeyword);

		//Action on OK/CANCEL
		getActionOnKeywordSelection();

	}




	//****************************************************************************************************************************************************************************************

	private static void variableInitialization(){
		textboxcounter=0;
		comboboxcounter=0;
		textAreacounter=0;
		choose_objectCnt = 0;
		chooseObjIndex=0;
		userAccessTextBoxValues=0;
		userAccessComboBoxValues=0;
		ObjectCnt = 0;
		dropDownItemList.clear();

	}

	//Displaying default Objects on selection
	private void displayDefaultObjects(){
		try{

			lblTS_desc = new JLabel("Test Step Desc.");
			lblTS_desc.setBounds(40, 37, 114, 22);
			contentPane.add(lblTS_desc);

			textField_TS_desc = new JTextField();
			textField_TS_desc.setBounds(180, 37, 200, 22);
			contentPane.add(textField_TS_desc);
			textField_TS_desc.setColumns(10);

			lbl_ExecFlag = new JLabel("Exec. Flag");
			lbl_ExecFlag.setBounds(40, 70, 86, 20);
			contentPane.add(lbl_ExecFlag);

			rdbtn_Exec_Y = new JRadioButton("Y");
			rdbtn_Exec_Y.setBounds(180, 69, 52, 22);
			contentPane.add(rdbtn_Exec_Y);
			rdbtn_Exec_Y.setSelected(true);

			rdbtn_Exec_N = new JRadioButton("N");
			rdbtn_Exec_N.setBounds(249, 70, 52, 21);
			contentPane.add(rdbtn_Exec_N);

			group.add(rdbtn_Exec_Y);
			group.add(rdbtn_Exec_N);

			//For test Segment
			lbl_segment = new JLabel("Step Segment");
			lbl_segment.setBounds(40, 370, 86, 20);
			contentPane.add(lbl_segment);

			comboBox_segment = new JComboBox();
			comboBox_segment.setBounds(178, 370, 76, 20);
			contentPane.add(comboBox_segment);
			comboBox_segment.addItem("BT");
			comboBox_segment.addItem("T");
			comboBox_segment.addItem("AT");

		}catch(Exception e){
			System.out.println("Exception is displaying default content objects - "+ e);
		}
	}

	//Display keyword related Objects
	private void displayKeywordRelatedObjects(String selectedKeyword){
		try{
			getKeywordFields(selectedKeyword);
			//Hiding the objects
			//hideKeywordObjects();			
		}catch(Exception e){
			System.out.println("Exception in displaying keywords related objects - "+ e);
		}
	}

	//Action to be done on OK and CANCEL
	private void getActionOnKeywordSelection(){
		try{
			//Action on clicking OK
			if(TestCreation.action.equals("add")){
				Button btn_ADD = new Button("ADD");
				btn_ADD.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {						
						getUserProvidedValues(); //Getting the values provided by the user						
						insertKeywordInExcel(); //Insert keyword in the Excel
						keywordFrame.dispose(); //Closing the frame
						userDecision=true;
						StepNumber.setStepNUmberInSequence(TestCreation.scriptingFile, TestCreation.TCSelected); //Reseting the step no. sequence						
						actionAfterKeywordFrame(); //Action after the work is done
						//refresh the table
					}
				});
				btn_ADD.setBounds(339, 445, 70, 22);
				contentPane.add(btn_ADD);
			}else{
				Button btn_ADD = new Button("UPDATE");
				btn_ADD.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						getUserProvidedValues(); //							
						updateKeywordInExcel();//Update the keyword field values
						keywordFrame.dispose();
						userDecision=true;
						StepNumber.setStepNUmberInSequence(TestCreation.scriptingFile, TestCreation.TCSelected);							
						actionAfterKeywordFrame();//Action after the work is done
						//refresh the table
					}
				});
				btn_ADD.setBounds(339, 445, 70, 22);
				contentPane.add(btn_ADD);
			}


			//Action on clicking CANCEL
			Button btn_cancel = new Button("Cancel");
			btn_cancel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					keywordFrame.dispose();
					userDecision=false;
					//Action after the work is done
					actionAfterKeywordFrame();
				}
			});
			btn_cancel.setBounds(435, 445, 70, 22);
			contentPane.add(btn_cancel);
		}catch(Exception e){
			System.out.println("Exception occured in OK/CANCEL is - "+ e);
		}
	}



	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////




	//Inserting the keyword into Excel
	private static void insertKeywordInExcel(){
		System.out.println("Inside function to insert keyword into excel");
		try{
			ExcelDataList.insertRowIntoExcel(TestCreation.scriptingFile, TestCreation.TCSelected, elementsToInsert,TestCreation.selectedTestStepNumber);
			System.out.println("Test Step inserted!!!!!!!!!!!!!!!! ");
		}catch(Exception e){
			System.out.println("Exception in inserting the Keyword in to the Excel - "+ e);
		}
	}


	//Update the keyword values
	private static void updateKeywordInExcel(){
		System.out.println("Inside function to update the keyword field values");
		try{
			System.out.println("Updating the test step");
			ExcelDataList.updateRowByRowNum(TestCreation.scriptingFile, TestCreation.TCSelected, TestCreation.selectedTestStepNumber, elementsToInsert);
		}catch(Exception e){
			System.out.println("Exception in updating the test step - "+ e);
		}
	}

	//Get Fields as per Keywords
	private static void getKeywordFields(String keywordSelected){
		try{
			KeywordGUIActions.getFieldDetailsFromExcel(keywordSelected);
			KeywordGUIActions.InputsToCreateFields();
		}catch(Exception e){
			System.out.println("The Exception in getting keyword related Fields -  "+ e);
		}
	}


	// Creating the Label Object
	public static void createLabel(String labelName){
		try{
			lbl_Field = new JLabel(labelName);
			int depth = (134+(ObjectCnt*40));
			//System.out.println("depth - "+depth );
			lbl_Field.setBounds(40, depth, 114, 22); //40, 101, 114, 22
			//ObjectCnt++;
			contentPane.add(lbl_Field);
			System.out.println("Label created !!!!!!!!!!!!  " + labelName);
		}catch(Exception e){
			System.out.println("Exception in creating label is - "+ e);
		}		
	}

	//Creating the TextBox Object
	public static void createTextBox(String ExcelfieldName){
		try{
			textField_srcLocatorValue[textboxcounter] = new JTextField();
			int depth = (134+(ObjectCnt*40));
			//System.out.println("depth - "+depth );
			textField_srcLocatorValue[textboxcounter].setBounds(180, depth, 200, 20);
			ObjectCnt++;
			contentPane.add(textField_srcLocatorValue[textboxcounter]);
			textField_srcLocatorValue[textboxcounter].setColumns(10);
			System.out.println("TextField created !!!!!!!!!!!! having index - "+textboxcounter );
			if(KeywordGUIActions.objectFieldIndex>0){
				chooseObjIndex= textboxcounter;
				System.out.println("value stored in objectFieldIndex- "+ chooseObjIndex);
			}
			/*if(TestCreation.action.equalsIgnoreCase("edit")){
					textField_srcLocatorValue[textboxcounter].setText(getTextBoxValuesList.get(textboxcounter));
				}*/
			textboxcounter++;
		}catch(Exception e){
			System.out.println("Exception in text box creation is - "+ e);
		}
	}

	// Create the choose button
	public static void createChooseButton(){
		try{
			System.out.println("To Create - choose Button for selecting objects !!!!!!!!!!");
			choose_objectCnt++;
			choose_Object = new Button("choose-Object");
			int depth = (134+((ObjectCnt-1)*40));
			//System.out.println("depth - "+ depth);
			choose_Object.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					keywordFrame.setEnabled(false);
					//Open the object-view frame
					ObjectRepositoryView.objRepoLaunchedFrom = "Keyword";
					ObjectRepositoryView.getFrame();

				}
			});
			choose_Object.setBounds(400, depth, 110, 22);
			contentPane.add(choose_Object);

		}catch(Exception e){
			System.out.println("Exception in creating choose button - "+ e);
		}
	}


	//Creating the Combobox Object
	public static void createComboBox(String name,String valuesToAdd){
		try{
			dropDownItemList.clear();
			comboBox_uservalues[comboboxcounter] = new JComboBox();
			int depth = (134+(ObjectCnt*40));
			//System.out.println("depth - "+depth );
			comboBox_uservalues[comboboxcounter].setBounds(180, depth, 200, 20); //180, 102, 86, 20
			ObjectCnt++;
			contentPane.add(comboBox_uservalues[comboboxcounter]);
			System.out.println("Combobox created !!!!!!!!!!!!");

			//Modifying the drop-down item list 
			valuesToAdd = valuesToAdd.replace(",", "|");
			//System.out.println("Modified drop-down item String - "+ valuesToAdd);
			String[] dropDownItemArray = SplitString.stringSplit(valuesToAdd);
			if(dropDownItemArray.length==1 && dropDownItemArray[0].equalsIgnoreCase("TClist")){
				//getting the list of Test cases				
				dropDownItemList.addAll(TestCreation.TCList);
			}else if(dropDownItemArray.length==1 && dropDownItemArray[0].equalsIgnoreCase("LookupList")){
				//create a hash map
				getHashMapList();
				//Create as per defined in the dropDown Excel sheet
				dropDownItemList.addAll(GetDropDownValues(TestCreation.selectedChildKeyword));
				comboBox_uservalues[comboboxcounter].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						//Creating the form as per the selection
						String getSelectedDropDownValue = comboBox_uservalues[comboboxcounter].getSelectedItem().toString();
						KeywordGUIActions.createField("value", "textField", 3);
					}
				});
			}
			else if (dropDownItemArray[dropDownItemArray.length-1].equalsIgnoreCase("ownValue")){
				comboBox_uservalues[comboboxcounter].setEditable(true);
				for(int cnt=0;cnt<dropDownItemArray.length-1;cnt++){
					dropDownItemList.add(dropDownItemArray[cnt]);
				}
			}else{
				for(int cnt=0;cnt<dropDownItemArray.length;cnt++){
					dropDownItemList.add(dropDownItemArray[cnt]);
				}
			}			
			addComboBoxElements(comboboxcounter,dropDownItemList);
			comboboxcounter++;
		}catch(Exception e){
			System.out.println("Exception in creating comboBox is - "+ e);
		}
	}

	public static void createTextArea(){
		try{			
			scrollPaneOwnCode = new JScrollPane(txtAreaOwnCode,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollPaneOwnCode.setBounds(180, 150, 300, 200);
			contentPane.add(scrollPaneOwnCode);
			txtAreaOwnCode = new JTextArea();						
			//txtAreaOwnCode.setBounds(180, 150, 300, 200);
			scrollPaneOwnCode.setViewportView(txtAreaOwnCode);
			//contentPane.add(txtAreaOwnCode);
		}catch(Exception e){
			System.out.println("Exception in creating textArea is - "+ e);
		}
	}

	//
	static private void getHashMapList(){
		try{
			HashMap hm = new HashMap();
			List<String> keywordCondFields = new ArrayList<String>();
			//List<String> keyList = new ArrayList<String>();
			//List<String> ValueList = new ArrayList<String>();

			keywordCondFields.addAll(ExcelDataList.getExcelDataInListByColName(Initialization.KeywordFile, "dropDownList", TestCreation.selectedChildKeyword));
			for(int cnt=0;cnt<keywordCondFields.size();cnt++){
				String[] keyValue = SplitString.stringSplit(keywordCondFields.get(cnt));
				//keyList.add(keyValue[0]);
				//ValueList.add(keyValue[1]);
				hm.put(keyValue[0], keyValue[1]);
			}

			//For printing hash map values
			/*Set HashMapset = hm.entrySet();
			Iterator itr = HashMapset.iterator();
			while(itr.hasNext()){
				Map.Entry me = (Map.Entry) itr.next();
				System.out.println("key - "+ me.getKey());
				System.out.println("Value - " + me.getValue());
			}*/

		}catch(Exception e){
			System.out.println("Exception in the getting the map list - "+ e);
		}
	}

	//Adding Objects in comboBox
	private static void addComboBoxElements(int index,List<String> elements){
		try{
			comboBox_uservalues[index].removeAllItems();
			int size = elements.size();
			//System.out.println("Size of the Combo box list of items - "+ size);
			for(int i=0;i<size;i++){
				comboBox_uservalues[index].addItem(elements.get(i));
				//System.out.println("Item "+ elements.get(i) + " added to the combo-box");
			}
			comboBox_uservalues[index].setSelectedIndex(0);
		}catch(Exception e){
			System.out.println("Exception in adding elements are - "+ e);
		}

	}

	//Get radio Button selected by user
	private static  String getSelectedButtonText(ButtonGroup buttonGroup) {
		for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
			AbstractButton button = buttons.nextElement();

			if (button.isSelected()) {
				return button.getText();
			}
		}

		return null;
	}


	//Mapping the user provided 
	private static void mapUserProvidedValues(List<String> fieldtype){
		try{
			String value=null;
			System.out.println("Size of the FieldType to map the dynamic value - "+ fieldtype.size());
			for(int itr=0;itr<fieldtype.size();itr++){
				System.out.println("Field Type selected - "+ fieldtype.get(itr));
				if(fieldtype.get(itr).contains("textField") && fieldtype.get(itr).contains("Object")){
					if(KeywordGUIActions.selectedHeaderName.get(itr).contains("element_identifiedBy")){
						elementfindBy = "excel";
						//elementfindValue = ViewObjectRepository.selectedScannedPage +"|"+ ViewObjectRepository.objectSelected ;
						elementfindValue = Keyword.getObjectValue();
					}
					else{
						targetElementFindBy = "excel";
						targetFindValue = ObjectRepositoryView.selectedScannedPage +"|"+ ObjectRepositoryView.objectSelected ;
					}
					userAccessTextBoxValues++;

				}else if(fieldtype.get(itr).contains("textField") && !(fieldtype.get(itr).contains("Object"))){
					System.out.println("Header name to assign ----- "+ KeywordGUIActions.selectedHeaderName.get(itr) + " with the value ------ "+ textField_srcLocatorValue[userAccessTextBoxValues].getText());
					KeywordGUIActions.mapvalue(KeywordGUIActions.selectedHeaderName.get(itr), textField_srcLocatorValue[userAccessTextBoxValues].getText());
					userAccessTextBoxValues++;
				}
				else if(fieldtype.get(itr).contains("dropDown")){
					System.out.println("Header name to assign ----- "+ KeywordGUIActions.selectedHeaderName.get(itr)+ " with the value ------ "+ comboBox_uservalues[userAccessComboBoxValues].getSelectedItem().toString());
					KeywordGUIActions.mapvalue(KeywordGUIActions.selectedHeaderName.get(itr), comboBox_uservalues[userAccessComboBoxValues].getSelectedItem().toString());
					userAccessComboBoxValues++;
				}else if(fieldtype.get(itr).contains("textArea")){
					System.out.println("Header name to assign ----- "+ KeywordGUIActions.selectedHeaderName.get(itr)+ " with the value ------ "+ txtAreaOwnCode.getText().toString());
					KeywordGUIActions.mapvalue(KeywordGUIActions.selectedHeaderName.get(itr),txtAreaOwnCode.getText().toString() );
				}
			}

		}catch(Exception e){
			System.out.println("Exception in mapping user Values to the column headers - "+ e);
		}
	}

	//Storing the values to insert into Excel
	private static void storeValuesToInsert(){
		try{
			checkValuesBeforeInsert();
			elementsToInsert.clear();
			elementsToInsert.add(TestCaseID);
			elementsToInsert.add(TestCaseName);
			elementsToInsert.add(executionFlag);
			elementsToInsert.add(TestStepNum);
			elementsToInsert.add(testStepDesc);			
			elementsToInsert.add(keyword);
			elementsToInsert.add(elementType);
			elementsToInsert.add(elementfindBy);
			elementsToInsert.add(elementfindValue);
			elementsToInsert.add(userValues);
			elementsToInsert.add(targetElementFindBy);
			elementsToInsert.add(targetFindValue);			
			elementsToInsert.add(testStepSegment);			
			elementsToInsert.add(Result);

			System.out.println("Element list to insert in Excel Row - ");
			for(int itr=0;itr<elementsToInsert.size();itr++){	
				System.out.println(elementsToInsert.get(itr));
			}
		}catch(Exception e){
			System.out.println("Exception in storing user Provided test step values");
		}
	}

	private static void checkValuesBeforeInsert(){
		try{
			if(TestStepNum.length()<1){
				TestStepNum = "NA";
			}if(elementfindBy.length()<1){
				elementfindBy = "NA";
			}if(elementfindValue.length()<1){
				elementfindValue="NA";
			}if(userValues.length()<1){
				userValues="NA";
			}if(targetElementFindBy.length()<1){
				targetElementFindBy="NA";
			}if(targetFindValue.length()<1){
				targetFindValue="NA";
			}

		}catch(Exception e){
			System.out.println("Exception in the checkValuesBeforeInsert() - "+ e);
		}
	}

	//Getting all the user provided values for the test step
	private static void getUserProvidedValues(){
		try{
			//Get the predefined values when test case is selected
			TestCaseName = TestCreation.TCSelected;
			TestCaseID = ExcelDataList.getDataByColumnFilter(Initialization.TestExecutionMappingFile, TestCreation.ProjectSelected+ "_suite", "TestCaseName", TestCaseName,"TC_ID").get(0);
			executionFlag = getSelectedButtonText(group);
			TestStepNum="NA";

			//Setting the description
			if(textField_TS_desc.getText().length() <1){
				testStepDesc = "Executing the action - "+ TestCreation.selectedChildKeyword;
			}else{
				testStepDesc = textField_TS_desc.getText();
			}

			keyword = TestCreation.selectedChildKeyword;
			elementType = "NA";
			mapUserProvidedValues(KeywordGUIActions.fieldTypes);
			testStepSegment= comboBox_segment.getSelectedItem().toString();
			Result="NA";

			storeValuesToInsert();

		}catch(Exception e){
			e.printStackTrace();
		}
	}

	// Setting Object value in the Choose-Object field
	public static void setObjectValue(String objectName){
		try{
			if(choose_objectCnt>0){
				textField_srcLocatorValue[chooseObjIndex].setText(objectName);
			}	
		}catch(Exception e){
			System.out.println("Exception in setting the value in the choose-textfield is - "+ e);
		}
	}

	public static String getObjectValue(){
		String objectLocator="";
		try{
			if(choose_objectCnt>0){
				objectLocator= textField_srcLocatorValue[chooseObjIndex].getText();
			}				
		}catch(Exception e){
			System.out.println("Exception in setting the value in the choose-textfield is - "+ e);
		}
		return objectLocator;
	}

	public static void setKeywordValuesToEdit(String keywordEdit){
		try{
			existingStepDetails.clear();
			//existingStepDetails.addAll(ExcelDataList.getExcelDataInListByRowValue(TestCreation.scriptingFile, TestCreation.TCSelected, "Keyword", keywordEdit));
			existingStepDetails.addAll(ExcelDataList.getExcelDataInListByRowIndex(TestCreation.scriptingFile, TestCreation.TCSelected,TestCreation.selectedTestStepNumber));
			textField_TS_desc.setText(existingStepDetails.get(4));
			if(existingStepDetails.get(2).equalsIgnoreCase("Y")){
				rdbtn_Exec_Y.setSelected(true);
			}else{
				rdbtn_Exec_N.setSelected(true);
			}

			comboBox_segment.setSelectedItem(existingStepDetails.get(12));//TODO
			getExistingValues(); //Calling to get value as per keywords-fields


		}catch(Exception e){

		}
	}

	//Getting the values to edit from Excel
	private static void getExistingValues(){
		try{
			//Getting the count header list of the field creation
			int headerSize = 0;
			int ColumnNum;
			int textBoxCntr=0;
			int comboBoxCnt=0;
			String valueFromScriptingSheet;
			String ObjectValues;
			for(int cnt=0;cnt<KeywordGUIActions.fieldInfo.size();cnt++){
				if((KeywordGUIActions.fieldInfo.get(cnt).contains("textField")) && !(KeywordGUIActions.fieldInfo.get(cnt).contains("Object"))){
					ColumnNum = ExcelFunctions.getColumnPosition(Initialization.projectPath+"//TestData//"+ TestCreation.ProjectSelected, TestCreation.ProjectSelected+"_scripting.xls", TestCreation.TCSelected,KeywordGUIActions.selectedHeaderName.get(headerSize));
					valueFromScriptingSheet = ExcelDataList.getValueByRowAndCol(TestCreation.scriptingFile, TestCreation.TCSelected,TestCreation.selectedTestStepNumber , ColumnNum);
					getTextBoxValuesList.add(valueFromScriptingSheet);
					System.out.println("Value retrieved for -"+KeywordGUIActions.selectedHeaderName.get(headerSize) + " is - "+ getTextBoxValuesList.get(textBoxCntr) );
					//textField_srcLocatorValue[textBoxCntr].setText(getTextBoxValuesList.get(textBoxCntr));
					textField_srcLocatorValue[textBoxCntr].setText(valueFromScriptingSheet);
					textBoxCntr++;
					headerSize++;
				}else if((KeywordGUIActions.fieldInfo.get(cnt).contains("textField")) && (KeywordGUIActions.fieldInfo.get(cnt).contains("Object"))){
					if(KeywordGUIActions.selectedHeaderName.get(headerSize).contains("element_identifiedBy")){
						ColumnNum=8;
						ObjectValues = ExcelDataList.getValueByRowAndCol(TestCreation.scriptingFile, TestCreation.TCSelected,TestCreation.selectedTestStepNumber , ColumnNum);
						System.out.println("Object value is - "+ ObjectValues);
					}else{
						ColumnNum=11;
						ObjectValues = ExcelDataList.getValueByRowAndCol(TestCreation.scriptingFile, TestCreation.TCSelected,TestCreation.selectedTestStepNumber , ColumnNum);
						System.out.println("Object value is - "+ ObjectValues);
					}
					//valueFromScriptingSheet = SplitString.stringSplit(ObjectValues)[1]; //setting the entire locator
					valueFromScriptingSheet = ObjectValues;
					getTextBoxValuesList.add(valueFromScriptingSheet);
					System.out.println("Value retrieved for -"+KeywordGUIActions.selectedHeaderName.get(headerSize) + " is - "+ getTextBoxValuesList.get(textBoxCntr) );
					//textField_srcLocatorValue[textBoxCntr].setText(getTextBoxValuesList.get(textBoxCntr));
					textField_srcLocatorValue[textBoxCntr].setText(valueFromScriptingSheet);
					textBoxCntr++;
					headerSize++;

				}else if((KeywordGUIActions.fieldInfo.get(cnt).contains("dropDown"))){
					ColumnNum = ExcelFunctions.getColumnPosition(Initialization.projectPath+"//TestData//"+ TestCreation.ProjectSelected, TestCreation.ProjectSelected+"_scripting.xls", TestCreation.TCSelected,KeywordGUIActions.selectedHeaderName.get(headerSize));
					valueFromScriptingSheet = ExcelDataList.getValueByRowAndCol(TestCreation.scriptingFile, TestCreation.TCSelected,TestCreation.selectedTestStepNumber , ColumnNum);
					getComboBoxValuesList.add(valueFromScriptingSheet);
					System.out.println("Value retrieved for -"+KeywordGUIActions.selectedHeaderName.get(headerSize) + " is - "+ getComboBoxValuesList.get(comboBoxCnt) );
					comboBox_uservalues[comboBoxCnt].setSelectedItem(valueFromScriptingSheet);
					comboBoxCnt++;
					headerSize++;
				}else if((KeywordGUIActions.fieldInfo.get(cnt).contains("textArea"))){
					ColumnNum = ExcelFunctions.getColumnPosition(Initialization.projectPath+"//TestData//"+ TestCreation.ProjectSelected, TestCreation.ProjectSelected+"_scripting.xls", TestCreation.TCSelected,KeywordGUIActions.selectedHeaderName.get(headerSize));
					valueFromScriptingSheet = ExcelDataList.getValueByRowAndCol(TestCreation.scriptingFile, TestCreation.TCSelected,TestCreation.selectedTestStepNumber , ColumnNum);
					txtAreaOwnCode.setText(valueFromScriptingSheet);
					headerSize++;
				}

			}

		}catch(Exception e){
			System.out.println("Exception occured in getExisting user values - "+ e);
		}
	}

	private static List<String> GetDropDownValues(String ChildKeywordAsSelected){			
		int columnNum = ExcelFunctions.getColumnPosition(Initialization.KeywordFileLocation,Initialization.KeywordFileName , "dropDownList", ChildKeywordAsSelected); //Get keyword Column
		List<String> dropDownValues = new ArrayList<String>();
		List<String> dropDownNameList = new ArrayList<String>();
		String dropDownName;
		dropDownValues.addAll(ExcelDataList.getExcelDataInListByColIndex(Initialization.KeywordFile, "dropDownList", columnNum));
		for(int cnt=0;cnt<dropDownValues.size();cnt++){
			dropDownName = SplitString.stringSplit(dropDownValues.get(cnt))[0];
			dropDownNameList.add(dropDownName);
		}
		return dropDownNameList;

	}

	//Performing after the frame is closed
	private static void actionAfterKeywordFrame(){
		try{
			keywordFrame.dispose();
			TestCreation.testCreationframe.setEnabled(true);
			TestCreation.testCreationframe.setFocusable(true);
			System.out.println("Selected Keyword is - "+ TestCreation.selectedKeyword);
			System.out.println("Selected Child Keyword is - "+ TestCreation.selectedChildKeyword);
			//refresh table


		}catch(Exception e){
			System.out.println("Exception after the Keyword frame is done - "+ e);
		}
	}


}

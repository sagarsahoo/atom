package selfitGui;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

import utilityPackage.SplitString;

public class KeywordGUIActions {
	
	//private static List<String> listOfElements = new ArrayList<String>();
	
	static String[][] excelData;
	public static List<String> fieldInfo = new ArrayList<String>();
	static String fieldName;
	static String fieldType;
	public static List<String> selectedHeaderName = new ArrayList<String>();
	public static List<String> fieldTypes = new ArrayList<String>();
	static String dropDownItems;
	public static int objectFieldIndex = 0;
	
	
	@Test
	//Getting the data from Excel for the field creation information
	public static void getFieldDetailsFromExcel(String selectedKeywordToAdd){
		try{
			//getting fields for keywords
			String FieldSheetName = "keywordFields";
			
			excelData = ExcelDataList.getSheetData(Initialization.KeywordFile, FieldSheetName);
			fieldInfo = ExcelDataList.getExcelDataInListByRowValue(Initialization.KeywordFile, FieldSheetName, "Keyword", selectedKeywordToAdd);			
			
		}catch(Exception e){
			System.out.println("Exception occured in getting the fields from excel sheet ");
		}
	}
	
	//Getting the field name and Field type for creating fields
	public static void InputsToCreateFields(){
		try{
			selectedHeaderName.clear();
			fieldTypes.clear();
			
			for(int cnt=1;cnt<fieldInfo.size();cnt++){
				if(fieldInfo.get(cnt).contains("Object")){
					System.out.println("Create Object field");
					fieldName = "Object";
					fieldType = "textField";
					objectFieldIndex=1;
					createField(fieldName,fieldType,cnt);
					Keyword.createChooseButton();					
					selectedHeaderName.add(excelData[0][cnt]); //Storing header name in a list
					fieldTypes.add(fieldInfo.get(cnt));
					
				}else if(fieldInfo.get(cnt).equalsIgnoreCase("NA")){
					System.out.println("No field creation");
					//map NA to variables
					mapvalue(excelData[0][cnt],"NA");
				}else if(fieldInfo.get(cnt).equalsIgnoreCase("textArea")){
					String[] fieldDetails = SplitString.stringSplit(fieldInfo.get(cnt));
					fieldName = fieldDetails[0];
					fieldType = fieldDetails[1];
					if(fieldType.contains("textArea")){
						dropDownItems= fieldDetails[2];
					}
					createField(fieldName,fieldType,cnt);
					selectedHeaderName.add(excelData[0][cnt]);
					fieldTypes.add(fieldType);
				}
				else {
					//get details of Field name and type
					System.out.println("Creation of the Field for ------------------ " + fieldInfo.get(cnt) );
					String[] fieldDetails = SplitString.stringSplit(fieldInfo.get(cnt));
					fieldName = fieldDetails[0];
					fieldType = fieldDetails[1];
					if(fieldType.contains("dropDown")){
						dropDownItems= fieldDetails[2];
					}
					objectFieldIndex=0;
					createField(fieldName,fieldType,cnt);
					selectedHeaderName.add(excelData[0][cnt]);
					fieldTypes.add(fieldType);
				}
			}
			for(int itr=0;itr<selectedHeaderName.size();itr++){
				System.out.println("################## Column headers stored for the selected keywod is - "+ selectedHeaderName.get(itr));
			}
			
			for(int itr=0;itr<fieldTypes.size();itr++){
				System.out.println("################## Field types created are - "+ fieldTypes.get(itr));
			}
		}catch(Exception e){
			
		}
	}
	
	//Creating the fields
	public static void createField(String name,String type,int counter){
		try{
			switch(type){
			case "textField":
				System.out.println("Creation of text field - "+ name);
				Keyword.createLabel(name);
				Keyword.createTextBox(excelData[0][counter]);
				break;
			
			case "dropDown":
				System.out.println("Creation of drop-down - "+ name);
				Keyword.createLabel(name);
				Keyword.createComboBox(excelData[0][counter],dropDownItems);
				break;
				
			case "textArea":
				System.out.println("Creation of text-area - "+ name);
				Keyword.createLabel(name);
				Keyword.createTextArea();
				break;
			}
			
		}catch(Exception e){
			
		}
	}
	
	
// Mapping values
	public static void mapvalue(String header,String value){
		try{
			
			System.out.println("Mapping the header - "+ header + " with the value - "+ value);
			if(value==null){
				value="NA";
			}
			switch(header){
			case "element_identifiedBy":
				Keyword.elementfindBy = value;
				break;
				
			case "element_identifier":
				Keyword.elementfindValue = value;
				break;
				
			case "pass_values":
				Keyword.userValues = value;
				break;
				
			case "target_identifiedBy":
				Keyword.targetElementFindBy = value;
				break;
				
			case "target_identifier":
				Keyword.targetFindValue=value;
				break;
				
			default:
				System.out.println("Column header not matched");
			}
			
		}catch(Exception e){
			System.out.println("Exception in method -mapvalue ---- mapping user Value with the keyword fields - ");
		}
	}
  
}

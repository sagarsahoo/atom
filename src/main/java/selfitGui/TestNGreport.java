package selfitGui;

import java.awt.Desktop;
import java.io.File;

import javax.swing.filechooser.FileNameExtensionFilter;

public class TestNGreport {
	
	
	//Display report
	  public static void displayReport(){
		  try{
			  String url = Initialization.projectPath + "//target//surefire-reports//emailable-report.html";
			  File htmlFile = new File(url);
			  Desktop.getDesktop().browse(htmlFile.toURI());
		  }catch(Exception e){
			  System.out.println("Exception in showing report - "+ e);
		  }
	  }
	  
	  //Display the log file
	  public static void displayLog(){
		  try{
			  String logFileSelected = null;
			  File logFolder = new File(Initialization.projectPath  + "//TestReports//Logs//"+ TestCreation.ProjectSelected + "_suite");
			  File[] listOfHtmlFiles = logFolder.listFiles();
			  
			  for(int cnt=0;cnt<listOfHtmlFiles.length;cnt++){
				  if(listOfHtmlFiles[cnt].getName().contains(".html") && !(listOfHtmlFiles[cnt].getName().contains("errorlog")) ){  //
					  logFileSelected = listOfHtmlFiles[cnt].getName();
					  System.out.println("Log File selected is - "+ logFileSelected);
					  break;
				  }
			  }
			  String log_File = logFolder + "//" + logFileSelected;
			  File LogFile = new File(log_File);
			  Desktop.getDesktop().browse(LogFile.toURI());
		  }catch(Exception e){
			  System.out.println("Exception in showing log file - "+ e);
		  }
		  
	  }
	  
	  public static void openFrameworkDocument(){
		  try{
			  String url = Initialization.FrameworkDocument;
			  File docFile = new File(url);
			  Desktop.getDesktop().browse(docFile.toURI());
			  
		  }catch(Exception e){
			  
		  }
	  }

}

package selfitGui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import qcrallyIntegration.HomeFrame;

public class LoginFrame {

	private JFrame frame;
	private JTextField textFieldUserName;
	private Connection SQLiteConn;
	private JPasswordField passwordField;
	private  JComboBox comboBoxTestType;
	private static List<String> typeOfTesting = new ArrayList<String>();
	static String LOGIN_IMG_PATH = Initialization.projectPath+ "//img//login_icon.png";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginFrame window = new LoginFrame();
					window.frame.setLocationRelativeTo(null);
					window.frame.setVisible(true);
					window.frame.setTitle("ATOM");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try{
			frame = new JFrame();
			frame.getContentPane().setBackground(Color.LIGHT_GRAY);
			frame.setFont(new Font("Calibri", Font.PLAIN, 10));
			frame.setBounds(100, 100, 925, 555);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(null);
			
			JLabel lblUN = new JLabel("USERNAME");
			lblUN.setFont(new Font("Tahoma", Font.BOLD, 15));
			lblUN.setBounds(409, 112, 97, 24);
			frame.getContentPane().add(lblUN);
			
			JLabel lblPassword = new JLabel("PASSWORD");
			lblPassword.setFont(new Font("Tahoma", Font.BOLD, 15));
			lblPassword.setBounds(409, 165, 97, 24);
			frame.getContentPane().add(lblPassword);
			
			textFieldUserName = new JTextField();
			textFieldUserName.setFont(new Font("Calibri", Font.PLAIN, 13));
			textFieldUserName.setBounds(516, 116, 160, 20);
			frame.getContentPane().add(textFieldUserName);
			textFieldUserName.setColumns(10);
			
			JButton btnLogin = new JButton("Login");
			btnLogin.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					/*try{
						SQLiteConn = SQLiteDbConn.sqlDbConnection();
						String credentialQuery = "select empid from master where username=? and password=?";
						PreparedStatement prst = SQLiteConn.prepareStatement(credentialQuery);
						prst.setString(1, textFieldUserName.getText());
						prst.setString(2, passwordField.getText());
						ResultSet rs = prst.executeQuery();
						
						int cnt=0;
						while(rs.next()){
							cnt ++;
						}
						
						if(cnt==1){
							//JOptionPane.showMessageDialog(null, "User Authentication Successfull");
							frame.dispose();
							TestCreation.selfitDesignerFrame();
						}
						else{
							JOptionPane.showMessageDialog(null, "User Authentication failed !!");
							textFieldUserName.setText("");
							passwordField.setText("");
						}
						
						prst.close();
						rs.close();
					
					}catch(Exception loginExp){
						System.out.println("Exception in the user authentication - "+ e);
					}
					finally{
						try {
							SQLiteConn.close();
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}*/
					
					try{
						if(textFieldUserName.getText().equals("testuser") && passwordField.getText().equals("password@123")){
							if(comboBoxTestType.getSelectedItem().equals("UI-Testing")){
								frame.dispose();
								TestCreation.selfitDesignerFrame();
							}
							else if(comboBoxTestType.getSelectedItem().equals("QC-Rally_integration")){
								frame.dispose();
								HomeFrame qcFrame = new HomeFrame();
								String[] args= null;
								qcFrame.main(args);
							}
						}else{
							JOptionPane.showMessageDialog(null, "User Authentication failed !!");
							textFieldUserName.setText("");
							passwordField.setText("");
						}
					}catch(Exception exc){
						System.out.println("Exception in user authentication - "+ e);
					}
				}
			});
			btnLogin.setBounds(553, 321, 102, 29);
			frame.getContentPane().add(btnLogin);
			
			passwordField = new JPasswordField();
			/*passwordField.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try{
						if(textFieldUserName.getText().equals("testuser") && passwordField.getText().equals("password@123")){
							frame.dispose();
							TestCreation.selfitDesignerFrame();
						}else{
							JOptionPane.showMessageDialog(null, "User Authentication failed !!");
							textFieldUserName.setText("");
							passwordField.setText("");
						}
					}catch(Exception exc){
						System.out.println("Exception in user authentication - "+ e);
					}
				}
			});*/
			passwordField.setBounds(516, 169, 160, 20);
			frame.getContentPane().add(passwordField);
			
			JLabel lblTestingType = new JLabel("Type");
			lblTestingType.setFont(new Font("Tahoma", Font.BOLD, 12));
			lblTestingType.setBounds(437, 231, 43, 24);
			frame.getContentPane().add(lblTestingType);
			
			
			 comboBoxTestType = new JComboBox();
			comboBoxTestType.setFont(new Font("Calibri", Font.PLAIN, 14));
			getTestingType();
			comboBoxTestType.setBounds(516, 232, 160, 24);
			frame.getContentPane().add(comboBoxTestType);
			
			BufferedImage login_img = ImageIO.read(new File(LOGIN_IMG_PATH));
		    ImageIcon login_icon=new ImageIcon(login_img);
			JLabel lblNewLabel = new JLabel(login_icon);
			lblNewLabel.setBounds(103, 115, 148, 118);
			frame.getContentPane().add(lblNewLabel);
			
			JLabel lblNewLabel_1 = new JLabel("<html><font color=blue>ATOM - Automation Tool Mart</font></html>");
			lblNewLabel_1.setFont(new Font("Calibri", Font.PLAIN, 27));
			lblNewLabel_1.setBounds(220, 29, 361, 46);
			frame.getContentPane().add(lblNewLabel_1);
		}catch(Exception e){
			System.out.println("Exception in the login frame - "+ e);
		}
		
	}
	
	private void getTestingType(){
		try{
			comboBoxTestType.addItem("UI-Testing");
			comboBoxTestType.addItem("Mobile Testing");
			comboBoxTestType.addItem("WebService Testing");
			comboBoxTestType.addItem("QC-Rally_integration");
		}catch(Exception e){
			
		}
	}
}

package selfitGui;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.JOptionPane;

public class SQLiteDbConn {
	static Connection conn = null;
	public static Connection sqlDbConnection(){
		
		
		try{
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:C:\\Accenture\\Project\\selenium\\guiDesigner\\selfit.sqlite");
			//JOptionPane.showMessageDialog(null, "Connection Established");		
			return conn;
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, "The Exception in DB connection is - "+ e);
			return conn;
		}
	}

}

package selfitGui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.testng.annotations.Test;

public class StepNumber {
  @Test
  public static void setStepNUmberInSequence(File ScriptingFile,String TestCaseSheet) {
	  try{
		  FileInputStream fis = new FileInputStream(ScriptingFile);
		  HSSFWorkbook wb = new HSSFWorkbook(fis);
		  Sheet sh = wb.getSheet(TestCaseSheet);
		  HSSFRow rw;
		  HSSFCell stepNumberCell;
		  int toatlRows = ExcelDataList.getTotalRows(ScriptingFile, TestCaseSheet);
		  for(int cnt=1;cnt<toatlRows;cnt++){
			  rw = (HSSFRow) sh.getRow(cnt);
			  stepNumberCell = rw.getCell(3);
			  stepNumberCell.setCellValue("TS_"+ cnt);
		  }
		  
		  FileOutputStream outputStream = new FileOutputStream(ScriptingFile);
		  wb.write(outputStream);
		  outputStream.close(); 
	  }catch(Exception e){
		  System.out.println("Exception in setting test step number - "+ e);
	  }
  }
}

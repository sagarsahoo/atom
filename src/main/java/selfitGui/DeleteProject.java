package selfitGui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.testng.annotations.Test;

public class DeleteProject {
	static String projectPath = System.getProperty("user.dir");
	static String selectedProjectName;
	
  @Test
  public static void removeProject(String ProjectName) {
	  TestCreation.testCreationframe.setEnabled(false);
	  
	  try{
		  /*String applicationToDelete = JOptionPane.showInputDialog("Enter the Project Name to remove");
		  selectedProjectName=applicationToDelete;*/
		  
		  selectedProjectName=ProjectName;
		  
		  if(selectedProjectName.equals("")){
			  NullPointerException ne = new NullPointerException ();
			  throw ne ;
		  }
		  //deleting the test data folder
		  File testdataFolder = new File(projectPath + "//TestData//"+ selectedProjectName);
		  FileUtils.deleteDirectory(testdataFolder);
		  
		  //deleting package
		  File projectPackage = new File(projectPath + "//src//main//java//"+ selectedProjectName);
		  FileUtils.deleteDirectory(projectPackage);
		  
		  
		  //deleting property file
		  File propertyFile = new File(projectPath + "//objRepo//"+ selectedProjectName+".properties");
		  FileUtils.deleteQuietly(propertyFile);
		  
		  
		  //deleting logfolder
		  File logFolder = new File(projectPath + "//TestReports//Logs//"+ selectedProjectName);
		  FileUtils.deleteDirectory(logFolder);
		  
		  		  
		  //deleting screenshot folder
		  File scrnshotFolder = new File(projectPath + "//TestReports//ScreenShots//"+ selectedProjectName);
		  FileUtils.deleteDirectory(scrnshotFolder);
		  
		  
		  //removing sheet in Execution File
		  String SheetName = selectedProjectName+ "_suite";
		  File ExecutionMappingFile = new File(projectPath+"//TestData//TestExecutionMapping.xls");
		  FileInputStream fis = new FileInputStream(ExecutionMappingFile);
		  HSSFWorkbook wb = new HSSFWorkbook(fis);
		  Sheet sh = wb.getSheet(SheetName);
		  int index = wb.getSheetIndex(sh);
		  wb.removeSheetAt(index);
		  
		  FileOutputStream output = new FileOutputStream(projectPath+"//TestData//TestExecutionMapping.xls");
		  wb.write(output);
		  output.close();
		  
		  //delete the testNG xml file
		  File testngXMLFile = new File(projectPath+ "//" + selectedProjectName+ "_suiteTestNG.xml");
		  FileUtils.deleteQuietly(testngXMLFile);
		  
		  JOptionPane.showMessageDialog(null, "Project deleted !!!!!!");
		  TestCreation.testCreationframe.setEnabled(true);
		  
	  }catch(Exception e){
		  if(selectedProjectName==null){
			  JOptionPane.showMessageDialog(null, "NO project name specified");
		  }
		  else{
			  JOptionPane.showMessageDialog(null, "Error while deleting project");
		  }
		  TestCreation.testCreationframe.setEnabled(true);
	  }
  }
}

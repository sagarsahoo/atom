package selfitGui;



import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;

import utilityPackage.CommonMethod;
import utilityPackage.RepositoryObject;
import utilityPackage.cellToString;
import javax.swing.JComboBox;

public class MasterSheetFrame extends JFrame {

	private JPanel contentPane;
	static MasterSheetFrame MSframe;
	private JTable table;
	static JList listEnv;
	private DefaultListModel envNameModel = new DefaultListModel();
	List<String> HeaderList = new ArrayList<String>();
	List<String> envAllList = new ArrayList<String>();
	JLabel lblExecEnv;
	JComboBox comboBoxEnvList;
	JButton btnSetEnvSelected;
	static Vector headers = new Vector();
	public static DefaultTableModel model = null;
	static Vector data = new Vector();
	static int tableWidth = 0; // set the tableWidth
	static int tableHeight = 0; // set the tableHeight	
	static String selectedEnv = null;
	static String manageOperation="view";
	static String selectedParameterName;
	static String selectedParameterValue;
	static String manageEnvOperation;
	static String selectedEnvForExec;
	static String ObjectPropertyFile;

	static String ADD_ENV = Initialization.projectPath+ "//img//ADD_Page.png";
	static String DELETE_ENV = Initialization.projectPath+ "//img//Delete_img.png";
	static String REFRESH_IMG_PATH = Initialization.projectPath+ "//img//refresh_img.png";


	public static void launchMasterSheetFrame() throws IOException{
		MSframe = new MasterSheetFrame();
		MSframe.setVisible(true);
		MSframe.setLocationRelativeTo(null);
		MSframe.setTitle("Master-Sheet");
		MSframe.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}


	public MasterSheetFrame() throws IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 739, 486);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		displayEnvNames(); //Getting the Environment names

		JLabel lblNewLabel = new JLabel("Environments");
		lblNewLabel.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblNewLabel.setBounds(35, 28, 98, 19);
		contentPane.add(lblNewLabel);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(190, 58, 322, 213);
		contentPane.add(scrollPane_1);

		table = new JTable();
		scrollPane_1.setViewportView(table);

		JButton btnAddParameter = new JButton("Add ");
		btnAddParameter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Add a new Parameter for the Env - ");
				manageEnvOperation="Existing";
				ManageParameterFrame.launchManageParameterFrame();
				displayEnvNames();//Refresh the Env. list
			}
		});
		btnAddParameter.setFont(new Font("Calibri", Font.PLAIN, 11));
		btnAddParameter.setBounds(522, 154, 55, 23);
		contentPane.add(btnAddParameter);

		JButton btnOK = new JButton("OK");
		btnOK.setFont(new Font("Calibri", Font.PLAIN, 11));
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actionAfterCompletion();
			}
		});
		btnOK.setBounds(512, 393, 65, 21);
		contentPane.add(btnOK);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.setFont(new Font("Calibri", Font.PLAIN, 11));
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actionAfterCompletion();
			}
		});
		btnCancel.setBounds(603, 393, 81, 21);
		contentPane.add(btnCancel);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 58, 135, 212);
		contentPane.add(scrollPane);

		listEnv = new JList(envNameModel);
		scrollPane.setViewportView(listEnv);
		listEnv.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Display the parameters for the Env. selected");
				selectedEnv = (String) listEnv.getSelectedValue();
				displaySelectedEnvParameters(selectedEnv);
			}
		});

		JButton btnEditParameter = new JButton("Edit");
		btnEditParameter.setFont(new Font("Calibri", Font.PLAIN, 11));
		btnEditParameter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Editing the parameter - ");
				manageEnvOperation="Existing";
				editParameter();
				displaySelectedEnvParameters(selectedEnv);
			}
		});
		btnEditParameter.setBounds(522, 109, 55, 23);
		contentPane.add(btnEditParameter);

		BufferedImage addEnv_img = ImageIO.read(new File(ADD_ENV));
		ImageIcon add_env_icon=new ImageIcon(addEnv_img);
		JButton btnAddEnv = new JButton(add_env_icon);
		btnAddEnv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Add a new Environment");
				manageEnvOperation = "New";
				ManageParameterFrame.launchManageParameterFrame();
				displayEnvNames();//Refresh the Env. list
				//displayEnvListInComboBox();
			}
		});
		btnAddEnv.setBounds(20, 281, 30, 23);
		contentPane.add(btnAddEnv);

		BufferedImage delEnv_img = ImageIO.read(new File(DELETE_ENV));
		ImageIcon del_env_icon=new ImageIcon(delEnv_img);
		JButton btnDeleteEnv = new JButton(del_env_icon);
		btnDeleteEnv.setBounds(81, 281, 30, 23);
		contentPane.add(btnDeleteEnv);

		lblExecEnv = new JLabel("Execution Env");
		lblExecEnv.setFont(new Font("Calibri", Font.BOLD, 12));
		lblExecEnv.setBounds(20, 349, 91, 19);
		contentPane.add(lblExecEnv);

		comboBoxEnvList = new JComboBox();
		comboBoxEnvList.setFont(new Font("Calibri", Font.PLAIN, 11));
		comboBoxEnvList.setBounds(137, 348, 89, 19);
		contentPane.add(comboBoxEnvList);
		displayEnvListInComboBox();

		btnSetEnvSelected = new JButton("Set");
		btnSetEnvSelected.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Saving the Enviroonmet set for the Execution - ");
				SaveEnvSelectedForExec();
				displayEnvListInComboBox();
			}
		});
		btnSetEnvSelected.setFont(new Font("Calibri", Font.PLAIN, 11));
		btnSetEnvSelected.setBounds(270, 347, 55, 19);
		contentPane.add(btnSetEnvSelected);
		
		BufferedImage Refresh_img = ImageIO.read(new File(REFRESH_IMG_PATH));
		ImageIcon refresh_icon=new ImageIcon(Refresh_img);
		JButton btnRefresh = new JButton(refresh_icon);
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Refresh the MasterShet Frame");
				displayEnvNames();
				displayEnvListInComboBox();
			}
		});
		btnRefresh.setBounds(22, 393, 44, 21);
		contentPane.add(btnRefresh);

	}


	//*****************************************************************************************************************************
	//Getting the Environment names from the master sheet
	private void displayEnvNames(){
		try{
			envNameModel.clear();
			envNameModel.removeAllElements();			
			//envAllList.addAll(ExcelDataList.getExcelDataInListByColName(TestCreation.scriptingFile, "Master_sheet", "Env"));
			Set<String> uniqueEnvList = new HashSet<String>(envAllList);
			envAllList.clear();
			envAllList.addAll(uniqueEnvList);
			for(int cnt=0;cnt<envAllList.size();cnt++){
				envNameModel.addElement(envAllList.get(cnt));
			}			
		}catch(Exception e){
			System.out.println("Exception in getting the Env. names - "+ e);
		}
	}

	private void displayEnvListInComboBox(){
		try{
			if(envAllList.size()>0){ //Adding the Env name in the Combo-box
				for(int cnt=0;cnt<envAllList.size();cnt++){
					comboBoxEnvList.addItem(envAllList.get(cnt));
				}
			}
			comboBoxEnvList.addItem("NA");
			ObjectPropertyFile = TestCreation.ProjectSelected + ".properties";
			selectedEnvForExec = RepositoryObject.IdentifierRepo(ObjectPropertyFile, "ExecutionEnv");			
			if(selectedEnvForExec.equalsIgnoreCase("NA")){ //Checking in env is set in property file
				comboBoxEnvList.setSelectedItem("NA");
			}else{
				comboBoxEnvList.setSelectedItem(selectedEnvForExec);
			}
		}catch(Exception e){
			System.out.println("Exception in displaying Env for execution - ");
		}		
	}

	private void SaveEnvSelectedForExec(){
		try{
			selectedEnvForExec = comboBoxEnvList.getSelectedItem().toString();
			if(selectedEnvForExec.length()>0){
				ObjectPropertyFile = TestCreation.ProjectSelected + ".properties";
				Path path_var = Paths.get(Initialization.projectPath + "\\objRepo\\"+ ObjectPropertyFile);
				Charset charset = StandardCharsets.UTF_8;
				String content = new String(Files.readAllBytes(path_var), charset);
				content = content.replaceAll("ExecutionEnv=.*", "ExecutionEnv="+ selectedEnvForExec);
				Files.write(path_var, content.getBytes(charset));
			}else{
				System.out.println("No env selected");
				JOptionPane.showMessageDialog(null, "Create a Env in master sheet");
			}
		}catch(Exception e){
			System.out.println("Exception in saving the Env for Execution- ");
		}		
	}

	//Getting the parameters from the Master sheet
	private void displaySelectedEnvParameters(String envName){
		try{
			HeaderList.clear();
			data.clear();
			HeaderList.add("parameter");
			HeaderList.add("value");
			headers = ScriptingData.getSelectedHeader(TestCreation.scriptingFile, "Master_sheet", HeaderList);
			data=getEnvParametersData(TestCreation.scriptingFile, "Master_sheet", HeaderList,envName);
			model = new DefaultTableModel(data, headers);
			tableWidth = model.getColumnCount() * 100;
			tableHeight = model.getRowCount() * 25;
			table.setPreferredSize(new Dimension(tableWidth, tableHeight));
			table.setModel(model);
		}catch(Exception e){
			System.out.println("Exception in displaying Env. parameters in the table - "+ e);
		}
	}

	//Getting the parameters in the table
	public static Vector getEnvParametersData(File scriptingFileName,String TCsheet,List<String> HeaderList,String envName){
		Vector TestStepsData = new Vector();
		try{
			File applicationScriptingFile = scriptingFileName;		  			
			FileInputStream fis =  new FileInputStream(applicationScriptingFile);
			HSSFWorkbook workbook =  new HSSFWorkbook (fis);
			Sheet sheet = workbook.getSheet(TCsheet);
			Vector d ;			  
			//getting column locations
			List<String> columnsLoc = new ArrayList<String>();
			for(int cnt=0;cnt<HeaderList.size();cnt++){
				columnsLoc.add(Integer.toString(ExcelDataList.getColumnIndex(scriptingFileName, sheet, HeaderList.get(cnt))));
			}
			TestStepsData.clear();
			HSSFCell cell = null;
			String value=null;
			for (int j = 1; j <= sheet.getLastRowNum(); j++) {
				HSSFRow row = (HSSFRow) sheet.getRow(j);
				cell = row.getCell(0);
				value = cellToString.cellTostring_HSSF(cell);
				if(value.equals(envName)){
					d = new Vector();					   
					for(int colCnt=0;colCnt<columnsLoc.size();colCnt++){
						cell = row.getCell(Integer.parseInt(columnsLoc.get(colCnt)));
						value = cellToString.cellTostring_HSSF(cell);
						d.add(value);
					}
					d.add("\n");
					TestStepsData.add(d);
				}
			}
			return TestStepsData;
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, "Error in getting Test Steps - "+ e);
			return TestStepsData;
		}
	}

	private void editParameter(){
		try{
			manageOperation = "editParameter";

			if(selectedEnv==null){
				JOptionPane.showMessageDialog(null, "Please select an Env.");
			}else{
				selectedEnv = listEnv.getSelectedValue().toString();
				if(table.getSelectedRow()<0){
					JOptionPane.showMessageDialog(null, "Please select a Parameter to Edit");
				}else{
					int selectedRow = (table.getSelectedRow()+1);
					selectedParameterName = ExcelDataList.getValueByRowAndCol(TestCreation.scriptingFile, "Master_sheet", selectedRow, 1);
					selectedParameterValue = ExcelDataList.getValueByRowAndCol(TestCreation.scriptingFile, "Master_sheet", selectedRow, 2);
					ManageParameterFrame.launchManageParameterFrame();
				}
			}

		}catch(Exception e){
			System.out.println("Exception in editing parameter is - "+ e);
		}
	}

	private void createNewEnvironment(){
		manageEnvOperation = "New";
		ManageParameterFrame.launchManageParameterFrame();
	}

	//Action to be performed on OK/cancel
	private void actionAfterCompletion(){
		MSframe.dispose();
		TestCreation.testCreationframe.setEnabled(true);
		TestCreation.testCreationframe.setFocusable(true);
	}

}

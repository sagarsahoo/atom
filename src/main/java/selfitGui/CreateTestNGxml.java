package selfitGui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;

import utilityPackage.ExcelFunctions;

public class CreateTestNGxml {
	
	static WebDriver driver = null;
	static String path = System.getProperty("user.dir");
    static String wb_loc =path + "\\TestData";
    static String TestExecutionMappingtData[][];
    static int execFlagColumn;
    static int classNameColumn;
    static int testcaseColumn;
    static String excelMappingFile = "TestExecutionMapping.xls";
    
    static Logger CreateTestNGxml_log = Logger.getLogger(CreateTestNGxml.class);

    
  @Test
  public static void getFromExcel (String testSuiteSheet) throws IOException
	{
	  		int[] RC_count = ExcelFunctions.getRowColumn(testSuiteSheet, wb_loc, excelMappingFile);
	  		CreateTestNGxml_log.info("Row is - "+ RC_count[0]);
	  		CreateTestNGxml_log.info("Column is - "+ RC_count[1]);
	        
	        TestExecutionMappingtData = new String[RC_count[0]][RC_count[1]];	        
	        TestExecutionMappingtData = ExcelFunctions.readKeywordFunc(driver, testSuiteSheet, wb_loc, excelMappingFile);
	        
	        execFlagColumn = ExcelFunctions.getColumnPosition(wb_loc, "TestExecutionMapping.xls", testSuiteSheet, "Exec_flag");
	        CreateTestNGxml_log.info("execution column is - "+ execFlagColumn);
	        classNameColumn = ExcelFunctions.getColumnPosition(wb_loc, "TestExecutionMapping.xls", testSuiteSheet, "className");
	        testcaseColumn = ExcelFunctions.getColumnPosition(wb_loc, "TestExecutionMapping.xls", testSuiteSheet, "TestCaseName");
	        
	        CreateTestNGxml_log.info("Excel read");
	        
	        
	        
	        try {
	        	
		        //Initializing the XML document
		        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		        DocumentBuilder builder = factory.newDocumentBuilder();
		        Document document = builder.newDocument();
		        Element rootElement = document.createElement("suite");
		        document.appendChild(rootElement);
		        rootElement.setAttribute("name","Test suite "+ testSuiteSheet);
	         	rootElement.setAttribute("preserve-order", "true");
	          
	          
	          Element Listeners = document.createElement("listeners");
		      Element Listener = document.createElement("listener");
	          rootElement.appendChild(Listeners);
	          Listeners.appendChild(Listener);
	          Listener.setAttribute("class-name", "utilityPackage.Listener");
	          
	          
	          for(int row=1;row<TestExecutionMappingtData.length;row++){
		        	if(TestExecutionMappingtData[row][execFlagColumn].equalsIgnoreCase("Y")){
		        		CreateTestNGxml_log.info("Test case should be executed");
		        		Element Test = document.createElement("test");
		        		rootElement.appendChild(Test);
		        		Test.setAttribute("name", TestExecutionMappingtData[row][testcaseColumn]);
		        		
		        		Element Classes = document.createElement("classes");
		        		Element Class = document.createElement("class");
		        		Test.appendChild(Classes);
		        		Classes.appendChild(Class);
		        		Class.setAttribute("name",TestExecutionMappingtData[row][classNameColumn]);
		        		//Class.setAttribute("name",TestExecutionMappingtData[row][classNameColumn]+"."+TestExecutionMappingtData[row][testcaseColumn]);
		        		
		        	}	        	
		        }
	          
		        
		        
		       
		        
		        
		        TransformerFactory tFactory = TransformerFactory.newInstance();

		        Transformer transformer = tFactory.newTransformer();
		        //Add indentation to output
		        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
		        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		        DOMImplementation domImpl = document.getImplementation();
		        
		        //DocumentType doctype = domImpl.createDocumentType("doctype","-//Oberon//YOUR PUBLIC DOCTYPE//EN","http://testng.org/testng-1.0.dtd");
		        DocumentType doctype = domImpl.createDocumentType("doctype","SYSTEM","http://testng.org/testng-1.0.dtd");
		        
		        //transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, doctype.getPublicId());
		        transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());

		        DOMSource source = new DOMSource(document);
		        StreamResult result = new StreamResult(new File(path +"//"+ testSuiteSheet + "TestNG.xml"));
		        //StreamResult result = new StreamResult(System.out);
		        transformer.transform(source, result);

		    }
		    catch (ParserConfigurationException e) {
		    	CreateTestNGxml_log.error("ParserConfigurationException " + e.getMessage());
		    } catch (TransformerConfigurationException e) {
		    	CreateTestNGxml_log.error("TransformerConfigurationException "+ e.getMessage());
		    } catch (TransformerException e) {
		    	CreateTestNGxml_log.error("TransformerException " + e.getMessage());
		    }    
	  		
	  	}
	  
	    
}

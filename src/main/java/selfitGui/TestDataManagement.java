package selfitGui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class TestDataManagement extends JFrame {
	
	public static TestDataManagement testDataFrame;
	private JPanel contentPane;
	private static JTable table;
	static JScrollPane scrollPane_1;
	static Vector testDataHeaders = new Vector();
	public static DefaultTableModel testDataModel = null;
	static Vector testData = new Vector();
	static int tableWidthTestData = 0; // set the tableWidth
	static int tableHeightTestData = 0; // set the tableHeight	 
	public static File testDataFile;
	
	static JButton btnManage;
	static JButton btnNewRow;
	static JButton btnOrgToSelect;
	static JButton btnSelectedToOriginal;
	static JButton btnCreate;
	static JScrollPane scrollPane_listTestSteps;
	static JList listTestSteps;
	static DefaultListModel TSdesc_notSelectedModel = new DefaultListModel();
	static JScrollPane scrollPane_listTestStepsSelected;
	static JList listTestStepsSelected;
	static DefaultListModel TSdesc_SelectedModel = new DefaultListModel();
	static JButton btnViewData;
	
	public static List<String> OriginaltestDescriptions = new ArrayList<String>();
	public static List<String> testDescriptions_notSelected = new ArrayList<String>();
	public static List<String> testDescriptions_selected = new ArrayList<String>();
	public static List<String> defaultHeaderName = new ArrayList<String>();
	public static List<String> existingHeaderList = new ArrayList<String>();
	
	static List<String> userSelectedListFromOriginal = new ArrayList<String>();
	static List<String> userSelectedListFromSelected = new ArrayList<String>();
	private static JButton btnEditRow;
	static String EDIT_IMG_PATH = Initialization.projectPath+ "//img//edit_img.png";
	static  String ADDDATAROW_IMG_PATH = Initialization.projectPath+ "//img//Project_ADD.png";
	static String REFRESH_IMG_PATH = Initialization.projectPath+ "//img//refresh_img.png";
	private JScrollPane scrollPane;
	public static String testDataAction ="Add";
	public static int selectedTestDataRow;
	
	
	public static void getFrame(){
		try{
			TestCreation.testCreationframe.setEnabled(false);
			testDataFrame = new TestDataManagement();
			testDataFrame.setLocationRelativeTo(null);
			testDataFrame.setTitle("Test Data Management");
			testDataFrame.setVisible(true);
			testDataFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);			 
		}catch(Exception e){
			System.out.println("Exception in opening the TestData Frame - "+ e);
		}
	}

	public TestDataManagement() throws IOException {
		
		String testDataExcelName = "UserInput_" + TestCreation.ProjectSelected +".xls" ;
		testDataFile = new File(Initialization.projectPath + "//TestData//"+ TestCreation.ProjectSelected + "//" + testDataExcelName);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 674, 605);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(121, 37, 457, 209);
		contentPane.add(scrollPane);
		
		scrollPane_1 = new JScrollPane();
		scrollPane.setViewportView(scrollPane_1);
		scrollPane_1.setVisible(false);
		
		table = new JTable();
		scrollPane_1.setViewportView(table);
		table.setVisible(false);
		
		btnManage = new JButton("Manage");
		btnManage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Manage the test data fields");
				manageFields();
			}
		});
		btnManage.setBounds(10, 37, 89, 23);
		contentPane.add(btnManage);
		
		BufferedImage AddData_img = ImageIO.read(new File(ADDDATAROW_IMG_PATH));
		ImageIcon AddData_icon=new ImageIcon(AddData_img);
		btnNewRow = new JButton(AddData_icon);
		btnNewRow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Add a new TestDataRow
				testDataAction ="Add";
				testDataFrame.setEnabled(false);
				TestDataRows.getKeywordFrame();
			}
		});
		btnNewRow.setBounds(588, 156, 28, 23);
		contentPane.add(btnNewRow);
		
		btnOrgToSelect = new JButton(">>");
		btnOrgToSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Action on selecting the test steps from original list and transfer to selected List");
				transferListToSelected();
			}
		});
		btnOrgToSelect.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnOrgToSelect.setBounds(282, 325, 62, 23);
		contentPane.add(btnOrgToSelect);
		btnOrgToSelect.setVisible(false);
		
		btnSelectedToOriginal = new JButton("<<");
		btnSelectedToOriginal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Action on selecting the test steps from selected list to original ");
				transferListToOriginal();
			}
		});
		btnSelectedToOriginal.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnSelectedToOriginal.setBounds(282, 423, 62, 23);
		contentPane.add(btnSelectedToOriginal);
		btnSelectedToOriginal.setVisible(false);
		
		btnCreate = new JButton("Create");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Inserting header");
				createHeaderForTestData();
			}
		});
		btnCreate.setBounds(504, 364, 74, 23);
		contentPane.add(btnCreate);
		btnCreate.setVisible(false);
		
		scrollPane_listTestSteps = new JScrollPane();
		scrollPane_listTestSteps.setBounds(121, 291, 140, 207);
		contentPane.add(scrollPane_listTestSteps);
		scrollPane_listTestSteps.setVisible(false);
		
		listTestSteps = new JList(TSdesc_notSelectedModel);
		scrollPane_listTestSteps.setViewportView(listTestSteps);
		
		scrollPane_listTestStepsSelected = new JScrollPane();
		scrollPane_listTestStepsSelected.setBounds(354, 291, 140, 207);
		contentPane.add(scrollPane_listTestStepsSelected);
		scrollPane_listTestStepsSelected.setVisible(false);
		
		listTestStepsSelected = new JList(TSdesc_SelectedModel);
		scrollPane_listTestStepsSelected.setViewportView(listTestStepsSelected);
		
		JButton btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Action after completion");
				actionAfterTestDataFrame();
			}
		});
		btnOK.setBounds(393, 531, 64, 23);
		contentPane.add(btnOK);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actionAfterTestDataFrame();
			}
		});
		btnCancel.setBounds(506, 531, 89, 23);
		contentPane.add(btnCancel);
		
		btnViewData = new JButton("View");
		btnViewData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("View the Test Data");
				displayTestDataInTable();
			}
		});
		btnViewData.setBounds(10, 75, 89, 23);
		contentPane.add(btnViewData);
		
		BufferedImage TD_edit_img = ImageIO.read(new File(EDIT_IMG_PATH));
	    ImageIcon TD_editIcon=new ImageIcon(TD_edit_img);
		btnEditRow = new JButton(TD_editIcon);
		btnEditRow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				testDataAction="edit"; //Setiing the action as Edit
				if(table.getSelectedRow()>=0){
					selectedTestDataRow = (table.getSelectedRow()+1); //Data row selected
					testDataFrame.setEnabled(false);
					TestDataRows.getKeywordFrame();	//Calling the frame - testDataRows
				}else{
					JOptionPane.showMessageDialog(null, "Please select a row to edit !");
				}							
			}
		});
		btnEditRow.setBounds(588, 109, 28, 23);
		contentPane.add(btnEditRow);
		
		BufferedImage Refresh_img = ImageIO.read(new File(REFRESH_IMG_PATH));
		ImageIcon refresh_icon=new ImageIcon(Refresh_img);
		JButton btnRefresh = new JButton(refresh_icon);
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Refreshing the testData Management - ");
				displayTestDataInTable();
			}
		});
		btnRefresh.setBounds(10, 530, 34, 25);
		contentPane.add(btnRefresh);
		btnEditRow.setVisible(false);
		
		displayTestDataInTable();
	}
	
	//Display TestData in table
	public static void displayTestDataInTable(){
		try{
			scrollPane_1.setVisible(true);
			table.setVisible(true);
			
			//String projectName = TestCreation.ProjectSelected;
			//String TCSelectedsheet = TestCreation.TCSelected;
			System.out.println("Selected Test case sheet is - "+ TestCreation.TCSelected);
 			
 			testDataHeaders = ScriptingData.getHeader(testDataFile, TestCreation.TCSelected);
 			testData= ScriptingData.getTestSteps(testDataFile, TestCreation.TCSelected);
 			
 			testDataModel = new DefaultTableModel(testData, testDataHeaders);
 			tableWidthTestData = testDataModel.getColumnCount() * 100;
 			tableHeightTestData = testDataModel.getRowCount() * 25;
 			
 			table.setPreferredSize(new Dimension(tableWidthTestData, tableHeightTestData));
 			table.setModel(testDataModel);
 			
 			btnEditRow.setVisible(true);
 			
		}catch(Exception e){
			System.out.println("Exception in displaying the test data - "+ e);
		}
	}
	
	//Manage the test data fields
	private static void manageFields(){
		try{
			btnOrgToSelect.setVisible(true);;
			btnSelectedToOriginal.setVisible(true);;
			btnCreate.setVisible(true);;
			scrollPane_listTestSteps.setVisible(true);;
			scrollPane_listTestStepsSelected.setVisible(true);
			getTestStepList();
			
			
		}catch(Exception e){
			System.out.println("Exception in managing the test data fields - "+ e);
		}
	}
	
	private static void getTestStepList(){
		try{
			OriginaltestDescriptions.clear();
			OriginaltestDescriptions.addAll(ExcelDataList.getExcelDataInListByColName(TestCreation.scriptingFile, TestCreation.TCSelected, "TS_NAME"));
			
			//Getting list of test steps selected - by getting the header name
			defaultHeaderName.add("ID");
			defaultHeaderName.add("RESULT");
			
			existingHeaderList.addAll(ExcelDataList.getExcelDataInListByRowIndex(testDataFile, TestCreation.TCSelected, 0));
			existingHeaderList.removeAll(defaultHeaderName);
			
			testDescriptions_selected.clear(); //Clearing the list
			testDescriptions_selected.addAll(ExcelDataList.getExcelDataInListByRowIndex(testDataFile, TestCreation.TCSelected, 0));
			testDescriptions_selected.removeAll(defaultHeaderName);			
			TSdesc_SelectedModel.clear();
			TSdesc_SelectedModel.removeAllElements();
			for(int cnt=0;cnt<testDescriptions_selected.size();cnt++){
				TSdesc_SelectedModel.addElement(testDescriptions_selected.get(cnt));
			}
			
			testDescriptions_notSelected.clear();
			testDescriptions_notSelected.addAll(OriginaltestDescriptions);
			testDescriptions_notSelected.removeAll(testDescriptions_selected);
			TSdesc_notSelectedModel.clear();
			TSdesc_notSelectedModel.removeAllElements();
			for(int cnt=0;cnt<testDescriptions_notSelected.size();cnt++){
				TSdesc_notSelectedModel.addElement(testDescriptions_notSelected.get(cnt));
			}
			
		}catch(Exception e){
			System.out.println("Exception in getting the original test step descriptions - "+ e);
		}
	}
	
	//Pressing the >> button
	private static void transferListToSelected(){
		try{
			userSelectedListFromOriginal.clear();
			userSelectedListFromOriginal.addAll(listTestSteps.getSelectedValuesList());
			
			TSdesc_notSelectedModel.clear();
			TSdesc_notSelectedModel.removeAllElements();
			testDescriptions_notSelected.removeAll(userSelectedListFromOriginal);
			for(int cnt=0;cnt<testDescriptions_notSelected.size();cnt++){
				TSdesc_notSelectedModel.addElement(testDescriptions_notSelected.get(cnt));
			}
			
			TSdesc_SelectedModel.clear();
			TSdesc_SelectedModel.removeAllElements();
			testDescriptions_selected.addAll(userSelectedListFromOriginal);
			for(int cnt=0;cnt<testDescriptions_selected.size();cnt++){
				TSdesc_SelectedModel.addElement(testDescriptions_selected.get(cnt));
			}
		}catch(Exception e){
			System.out.println("Exception in Transfer of List from ORiginal to Selected is - "+ e);
		}
	}
	
	//Pressing the  << button
	private static void transferListToOriginal(){
		try{
			userSelectedListFromSelected.clear();
			userSelectedListFromSelected.addAll(listTestStepsSelected.getSelectedValuesList());
			
			TSdesc_notSelectedModel.clear();
			TSdesc_notSelectedModel.removeAllElements();
			testDescriptions_notSelected.addAll(userSelectedListFromSelected);
			for(int cnt=0;cnt<testDescriptions_notSelected.size();cnt++){
				TSdesc_notSelectedModel.addElement(testDescriptions_notSelected.get(cnt));
			}
			
			TSdesc_SelectedModel.clear();
			TSdesc_SelectedModel.removeAllElements();
			testDescriptions_selected.removeAll(userSelectedListFromSelected);
			for(int cnt=0;cnt<testDescriptions_selected.size();cnt++){
				TSdesc_SelectedModel.addElement(testDescriptions_selected.get(cnt));
			}
		}catch(Exception e){
			System.out.println("Exception in Transfer of List from Selected to Original is - "+ e);
		}
	}
	
	//Creating headers
	private static void createHeaderForTestData(){
		try{
			testDescriptions_selected.removeAll(existingHeaderList);
			ExcelDataList.insertHeaderInTestData(testDataFile, TestCreation.TCSelected, testDescriptions_selected);			
			displayTestDataInTable();
		}catch(Exception e){
			System.out.println("Exception in creating header - "+ e);
		}
	}
	
	//
	private static void actionAfterTestDataFrame(){
		try{
			testDataFrame.dispose();
			TestCreation.testCreationframe.setEnabled(true);
			TestCreation.testCreationframe.setFocusable(true);
		}catch(Exception e){
			
		}
	}
}

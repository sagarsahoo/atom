package selfitGui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JProgressBar;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class ObjectRepositoryScan extends JFrame {
	private JTextField textFieldUrl;
	static ObjectRepositoryScan repoFrame;
	private JTextField textFieldPageName;
	static JButton btnOKScan;
	static JProgressBar progressBar;

	
	public static void getObjectRepositoryFrame(){
		TestCreation.testCreationframe.setEnabled(false);
		repoFrame = new ObjectRepositoryScan();
		repoFrame.setLocationRelativeTo(null);
		repoFrame.setTitle("Scan-URL");
		repoFrame.setVisible(true);
		repoFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		
	}

	public ObjectRepositoryScan() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 479, 299);
		getContentPane().setLayout(null);
		
		JLabel lblPageURLToScan = new JLabel("URL");
		lblPageURLToScan.setFont(new Font("Calibri", Font.BOLD, 12));
		lblPageURLToScan.setBounds(254, 26, 30, 16);
		getContentPane().add(lblPageURLToScan);
		
		textFieldUrl = new JTextField();
		textFieldUrl.setBounds(254, 53, 135, 18);
		getContentPane().add(textFieldUrl);
		textFieldUrl.setColumns(10);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 110, 240);
		getContentPane().add(panel);
		
		JButton btnScan = new JButton("scan");
		//Clicking on Scan button
		btnScan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				repoFrame.setEnabled(false);
				/*progressBar.setEnabled(true);
				progressBar.setVisible(true);
				progressBar.setIndeterminate(true);*/
				System.out.println("Scan has been started");
				
				String pageurl = textFieldUrl.getText().toString();
				//Called function to add objects
				PageScanner.getWebElements(pageurl,textFieldPageName.getText());
				
				repoFrame.setEnabled(true);
				btnOKScan.setVisible(true);
				//TestCreation.testCreationframe.setEnabled(true);	
			}
		});
		btnScan.setBounds(349, 88, 64, 23);
		getContentPane().add(btnScan);
		
		
		
		JButton btnCancel = new JButton("cancel");
		btnCancel.setFont(new Font("Calibri", Font.PLAIN, 12));
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Action after frame completes");
				actionAfterComplete();
			}
		});
		btnCancel.setBounds(334, 223, 78, 20);
		getContentPane().add(btnCancel);
		
		JLabel lblPagename = new JLabel("Page-Name");
		lblPagename.setFont(new Font("Calibri", Font.BOLD, 12));
		lblPagename.setBounds(142, 27, 73, 15);
		getContentPane().add(lblPagename);
		
		textFieldPageName = new JTextField();
		textFieldPageName.setBounds(142, 53, 86, 18);
		getContentPane().add(textFieldPageName);
		textFieldPageName.setColumns(10);
		
		btnOKScan = new JButton("OK");
		btnOKScan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Action after frame completes");
				actionAfterComplete();
			}
		});
		btnOKScan.setBounds(246, 223, 59, 21);
		getContentPane().add(btnOKScan);
		btnOKScan.setVisible(false);
		
		progressBar = new JProgressBar();
		progressBar.setBounds(189, 141, 146, 14);
		getContentPane().add(progressBar);
		progressBar.setVisible(false);
		
	}
	
	private void actionAfterComplete(){
		try{
			repoFrame.dispose();
			if(ObjectRepositoryView.ScanObjectRepoFrameLauchedFRom.equalsIgnoreCase("ViewRepo")){
				ObjectRepositoryView.objRepoViewFrame.setEnabled(true);
				ObjectRepositoryView.objRepoViewFrame.setFocusable(true);
				ObjectRepositoryView.displayPages();
			}else{
				TestCreation.testCreationframe.setEnabled(true);
				TestCreation.testCreationframe.setFocusable(true);
			}
					
		}catch(Exception e){
			System.out.println("Exception in executing actions after scamn frame completes - "+ e);
		}
	}
}

package selfitGui;

import java.io.File;

import org.testng.annotations.Test;

public class Initialization {
	
	public static String projectPath = System.getProperty("user.dir");
	public static File KeywordFile = new File(projectPath+ "\\selenium_documents\\KeywordList.xls");
	public static File TestExecutionMappingFile = new File(projectPath + "\\TestData\\TestExecutionMapping.xls");
	public static File ObjectRepositoryFile = new File (projectPath+"\\TestData\\"+ TestCreation.ProjectSelected+ "\\ObjectProperty.xls");
	public static String[][] KeywordFieldDefinationData;
	String FieldSheetName="keywordFields";
	public static String  FrameworkDocument = projectPath+"\\selenium_documents\\SeleniumFramework_v1.2.docx";
	public static String KeywordFileLocation = projectPath+ "\\selenium_documents\\";
	public static String KeywordFileName = "KeywordList.xls";
	
	public Initialization(){
		KeywordFieldDefinationData = ExcelDataList.getSheetData(Initialization.KeywordFile, FieldSheetName);
	}
	
  @Test
  public void f() {
  }
}

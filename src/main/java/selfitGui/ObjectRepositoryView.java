package selfitGui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ObjectRepositoryView extends JFrame {

	static String projectPath = System.getProperty("user.dir");
	private JPanel contentPane;
	public static ObjectRepositoryView objRepoViewFrame;
	static JComboBox<String> comboBoxElementType;
	static JList listScanPages;
	static String selectedElementType;
	private static DefaultListModel<String> WebPagesModel = new DefaultListModel();
	private static DefaultListModel scannedElementsModel = new DefaultListModel();
	public static List<String> getSelectedObjectList = new ArrayList<String>();
	private JButton btnOK;
	private JButton btnCancel;
	private static JList listSelectedElements;
	private JScrollPane scrollPane_1;
	static DefaultTableModel propertyDetailmodel = null;
	static Vector headers = new Vector();
	static Vector data = new Vector();
	static int tableWidth = 0; // set the tableWidth
	static int tableHeight = 0; // set the tableHeight	
	private static JTable table;
	private static JScrollPane scrollPane_2;
	private static JButton btnEditObject;
	
	public static String selectedScannedPage;
	public static String objectSelected;
	public static String typeSelected;
	public static String objRepoFilepath;
	public static File objectExtractionFile ;
	static List<String> selectedObjectRowValues = new ArrayList<String>();
	public static String objRepoLaunchedFrom=null;
	public static List<String> SelectedObjectProperty = new ArrayList<String>();
	static  String IMG_PATH = Initialization.projectPath+ "//img//12_em_plus.png";
	static String ADD_PAGE = Initialization.projectPath+ "//img//ADD_Page.png";
	public static String objectPtropertyFrameLauched;
	JButton btnAddObjectIcon;
	public static String ScanObjectRepoFrameLauchedFRom;
	private JButton btnDeleteObject;
	private JButton btnDeletePage;
	static String DELETE_IMG_PATH = Initialization.projectPath+ "//img//Delete_img.png";
	static int rowNumToEdit;
	
	
	
	
	public static void getFrame(){
		try{
			TestCreation.testCreationframe.setEnabled(false);
			objRepoViewFrame = new ObjectRepositoryView();
			objRepoViewFrame.setLocationRelativeTo(null);
			objRepoViewFrame.setTitle("View Object Repository");
			objRepoViewFrame.setVisible(true);
			objRepoViewFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			SelectedObjectProperty.clear();
			 
		}catch(Exception e){
			System.out.println("Exception in opening the frame to view OR - "+ e);
		}
	}

	public ObjectRepositoryView() throws IOException {
		
		objRepoFilepath = projectPath+ "//TestData//" + TestCreation.ProjectSelected + "//ObjectProperty.xls";
		objectExtractionFile = new File(objRepoFilepath);
		System.out.println("Project selected is - "+ objRepoFilepath);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 630, 426);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPages = new JLabel("Pages");
		lblPages.setFont(new Font("Calibri", Font.PLAIN, 15));
		lblPages.setBounds(32, 11, 44, 14);
		contentPane.add(lblPages);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 36, 102, 177);
		contentPane.add(scrollPane);
		
		listScanPages = new JList(WebPagesModel);
		listScanPages.setFont(new Font("Tahoma", Font.PLAIN, 10));
		//Selecting the web-page to see the scanned objects
		listScanPages.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {							
				System.out.println("DIsplay the Object LIst for the selected Page");
				displayElementsOnPageSelection();
				displayObjectsOfScannedPage();
			}
		});
		scrollPane.setViewportView(listScanPages);
		//Calling function to display all the sheets in the object repository
		displayPages();
		
		comboBoxElementType = new JComboBox<String>();
		//Selecting the type of the object
		comboBoxElementType.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displaySelectedObjects();
			}
		});
		comboBoxElementType.setBounds(137, 8, 125, 23);
		contentPane.add(comboBoxElementType);
		comboBoxElementType.setVisible(false);
		
		btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				objRepoViewFrame.dispose();
				System.out.println("Object Repository launched from - "+ ObjectRepositoryView.objRepoLaunchedFrom);
				System.out.println("selected Object value - "+ table.getSelectedRow());				
				if(ObjectRepositoryView.objRepoLaunchedFrom.equalsIgnoreCase("TestCreation")){
					TestCreation.testCreationframe.setEnabled(true);
					TestCreation.testCreationframe.setFocusable(true);
				}else{
					System.out.println("Selected object is - "+ objectSelected);
					Keyword.keywordFrame.setEnabled(true);
					Keyword.keywordFrame.setVisible(true);
					Keyword.setObjectValue(selectedScannedPage+ "|"+objectSelected);
				}
				
				SelectedObjectProperty.addAll(ExcelDataList.getExcelDataInListByRowValue(objectExtractionFile, selectedScannedPage, "Name", objectSelected));
				System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
				System.out.println("IdentifiedBy - "+ SelectedObjectProperty.get(2));
				System.out.println("IdentifiedBy - "+ SelectedObjectProperty.get(3));
				
			}
		});
		btnOK.setBounds(353, 337, 68, 23);
		contentPane.add(btnOK);
		btnOK.setVisible(false);
		
		btnCancel = new JButton("Cancel");
		//Close frame on Cancel and go back to main frame
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				objRepoViewFrame.dispose();
				if(ObjectRepositoryView.objRepoLaunchedFrom.equalsIgnoreCase("TestCreation")){
					TestCreation.testCreationframe.setEnabled(true);
					TestCreation.testCreationframe.setFocusable(true);
					//TestCreation.selectedKeyword="browser";
				}else{
					Keyword.keywordFrame.setEnabled(true);
					Keyword.keywordFrame.setVisible(true);
				}
			}
		});
		btnCancel.setBounds(470, 337, 75, 23);
		contentPane.add(btnCancel);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(137, 37, 125, 177);
		contentPane.add(scrollPane_1);
		scrollPane_1.setVisible(false);
		
		listSelectedElements = new JList(scannedElementsModel);
		//Display detail selected object property
		listSelectedElements.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				System.out.println("Display on Object selection");
				displayElementOnObjectSelection();
				displayDetailObjectProperty();
				btnOK.setVisible(true);
			}
		});
		
		/*listSelectedElements.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				displayDetailObjectProperty();
			}
		});*/
		listSelectedElements.setFont(new Font("Tahoma", Font.PLAIN, 10));
		scrollPane_1.setViewportView(listSelectedElements);
		listSelectedElements.setVisible(false);
		
		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(300, 36, 175, 177);
		contentPane.add(scrollPane_2);
		
		table = new JTable(propertyDetailmodel);
		scrollPane_2.setViewportView(table);
		scrollPane_2.setVisible(false);
		table.setVisible(false);
		
		//displayAddIcon();
		BufferedImage img = ImageIO.read(new File(IMG_PATH));
	    ImageIcon icon=new ImageIcon(img);
		btnAddObjectIcon = new JButton(icon);
		btnAddObjectIcon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(selectedScannedPage!=null){
					System.out.println("Adding object inside the page - "+ selectedScannedPage);
					objectPtropertyFrameLauched="new";
					ObjectPropertyElement.getFrame();
					objRepoViewFrame.setEnabled(false);
				}else{
					JOptionPane.showMessageDialog(null, "Please select a page to add Objects");
				}
				
			}
		});
		btnAddObjectIcon.setBounds(207, 225, 27, 23);
		contentPane.add(btnAddObjectIcon);
		btnAddObjectIcon.setVisible(false);
		
		btnEditObject = new JButton("Edit");
		btnEditObject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setObjectUpdatedValue();
			}
		});
		btnEditObject.setBounds(487, 188, 68, 23);
		contentPane.add(btnEditObject);
		btnEditObject.setVisible(false);
		
		BufferedImage imgAddPage = ImageIO.read(new File(ADD_PAGE));
	    ImageIcon AddPageIcon=new ImageIcon(imgAddPage);
		JButton btnAddPage = new JButton(AddPageIcon);
		btnAddPage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ObjectRepositoryView.objRepoViewFrame.setEnabled(false);
				System.out.println("Opening the ScanObjectREpository frame ");
				ScanObjectRepoFrameLauchedFRom = "ViewRepo";
				ObjectRepositoryScan.getObjectRepositoryFrame();
			}
		});
		btnAddPage.setBounds(68, 224, 27, 24);
		contentPane.add(btnAddPage);
		
		BufferedImage del_Pageimg = ImageIO.read(new File(DELETE_IMG_PATH));
	    ImageIcon delete_Pageicon=new ImageIcon(del_Pageimg);
		btnDeletePage = new JButton(delete_Pageicon);
		btnDeletePage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Delete the page selected - "+ selectedScannedPage );
				if(ExcelDelete.deleteExcelSheet(objectExtractionFile, selectedScannedPage)){
					JOptionPane.showMessageDialog(null, "Page deleted");
				}
				displayPages();
			}
		});
		btnDeletePage.setBounds(20, 224, 27, 23);
		contentPane.add(btnDeletePage);
		
		BufferedImage del_Objimg = ImageIO.read(new File(DELETE_IMG_PATH));
	    ImageIcon delete_Objicon=new ImageIcon(del_Objimg);
		btnDeleteObject = new JButton(delete_Objicon);
		btnDeleteObject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Deleting the object selected - "+ objectSelected);
				int rowToDelete = ExcelDataList.getRownumByColumnValue(objectExtractionFile, selectedScannedPage, "Name", objectSelected);
				if(ExcelDelete.DeleteRowGivenRowNum(objectExtractionFile, selectedScannedPage, rowToDelete)){
					JOptionPane.showMessageDialog(null, "Object deleted !");
				};
				displayObjectsOfScannedPage();
			}
		});
		btnDeleteObject.setBounds(157, 225, 27, 23);
		contentPane.add(btnDeleteObject);
		btnDeleteObject.setVisible(false);
		
	}
	
	//******************************************************************************************************************************************************************************
	//******************************************************************************************************************************************************************************
	
	private static void addParametersinEleType(){
		try{
			comboBoxElementType.addItem("All");
			comboBoxElementType.addItem("text");
			comboBoxElementType.addItem("button");
			comboBoxElementType.addItem("radio-buttons");
			comboBoxElementType.addItem("check-box");
			comboBoxElementType.addItem("link");
			comboBoxElementType.addItem("dropdown");
			comboBoxElementType.addItem("frame");
			
			
		}catch(Exception e){
			System.out.println("Exception in adding combo-box elements is - "+ e);
		}
	}
	
	//Displaying Elements on the Page selection
	private void displayElementsOnPageSelection(){
		try{
			comboBoxElementType.setVisible(true);
			addParametersinEleType();
			scrollPane_1.setVisible(true);
			listSelectedElements.setVisible(true);
			btnAddObjectIcon.setVisible(true);
			btnDeleteObject.setVisible(true);
		}catch(Exception e){
			
		}
	}
	
	//Displaying Elements on the Object selection
	private void displayElementOnObjectSelection(){
		try{
			scrollPane_2.setVisible(true);
			table.setVisible(true);
			btnEditObject.setVisible(true);
			
			
		}catch(Exception e){
			
		}
	}
	
	//Display the list of scanned pages
	public static void displayPages(){
		try{
			System.out.println("Inside method displayPages to display all the scanned pages in the list");
			WebPagesModel.removeAllElements();
			WebPagesModel.clear();

			List<String> webPages = ExcelDataList.getSheetsInExcel(objectExtractionFile);
			String scanPage = null;
			for(int cnt =0;cnt<webPages.size();cnt++){
				scanPage = webPages.get(cnt);
				System.out.println(cnt + " webPage is  - "+ scanPage);
				WebPagesModel.addElement(scanPage.toString());
			}
		}catch(Exception e){
			System.out.println("Exception in displaying all the webpages scanned - "+ e);
		}
	}
	
	//DIsplaying the list of web-elements on page selection
	public static void displayObjectsOfScannedPage(){
		try{
			
			System.out.println("Inside method displayScannedElements to display all the scanned objects for the selected page");
			scannedElementsModel.removeAllElements();
			scannedElementsModel.clear();
			selectedScannedPage = listScanPages.getSelectedValue().toString();
			List<String> scannedElements = ExcelDataList.getExcelDataInListByColName(objectExtractionFile, selectedScannedPage, "Name");
			for(int cnt=0;cnt<scannedElements.size();cnt++){
				scannedElementsModel.addElement(scannedElements.get(cnt).toString());
			}
		}catch(Exception e){
			System.out.println("Exception in displaying scanned elements of the page - "+e);
		}
	}
	
	//Display the detail property of the object selected
	private static void displayDetailObjectProperty(){
		try{
			
			System.out.println("Inside function displayDetailObjectProperty to display the detail property of the object selected");
			headers.clear();
			headers.removeAllElements();
			data.removeAllElements();
			data.clear();
			
			
			
			objectSelected = listSelectedElements.getSelectedValue().toString();
			System.out.println("Selected Element is - "+ objectSelected);
			
			headers.add("Identifier");
			headers.add("IdentifierValue");
			propertyDetailmodel = new DefaultTableModel(data, headers);
			
			//Getting list of values for the identifiers
			selectedObjectRowValues.clear();
			selectedObjectRowValues = ExcelDataList.getExcelDataInListByRowValue(objectExtractionFile, selectedScannedPage, "Name", objectSelected);
			
			propertyDetailmodel.addRow(new Object[]{selectedObjectRowValues.get(2), selectedObjectRowValues.get(3)});
			
 			tableWidth = propertyDetailmodel.getColumnCount() * 100;
 			tableHeight = propertyDetailmodel.getRowCount() * 25;
 			
 			table.setPreferredSize(new Dimension(tableWidth, tableHeight));
 			table.setModel(propertyDetailmodel);
			
		}catch(Exception e){
			System.out.println("Exception in displaying the detail property is - "+ e);
		}
	}
	
	//Displaying the selected objects as per the type selected
	private static void displaySelectedObjects(){
		try{
			System.out.println("Inside method - displaySelectedObjects to display the objects mathing to the type selected");
			typeSelected = comboBoxElementType.getSelectedItem().toString();
			System.out.println("Type selected - "+ typeSelected );
			if(!typeSelected.equalsIgnoreCase("ALL")){
				List<String> selectedTypeObjects = ExcelDataList.getDataByColumnFilter(objectExtractionFile, selectedScannedPage, "Type", typeSelected,"Name");
				scannedElementsModel.removeAllElements();
				scannedElementsModel.clear();
				for(int cnt=0;cnt<selectedTypeObjects.size();cnt++){
					scannedElementsModel.addElement(selectedTypeObjects.get(cnt).toString());
				}
			}
			else{
				System.out.println("Type - ALL selected");
				if(selectedScannedPage!=null){
					scannedElementsModel.clear();
					scannedElementsModel.removeAllElements();
					List<String> scannedElements = ExcelDataList.getExcelDataInListByColName(objectExtractionFile, selectedScannedPage, "Name");
					for(int cnt=0;cnt<scannedElements.size();cnt++){
						scannedElementsModel.addElement(scannedElements.get(cnt).toString());
					}	
				}				
			}	
		}catch(Exception e){
			System.out.println("Exception in getting the selected objects - "+ e);
		}
	}
	
	//Updating the Object values
	private static void setObjectUpdatedValue(){
		try{
			System.out.println("Project selected - " + TestCreation.ProjectSelected);
			Initialization.ObjectRepositoryFile = new File (projectPath+"\\TestData\\"+ TestCreation.ProjectSelected+ "\\ObjectProperty.xls");
			//int targetRowNum = ExcelDataList.getRownumByColumnValue(Initialization.ObjectRepositoryFile, selectedScannedPage, "Name", objectSelected);
			getSelectedObjectList.addAll(ExcelDataList.getExcelDataInListByRowValue(Initialization.ObjectRepositoryFile, selectedScannedPage, "Name", objectSelected));
			rowNumToEdit = ExcelDataList.getRownumByColumnValue(Initialization.ObjectRepositoryFile, selectedScannedPage, "Name", objectSelected);
			objectPtropertyFrameLauched = "edit";
			ObjectPropertyElement.getFrame();	
		}catch(Exception e){
			
		}
	}
}

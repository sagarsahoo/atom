package selfitGui;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.io.File;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TCmapping extends JFrame {

	private JPanel contentPane;
	private JTable table;
	 static int tableWidth = 0; // set the tableWidth
	 static int tableHeight = 0; // set the tableHeight
	static Vector headerMapping = new Vector();
	static Vector dataMapping = new Vector();
	static DefaultTableModel model = null;
	static TCmapping frame;

	
	public static void displayMapping(File file, String sheet){
		frame = new TCmapping(file,sheet);
		frame.setVisible(true);
		
	}

	public TCmapping(File file, String sheet) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 893, 433);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPaneMapping = new JScrollPane();
		scrollPaneMapping.setBounds(44, 42, 769, 220);
		contentPane.add(scrollPaneMapping);
		
		table = new JTable();
		scrollPaneMapping.setViewportView(table);
		
		mappingDetails(file,sheet);
		model = new DefaultTableModel(dataMapping, headerMapping);
		tableWidth = model.getColumnCount() * 100;
		tableHeight = model.getRowCount() * 25;
		table.setPreferredSize(new Dimension(tableWidth, tableHeight));
		table.setModel(model);
		
		JButton btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					frame.setVisible(false);
					TestCreation.testCreationframe.setVisible(true);
					TestCreation.testCreationframe.setEnabled(true);
				}catch(Exception exc){
					JOptionPane.showMessageDialog(null, "Error in selecting OK in mapping - "+ exc);
				}
			}
		});
		btnOK.setBounds(329, 312, 89, 23);
		contentPane.add(btnOK);
	}
	
	//getting the mapping informations
	public static void mappingDetails(File mappingFile, String mappingSheet ){
		try{
			headerMapping = ScriptingData.getHeader(mappingFile, mappingSheet);
			dataMapping = ScriptingData.getTestSteps(mappingFile, mappingSheet);		
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, "Error in method mapping details - "+ e);
		}
	}
}

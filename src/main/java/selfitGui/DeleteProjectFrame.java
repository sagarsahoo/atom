package selfitGui;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DeleteProjectFrame extends JFrame {

	private JPanel contentPane;
	public static DeleteProjectFrame deleteProjectFrame;
	List<String> ProjList = new ArrayList<String>();
	JComboBox comboBoxProjectList;

	
	public static void getFrame(){
		 deleteProjectFrame = new DeleteProjectFrame();
		 deleteProjectFrame.setLocationRelativeTo(null);
		 deleteProjectFrame.setEnabled(true);
		 deleteProjectFrame.setVisible(true);
		 deleteProjectFrame.setFocusable(true);
		 deleteProjectFrame.setTitle("Delete-Project");
		 
	}

	/**
	 * Create the frame.
	 */
	public DeleteProjectFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 345, 194);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		JLabel lblProjName = new JLabel("Select Project");
		lblProjName.setFont(new Font("Calibri", Font.BOLD, 13));
		lblProjName.setBounds(30, 64, 90, 14);
		contentPane.add(lblProjName);
		
		comboBoxProjectList = new JComboBox();
		comboBoxProjectList.setBounds(154, 60, 130, 18);
		contentPane.add(comboBoxProjectList);
		ProjList.addAll(TestCreation.getProjectList());
		for(int cnt=0;cnt<ProjList.size();cnt++){
			comboBoxProjectList.addItem(ProjList.get(cnt).toString());
		}
		
		JButton btnNewButton = new JButton("OK");
		btnNewButton.setFont(new Font("Calibri", Font.PLAIN, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String selectedProject = comboBoxProjectList.getSelectedItem().toString();
				DeleteProject.removeProject(selectedProject);
				deleteProjectFrame.dispose();
				TestCreation.testCreationframe.setEnabled(true);
				TestCreation.testCreationframe.setFocusable(true);
				
				TestCreation.displaydefaultOnProjectDeletion();
				
				
			}
		});
		btnNewButton.setBounds(105, 115, 51, 23);
		contentPane.add(btnNewButton);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteProjectFrame.dispose();
				TestCreation.testCreationframe.setEnabled(true);
				TestCreation.testCreationframe.setFocusable(true);
			}
		});
		btnCancel.setFont(new Font("Calibri", Font.PLAIN, 11));
		btnCancel.setBounds(194, 115, 70, 23);
		contentPane.add(btnCancel);
	}
}

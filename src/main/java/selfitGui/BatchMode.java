package selfitGui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import utilityPackage.CreateTestNGxml;

public class BatchMode extends JFrame {

	private JPanel contentPane;
	public static BatchMode batchModeFrame;
	static JScrollPane scrollPane_TCBatchMode;
	static JList listTC_batchMode;
	static JList list_selectedBatchModeTC;
	private JScrollPane scrollPane_selectedBatchMode;
	static JButton btnToSelect;
	private JButton btnToDeselect;
	private JButton btnBM_OK;
	private JButton btnBM_Cancel;
	
	private static DefaultListModel TCModelOriginalTCList = new DefaultListModel();
	private static DefaultListModel TCModelBatchTCList = new DefaultListModel();
	
	static List<String> displayTCListInOriginal = new ArrayList<String>();
	//static List<String> OriginalTCList = new ArrayList<String>(); 
	static List<String> OriginalTCExecMapFlagY = new ArrayList<String>();
	static List<String> OriginalTCExecMapFlagN = new ArrayList<String>();
	static List<String> SelectedOriginalTCtoTransfer = new ArrayList<String>();
	
	static List<String> BatchTC = new ArrayList<String>();
	static List<String> BatchTCFromOriginal = new ArrayList<String>();
	static List<String> SelectedBatchTCtoTransfer = new ArrayList<String>();
	
	private JButton btnCreate;
	static String EXEC_IMG_PATH = Initialization.projectPath+ "//img//Execute_img.png";
	
	public BatchMode() throws IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 714, 550);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		scrollPane_TCBatchMode = new JScrollPane();
		scrollPane_TCBatchMode.setBounds(69, 59, 171, 329);
		contentPane.add(scrollPane_TCBatchMode);
		
		listTC_batchMode = new JList(TCModelOriginalTCList);
		scrollPane_TCBatchMode.setViewportView(listTC_batchMode);
		displayTCOriginalList();
		 
		
		scrollPane_selectedBatchMode = new JScrollPane();
		scrollPane_selectedBatchMode.setBounds(358, 59, 171, 329);
		contentPane.add(scrollPane_selectedBatchMode);
		
		list_selectedBatchModeTC = new JList(TCModelBatchTCList);
		scrollPane_selectedBatchMode.setViewportView(list_selectedBatchModeTC);
		getSelectedTCFromExecMapSheet();
		
		btnToSelect = new JButton(">>");
		btnToSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectedOriginalTCtoTransfer.clear();
				BatchTCFromOriginal.clear();
				transferTCtoBatch();
			}
		});
		btnToSelect.setBounds(269, 133, 51, 23);
		contentPane.add(btnToSelect);
		
		btnToDeselect = new JButton("<<");
		btnToDeselect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectedBatchTCtoTransfer.clear();
				transferBatchToTC();
			}
		});
		btnToDeselect.setBounds(269, 196, 51, 23);
		contentPane.add(btnToDeselect);
		
		btnBM_OK = new JButton("OK");
		btnBM_OK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				performAfterBatchCreation();
			}
		});
		btnBM_OK.setBounds(461, 462, 89, 23);
		contentPane.add(btnBM_OK);
		
		btnBM_Cancel = new JButton("Cancel");
		btnBM_Cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				performAfterBatchCreation();
			}
		});
		btnBM_Cancel.setBounds(584, 462, 89, 23);
		contentPane.add(btnBM_Cancel);
		
		btnCreate = new JButton("Create");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Creating Batch file");
				createBatchXMLfile();
				JOptionPane.showMessageDialog(null, "XML created");
			}
		});
		btnCreate.setBounds(566, 180, 89, 23);
		contentPane.add(btnCreate);
		
		BufferedImage AddTC_img = ImageIO.read(new File(EXEC_IMG_PATH));
	    ImageIcon AddTC_icon=new ImageIcon(AddTC_img);
		JButton btnExecBatch = new JButton(AddTC_icon);
		btnExecBatch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				try{
					System.out.println("Executing the batch file");
					TestCase.executeBatchXml();
				}catch(Exception excp){
					System.out.println("Exception in TC execution");
				}
			}
		});
		btnExecBatch.setBounds(69, 462, 38, 23);
		contentPane.add(btnExecBatch);
	}
	
	public static void getBatchFrame() throws IOException{
		TestCreation.testCreationframe.setEnabled(false);
		batchModeFrame = new BatchMode();
		batchModeFrame.setVisible(true);
		batchModeFrame.setLocationRelativeTo(null);
		batchModeFrame.setTitle("Batch-Mode");
		batchModeFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		//OriginalTCList.addAll(TestCreation.TCList);
		
		
	}
	
	//*********************************************************************************
	
	//Auto-populating batchFile TC from Execution Mapping file
	private static void getSelectedTCFromExecMapSheet(){
		try{
			//Getting TC list flag as Y from exec mapping file
			OriginalTCExecMapFlagY.clear();
			OriginalTCExecMapFlagY.addAll(ExcelDataList.getDataByColumnFilter(Initialization.TestExecutionMappingFile, TestCreation.ProjectSelected+"_suite", "Exec_flag", "Y","TestCaseName"));
			TCModelBatchTCList.removeAllElements();
			for(int cnt=0;cnt<OriginalTCExecMapFlagY.size();cnt++){
				TCModelBatchTCList.add(cnt, OriginalTCExecMapFlagY.get(cnt));
			}
			BatchTC.addAll(OriginalTCExecMapFlagY);
		}catch(Exception e){
			System.out.println("Error in getting the Test case list from Execution mapping file flag as Y - "+ e);
		}
	}
	
	//Actions to be done after OK/Cancel
	private static void performAfterBatchCreation(){
		try{
			batchModeFrame.dispose();
			TestCreation.testCreationframe.setEnabled(true);
		}catch(Exception e){
			System.out.println("Exception in the actions after batchframe - "+ e);
		}
	}
	
	//Creating batch file after buttom Create is selected
	private static void createBatchXMLfile(){
		try{
			System.out.println("TC list to be included in batch - ");
			for(int cnt=0;cnt<BatchTC.size();cnt++){
				System.out.println(BatchTC.get(cnt));
			}
			setFlagInExecMapFile(BatchTC);
			CreateTestNGxml.getFromExcel("file",TestCreation.ProjectSelected+"_suite");
		}catch(Exception e){
			System.out.println("Exception in creating XML file for batch mode - "+ e);
		}
	}
	
	private static void setFlagInExecMapFile(List<String> TCListInBatch){
		try{
			FileInputStream fis = new FileInputStream(Initialization.TestExecutionMappingFile);
			HSSFWorkbook wb = new HSSFWorkbook (fis);
			HSSFSheet ws = wb.getSheet(TestCreation.ProjectSelected+"_suite");
			HSSFCell cell;
			HSSFRow row;
			String[][] MappingSheetData = ExcelDataList.getSheetData(Initialization.TestExecutionMappingFile, TestCreation.ProjectSelected+"_suite");
			int totalRows = ExcelDataList.getTotalRows(Initialization.TestExecutionMappingFile, TestCreation.ProjectSelected+"_suite");
			for(int i=0;i<TCListInBatch.size();i++){
				for(int rowCnt=1;rowCnt<totalRows;rowCnt++){
					row = ws.getRow(rowCnt);
					cell = row.getCell(6);
					if(MappingSheetData[rowCnt][1].equals(TCListInBatch.get(i))){						
						cell.setCellValue("Y");
						break;
					}
				}
			}
			fis.close();
			  FileOutputStream outputStream = new FileOutputStream(Initialization.TestExecutionMappingFile);
			  wb.write(outputStream);
			  outputStream.close();			
		}catch(Exception e){
			System.out.println("Exception occured in setFlagInExecMapFile - "+ e);
		}
	}
	
	//transfer the TC from Original TC list to Batch file list
	private static void transferTCtoBatch(){
		try{
			// getting selected TC from original TC list
			//displayTCListInOriginal.addAll(OriginalTCExecMapFlagN);
			
			if(listTC_batchMode.getSelectedValuesList().size()>0){
				SelectedOriginalTCtoTransfer.addAll(listTC_batchMode.getSelectedValuesList());
				BatchTCFromOriginal.addAll(SelectedOriginalTCtoTransfer);
				
				//removing selected TC from original list
				//OriginalTCList.removeAll(SelectedOriginalTCtoTransfer);
				
				System.out.println("TC-> Batch ######### Batch List ------------------------------");
				//Displaying the selected TC in the batch list
				if(BatchTCFromOriginal.size()>0){
					for(int i=0;i<BatchTCFromOriginal.size();i++){
						TCModelBatchTCList.add(i, BatchTCFromOriginal.get(i));
						BatchTC.add(BatchTCFromOriginal.get(i));
						System.out.println(BatchTCFromOriginal.get(i));						
					}					
				}else{
					System.out.println("no-TC");
				}
				
				
				//Displaying the updated original TC
				System.out.println("TC-> Batch ############## Original List =================== ");
				displayTCListInOriginal.removeAll(SelectedOriginalTCtoTransfer);
				TCModelOriginalTCList.clear();
				TCModelOriginalTCList.removeAllElements();
				if(displayTCListInOriginal.size()>0){
					for(int i=0;i<displayTCListInOriginal.size();i++){
						TCModelOriginalTCList.add(i, displayTCListInOriginal.get(i));
						System.out.println(displayTCListInOriginal.get(i));
					}	
				}else{
					System.out.println("no-TC");
				}
				
			}
			
		}catch(Exception e){
			
		}
	}
	
	//Transfering TC from batch display to Original List
	private static void transferBatchToTC(){
		try{
			int itemIndextoremove;
			//list_selectedBatchModeTC.removeAll();
			
			//Getting selected TC from batch
			if(list_selectedBatchModeTC.getSelectedValuesList().size()>0){
				//Adding selected values to a list
				SelectedBatchTCtoTransfer.addAll(list_selectedBatchModeTC.getSelectedValuesList());
				
				//Adding to list displaying the original list				
				displayTCListInOriginal.addAll(list_selectedBatchModeTC.getSelectedValuesList());
				TCModelOriginalTCList.clear();
				TCModelOriginalTCList.removeAllElements();
				
				//Displaying updated original list
				System.out.println("Batch->TC ############## Original List =================== ");
				for(int i=0;i<displayTCListInOriginal.size();i++){					
					TCModelOriginalTCList.addElement(displayTCListInOriginal.get(i));
					System.out.println(displayTCListInOriginal.get(i));
				}
				
				//removing the selected values from batch_list
				for(int cnt=0;cnt<SelectedBatchTCtoTransfer.size();cnt++){
					itemIndextoremove = BatchTC.indexOf(SelectedBatchTCtoTransfer.get(cnt));
					BatchTC.remove(itemIndextoremove);										
				}
				
				//Display updated TC in batch list
				System.out.println("TC->Batch ############## Batch List =================== ");
				TCModelBatchTCList.clear();
				for(int cnt=0;cnt<BatchTC.size();cnt++){
					TCModelBatchTCList.addElement(BatchTC.get(cnt));
					System.out.println(BatchTC.get(cnt));
				}
			}
		}catch(Exception e){
			
		}
	}
	
	//Displaying list of TC in the Original list as start
	private static void displayTCOriginalList(){
		try{
			TCModelOriginalTCList.clear();
			TCModelOriginalTCList.removeAllElements();
			OriginalTCExecMapFlagN.clear();
			OriginalTCExecMapFlagN.addAll(ExcelDataList.getDataByColumnFilter(Initialization.TestExecutionMappingFile, TestCreation.ProjectSelected+"_suite", "Exec_flag", "N","TestCaseName"));
			if(OriginalTCExecMapFlagN!=null){
				for(int cnt=0;cnt<TestCreation.TCList.size();cnt++){
					 TCModelOriginalTCList.addElement(OriginalTCExecMapFlagN.get(cnt));
					 displayTCListInOriginal.add(OriginalTCExecMapFlagN.get(cnt));
		           }
			}
		}catch(Exception e){
			
		}
	}
}

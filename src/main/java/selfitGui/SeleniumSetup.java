package selfitGui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public  class SeleniumSetup {
	
	/*	creation of Mapping Sheet in TestExecutionMapping.xls having name 	"applicationName_suite"
	Creation of property file in the name 								"applicationName.properties"
	creation of application testData folder under //TestData			"applicationName"
	creation of scripting file inside folder							"applicationName_scripting.xls"
	creation of UserInput file inside folder							"UserInput_applicationName.xls"
	creation of package under src										"applicationName"
	copying MasterClass.java  inside application package
*/

static String projectPath = System.getProperty("user.dir");
static String objectPropertyFileName;
public static String applicationPackage;

	public static void main(String[] args) throws IOException {
		
		
		JFrame frame = new JFrame();
		  Icon icon = null;
		  
		  try{
			  
			  applicationPackage= JOptionPane.showInputDialog("Please provide the name of the application: \n (Do not provide space or any special character)");
			  if(applicationPackage.equals("")){
				  NullPointerException ne = new NullPointerException ();
				  throw ne ;
			  }else{
				  applicationPackage.replaceAll(" ", "");
				  applicationPackage.replaceAll("[-+._:,><]", "");
			  }
		      String ScriptingFileName = applicationPackage + "_scripting.xls";
		      String UserInputFileName = "UserInput_" + applicationPackage + ".xls";
		      String PropertyFileName = applicationPackage + ".properties";
		      objectPropertyFileName = "ObjectProperty.xls";
		      //String ExecutionMappingFilename = "TestExecutionMapping.xls";
		      
		      
		      
		      File ObjectProperty = new File(projectPath + "\\objRepo\\"+ PropertyFileName);
		      File ScriptingFile = new File(projectPath+"\\TestData\\"+applicationPackage+"\\"+ ScriptingFileName);
		      File UserInputFile = new File(projectPath+"\\TestData\\"+applicationPackage+"\\"+ UserInputFileName);
		      File ObjectPropertyFile = new File(projectPath+"\\TestData\\"+applicationPackage+"\\"+ objectPropertyFileName);
		      //File ExecutionMappingFile = new File(projectPath+"\\TestData\\"+ ExecutionMappingFilename);
		      
		      
		      //Creation of Execution Mapping file
		      createSheetInExecutionMapping(applicationPackage);

		      //Creation of property file
		      if(!ObjectProperty.exists()){
		    	  System.out.println("Property file not present - Creating property File");
		    	  
		    	  File source = new File(projectPath + "\\objRepo\\sample.properties");
		    	  File target = ObjectProperty;
		    	  FileUtils.copyFile(source, target);
		    	  Path path_var = Paths.get(projectPath + "\\objRepo\\"+ PropertyFileName);
		    	  Charset charset = StandardCharsets.UTF_8;
		    	  String content = new String(Files.readAllBytes(path_var), charset);
		    	  content = content.replaceAll("UserScriptingFile=.*", "UserScriptingFile="+ ScriptingFileName);
		    	  content = content.replaceAll("UserInputFile=.*", "UserInputFile="+ UserInputFileName);
		    	  content = content.replaceAll("TestDataFolder=.*", "TestDataFolder="+applicationPackage);
		    	  Files.write(path_var, content.getBytes(charset));
		    	  
		    	  System.out.println("Proprty File created !!!!!!!!!!!!!!!!!!!!!!!!!!");
		      }
		      else{
		    	  System.out.println("Property file already present");
		      }
		      
			 
			  
			  //Creation of application folder under TestData
			  File applicationDir = new File(projectPath+"\\TestData\\"+ applicationPackage);
			  if (!applicationDir.exists()) {
				  System.out.println("creating directory: " + applicationPackage);
				  applicationDir.mkdir();		  
			  }
			  else{
				  System.out.println("Directory already exists");
			  }
			  
			  //Creation of Screenshot folder for the application
			  File applicationSrnDir = new File(projectPath+"\\TestReports\\ScreenShots\\"+ applicationPackage);
			  if (!applicationSrnDir.exists()) {
				  System.out.println("creating ScreenShot directory: " + applicationPackage);
				  applicationSrnDir.mkdir();		  
			  }
			  else{
				  System.out.println(" ScreenShot Directory already exists");
			  }
			  
			  
			  
			  //Creation of scripting file inside application folder
			  if(!ScriptingFile.exists()){
				  System.out.println("Creating Scripting file inside testData");
				  createExcelScripting(applicationPackage,ScriptingFileName);
			  }
			  else{
				  System.out.println("Scripting file already present");
			  }
			  
			  
			  
			  //Creation of UserInput file in the application Folder
			  if(!UserInputFile.exists()){
				  System.out.println("Creating UserInput Excel file inside testdata");
				  createExcelUserInput(applicationPackage,UserInputFileName);
			  }
			  else{
				  System.out.println("UserInput file already present");
			  }
			  
			  //Creation of ObjectPropertyFile
			  if(!ObjectPropertyFile.exists()){
				  System.out.println("Creating ObjectProperty Excel file inside testdata");
				  createExcelObjectProperty(applicationPackage,objectPropertyFileName);
			  }
			  else{
				  System.out.println("UserInput file already present");
			  }
			  
			  
			  //Creation of application package to contain the .java files in source
			  File applicationPackageFolder = new File(projectPath+"\\src\\main\\java\\"+ applicationPackage);
			  if (!applicationPackageFolder.exists()) {
				  System.out.println("creating package under src : " + applicationPackage);
				  applicationPackageFolder.mkdir();	
				  System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!! - Package created");
				  //Copying master class in the application package
				  File sourceMasterClassFile = new File(projectPath + "\\src\\main\\java\\utilityPackage\\MasterClass.java");
				  File targetMasterClass = new File(projectPath+"\\src\\main\\java\\"+ applicationPackage + "\\MasterClass" + applicationPackage + ".java");
				  FileUtils.copyFile(sourceMasterClassFile, targetMasterClass);
				  
				  //Passing the parameter of the application
				  String targetBatchFile = projectPath + "\\TestCreate.bat";
				  String BatchFileStringToreplace = "set packageName.*"; //=testapplication";
				  String BatchFileTargetString = "set packageName="+ applicationPackage ;
				  replaceContent(targetBatchFile,BatchFileStringToreplace,BatchFileTargetString);
				  
				  
				  
				  
				  String applicationMasterClannName = targetMasterClass.toString();
				  
				  //Change  package name in the .java file
				  String orgPackageName = "package utilityPackage;";
				  String changedPackageName = "package "+ applicationPackage + ";"+ "\n" +"import utilityPackage.*;";
				  replaceContent(applicationMasterClannName,orgPackageName,changedPackageName);
				  
				  //Change the class name in the .java file
				  String orgClassName = "public class MasterClass";
				  String changedClassName = "public class MasterClass"+applicationPackage;
				  replaceContent(applicationMasterClannName,orgClassName,changedClassName);
				  
				  //Change application name in the class in the .java file
				  String srcApplName = "static String applicationName = \"testApplication\";";
				  String targetAplName = "static String applicationName = \""+  applicationPackage + "\";";
				  replaceContent(applicationMasterClannName,srcApplName,targetAplName);
				  
				  
			  }
			  else{
				  System.out.println("Package already exists");
			  }
			  
			  
			  
			  
			  
			  JOptionPane.showMessageDialog(frame,
					  "Package created in the name of Application name :"+ applicationPackage + "\n"
					  + "Application Folder created under TestData Directory :"+ applicationPackage + "\n"
					  + "Scripting file created under Application Folder  : "+ ScriptingFileName + "\n"
					  + "UserInput file created under Application Folder  : "+ UserInputFileName + "\n\n"
					  + "Repository file created inside ObjRepo folder :" + PropertyFileName + "\n"
					  + "Sheet added in the TestExecutionMapping File :" +applicationPackage+"_suite" );

			  

		  }catch(Exception e){
			  JOptionPane.showMessageDialog(null, "NO application Name provided");
		  }
	}

	      
	  
	  
	  // **************************************************************************************************************************************************************************************
	  
	  
	  
	  static void replaceContent(String FiletoReplace,String stringToReplace, String targetString){
		  try{
			  String targetFile = FiletoReplace;
			  
			  //replacing the test case and the execution sheet name
			  Path path_var = Paths.get(targetFile);
			  Charset charset = StandardCharsets.UTF_8;

			  String content = new String(Files.readAllBytes(path_var), charset);
			  content = content.replaceAll(stringToReplace,targetString);
			  Files.write(path_var, content.getBytes(charset));
		  }
		  catch(Exception e){
			  System.out.println("Exception in replaceing content - ");
		  }
	  }
	  
	  
	  
	  //Create UserInputExcel file
	  static void createExcelUserInput(String applicationDirectory,String filename) throws IOException{
		  
		  Workbook wb = new HSSFWorkbook();
			//createHeaderUserInputFile(wb);
			String[] UserInputHeader = {"ID","RESULT"};
			createHeader(wb,"TestCase",UserInputHeader);
			
		    FileOutputStream fileOut = new FileOutputStream(projectPath+"\\TestData\\"+applicationDirectory+"\\" + filename);
		    wb.write(fileOut);
		    fileOut.close();
	  }
	  

	  //Create ScriptingExcel file
	  static void createExcelScripting(String applicationDirectory,String filename) throws IOException{
			
		Workbook wb = new HSSFWorkbook();
		//createHeaderScriptingFile(wb);
		String[] MasterSheetHeader = {"Env","parameter","value"};
		createHeader(wb,"Master_sheet",MasterSheetHeader);
		String[] ScriptingHeader = {"ID","TC_NAME","Exec_Flag","TS_ID","TS_NAME","Keyword","Element_type","element_identifiedBy","element_identifier",
									"pass_values","target_identifiedBy","target_identifier","testStep_segment","RESULT"};
		createHeader(wb,"TestCase",ScriptingHeader);
		
	    FileOutputStream fileOut = new FileOutputStream(projectPath+"\\TestData\\"+applicationDirectory+"\\" + filename);
	    wb.write(fileOut);
	    fileOut.close();
		
	  }
	  
	  //Create Object Propert Excel file
	  static void createExcelObjectProperty(String appDirectory,String NameOfExcelFile){
		  try{
			  Workbook wb = new HSSFWorkbook();
			  FileOutputStream fileOut = new FileOutputStream(projectPath+"\\TestData\\"+appDirectory+"\\" + NameOfExcelFile);
			    wb.write(fileOut);
			    fileOut.close();
			  
		  }catch(Exception e){
			  System.out.println("Excepion in creation of Object Property Excel file - "+ e);
		  }
	  }
	  
	  //create header
	  public static void createHeader(Workbook wb, String sheetName, String[] headerList){
		  
		  Sheet workbookSheet = wb.createSheet(sheetName);
		  Row sheetHeader = workbookSheet.createRow(0);
		  for(int cnt =0;cnt<headerList.length;cnt++){
			  sheetHeader.createCell(cnt).setCellValue(headerList[cnt]);
		  }
		  
	  }
	  
	  //Create sheet in ExecutionMappingFile
	  public static void createSheetInExecutionMapping(String applicationName) throws IOException{
		  
		  try{
			  	HSSFWorkbook workbook = null;
			  	int match_cnt=0;
			  
			    File file = new File(projectPath+"\\TestData\\TestExecutionMapping.xls");
			    FileInputStream inputStream = new FileInputStream(file);
			    workbook = new HSSFWorkbook(inputStream);
			    int totalSheets = workbook.getNumberOfSheets();
			    for(int cnt=0;cnt<totalSheets;cnt++){
			    	if(workbook.getSheetName(cnt).equals(applicationName+"_suite")){
			    		System.out.println("Sheet inside ExecutionMapping File already present");
			    		match_cnt++;	    		
			    	}
			    }
			    if(match_cnt==0){
			    	//HSSFSheet sheet = workbook.createSheet(applicationName+"_suite");
			    	String[] ExecutionMappingHeader = {"TC_ID","TestCaseName","className","RepositoryFile","ExcelSheetName","UserInputFilePresent","Exec_flag"};
				  	createHeader(workbook,applicationName+"_suite",ExecutionMappingHeader);
			    	FileOutputStream fileOut = new FileOutputStream(file);
				    workbook.write(fileOut);
				    fileOut.close();
			    }   
		  }
		  catch(Exception e){
			  System.out.println("The Exception in create Excel sheet in the ExecutionMApping Sheet is - "+ e);
		  }
		  	    	  
	  

	}

}

package selfitGui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ManageParameterFrame extends JFrame {

	private JPanel contentPane;
	public static ManageParameterFrame getFrame;
	private JTextField textFieldParameterName;
	private JTextField textField_ParameterValue;
	private JButton btnSave;
	private JButton btnCancel;
	private String actionfromMasterSheet;
	List<String> ColumnList =  Arrays.asList("Env","parameter");
	List<String> ValueList =  new ArrayList<String>();
	private JTextField textFieldEnv;


	public static void launchManageParameterFrame(){
		getFrame = new ManageParameterFrame();
		getFrame.setVisible(true);
		getFrame.setLocationRelativeTo(null);
		getFrame.setTitle("Manage Parameters");
		getFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}

	public ManageParameterFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 390, 229);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		JLabel lblParameterName = new JLabel("Name");
		lblParameterName.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblParameterName.setBounds(37, 63, 46, 14);
		contentPane.add(lblParameterName);

		textFieldParameterName = new JTextField();
		textFieldParameterName.setBounds(93, 59, 195, 18);
		contentPane.add(textFieldParameterName);
		textFieldParameterName.setColumns(10);

		JLabel lblParameterValue = new JLabel("Value");
		lblParameterValue.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblParameterValue.setBounds(37, 103, 46, 14);
		contentPane.add(lblParameterValue);

		textField_ParameterValue = new JTextField();
		textField_ParameterValue.setBounds(93, 99, 195, 20);
		contentPane.add(textField_ParameterValue);
		textField_ParameterValue.setColumns(10);

		btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("saving the parameter - ");
				actionForParameterSave();
				closeFrame();
			}
		});
		btnSave.setFont(new Font("Calibri", Font.PLAIN, 11));
		btnSave.setBounds(175, 147, 70, 20);
		contentPane.add(btnSave);

		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeFrame();
			}
		});
		btnCancel.setFont(new Font("Calibri", Font.PLAIN, 11));
		btnCancel.setBounds(270, 146, 81, 21);
		contentPane.add(btnCancel);

		JLabel lblNewLabel = new JLabel("Env");
		lblNewLabel.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblNewLabel.setBounds(37, 23, 46, 14);
		contentPane.add(lblNewLabel);

		textFieldEnv = new JTextField();
		textFieldEnv.setBounds(93, 19, 86, 20);
		contentPane.add(textFieldEnv);
		textFieldEnv.setColumns(10);
		if(!(MasterSheetFrame.manageEnvOperation.equalsIgnoreCase("New"))){
			textFieldEnv.setText(MasterSheetFrame.selectedEnv);
		}

		//Actions as per operation selected in MasterSheetFrame
		ViewForEditOperation();
	}
	//**************************************************************************************************************

	//Action for SAVE
	private void actionForParameterSave(){
		try{
			List<String> parameterList = new ArrayList<String>(); //Declaring a list to store the parameter name and value
			String parameterName = textFieldParameterName.getText(); //Getting the name of the parameter provided by user
			String parameterValue = textField_ParameterValue.getText(); //Getting the value of the parameter provided by user
			String EnvName = textFieldEnv.getText();
			if(parameterName.length()==0 || parameterValue.length()==0 || EnvName.length()==0){
				JOptionPane.showMessageDialog(null, "Please provide values in all Fields");
			}

			parameterList.add(EnvName);
			parameterList.add(parameterName); // Adding name to the list
			parameterList.add(parameterValue); //Adding value to the list

			if(actionfromMasterSheet.equals("editParameter")){
				/*	//Updating the existing record				
				ColumnList.clear();
				ColumnList.add("Env");
				ColumnList.add("parameter");
				//Adding the value list to get the rownum
				 */				ValueList.clear();
				 ValueList.add(MasterSheetFrame.selectedEnv);
				 ValueList.add(MasterSheetFrame.selectedParameterName);
				 int rowNumToUpdate = ExcelDataList.getRowNumByColValueList(TestCreation.scriptingFile, "Master_sheet", ColumnList, ValueList); //Getting the rownum from master sheet to update
				 ExcelDataList.updateRowByRowNum(TestCreation.scriptingFile, "Master_sheet", rowNumToUpdate, parameterList); //Updating the master sheet

			}else{
				//Insert new record
				ExcelDataList.insertRowIntoExcel(TestCreation.scriptingFile, "Master_sheet", parameterList, 0);
			}
		}catch(Exception e){
			System.out.println("Exception in saving the parameter - "+ e);				
		}
	}

	//Closing the frame
	private void closeFrame(){
		getFrame.dispose();
		MasterSheetFrame.MSframe.setVisible(true);
		MasterSheetFrame.MSframe.setEnabled(true);
	}

	//Setting the text for the edit operation
	private void ViewForEditOperation(){
		actionfromMasterSheet = MasterSheetFrame.manageOperation;
		if(actionfromMasterSheet.equals("editParameter")){
			textFieldParameterName.setText(MasterSheetFrame.selectedParameterName);
			textField_ParameterValue.setText(MasterSheetFrame.selectedParameterValue);
		}
	}
}

package selfitGui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.testng.annotations.Test;

import utilityPackage.MasterClass;
import utilityPackage.SeleniumSetup;
import utilityPackage.ThirdPartyTools;

public class TestCase {
	static String testdatapath = System.getProperty("user.dir");
	static String SheetCreationName;
	static String selectedProject;
	static String tcId = null;	
	@Test
	public static void newTestCaseCreation(String projectName,String TCname){
		try{
			TCname.replaceAll(" ", "");
			TCname.replaceAll("[-,:]", "");
			SheetCreationName=null;
			selectedProject=projectName;
			createScrptingSheet(TCname);
			createUserInputSheet();
			mapTCtoMappingSheet(TCname);
			//Creating the .java file for the new TestCase
			String project[] = {projectName};
			MasterClass.main(project);
		}catch(Exception e){
			System.out.println("Error in new Test Case Creation - "+ e);
		}

	}

	public static void execution(String TC_selected,String project_selected){
		try{
			selectedProject = project_selected;
			/*String xml[] = {selectedProject+"_suiteTestNG.xml"};
		  //Update the flag to Y in the Execution Mapping sheet and other TC to N
		  updateExecFlag(TC_selected);
		  //Create the method to create the xml file
		  CreateTestNGxml.getFromExcel(selectedProject+"_suite");
		  //Execute the xml file
		  TestNGxmlExecution.executeXML(xml);*/

			FileActions.editSingleTestNGXML(project_selected,TC_selected);
			ThirdPartyTools.executeWindowBatch(Initialization.projectPath + "\\batchFiles\\", "singleTestExecution.bat");
			System.out.println("Test case execution completed");
		}catch(Exception e){
			System.out.println("Exception in executing the test case - "+e);
		}
	}

	//Execute the batch file
	public static void executeBatchXml(){
		try{
			System.out.println("Executing the batch XML");
			ThirdPartyTools.executeWindowBatch(Initialization.projectPath + "\\batchFiles\\", "batchExecution.bat");
			System.out.println("Test case execution completed");
		}catch(Exception e){

		}
	}

	public static void deleteTestCase(String projectSelected,String TC_selected){
		try{
			//Asking for the confirmation
			System.out.println("Deleting Test Case");
			if (JOptionPane.showConfirmDialog(null, "Are you sure?", "WARNING",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				System.out.println("User confirmed to delete");

				//Deleting from scripting file
				deleteScriptingSheet(projectSelected,TC_selected);
				//Deleting from UserInput Sheet
				deleteUserInputSheet(projectSelected,TC_selected);
				//Deleting from Execution mapping
				removeTCFromMapping(projectSelected,TC_selected);
			} 
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, "Exception in deleting TC - "+ e);
		}
	}




	// *********************************************************************************************

	//Getting scripting Workbook as per project selected
	public static HSSFWorkbook getScriptingWorkbook(String projectRequired){
		HSSFWorkbook scriptingWB = null;
		try{
			String scriptingFileName = projectRequired + "_scripting.xls";
			File scriptingFile = new File(testdatapath+"\\TestData\\"+projectRequired+"\\"+ scriptingFileName );
			FileInputStream fis = new FileInputStream(scriptingFile);
			scriptingWB = new HSSFWorkbook(fis);
			return scriptingWB;
		}catch(Exception e){
			System.out.println("Exception in getting scripting workbook is - "+ e);
			return scriptingWB; 
		}
	}

	//Getting Userinput workbook as per project selected
	public static HSSFWorkbook getUserInputWorkbook(String projectRequired){
		HSSFWorkbook userInputWB = null;
		try{
			String userInputFile = "UserInput_"+ projectRequired + ".xls";
			File scriptingFile = new File(testdatapath+"\\TestData\\"+projectRequired+"\\"+ userInputFile );
			FileInputStream fis = new FileInputStream(scriptingFile);
			userInputWB = new HSSFWorkbook(fis);
			return userInputWB;
		}catch(Exception e){
			System.out.println("Exception in getting scripting workbook is - "+ e);
			return userInputWB; 
		}
	}


	@Test(dependsOnMethods={"newTestCaseCreation"})
	private static void createScrptingSheet(String testCaseName) {
		try{		  
			HSSFWorkbook wb = getScriptingWorkbook(selectedProject);
			int Sheetscnt = wb.getNumberOfSheets();
			SheetCreationName = testCaseName;

			//Add header
			String[] ScriptingHeader = {"ID","TC_NAME","Exec_Flag","TS_ID","TS_NAME","Keyword","Element_type",
					"element_identifiedBy","element_identifier","pass_values","target_identifiedBy","target_identifier","testStep_segment","RESULT"};
			SeleniumSetup.createHeader(wb, SheetCreationName, ScriptingHeader);

			FileOutputStream output = new FileOutputStream(testdatapath+"\\TestData\\"+selectedProject+"\\"+ selectedProject+"_scripting.xls");
			wb.write(output);
			output.close();

		}catch(Exception e){
			System.out.println("Error in creating scripting sheet is - "+ e);
		}
	}

	@Test(dependsOnMethods={"createScrptingSheet"})
	private static void createUserInputSheet(){
		try{
			String userInputFileName = "UserInput_"+ selectedProject + ".xls";
			//Add header
			HSSFWorkbook wb = getUserInputWorkbook(selectedProject);
			String[] UserInputHeader = {"ID","RESULT"};
			SeleniumSetup.createHeader(wb,SheetCreationName,UserInputHeader);

			FileOutputStream output = new FileOutputStream(testdatapath+"\\TestData\\"+selectedProject+"\\"+ userInputFileName);
			wb.write(output);
			output.close();

		}catch(Exception e){
			System.out.println("Error in creating UserInput sheet is - "+ e);
		}
	}

	//
	private static void mapTCtoMappingSheet(String nameofTC){
		try{
			File MappingFile = new File(testdatapath+"//TestData//TestExecutionMapping.xls");
			String ExecutionMappingSheet = selectedProject+"_suite";
			String applicationName = selectedProject;
			tcId=createTCID(MappingFile,ExecutionMappingSheet);
			List<String> insertValues = new ArrayList<String>();
			insertValues.add(tcId);
			insertValues.add(nameofTC);
			insertValues.add(selectedProject+"."+nameofTC );
			insertValues.add(selectedProject+ ".properties");
			insertValues.add(SheetCreationName);
			insertValues.add("N");
			insertValues.add("Y");
			insertIntoExcel(MappingFile,ExecutionMappingSheet,insertValues);
		}catch(Exception e){
			System.out.println("Exception in mapping the new Testcase in the ExecutionMapping Sheet - "+ e);
		}
	}

	//Insert into Excel
	private static void insertIntoExcel(File ExcelFile,String ExecutionMappingSheet,List<String> values){
		try{
			FileInputStream fis = new FileInputStream(ExcelFile);
			HSSFWorkbook wb = new HSSFWorkbook(fis);
			Sheet sh = wb.getSheet(ExecutionMappingSheet);
			int rowToInsert = sh.getLastRowNum()+1;
			HSSFRow rw = (HSSFRow) sh.createRow(rowToInsert);
			for(int cellCnt=0;cellCnt<sh.getRow(0).getLastCellNum();cellCnt++){
				Cell cell = rw.createCell(cellCnt);
				cell.setCellValue(values.get(cellCnt));
			}
			fis.close();
			FileOutputStream outputStream = new FileOutputStream(ExcelFile);
			wb.write(outputStream);
			outputStream.close();
		}catch(Exception e){
			System.out.println("Exception in inserting into Excel is  - "+ e);
		}
	}


	private static void updateExecFlag(String selectedTC ){
		List<String> TCnames = new ArrayList<String>();
		try{
			File ExcelFile = new File(testdatapath+"\\TestData\\TestExecutionMapping.xls");
			String SheetName = selectedProject+"_suite";
			String columnName = "TestCaseName";
			TCnames=ExcelDataList.getExcelDataInListByColName(ExcelFile, SheetName, columnName);
			int totalRows = ExcelDataList.getTotalRows(ExcelFile, SheetName);
			int TCExecFlagNumber = 6; 

			for(int Listcnt=0;Listcnt<TCnames.size();Listcnt++){
				if(TCnames.get(Listcnt).equals(selectedTC)){
					System.out.println("TC found !!!!!!!!!!!!");
					System.out.println("Updating the Execution Flag to Y");
					ExcelDataList.writeToExcel(ExcelFile, SheetName, (Listcnt+1), TCExecFlagNumber, "Y");
				}
				else {
					//Update the executionFlag to N
					ExcelDataList.writeToExcel(ExcelFile, SheetName, (Listcnt+1), TCExecFlagNumber, "N");
				}
			}
		}catch(Exception e){
			System.out.println("Error in updating the TestExecutionMapping sheet is - "+ e);
		}	  
	}


	//Removing a sheet from the scripting file
	private static void deleteScriptingSheet(String ProjectAsSelected,String selectedTC){
		try{
			String scriptingFileName = ProjectAsSelected + "_scripting.xls";
			HSSFWorkbook wb = getScriptingWorkbook(ProjectAsSelected);
			int index = 0;

			HSSFSheet sheet = wb.getSheet(selectedTC);
			if(sheet != null)   {
				index = wb.getSheetIndex(sheet);
				wb.removeSheetAt(index);
			}
			FileOutputStream output = new FileOutputStream(testdatapath+"\\TestData\\"+ProjectAsSelected+"\\"+ scriptingFileName);
			wb.write(output);
			output.close();
		}catch(Exception e){
			System.out.println("Exception in deleting sheet from scripting file is - "+ e);
		}
	}

	//Removing a sheet from the UserInput file
	private static void deleteUserInputSheet(String ProjectAsSelected,String selectedTC){
		try{
			String userInputFile = "UserInput_"+ ProjectAsSelected + ".xls";
			HSSFWorkbook wb = getUserInputWorkbook(ProjectAsSelected);
			int index = 0;

			HSSFSheet sheet = wb.getSheet(selectedTC);
			if(sheet != null)   {
				index = wb.getSheetIndex(sheet);
				wb.removeSheetAt(index);
			}
			FileOutputStream output = new FileOutputStream(testdatapath+"\\TestData\\"+ProjectAsSelected+"\\"+ userInputFile);
			wb.write(output);
			output.close();
		}catch(Exception e){
			System.out.println("Exception in deleting sheet from UserInput file is - "+ e);
		}
	}

	//Removing test case from Execution Mapping file
	private static void removeTCFromMapping(String ProjectAsSelected,String selectedTC){
		List<String> TCnames = new ArrayList<String>();
		int cnt=0;
		int rowToRemove=0;
		try{
			File ExcelFile = new File(testdatapath+"\\TestData\\TestExecutionMapping.xls");
			String SheetName = ProjectAsSelected+"_suite";
			FileInputStream fis = new FileInputStream(ExcelFile);
			HSSFWorkbook work = new HSSFWorkbook(fis);

			HSSFSheet sh = work.getSheet(SheetName);
			int numberOfRow = ExcelDataList.getTotalRows(ExcelFile, SheetName);
			TCnames=ExcelDataList.getExcelDataInListByColName(ExcelFile, SheetName, "TestCaseName");
			int lastRowNum = sh.getLastRowNum();

			/*for(cnt=0;cnt<numberOfRow;cnt++){
			  if(TCnames.get(cnt).equals(selectedTC)){
				  rowToRemove = cnt+1;
				  HSSFRow removingRow=sh.getRow(rowToRemove);
				  if(removingRow!=null){
			            sh.removeRow(removingRow);
			            break;
			        }
			  }
		  }
		  if((cnt+2) != numberOfRow){
			  sh.shiftRows(cnt+1, sh.getLastRowNum(), -1);
		  }*/

			for(cnt=0;cnt<numberOfRow;cnt++){
				if(TCnames.get(cnt).equals(selectedTC)){
					rowToRemove = cnt+1;
				}
			}
			if(rowToRemove>=0&&rowToRemove<lastRowNum){
				sh.shiftRows(rowToRemove+1,lastRowNum, -1);
			}
			if(rowToRemove==lastRowNum){
				HSSFRow removingRow=sh.getRow(rowToRemove);
				if(removingRow!=null){
					sh.removeRow(removingRow);
				}
			}




			FileOutputStream output = new FileOutputStream(testdatapath+"\\TestData\\TestExecutionMapping.xls");
			work.write(output);
			output.close();

			numberOfRow = ExcelDataList.getTotalRows(ExcelFile, SheetName);
			System.out.println("Number of rows after TC deletion in the Excetion mapping file - "+ numberOfRow);

		}catch(Exception e){
			System.out.println("Exception is -"+ e); 
		}
	}

	//Creating TcID for new TestCase
	static String createTCID(File ExecutionMappingFile, String nameOfSheet){
		String testCaseID = null;
		try{
			FileInputStream fis = new FileInputStream(ExecutionMappingFile);
			HSSFWorkbook wb = new HSSFWorkbook(fis);
			Sheet sh = wb.getSheet(nameOfSheet);
			int lastrow = sh.getLastRowNum()+1; //As the new entry will be lastRow +1
			testCaseID = Integer.toString(lastrow);
			return testCaseID;
		}catch(Exception e){
			System.out.println("Exception in creating the TestCaseID for the new Test Case"+ e);
			return testCaseID;
		}
	}

	//Running the execution of the TC in a separate thread
	/*public void run() {
		try{
			selectedProject = TestCreation.ProjectSelected;
			  String xml[] = {selectedProject+"_suiteTestNG.xml"};
			  //Update the flag to Y in the Execution Mapping sheet and other TC to N
			  updateExecFlag(TestCreation.TCSelected);
			  //Create the method to create the xml file
			  CreateTestNGxml.getFromExcel(selectedProject+"_suite");

			  //Execute the xml file
			  //TestNGxmlExecution.executeXML(xml);
			  Thread thread = new Thread(new TestNGxmlExecution());
			  thread.start();
			  System.out.println("Test case execution completed");
		}catch(Exception e){
			System.out.println("The Exception is - "+ e);
		}
	}*/

}

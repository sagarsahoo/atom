package selfitGui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.swing.JOptionPane;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.testng.annotations.Test;

public class ExcelDelete {
	@Test
	public static boolean DeleteRowGivenRowNum(File ExcelFile,String SheetName,int rowNum) {
		try{
			FileInputStream fis = new FileInputStream(ExcelFile);
			HSSFWorkbook wb = new HSSFWorkbook(fis);		  
			HSSFSheet sh = wb.getSheet(SheetName);
			int numberOfRow = sh.getLastRowNum();
			System.out.println("Number of rows before Test Step deletion  - "+ numberOfRow);

			//Shifting of the rows
			if(rowNum < numberOfRow){
				sh.shiftRows(rowNum+1, sh.getLastRowNum(), -1);
				/*for(int cnt=rowNum;cnt<sh.getLastRowNum()+1;cnt++){
				  sh.shiftRows(rowNum+1, sh.getLastRowNum(), -1);
			  }*/
				HSSFRow removingRow=sh.getRow(numberOfRow);
				sh.removeRow(removingRow);
			}
			if(rowNum == numberOfRow){
				HSSFRow removingRow=sh.getRow(rowNum);
				if(removingRow!=null){
					sh.removeRow(removingRow);
				}
			}
			FileOutputStream output = new FileOutputStream(ExcelFile);
			wb.write(output);
			output.close();
			numberOfRow = ExcelDataList.getTotalRows(ExcelFile, SheetName);
			System.out.println("Number of rows after Test Step deletion  - "+ numberOfRow);
			return true;

		}catch(Exception e){
			System.out.println("Exception in deleting row in excel - "+ e);
			return false;
		}
	}

	public static boolean deleteExcelSheet(File ExcelFile,String SheetName){
		try{
			FileInputStream fis = new FileInputStream(ExcelFile);
			HSSFWorkbook wb = new HSSFWorkbook(fis);
			int noOfSheet = wb.getNumberOfSheets();
			for(int cnt=0;cnt<noOfSheet;cnt++){
				if(wb.getSheetName(cnt).equals(SheetName)){
					wb.removeSheetAt(cnt);
					break;
				}
			}
			FileOutputStream output = new FileOutputStream(ExcelFile);
			wb.write(output);
			output.close();
			return true;
		}catch(Exception e){
			System.out.println("Exception in deleting sheet - "+ e);
			return false;
		}
	}
}

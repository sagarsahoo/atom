package selfitGui;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JOptionPane;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.testng.annotations.Test;

import utilityPackage.cellToString;

public class ScriptingData {
	
	 static Vector headerData = new Vector();
	 static Vector TestStepsData = new Vector();
	 static String path = System.getProperty("user.dir");
	 @Test
	 public static Vector getHeader(File scriptingFileName,String TCsheet) {
		 //Vector headerData = null;
		 try{
			 //File applicationScriptingFile = new File("C:\\Accenture\\Project\\selenium\\workspace\\Automation\\TestData\\abcd\\"+ scriptingFileName);
			 File applicationScriptingFile = scriptingFileName;		  
			 HSSFWorkbook workbook = null;
			 FileInputStream fis =  new FileInputStream(applicationScriptingFile);
			 workbook = new HSSFWorkbook (fis);		   
			 Sheet sheet = workbook.getSheet(TCsheet);
			 headerData.clear();
			 for (int i = 0; i < sheet.getRow(0).getLastCellNum(); i++) {
				 HSSFRow row = (HSSFRow) sheet.getRow(0);
				 HSSFCell cell = row.getCell(i);
				 String value = cellToString.cellTostring_HSSF(cell);
				 headerData.add(value);
			 }		  
			 return headerData;
		 }catch(Exception e){
			 JOptionPane.showMessageDialog(null, "Error in Excel Header - "+ e);
			 return headerData;
		 }
	 }

	 public static Vector getSelectedHeader(File excelFile,String TCsheet,List<String> HeaderList){
		 try{
			 File applicationScriptingFile = excelFile;		  
			 HSSFWorkbook workbook = null;
			 FileInputStream fis =  new FileInputStream(applicationScriptingFile);
			 workbook = new HSSFWorkbook (fis);
			 Sheet sheet = workbook.getSheet(TCsheet);
			 int cnt=0;
			 headerData.clear();
			 headerData.addAll(HeaderList);		  
			 return headerData;
		 }catch(Exception e){
			 JOptionPane.showMessageDialog(null, "Error in Excel Header - "+ e);
			 return headerData;
		 }
	 }

	 //
	 public static Vector getTestSteps(File scriptingFileName,String TCsheet){
		 //Vector TestStepsData = null;
		 try{

			 File applicationScriptingFile = scriptingFileName;		  
			 HSSFWorkbook workbook = null;
			 FileInputStream fis =  new FileInputStream(applicationScriptingFile);
			 workbook = new HSSFWorkbook (fis);		   
			 Sheet sheet = workbook.getSheet(TCsheet);
			 Iterator rowItr = sheet.rowIterator();
			 Iterator cellItr ;
			 Vector d ;

			 /*TestStepsData.clear();
		   while ( rowItr.hasNext() ) {
			   d = new Vector();
		        HSSFRow row = (HSSFRow) rowItr.next();
		        System.out.println("ROW:-->");
		        cellItr = row.cellIterator();

		        while ( cellItr.hasNext() ) {
		            HSSFCell cell = (HSSFCell) cellItr.next();
		            String value = cellToString.cellTostring_HSSF(cell);
					d.add(value);
		        }
		        d.add("\n");
			    TestStepsData.add(d);
		    }
		   return TestStepsData;*/

			 TestStepsData.clear();
			 for (int j = 1; j <= sheet.getLastRowNum(); j++) {
				 d = new Vector();
				 HSSFRow row = (HSSFRow) sheet.getRow(j);
				 cellItr = row.cellIterator();
				 while ( cellItr.hasNext() ) {
					 HSSFCell cell = (HSSFCell) cellItr.next();
					 String value = cellToString.cellTostring_HSSF(cell);
					 d.add(value);
				 }
				 d.add("\n");
				 TestStepsData.add(d);
			 }
			 return TestStepsData;

		 }catch(Exception e){
			 JOptionPane.showMessageDialog(null, "Error in getting Test Steps - "+ e);
			 return TestStepsData;
		 }	  
	 }

	 public static Vector getSelectedTestSteps(File scriptingFileName,String TCsheet,List<String> HeaderList){
		 try{
			 File applicationScriptingFile = scriptingFileName;		  
			 HSSFWorkbook workbook = null;
			 FileInputStream fis =  new FileInputStream(applicationScriptingFile);
			 workbook = new HSSFWorkbook (fis);		   
			 Sheet sheet = workbook.getSheet(TCsheet);
			 Iterator rowItr = sheet.rowIterator();
			 Iterator cellItr ;
			 Vector d ;

			 //getting column locations
			 List<String> columnsLoc = new ArrayList<String>();
			 for(int cnt=0;cnt<HeaderList.size();cnt++){
				 columnsLoc.add(Integer.toString(ExcelDataList.getColumnIndex(scriptingFileName, sheet, HeaderList.get(cnt))));
			 }

			 TestStepsData.clear();
			 HSSFCell cell = null;
			 String value=null;
			 for (int j = 1; j <= sheet.getLastRowNum(); j++) {
				 d = new Vector();
				 HSSFRow row = (HSSFRow) sheet.getRow(j);

				 for(int colCnt=0;colCnt<columnsLoc.size();colCnt++){
					 cell = row.getCell(Integer.parseInt(columnsLoc.get(colCnt)));
					 value = cellToString.cellTostring_HSSF(cell);
					 d.add(value);
				 }
				 d.add("\n");
				 TestStepsData.add(d);
			 }
			 return TestStepsData;
		 }catch(Exception e){
			 JOptionPane.showMessageDialog(null, "Error in getting Test Steps - "+ e);
			 return TestStepsData;
		 }
	 }

}

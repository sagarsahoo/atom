package selfitGui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.testng.annotations.Test;

import utilityPackage.OpenBrowser;

public class PageScanner {

	static String projectPath = System.getProperty("user.dir");

	static List<String> TextElements = new ArrayList<String>();
	static List<String> ClickableElements = new ArrayList<String>();
	static List<String> InputElements = new ArrayList<String>();
	String[][] data ;
	static String elementType;
	static String elementIdentifiedBy;
	static String elementIdentifier;
	static String elementName;
	static String sheetName;
	static int row=0;
	static int col=0;
	static List<String> allElements = new ArrayList<String>();
	static List<String> elementInfo = new ArrayList<String>();
	static Sheet sheet;
	static List<WebElement> numIDElements;
	static List<WebElement> numnameElements;
	static List<WebElement> numclassElements;
	static List<WebElement> numLinkElements;
	static WebDriver _driver;
	static int count=0;
	static String projectName;

	@Test
	public void inspectElement(){
		try{
			String browser_name = "IE";
			OpenBrowser.open_browser(browser_name, "NA");
		}catch(Exception e){
			System.out.println("Exception - "+ e);
		}
	}
	
	
	public static void getWebElements(String url,String pageName) {

		sheetName = pageName;
		projectName = TestCreation.ProjectSelected;

		if(url.length()==0){
			deleteEntries();
		}else{
			try{
				deleteEntries();
				TextElements.clear();
				ClickableElements.clear();
				_driver = OpenBrowser.open_browser("FF", "NA");
				_driver.get(url);			  
				allElements.clear();
				scanObjects();
				writeToExcel(allElements);
				count=0;
				_driver.quit(); 
			}catch(Exception e){
				System.out.println("Exception in scanning page- "+ e);
			}

		}	
	}

	public static void getWebElementsFRomExistingPage(WebDriver driver,String scanPageName,String NameOfApplication){
		try{
			System.out.println("Inside function -getWebElementsFRomExistingPage()");
			projectName = NameOfApplication;
			sheetName = scanPageName;
			deleteEntries(); 
			TextElements.clear();
			ClickableElements.clear();

			_driver = driver;
			allElements.clear();
			scanObjects();
			writeToExcel(allElements);
			count=0;
		}catch(Exception e){

		}
	}



	//Scanning the different Object types
	private static void scanObjects(){
		try{
			List<WebElement> inputType = _driver.findElements(By.xpath("//input"));
			System.out.println("############################# Displaying INPUT TEXT WebElements ##########################");
			getElementInfo(inputType,"inputText");

			System.out.println("############################# Displaying INPUT RADIO WebElements ##########################");
			getElementInfo(inputType,"inputRadio");

			System.out.println("############################# Displaying INPUT CHECKBOX WebElements ##########################");
			getElementInfo(inputType,"inputCheckbox");

			System.out.println("############################# Displaying Button WebElements ##########################");
			getElementInfo(inputType,"button");
			
			System.out.println("############################# Displaying SELECT WebElements ##########################");
			List<WebElement> SelectElements = _driver.findElements(By.xpath("//select"));	  
			System.out.println("Displaying all the elements having  select tags");
			getElementInfo(SelectElements,"dropdown");
			
			System.out.println("############################# Displaying LINKS WebElements ##########################");
			List<WebElement> linkElements = _driver.findElements(By.xpath("//a"));	  
			System.out.println("Displaying all the elements having  Links");
			getElementInfo(linkElements,"link");
			
			System.out.println("############################# Displaying FRAME WebElements ##########################");
			List<WebElement> frameList = _driver.findElements(By.tagName("iframe"));
			System.out.println("Displaying all frames");
			getElementInfo(frameList,"frame");

		}catch(Exception e){
			System.out.println("Exception in scanning objects - "+ e);
		}
	}

	// Getting the element info
	public static void getElementInfo(List<WebElement> ElementsList,String webType){
		try{
			switch(webType){

			case "link":
				for ( WebElement e : ElementsList ) {
					if(e.isEnabled() && e.isDisplayed()){
						if(!((e.getText().length())==0) || !(e.getText().equals(" "))){

							System.out.println(e.getText() + " ####### length - "+ e.getText().length());
							//ClickableElements.add(e.getText().trim());

							elementName = e.getText();
							elementType = "link";
							elementIdentifiedBy = "linkText";
							elementIdentifier = e.getText();						  

							numLinkElements = _driver.findElements(By.linkText(elementName));
							System.out.println("finder size - "+ numLinkElements.size());

							elementInfo.clear();
							elementInfo.add(elementName);
							elementInfo.add(elementType);
							elementInfo.add(elementIdentifiedBy);
							elementInfo.add(elementIdentifier);

							//writeToExcel(elementInfo);
							allElements.addAll(elementInfo);
							System.out.println("size of allElements - "+ allElements.size());
							for(int i=0;i<allElements.size();i++){
								System.out.println("List of List of String at index - "+ i + " - "+ allElements.get(i));
							}					  
							count++;
							System.out.println("===============================");
						}					  
					}
				}			  
				break;

			case "inputText":
				for ( WebElement e : ElementsList ) {
					if(!e.getAttribute("type").equalsIgnoreCase("hidden")){
						if(e.getAttribute("type").equals("text") || e.getAttribute("type").equals("password") ){
							displayIdentifiedBy(e,"text");
						}						  
					}				  
				}

				break;

			case "button":
				for ( WebElement e : ElementsList ) {
					if(!e.getAttribute("type").equalsIgnoreCase("hidden")){
						if(e.getAttribute("type").equals("btn") || e.getAttribute("type").equals("submit")){
							displayIdentifiedBy(e,"button");					  
						}						  
					}				  
				}
				break;

			case "inputRadio":
				for ( WebElement e : ElementsList ) {
					if(!e.getAttribute("type").equalsIgnoreCase("hidden")){
						if(e.getAttribute("type").equals("radio")){
							displayIdentifiedBy(e,"radio");					  
						}						  
					}				  
				}
				break;

			case "inputCheckbox":
				for ( WebElement e : ElementsList ) {
					if(!e.getAttribute("type").equalsIgnoreCase("hidden")){
						if(e.getAttribute("type").equals("checkbox")){
							displayIdentifiedBy(e,"checkBox");
						}						  
					}					  
				}
				break;

			case "dropdown":
				for ( WebElement e : ElementsList ) {
					if(!e.getAttribute("type").equalsIgnoreCase("hidden")){
						displayIdentifiedBy(e,"dropdown");			  
					}					  
				}
				break;

			case "frame":
				for ( WebElement e : ElementsList ) {
					displayIdentifiedBy(e,"frame");				  
				}
				break;

			default:
				System.out.println("NO matching Type");		  
			}

			System.out.println("Number of elements to be considered - "+ count);
			System.out.println("%%%%%%%%%%%%%%%%%%%%%% COMPLETED %%%%%%%%%%%%%%%%%%%%%%%%");

		}catch(Exception e){
			System.out.println("Exception in getting element information - "+ e);
		}

	}


	//Display identifiers for the webelements
	public static void displayIdentifiedBy(WebElement we,String eleType){
		try{
			count++;
			String bestMatchBy = null;
			String bestMatchValue = null;
			numIDElements = _driver.findElements(By.id(we.getAttribute("id")));
			numnameElements = _driver.findElements(By.name(we.getAttribute("name")));
			String classExpression = "//*[@class='" +we.getAttribute("class") +  "']";
			numclassElements = _driver.findElements(By.xpath(classExpression));


			System.out.println("TYPE - "+ eleType + "-----" );
			System.out.println("ID - "+ we.getAttribute("id")+ "	###### OCCURANCE - "+ numIDElements.size());
			System.out.println("NAME - "+ we.getAttribute("name") + "	###### OCCURANCE - "+ numnameElements.size() );
			System.out.println("CLASS - "+ we.getAttribute("class") + "	###### OCCURANCE - "+ numclassElements.size());


			//Considering identifiedBy
			if(numIDElements.size()==1){
				bestMatchBy = "ID";
				bestMatchValue = we.getAttribute("id");
				System.out.println("Best Match IdentiviedBy is - "+ bestMatchBy + " and the value is - "+ bestMatchValue);
				System.out.println("=============================================================");
			}
			else if(numnameElements.size()==1){
				bestMatchBy = "name";
				bestMatchValue = we.getAttribute("name");
				System.out.println("Best Match IdentiviedBy is - "+ bestMatchBy + " and the value is - "+ bestMatchValue);
				System.out.println("=============================================================");
			}else if(numclassElements.size()==1){
				bestMatchBy = "className";
				bestMatchValue = classExpression;
				System.out.println("Best Match IdentiviedBy is - "+ bestMatchBy + " and the value is - "+ bestMatchValue);
				System.out.println("=============================================================");
			}else{
				//Combining attributes
				System.out.println("Combining attributes");
				System.out.println("=============================================================");
				System.out.println("Creating xpath");
				bestMatchBy = "xpath";
				bestMatchValue = getXpath(we);
			}

			//Call the function to write to Excel
			elementInfo.clear();
			if(we.getAttribute("name").toString().length()!=0){
				elementInfo.add( we.getAttribute("name"));
				elementInfo.add(eleType);		  
				elementInfo.add(bestMatchBy);
				elementInfo.add(bestMatchValue);

				allElements.addAll(elementInfo);
				//writeToExcel(elementInfo);
			}else if(we.getAttribute("id").toString().length()!=0){
				elementInfo.add( we.getAttribute("id"));
				elementInfo.add(eleType);		  
				elementInfo.add(bestMatchBy);
				elementInfo.add(bestMatchValue);

				allElements.addAll(elementInfo);
			}

			numIDElements.clear();
			numnameElements.clear();
			numclassElements.clear();
		}catch(Exception e){
			System.out.println("Exception in getting the possible IdentifiedBy values for the element is - "+ e);
		}
	}



	//Deleting the Entries from the Excel
	public static void deleteEntries(){
		try{
			System.out.println("Inside the function deleteEntries()");
			System.out.println("Project Name - "+ projectName);
			File ExcelFile = new File(projectPath +"//TestData//"+ projectName + "//ObjectProperty.xls" );
			FileInputStream inputStream = new FileInputStream(ExcelFile);

			Workbook wb = new HSSFWorkbook(inputStream);
			//Read excel sheet by sheet name 

			if(!(wb.getSheet(sheetName)==null)){
				sheet = wb.getSheet(sheetName);
				int sheetIndex = wb.getSheetIndex(sheet);
				wb.removeSheetAt(sheetIndex);
			}

			//Create sheet
			sheet = wb.createSheet(sheetName);

			//create header
			Row rw = sheet.createRow(0);
			System.out.println("number of rows - " +sheet.getLastRowNum() );
			rw.createCell(0).setCellValue("Name");
			rw.createCell(1).setCellValue("Type");	
			rw.createCell(2).setCellValue("IdentifiedBy");
			rw.createCell(3).setCellValue("IdentifierValue");

			FileOutputStream outputStream  = new FileOutputStream(ExcelFile);
			wb.write(outputStream);
			outputStream.close();

			//System.out.println("Number of rows after deletion - "+(sheet.getLastRowNum()+1) );
		}catch(Exception e){
			System.out.println("Exception in deleting the entries fron the excel OR - "+ e);
		}
	}


	//Write into Excel
	public static void writeToExcel(List dataToInsert ){
		try{
			System.out.println("Project Name - "+ projectName);
			File ExcelFile = new File(projectPath +"//TestData//"+ projectName + "//ObjectProperty.xls" );
			FileInputStream inputStream = new FileInputStream(ExcelFile);
			Workbook wb = new HSSFWorkbook(inputStream);
			//Read excel sheet by sheet name    
			sheet = wb.getSheet(sheetName);

			// Alternate code
			int numOfRows = dataToInsert.size()/4;
			int eleCnt=0;
			int rowToInsert;
			Row row;
			String value;
			for(int rowcnt=0;rowcnt<numOfRows;rowcnt++){
				rowToInsert = sheet.getLastRowNum();
				row = sheet.createRow(rowToInsert+1);
				for(int colCnt=0;colCnt<4;colCnt++){
					Cell cell = row.createCell(colCnt);
					if(dataToInsert.get(eleCnt).toString().length()==0){
						value = "NA";
					}else{
						value = dataToInsert.get(eleCnt).toString();
					}
					cell.setCellValue(value);
					eleCnt++;
				}
			}

			/* int rowToInsert = sheet.getLastRowNum();
		  Row row = sheet.createRow(rowToInsert+1);
		  for(int colCnt=0;colCnt<4;colCnt++){
				  Cell cell = row.createCell(colCnt);
				  String value = dataToInsert.get(colCnt).toString();
				  cell.setCellValue(value);
		  }*/

			inputStream.close();
			FileOutputStream outputStream = new FileOutputStream(ExcelFile);
			wb.write(outputStream);
			outputStream.close();

		}catch(Exception e){
			System.out.println("Exception in writing to excel is - "+ e); 
		}
	}

	public static String getXpath(WebElement element){
		JavascriptExecutor executor = (JavascriptExecutor) _driver;
		Object aa=executor.executeScript("var items = {}; for (index = 0; index < arguments[0].attributes.length; ++index) { items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value; }; return items;", element);
		System.out.println("Attributes - "+aa.toString());
		Object bb=executor.executeScript("var val=arguments[0].value;var xpath = '';for ( ; arguments[0] && arguments[0].nodeType == 1; arguments[0] = arguments[0].parentNode ){var id = $(arguments[0].parentNode).children(arguments[0].tagName).index(arguments[0]) + 1;id > 1 ? (id = '[' + id + ']') : (id = '');xpath = '/' + arguments[0].tagName.toLowerCase() + id + xpath;}return xpath;", element);
		System.out.println("xpath- "+bb);
		return bb.toString();
	}

}

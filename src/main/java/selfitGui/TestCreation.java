package selfitGui;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.util.SystemOutLogger;

import utilityPackage.GenerateScript;
import utilityPackage.SeleniumSetup;
import utilityPackage.ThirdPartyTools;
import utilityPackage.cellToString;

public class TestCreation extends JFrame {

	private JPanel contentPane;
	private Connection conn;
	private JMenuBar menuBar;
	private JMenu mnFile;
	private JMenuItem mntmNewProject;
	private JMenu mnOpenProject;
	private JMenuItem mntmProjectList;
	private JMenu mnTestCase;
	private JMenuItem mntmNewTc;
	private JMenu mnAbout;
	private JMenuItem mntmFramework;
	private JMenuItem mntmAbout;
	private JSeparator separator;
	private JMenuItem mntmMappings;
	static String projectPath = System.getProperty("user.dir");
	static List<String> projectList = new ArrayList<String>();
	private DefaultListModel projectModel = new DefaultListModel();
	public static List<String> TCList = new ArrayList<String>();
	static List<String> ChildkeywordList = new ArrayList<String>();
	private static DefaultListModel TCModel = new DefaultListModel();
	private static DefaultListModel TCModelKeyword = new DefaultListModel();
	private JList list;
	private static JLabel lblTC;
	private JList list_TC;
	private JSeparator separator_1;
	private static JComboBox comboBoxProject;
	private static JList list_TestCaseList;
	private static JScrollPane scrollPaneTCList;
	static List<String> tc_sheetName = new ArrayList<String>();
	public static JTable TCtable;
	private static JScrollPane scrollPaneTCdetails;
	public static JList list_keywords;
	public static  JScrollPane scrollPane_parentKeywords;
	static JMenuItem mntmScanpages;
	static JMenu mnObjectrepo;
	static JMenuItem mntmViewObjects;
	static JMenuItem mntmBatch;

	static Vector headers = new Vector();
	static List<String> headerList = new ArrayList<String>();
	public static DefaultTableModel model = null;
	static Vector data = new Vector();
	static int tableWidth = 0; // set the tableWidth
	static int tableHeight = 0; // set the tableHeight	 
	static String path = System.getProperty("user.dir");
	private JMenuItem mntmDeleteProject;
	private static JButton btnTCExecute;
	private static JButton btnTCReport;
	private static JButton btnAddStep;
	private static JButton btnDelete_TC;
	private static JButton btnEdit_TC;
	private static JLabel lblProjList;
	private static JButton btnDeleteTS;
	private static JButton btnGenScript;
	private JButton btnProjLoc;
	private  ButtonGroup group = new ButtonGroup();

	private JMenu mnIntegration;
	public static  JButton btnAddKeyword;	 
	private JMenuItem mntmJenkins;
	private static JButton btnTC_log;
	private JButton refreshButton;

	public static TestCreation testCreationframe;
	public static String TCSelected;
	public static String selectedChildKeyword=null;
	public static String selectedKeyword=null;
	public static String ProjectSelected=null;
	public static File scriptingFile;
	public static File TestDataFile;
	public static int selectedTestStepNumber=-1;
	private static JButton btnAddProject;
	private static JButton btnAddTC;
	static  String ADDProject_IMG_PATH = Initialization.projectPath+ "//img//Project_ADD.png";
	static  String ADDTC_IMG_PATH = Initialization.projectPath+ "//img//TC_ADD.png";
	static String REFRESH_IMG_PATH = Initialization.projectPath+ "//img//refresh_img.png";
	static String EDIT_IMG_PATH = Initialization.projectPath+ "//img//edit_img.png";
	static String DELETE_IMG_PATH = Initialization.projectPath+ "//img//Delete_img.png";
	static String EXECUTE_IMG_PATH = Initialization.projectPath+ "//img//Execute_img.png";
	static String TS_ADD_IMG_PATH = Initialization.projectPath+ "//img//TS_ADD.png";
	static String REPORT_IMG_PATH = Initialization.projectPath+ "//img//report_img.png";
	static String LOGFILE_IMG_PATH = Initialization.projectPath+ "//img//Log_file.png";
	static String DELETE_TESTSTEP_IMG_PATH = Initialization.projectPath+ "//img//DeleteTS_img.png";
	private JMenuItem mntmConfigFile;
	private static JComboBox<String> comboBoxChildKeywords;
	static String action=null;
	private static JButton btnTestData;
	static String scriptingExcelName;
	static String testDataExcelName;
	private static JButton btnViewScript;



	public static void selfitDesignerFrame(){
		testCreationframe = new TestCreation();
		testCreationframe.setSize(Toolkit.getDefaultToolkit().getScreenSize().width-350, Toolkit.getDefaultToolkit().getScreenSize().height-50);
		//testCreationframe.pack();
		testCreationframe.setLocationRelativeTo(null);
		testCreationframe.setTitle("Selfit-designer");
		testCreationframe.setVisible(true);
		//testCreationframe.getContentPane().setBackground(Color.black);
		getKeywords();
	}

	public TestCreation() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1031, 689);

		//Displaying Menu Objects on start
		dispalyMenuObjects();

		//Elements when project is selected 
		displayObjectsOnProjectSelection();

		//Hide the Objects on Start
		hideObjectsonStart();

	}

	//******************************************************************************************************************************************************************************************
	//******************************************************************************************************************************************************************************************

	public void displayObjectsOnProjectSelection(){
		try{


			//Displaying the label of Test Cases Corresponding of the project selected
			lblTC = new JLabel("Test Cases");
			lblTC.setFont(new Font("Calibri", Font.BOLD, 15));
			lblTC.setBounds(56, 109, 91, 20);
			contentPane.add(lblTC);
			lblTC.setVisible(false);

			comboBoxProject = new JComboBox();
			//Action when project is selected
			comboBoxProject.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ProjectSelected = comboBoxProject.getSelectedItem().toString();
					scriptingExcelName = ProjectSelected + "_Scripting.xls";
					scriptingFile = new File(path + "//TestData//"+ ProjectSelected + "//" + scriptingExcelName);
					TestDataFile = new  File(path + "//TestData//"+ ProjectSelected + "//" + testDataExcelName);
					displayObjectsOnProjetSelection();
					System.out.println("Calling the display TC list function");
					displatTCList();					
				}
			});
			comboBoxProject.setBounds(21, 59, 161, 27);
			contentPane.add(comboBoxProject);

			scrollPaneTCList = new JScrollPane();
			scrollPaneTCList.setBounds(21, 140, 163, 240);
			contentPane.add(scrollPaneTCList);

			//Displaying the Test cases in a list when project is selected
			list_TestCaseList = new JList(TCModel);
			scrollPaneTCList.setViewportView(list_TestCaseList);
			list_TestCaseList.addListSelectionListener(new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent e) {
					try{
						System.out.println("Displaying the Detail Test Steps in the table");
						displayTCinTable();
					}catch(Exception exc){
						JOptionPane.showMessageDialog(null, "Error in loading TC  in table - "+ exc );
					}

				}
			});

			scrollPaneTCdetails = new JScrollPane(TCtable,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollPaneTCdetails.setBounds(250, 402, 614, 183);
			contentPane.add(scrollPaneTCdetails);

			TCtable = new JTable();
			scrollPaneTCdetails.setViewportView(TCtable);
			TCtable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			TCtable.doLayout();

			// Executing the Test Case selected
			BufferedImage execute_img = ImageIO.read(new File(EXECUTE_IMG_PATH));
			ImageIcon execute_icon=new ImageIcon(execute_img);
			btnTCExecute = new JButton(execute_icon);
			btnTCExecute.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try{
						System.out.println("Executing the TC selected");
						TestCase.execution(TCSelected,ProjectSelected);
					}catch(Exception excp){
						System.out.println("Exception in TC execution");
					}
				}
			});
			btnTCExecute.setBounds(669, 596, 38, 23);
			contentPane.add(btnTCExecute);

			BufferedImage report_img = ImageIO.read(new File(REPORT_IMG_PATH));
			ImageIcon report_icon=new ImageIcon(report_img);
			btnTCReport = new JButton(report_icon);
			btnTCReport.setToolTipText("Report");
			btnTCReport.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("DIsplay report");
					TestNGreport.displayReport();
				}
			});			
			btnTCReport.setBounds(809, 596, 38, 23);
			contentPane.add(btnTCReport);

			//Displaying the Keywords on new step addition
			BufferedImage TSadd_img = ImageIO.read(new File(TS_ADD_IMG_PATH));
			ImageIcon TS_add_icon=new ImageIcon(TSadd_img);
			btnAddStep = new JButton(TS_add_icon);
			btnAddStep.setToolTipText("New TestStep");
			btnAddStep.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("Displaying the parent and Child keyword list");
					displayObjectsOnAddNewStep();
					selectedTestStepNumber=TCtable.getSelectedRow();
					//getKeywords();
				}
			});
			btnAddStep.setBounds(282, 596, 38, 23);
			contentPane.add(btnAddStep);

			scrollPane_parentKeywords = new JScrollPane();
			scrollPane_parentKeywords.setBounds(250, 63, 178, 317);
			contentPane.add(scrollPane_parentKeywords);

			//Displaying the Child Keywords
			list_keywords = new JList(TCModelKeyword);
			list_keywords.addListSelectionListener(new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent e) {
					//viewParentKeywordList();
					selectedKeyword = list_keywords.getSelectedValue().toString();
					System.out.println("Displaying the child keywords");
					displayChildKeywordList();
					System.out.println("CHild keyword selected automatically is - "+ selectedChildKeyword);
				}
			});
			scrollPane_parentKeywords.setViewportView(list_keywords);

			BufferedImage edit_img = ImageIO.read(new File(EDIT_IMG_PATH));
			ImageIcon edit_icon=new ImageIcon(edit_img);
			btnEdit_TC = new JButton(edit_icon);
			btnEdit_TC.setToolTipText("Edit TestStep");
			btnEdit_TC.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("Editing a test step");
					editTestStep();
				}
			});
			btnEdit_TC.setBounds(344, 596, 38, 23);
			contentPane.add(btnEdit_TC);

			//Deleting the test case
			BufferedImage TC_delete_img = ImageIO.read(new File(DELETE_IMG_PATH));
			ImageIcon TCdelete_icon=new ImageIcon(TC_delete_img);
			btnDelete_TC = new JButton(TCdelete_icon);
			btnDelete_TC.setToolTipText("Delete TestCase");
			btnDelete_TC.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					TestCase.deleteTestCase(ProjectSelected,TCSelected);
					System.out.println("Calling the display TC list function");
					displatTCList();
				}
			});
			btnDelete_TC.setBounds(194, 273, 27, 20);
			contentPane.add(btnDelete_TC);

			lblProjList = new JLabel("Project");
			lblProjList.setFont(new Font("Calibri", Font.BOLD, 15));
			lblProjList.setBounds(64, 25, 62, 25);
			contentPane.add(lblProjList);

			//Adding a keyword in test step
			btnAddKeyword = new JButton("Add Keyword");		
			btnAddKeyword.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try{
						action = "add";
						System.out.println("Opening the frame to add values in selected Keyword");
						selectedChildKeyword = comboBoxChildKeywords.getSelectedItem().toString();
						System.out.println("Selected child keyword is - "+ selectedChildKeyword);
						addKeywordValues();			
					}catch(Exception excp){
						System.out.println("Exception in lauching the Add Keyword frame is - "+ excp);
					}
				}
			});
			btnAddKeyword.setFont(new Font("Calibri", Font.BOLD, 13));
			btnAddKeyword.setBounds(703, 212, 127, 27);
			contentPane.add(btnAddKeyword);

			BufferedImage LogFile_img = ImageIO.read(new File(LOGFILE_IMG_PATH));
			ImageIcon LogFile_icon=new ImageIcon(LogFile_img);
			btnTC_log = new JButton(LogFile_icon);
			btnTC_log.setToolTipText("Logs");
			btnTC_log.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					//Fetch the log file
					System.out.println("Fetching the log file in the htlm format");
					TestNGreport.displayLog();
				}
			});
			btnTC_log.setBounds(732, 596, 45, 23);
			contentPane.add(btnTC_log);

			BufferedImage Refresh_img = ImageIO.read(new File(REFRESH_IMG_PATH));
			ImageIcon refresh_icon=new ImageIcon(Refresh_img);
			refreshButton = new JButton(refresh_icon);
			refreshButton.setToolTipText("Refresh");
			refreshButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					TestCreation.model.fireTableDataChanged();
					displayTCinTable();
					selectedTestStepNumber=-1;
				}
			});			
			refreshButton.setBounds(21, 592, 38, 27);
			contentPane.add(refreshButton);

			BufferedImage AddProject_img = ImageIO.read(new File(ADDProject_IMG_PATH));
			ImageIcon AddProj_icon=new ImageIcon(AddProject_img);
			btnAddProject = new JButton(AddProj_icon);
			btnAddProject.setToolTipText("Add Project");
			btnAddProject.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("Calling method to create a new project");
					newProjectSetup();
					displayObjectsOnProjetSelection();
					System.out.println("Calling the display TC list function");
					displatTCList();
					displayProjectInComboBox();

				}
			});
			btnAddProject.setBounds(192, 66, 25, 20);
			contentPane.add(btnAddProject);
			btnAddProject.setEnabled(false);
			btnAddProject.setVisible(false);

			BufferedImage AddTC_img = ImageIO.read(new File(ADDTC_IMG_PATH));
			ImageIcon AddTC_icon=new ImageIcon(AddTC_img);
			btnAddTC = new JButton(AddTC_icon);
			btnAddTC.setToolTipText("Add TestCase");
			btnAddTC.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("Calling function to Add a NEW TC");
					String newTC = JOptionPane.showInputDialog("Please provide the TestCase Name \n (Do not provide any special Characters)");
					if(!newTC.equals(null)){
						TestCase.newTestCaseCreation(ProjectSelected,newTC);
						JOptionPane.showMessageDialog(null, newTC + " Test case created");
						//Display the TC
						displatTCList();
					}
				}
			});
			btnAddTC.setBounds(194, 212, 25, 20);
			contentPane.add(btnAddTC);
			btnAddTC.setEnabled(false);
			btnAddTC.setVisible(false);

			BufferedImage TS_delete_img = ImageIO.read(new File(DELETE_TESTSTEP_IMG_PATH));
			ImageIcon TSdelete_icon=new ImageIcon(TS_delete_img);
			btnDeleteTS = new JButton(TSdelete_icon);
			btnDeleteTS.setToolTipText("Delete TestStep");
			btnDeleteTS.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("Deleting the test step selected - ");
					deleteTestStep();
					displayTCinTable();
				}
			});
			btnDeleteTS.setBounds(405, 596, 38, 23);
			contentPane.add(btnDeleteTS);
			btnDeleteTS.setVisible(false);

			comboBoxChildKeywords = new JComboBox();
			/*comboBoxChildKeywords.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					selectedChildKeyword = comboBoxChildKeywords.getSelectedItem().toString();
					System.out.println("Selected child keyword is - "+ selectedChildKeyword);
				}
			});*/
			comboBoxChildKeywords.setBounds(461, 212, 189, 27);
			contentPane.add(comboBoxChildKeywords);

			btnTestData = new JButton("Test Data");
			btnTestData.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("Opening the test data Management Frame");
					TestDataManagement.getFrame();
				}
			});
			btnTestData.setBounds(151, 562, 89, 23);
			contentPane.add(btnTestData);

			btnProjLoc = new JButton("Proj Folder");
			btnProjLoc.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						if(!(ProjectSelected.length()==0)){
							String projectLocation = Initialization.projectPath + "//TestData//" + ProjectSelected;
							Desktop.getDesktop().open(new File(projectLocation));
						}else{
							JOptionPane.showMessageDialog(null, "Please select a project");
						}

					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			});
			btnProjLoc.setFont(new Font("Calibri", Font.PLAIN, 11));
			btnProjLoc.setBounds(881, 26, 89, 23);
			contentPane.add(btnProjLoc);
			btnProjLoc.setVisible(false);

			btnGenScript = new JButton("Gen");
			btnGenScript.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					//Calling the Generate SCript class
					System.out.println("Calling the generate script method");
					GenerateScript.scriptGeneration();
				}
			});
			btnGenScript.setBounds(874, 468, 89, 23);
			contentPane.add(btnGenScript);

			btnViewScript = new JButton("View");
			btnViewScript.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("Open the Generated test script");
					displayGeneratedScript();
				}
			});
			btnViewScript.setBounds(881, 512, 73, 20);
			contentPane.add(btnViewScript);


		}catch(Exception e){
			System.out.println("Exception in displayObjectsOnProjectSelection - "+ e);
		}
	}



	//New Project creation
	public void newProjectSetup(){
		try{
			//calling the selenium setup for new project
			String[] args = {"test"};
			SeleniumSetup.main(args);			
			ProjectSelected = SeleniumSetup.applicationPackage;
			if(ProjectSelected!=null){
				displayObjectsOnProjetSelection();
			}		
		}catch(Exception ex){
			System.out.println("Exception in selenium seup project - "+ ex);
		}
	}



	//Displaying list in comboBox
	public static void displayProjectInComboBox(){
		try{
			lblProjList.setVisible(true);
			comboBoxProject.setVisible(true);
			lblTC.setVisible(true);
			list_TestCaseList.setVisible(true);
			scrollPaneTCList.setVisible(true);
			projectList.clear();

			//Calling the getting project list function
			projectList = getProjectList();
			//comboBoxProject.removeAllItems();
			int initialComboBoxCounter = comboBoxProject.getItemCount();
			System.out.println("Number of items in the comboBox before - "+ initialComboBoxCounter);
			/*if(projectList.size()<initialComboBoxCounter){
				for(int itr=0;itr<initialComboBoxCounter;itr++){
					System.out.println("Total items - "+ comboBoxProject.getItemCount() + " :: Removing item - "+ comboBoxProject.getItemAt(0));
					comboBoxProject.removeItemAt(0);
				}
			}*/
			for(int itr=0;itr<projectList.size();itr++){
				int matchcnt=0;
				for(int comboxCnt=0;comboxCnt<comboBoxProject.getItemCount();comboxCnt++){
					if(projectList.get(itr).toString().equals(comboBoxProject.getItemAt(comboxCnt))){
						System.out.println("Item already present");
						matchcnt++;
					}
				}
				if(matchcnt==0){
					//System.out.println("Adding Item");
					comboBoxProject.addItem(projectList.get(itr).toString());
				}
			}


			System.out.println("Number of items in the comboBox after - "+ comboBoxProject.getItemCount());
			//ProjectSelected = comboBoxProject.getSelectedItem().toString();
			//comboBoxProject.setSelectedIndex(0);
			ProjectSelected = comboBoxProject.getSelectedItem().toString();

		}catch(Exception ProjExc){
			comboBoxProject.getItemCount();
			JOptionPane.showMessageDialog(null, "ERROR in displaying project List - "+ ProjExc);
		}
	}

	//Displaying list of TC as per Project selected
	public static void displatTCList(){
		try{
			//String selectedValue = list.getSelectedValue().toString();
			//btnNewTC.setVisible(true);
			ProjectSelected = comboBoxProject.getSelectedItem().toString();
			System.out.println("Value selected is - "+ ProjectSelected);

			//Clearing the existing TC list and Calling function to populate in the new list of Test Cases
			TCList.clear();
			TCList = getTCList();
			//removing the existing data
			TCModel.removeAllElements();
			TCModel.clear();

			System.out.println("Size of TCModel brfore adding for -" + ProjectSelected + " is - " + TCModel.size());
			for(int cnt=0;cnt<TCList.size();cnt++){
				TCModel.addElement(TCList.get(cnt));
			}
			System.out.println("Size of TCModel after adding for - "+ ProjectSelected + " is - "+ TCModel.size());

		}catch(Exception TCExc){
			JOptionPane.showMessageDialog(null, "ERROR in displaying TC list - "+ TCExc);
		}
	}


	//listing all the projects
	public static List<String> getProjectList(){
		projectList.clear();
		String reqString=null;
		int endIndex = 0;
		String projectName=null;
		try{
			//Reading testExecutionMapping file
			File mappingFile = new File(projectPath + "\\TestData\\TestExecutionMapping.xls");
			FileInputStream st = new FileInputStream(mappingFile);
			HSSFWorkbook wb = new HSSFWorkbook(st);
			int noSheets = wb.getNumberOfSheets();
			for(int i=0;i<noSheets;i++){
				reqString=wb.getSheetName(i);
				if(reqString.contains("_")){
					endIndex = reqString.indexOf("_");
					projectName=reqString.substring(0, endIndex);
				}
				else{
					projectName=reqString;
				}
				//System.out.println("Project Name is - "+projectName );
				projectList.add(projectName);
			}
			return projectList;
		}catch(Exception getLiistEx){
			JOptionPane.showMessageDialog(null, "Exception in getting List of Projects - "+ getLiistEx);;
			return projectList;
		}
	}

	//Getting the list of test cases for the selected project
	public static  List<String> getTCList(){
		Sheet sh = null;
		String TC_name = null; 
		try{
			File mappingFile = new File(projectPath + "\\TestData\\TestExecutionMapping.xls");
			FileInputStream st = new FileInputStream(mappingFile);
			HSSFWorkbook wb = new HSSFWorkbook(st);
			sh = wb.getSheet(ProjectSelected + "_suite");
			//Getting the name of the test cases
			int no_rows = sh.getLastRowNum();
			for(int rowcnt=1;rowcnt<=no_rows;rowcnt++){
				HSSFRow rw = (HSSFRow) sh.getRow(rowcnt);
				HSSFCell cell = rw.getCell(1);
				TC_name = cellToString.cellTostring_HSSF(cell);
				TCList.add(TC_name);
			}
			return TCList;
		}catch(Exception e){
			JOptionPane.showMessageDialog(null,"Exception in getting TC list - "+ e);
			return TCList;
		}
	}


	/*	// Handler for list selection changes
	 	public void valueChanged( ListSelectionEvent event )
	 	{
	 		// See if this is a listbox selection and the
	 		// event stream has settled
			if( event.getSource() == list_TestCaseList && !event.getValueIsAdjusting() )
			{
				// Get the current selection and place it in the
				// edit field
				String stringValue = comboBoxProject.getSelectedItem().toString();
				if( stringValue != null )
					displatTCList();
			}
	 	}*/

	//Get SheetName of the Test Case selected
	public static String TCSheet(String projectSelected,String selectedTC){
		Sheet projectSuiteSheet = null;
		String testCaseName = null;
		String TC_refSheet = null;
		try{
			File mappingFile = new File(projectPath + "\\TestData\\TestExecutionMapping.xls");
			FileInputStream st = new FileInputStream(mappingFile);
			HSSFWorkbook wb = new HSSFWorkbook(st);
			projectSuiteSheet = wb.getSheet(projectSelected + "_suite");
			//Getting the name of the test cases
			int no_rows = projectSuiteSheet.getLastRowNum();
			for(int rowcnt=1;rowcnt<=no_rows;rowcnt++){
				HSSFRow rw = (HSSFRow) projectSuiteSheet.getRow(rowcnt);
				HSSFCell cell = rw.getCell(1);
				testCaseName = cellToString.cellTostring_HSSF(cell);
				if(testCaseName.equals(selectedTC)){
					HSSFCell Sheetcell = rw.getCell(4);
					TC_refSheet = cellToString.cellTostring_HSSF(Sheetcell);
					return TC_refSheet;
				}
			}return TC_refSheet;

		}catch(Exception e){
			JOptionPane.showMessageDialog(null, "Exception in TC details is - "+ e);
			return TC_refSheet;
		}
	}

	//Display TC details
	public void displayTCinTable(){
		try{
			ProjectSelected = comboBoxProject.getSelectedItem().toString();
			System.out.println("Project selected - "+ ProjectSelected);
			TCSelected = list_TestCaseList.getSelectedValue().toString();
			System.out.println("TC selected - "+ TCSelected);
			String TCSelectedsheet = TCSheet(ProjectSelected,TCSelected);
			System.out.println("Selected Test case sheet is - "+ TCSelectedsheet);

			TCtable.setVisible(true);
			scrollPaneTCdetails.setVisible(true);

			//Get the scripting file location
			scriptingExcelName = ProjectSelected + "_Scripting.xls";
			scriptingFile = new File(path + "//TestData//"+ ProjectSelected + "//" + scriptingExcelName);

			//Get the testdata file
			testDataExcelName = "UserInput_" + ProjectSelected + ".xls" ;
			TestDataFile = new  File(path + "//TestData//"+ ProjectSelected + "//" + testDataExcelName);

			//headers = ScriptingData.getHeader(scriptingFile, TCSelectedsheet);
			//data= ScriptingData.getTestSteps(scriptingFile, TCSelectedsheet);
			getHeaders();
			headers = ScriptingData.getSelectedHeader(scriptingFile, TCSelectedsheet, headerList);
			data = ScriptingData.getSelectedTestSteps(scriptingFile, TCSelectedsheet, headerList);

			model = new DefaultTableModel(data, headers);
			tableWidth = model.getColumnCount() * 100;
			tableHeight = model.getRowCount() * 25;

			TCtable.setPreferredSize(new Dimension(tableWidth, tableHeight));
			TCtable.setModel(model);

			btnTCExecute.setVisible(true);
			btnTCReport.setVisible(true); 
			btnAddStep.setVisible(true);
			btnViewScript.setVisible(true);
			btnGenScript.setVisible(true);
			btnDelete_TC.setVisible(true);
			btnDeleteTS.setVisible(true);
			btnEdit_TC.setVisible(true);
			btnTC_log.setVisible(true);
			refreshButton.setVisible(true);
			btnTestData.setVisible(true);
		}catch(Exception e){
			System.out.println("Exception in displaying the TC in table - "+ e);
		}
	}

	private void getHeaders(){
		headerList.clear();
		headerList.add("Exec_Flag");
		headerList.add("TS_ID");
		headerList.add("TS_NAME");
		headerList.add("Keyword");
		//headerList.add("element_identifiedBy");
		//headerList.add("element_identifier");
		headerList.add("pass_values");
		//headerList.add("target_identifiedBy");
		//headerList.add("target_identifier");
		headerList.add("testStep_segment");
	}

	//Displaying the Mapping sheet in a second frame
	public static void showMapping(){
		try{
			//Getting Mapping sheet
			File mappingFile = new File(path+"\\TestData\\TestExecutionMapping.xls");
			TCmapping.displayMapping(mappingFile,ProjectSelected+"_suite");
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, "Error in getting mapping file - "+ e);
		}
	}

	//Display the Keywords
	public static void getKeywords(){
		try{
			List<String> keywordList = new ArrayList<String>();
			File keywordFile = new File(path+"\\selenium_documents\\KeywordList.xls");
			String sheetName = "mapping";
			int columnIndex=0;
			keywordList = ExcelDataList.getExcelDataInListByColIndex(keywordFile, sheetName, columnIndex);

			//TCModelKeyword.removeAllElements();
			TCModelKeyword.clear();
			//System.out.println("Size of keyword list - "+ keywordList.size());
			for(int i=0;i<keywordList.size();i++){
				TCModelKeyword.add(i, keywordList.get(i));
				//TCModelKeyword.addElement(keywordList.get(i));
			}

		}catch(Exception e){
			System.out.println("The Exception in getting Keywords is - "+ e);
		}
	}



	//
	public static  void displayObjectsOnProjetSelection(){

		list_keywords.setVisible(false);
		scrollPane_parentKeywords.setVisible(false);
		comboBoxChildKeywords.setVisible(false);
		TCtable.setVisible(false);
		scrollPaneTCdetails.setVisible(false);
		btnEdit_TC.setVisible(false);
		btnTCExecute.setVisible(false);
		btnTC_log.setVisible(false);
		btnTCReport.setVisible(false);
		btnAddStep.setVisible(false);
		btnViewScript.setVisible(false);
		btnGenScript.setVisible(false);
		btnTestData.setVisible(false);
		btnDelete_TC.setVisible(false);
		btnDeleteTS.setVisible(false);
		btnAddKeyword.setVisible(false);

	}

	private void hideObjectsonStart(){
		btnAddKeyword.setVisible(false);
		lblProjList.setVisible(false);
		btnDelete_TC.setVisible(false);
		btnEdit_TC.setVisible(false);
		list_keywords.setVisible(false);
		comboBoxChildKeywords.setVisible(false);
		scrollPane_parentKeywords.setVisible(false);
		btnAddStep.setVisible(false);
		btnViewScript.setVisible(false);
		btnGenScript.setVisible(false);
		btnTestData.setVisible(false);
		btnDeleteTS.setVisible(false);
		btnTCReport.setVisible(false);
		TCtable.setVisible(false);
		list_TestCaseList.setVisible(false);
		comboBoxProject.setVisible(false);
		scrollPaneTCList.setVisible(false);
		scrollPaneTCdetails.setVisible(false);
		btnTCExecute.setVisible(false);
		btnTC_log.setVisible(false);
		refreshButton.setVisible(false);


	}

	private void displayObjectsOnAddNewStep(){
		try{
			TestCreation.scrollPane_parentKeywords.setEnabled(true);
			TestCreation.list_keywords.setEnabled(true);
			TestCreation.scrollPane_parentKeywords.setVisible(true);
			TestCreation.list_keywords.setVisible(true);
			btnAddKeyword.setVisible(true);

			/*if(selectedKeyword!=null){
					viewParentKeywordList();
				}*/
		}catch(Exception e){
			System.out.println("Error -"+ e);
		}
	}

	private void dispalyMenuObjects(){
		try{
			// Menu bar Items defined
			menuBar = new JMenuBar();
			setJMenuBar(menuBar);

			//Menu for Project
			mnFile = new JMenu("Project");
			mnFile.setFont(new Font("Calibri", Font.PLAIN, 15));
			menuBar.add(mnFile);

			//Menu-Item for New-Project
			mntmNewProject = new JMenuItem("New Project");
			mntmNewProject.setFont(new Font("Calibri", Font.PLAIN, 12));
			mntmNewProject.setBackground(Color.LIGHT_GRAY);
			mntmNewProject.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("Calling method to create a new project");
					newProjectSetup();
					displayProjectInComboBox();
				}
			});
			mnFile.add(mntmNewProject);

			//Open Project
			mnOpenProject = new JMenu("Open Project");
			mnOpenProject.setFont(new Font("Calibri", Font.PLAIN, 12));
			mnFile.add(mnOpenProject);

			//Project List
			mntmProjectList = new JMenuItem("Project List");
			mntmProjectList.addActionListener(new ActionListener() {
				//Displaying list of Projects
				public void actionPerformed(ActionEvent e) {
					//comboBoxProject.removeAllItems();
					btnAddProject.setEnabled(true);
					btnAddProject.setVisible(true);
					btnAddTC.setEnabled(true);
					btnAddTC.setVisible(true);
					btnProjLoc.setVisible(true);
					projectList.clear();
					displayProjectInComboBox();
					mnTestCase.setEnabled(true);
					mnIntegration.setEnabled(true);
					mnObjectrepo.setEnabled(true);
				}
			});
			mnOpenProject.add(mntmProjectList);

			JMenuItem MenuItemExit = new JMenuItem("EXIT");
			MenuItemExit.setForeground(Color.RED);
			MenuItemExit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try{
						testCreationframe.dispose();
						System.exit(0);
					}catch(Exception ExitExc){
						JOptionPane.showMessageDialog(null, "Error in Closing - "+ ExitExc);
					}
				}
			});

			JSeparator separator_2 = new JSeparator();
			mnFile.add(separator_2);

			mntmDeleteProject = new JMenuItem("Delete Project");
			mntmDeleteProject.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					testCreationframe.setEnabled(false);
					DeleteProjectFrame.getFrame();
					/*displayObjectsOnProjetSelection();
					System.out.println("Calling the display TC list function");
					displatTCList();
					displayProjectInComboBox();*/
					System.out.println("");
				}
			});
			mntmDeleteProject.setFont(new Font("Calibri", Font.BOLD, 12));
			mnFile.add(mntmDeleteProject);

			separator_1 = new JSeparator();
			mnFile.add(separator_1);
			MenuItemExit.setFont(new Font("Calibri", Font.BOLD, 15));
			mnFile.add(MenuItemExit);

			mnTestCase = new JMenu("Test Case");
			mnTestCase.setFont(new Font("Calibri", Font.PLAIN, 15));
			menuBar.add(mnTestCase);
			mnTestCase.setEnabled(false);

			mntmNewTc = new JMenuItem("New Test Case");
			mntmNewTc.setFont(new Font("Calibri", Font.BOLD, 15));
			mnTestCase.add(mntmNewTc);

			separator = new JSeparator();
			mnTestCase.add(separator);

			//Showing the Execution MApping for the Project selected
			mntmMappings = new JMenuItem("Mappings");
			mntmMappings.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					//Disable the first frame
					testCreationframe.setEnabled(false);
					showMapping();
				}
			});
			mntmMappings.setFont(new Font("Calibri", Font.BOLD, 15));
			mnTestCase.add(mntmMappings);

			JMenuItem mntmMasterSheet = new JMenuItem("Master Sheet");
			mntmMasterSheet.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("Opening the Master Sheet for the Environment stuffs!");
					testCreationframe.setEnabled(false);
					try {
						MasterSheetFrame.launchMasterSheetFrame();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			});
			mntmMasterSheet.setFont(new Font("Calibri", Font.BOLD, 15));
			mnTestCase.add(mntmMasterSheet);

			JSeparator separator_3 = new JSeparator();
			mnTestCase.add(separator_3);

			mntmConfigFile = new JMenuItem("Config File");
			mntmConfigFile.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("Opening the configuration file for DB Properties");
					//openConfigFile();
				}
			});
			mntmConfigFile.setFont(new Font("Calibri", Font.BOLD, 15));
			mnTestCase.add(mntmConfigFile);


			// FOr Object repositories
			mnObjectrepo = new JMenu("ObjectRepo");
			mnObjectrepo.setFont(new Font("Calibri", Font.PLAIN, 15));
			menuBar.add(mnObjectrepo);
			mnObjectrepo.setEnabled(false);

			mntmScanpages = new JMenuItem("Scan-pages");
			mntmScanpages.setFont(new Font("Calibri", Font.BOLD, 15));
			mntmScanpages.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("calling the repository frame");
					ObjectRepositoryView.ScanObjectRepoFrameLauchedFRom = "TestCreation";
					ObjectRepositoryScan.getObjectRepositoryFrame();
				}
			});
			mnObjectrepo.add(mntmScanpages);

			//menuItem to view the objects
			mntmViewObjects = new JMenuItem("View Objects");
			mntmViewObjects.setFont(new Font("Calibri", Font.BOLD, 15));
			mntmViewObjects.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ObjectRepositoryView.objRepoLaunchedFrom="TestCreation";
					ObjectRepositoryView.getFrame();
				}
			});
			mnObjectrepo.add(mntmViewObjects);

			mnIntegration = new JMenu("Integration");
			mnIntegration.setFont(new Font("Calisto MT", Font.PLAIN, 15));
			menuBar.add(mnIntegration);
			mnIntegration.setEnabled(false);

			mntmJenkins = new JMenuItem("JENKINS");
			mntmJenkins.setFont(new Font("Calibri", Font.BOLD, 15));
			mnIntegration.add(mntmJenkins);

			JMenuItem mntmDeploy = new JMenuItem("Deploy");
			mntmDeploy.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					//calling a batch file to create the project jar file
					System.out.println("Calling batch file");
					ThirdPartyTools.executeWindowBatch(Initialization.projectPath+ "\\batchFiles\\","CreateProject-jarFile.bat");
					System.out.println("Exceution completed");
				}
			});
			mntmDeploy.setFont(new Font("Calibri", Font.BOLD, 15));
			mnIntegration.add(mntmDeploy);

			JMenuItem mntmGrid = new JMenuItem("GRID");
			mntmGrid.setFont(new Font("Calibri", Font.BOLD, 15));
			mnIntegration.add(mntmGrid);

			mntmBatch = new JMenuItem("Batch Mode");
			mntmBatch.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					//creating frame
					System.out.println("Creating xml for Batch mode execution");
					try {
						BatchMode.getBatchFrame();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			});
			mntmBatch.setFont(new Font("Calibri", Font.BOLD, 15));
			mnIntegration.add(mntmBatch);

			mnAbout = new JMenu("Help");
			mnAbout.setFont(new Font("Calibri", Font.PLAIN, 15));
			menuBar.add(mnAbout);

			//Display all the keywords defined
			mntmFramework = new JMenuItem("Framework");
			mntmFramework.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("Framework document");
					TestNGreport.openFrameworkDocument();
				}
			});
			mntmFramework.setFont(new Font("Calibri", Font.BOLD, 15));
			mnAbout.add(mntmFramework);

			mntmAbout = new JMenuItem("About");
			mntmAbout.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JOptionPane.showMessageDialog(null, "SELFIT \n Selenium Framework for Integrated Testing \n Developed By - Sagar");
				}
			});
			mntmAbout.setFont(new Font("Calibri", Font.BOLD, 15));
			mnAbout.add(mntmAbout);
			contentPane = new JPanel();
			contentPane.setBackground(Color.LIGHT_GRAY);
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);

		}catch(Exception e){
			System.out.println("Error - "+e);
		}
	}

	//Displaying the Child Keyword List
	private  void displayChildKeywordList(){
		try{
			comboBoxChildKeywords.setVisible(true);
			comboBoxChildKeywords.setEnabled(true);
			System.out.println("Keyword selected is - "+ selectedKeyword);
			ChildkeywordList.clear();
			ChildkeywordList.addAll(ExcelDataList.getExcelDataInListByColName(Initialization.KeywordFile, "mapping",selectedKeyword ));

			comboBoxChildKeywords.removeAllItems();
			String item;
			for(int cnt=0;cnt<ChildkeywordList.size();cnt++){
				item = ChildkeywordList.get(cnt).toString();
				comboBoxChildKeywords.addItem(item);
				System.out.println(comboBoxChildKeywords.getItemAt(cnt));
			}

			/*list_ChildKeywords.setVisible(true);
		 		scrollPane_ChildKeywords.setVisible(true);
				TCModelChildKeyword.clear();
				list_ChildKeywords.setModel(TCModelChildKeyword);
				TCModelChildKeyword.clear();
				for(int cnt=0;cnt<ChildkeywordList.size();cnt++){
					TCModelChildKeyword.addElement(ChildkeywordList.get(cnt).toString());
		        }
				list_ChildKeywords.setModel(TCModelChildKeyword);*/
		}catch(Exception e){
			System.out.println("Exception in displaying Child Keyword - "+ e);		
		}
	}

	//Editing a test step
	private static void editTestStep(){
		try{
			int selectedRowCount = TCtable.getSelectedRowCount();
			if(selectedRowCount==1){
				selectedTestStepNumber = TCtable.getSelectedRow();
				if(selectedTestStepNumber>=0){
					System.out.println("Selected row to edit - "+ selectedTestStepNumber);
					selectedTestStepNumber = (selectedTestStepNumber+1);
					System.out.println("Selected row to edit in Excel file - "+ selectedTestStepNumber);
					String selectedKeywordToEdit = ExcelDataList.getValueByRowAndCol(scriptingFile, TCSelected, (selectedTestStepNumber), 5);
					selectedChildKeyword=selectedKeywordToEdit;
					action="edit";
					addKeywordValues();//calling the same frame where we provided the keyword related field values
					Keyword.setKeywordValuesToEdit(selectedChildKeyword);
				}else{
					JOptionPane.showMessageDialog(null, "Please select a row to Edit");
				}	
			}else{
				JOptionPane.showMessageDialog(null, "Please select a single row to Edit");
			}					
		}catch(Exception e){
			System.out.println("Exception in editing the test step - "+ e);
		}
	}

	//Add keyword values when A Keyword button is clicked
	private static void addKeywordValues(){
		try{
			//Check whether the child keyword is selected
			if(selectedChildKeyword==null){
				JOptionPane.showMessageDialog(null,"Please select the child keyword");
			}
			else{
				System.out.println("Selected Child keyword is - "+ selectedChildKeyword);
				testCreationframe.setEnabled(false);												
				Keyword.getKeywordFrame();
			}			
		}catch(Exception e){
			System.out.println("Exceptio in showing frame to add keyword values");
		}
	}

	//DEtete a test step
	private static void deleteTestStep(){
		try{
			if(TCtable.getSelectedRowCount()==1){
				selectedTestStepNumber = TCtable.getSelectedRow()+1;
				System.out.println("Test step to delete row num - "+ selectedTestStepNumber);
				if(selectedTestStepNumber!=0){
					ExcelDelete.DeleteRowGivenRowNum(TestCreation.scriptingFile, TestCreation.TCSelected, selectedTestStepNumber);
					JOptionPane.showMessageDialog(null, "Row deleted ! refresh");


				}else{
					JOptionPane.showMessageDialog(null, "Please select a row to Edit");
				}	 
			}else{
				JOptionPane.showMessageDialog(null, "Please select a single row to Edit");
			}

		}catch(Exception e){
			System.out.println("Exception in deleting a test step - "+ e);
		}
	}

	private static void displayGeneratedScript(){
		try{
			String scriptJavaFileName = projectPath + "//src//main//java//"+ProjectSelected+ "//"+ TCSelected+".java";
			File scriptJavaFile = new File(scriptJavaFileName);
			//Desktop.getDesktop().browse(scriptJavaFile.toURI());
			Desktop.getDesktop().open(scriptJavaFile);
		}catch(Exception e){
			System.out.println("Exception in viewing generated script - "+ e);
		}
	}

	public static void displaydefaultOnProjectDeletion(){
		try{
			projectList.clear();
			displayProjectInComboBox();

			ProjectSelected = comboBoxProject.getItemAt(1).toString();
			comboBoxProject.setSelectedIndex(0);
			scriptingExcelName = ProjectSelected + "_Scripting.xls";
			scriptingFile = new File(path + "//TestData//"+ ProjectSelected + "//" + scriptingExcelName);
			TestDataFile = new  File(path + "//TestData//"+ ProjectSelected + "//" + testDataExcelName);
			displayObjectsOnProjetSelection();
			System.out.println("Calling the display TC list function");
			displatTCList();
		}catch(Exception e){

		}
	}
}

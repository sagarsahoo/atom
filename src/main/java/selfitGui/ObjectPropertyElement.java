package selfitGui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ObjectPropertyElement extends JFrame {

	private JPanel contentPane;
	public static ObjectPropertyElement ObjectPropertyFrame;
	private static JTextField textFieldObjName;
	private static JTextField textFieldObjLocValue;
	static JLabel lblObjName;
	static JLabel lblObjType;
	static JLabel lblLocator;
	static JLabel lblLocValue;
	static JComboBox comboBoxLocType;
	static JButton btnSave;
	static JButton btnCancel;
	private static JComboBox comboBoxObjectType;
	public static List<String> newObjectPropertyList = new ArrayList<String>();
	

	public static void getFrame(){		
		ObjectRepositoryView.objRepoViewFrame.setEnabled(false);
		ObjectPropertyFrame = new ObjectPropertyElement();
		ObjectPropertyFrame.setLocationRelativeTo(null);
		ObjectPropertyFrame.setTitle("Manage Object Repository");
		ObjectPropertyFrame.setVisible(true);
		ObjectPropertyFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setValues();
	}
	
	public ObjectPropertyElement() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 470, 353);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		//contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblObjName = new JLabel("Name");
		lblObjName.setFont(new Font("Calibri", Font.PLAIN, 15));
		lblObjName.setBounds(47, 38, 47, 14);
		contentPane.add(lblObjName);
		
		textFieldObjName = new JTextField();
		textFieldObjName.setBounds(153, 35, 126, 20);
		contentPane.add(textFieldObjName);
		textFieldObjName.setColumns(10);
		
		lblObjType = new JLabel("Object Type");
		lblObjType.setFont(new Font("Cambria Math", Font.PLAIN, 15));
		lblObjType.setBounds(47, 90, 82, 17);
		contentPane.add(lblObjType);
		
		lblLocator = new JLabel("Locator ");
		lblLocator.setFont(new Font("Calibri", Font.PLAIN, 15));
		lblLocator.setBounds(47, 139, 72, 14);
		contentPane.add(lblLocator);
		
		textFieldObjLocValue = new JTextField();
		textFieldObjLocValue.setBounds(153, 191, 126, 20);
		contentPane.add(textFieldObjLocValue);
		textFieldObjLocValue.setColumns(10);
		
		lblLocValue = new JLabel("Locator Value");
		lblLocValue.setFont(new Font("Calibri", Font.PLAIN, 15));
		lblLocValue.setBounds(47, 192, 91, 18);
		contentPane.add(lblLocValue);
		
		comboBoxLocType = new JComboBox();
		comboBoxLocType.setBounds(153, 137, 82, 19);
		contentPane.add(comboBoxLocType);
		
		
		btnSave = new JButton("SAVE");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(ObjectRepositoryView.objectPtropertyFrameLauched.equals("new")){
					insertNewObject();
				}else{
					updateExistingObject();
				}
				
				performAfterFrameClose();
			}
		});
		btnSave.setBounds(226, 260, 72, 20);
		contentPane.add(btnSave);
		
		btnCancel = new JButton("CANCEL");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				performAfterFrameClose();
			}
		});
		btnCancel.setBounds(318, 260, 91, 22);
		contentPane.add(btnCancel);		
		
		comboBoxObjectType = new JComboBox();
		comboBoxObjectType.setBounds(153, 90, 82, 20);
		contentPane.add(comboBoxObjectType);
		
	}
	
	//Insert new Object into the repository
	private static void insertNewObject(){
		try{
			getObjectPropertySet();
			boolean ObjectMatchResult = checkForDuplicateObject(newObjectPropertyList.get(0)); //Checking for Duplicate elementName
			if(ObjectMatchResult){
				File objectFile = new File(Initialization.projectPath+"//TestData//"+ TestCreation.ProjectSelected+"//ObjectProperty.xls");			
				ExcelDataList.insertRowIntoExcel(objectFile, ObjectRepositoryView.selectedScannedPage, newObjectPropertyList, 0); //Inserting row in the Excel
			}else{
				JOptionPane.showMessageDialog(null, "Element Name already exist ! \n Try a diff Name");
				
			}			
		}catch(Exception e){
			System.out.println("Exception in inserting new Object is - "+ e);
		}
	}
	
	//Updating Existing Object
	private static void updateExistingObject(){
		try{
			getObjectPropertySet();
			ExcelDataList.updateRowByRowNum(Initialization.ObjectRepositoryFile, ObjectRepositoryView.selectedScannedPage, ObjectRepositoryView.rowNumToEdit, newObjectPropertyList);
		}catch(Exception e){
			System.out.println("Exception in Updating Object properties - "+ e);
		}
	}
	
	private static void setValues(){
		try{
			
			comboBoxObjectType.addItem("text");
			comboBoxObjectType.addItem("button");
			comboBoxObjectType.addItem("radio-buttons");
			comboBoxObjectType.addItem("check-box");
			comboBoxObjectType.addItem("link");
			comboBoxObjectType.addItem("dropdown");
			comboBoxObjectType.addItem("frame");
			
			comboBoxLocType.addItem("id");
			comboBoxLocType.addItem("name");
			comboBoxLocType.addItem("linkText");
			comboBoxLocType.addItem("xpath");
			comboBoxLocType.addItem("css-selector");
			comboBoxLocType.addItem("class");
			
			
			if(ObjectRepositoryView.objectPtropertyFrameLauched.equalsIgnoreCase("edit")){
				textFieldObjName.setText(ObjectRepositoryView.getSelectedObjectList.get(0));				
				comboBoxObjectType.setSelectedItem(ObjectRepositoryView.getSelectedObjectList.get(1));
				comboBoxLocType.setSelectedItem(ObjectRepositoryView.getSelectedObjectList.get(2));
				textFieldObjLocValue.setText(ObjectRepositoryView.getSelectedObjectList.get(3));
			}
		}catch(Exception e){
			
		}
	}
	
	private static List<String> getObjectPropertySet(){
		newObjectPropertyList.clear();
		String ObjectName = textFieldObjName.getText().toString(); //Getting the Object Name from the User Provided
		String ObjectType = comboBoxObjectType.getSelectedItem().toString();
		String ObjectLocator = comboBoxLocType.getSelectedItem().toString();
		String ObjectLocatorValue = textFieldObjLocValue.getText().toString();
		//Add values to a list
		newObjectPropertyList.add(ObjectName);
		newObjectPropertyList.add(ObjectType);
		newObjectPropertyList.add(ObjectLocator);
		newObjectPropertyList.add(ObjectLocatorValue);
		return newObjectPropertyList;
	}
	
	private static boolean checkForDuplicateObject(String ObjectName){
		List<String> ObjectNameList = new ArrayList<String>();
		File objectRepositoryFile = new File(Initialization.projectPath+"\\TestData\\"+ TestCreation.ProjectSelected+"\\ObjectProperty.xls");
		ObjectNameList.addAll(ExcelDataList.getExcelDataInListByColIndex(objectRepositoryFile, ObjectRepositoryView.selectedScannedPage, 0)); //Getting the list of Object names
		//CHecking for the duplicate Object Name
		for(int cnt=0;cnt<ObjectNameList.size();cnt++){
			if(ObjectName.equals(ObjectNameList.get(cnt))){
				return false;
			}
		}
		return true;
	}
	
	private static void performAfterFrameClose(){
		try{
			ObjectPropertyFrame.dispose();
			ObjectRepositoryView.objRepoViewFrame.setEnabled(true);
			ObjectRepositoryView.objRepoViewFrame.setFocusable(true);
			ObjectRepositoryView.displayObjectsOfScannedPage();
		}catch(Exception e){
			
		}
	}
}

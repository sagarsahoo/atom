package selfitGui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import utilityPackage.cellToString;

public class ExcelToJTable extends JFrame {

	static JTable table;
	static JScrollPane scroll;
	// header is Vector contains table Column
	static Vector headers = new Vector();
	// Model is used to construct JTable
	static DefaultTableModel model = null;
	// data is Vector contains Data from Excel File
	static Vector data = new Vector();
	static JButton jbClick;
	static JFileChooser jChooser;
	static int tableWidth = 0; // set the tableWidth
	static int tableHeight = 0; // set the tableHeight

	public ExcelToJTable() {
		super("Import Excel To JTable");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBackground(Color.white);
		jChooser = new JFileChooser();
		jbClick = new JButton("Select Excel File");
		buttonPanel.add(jbClick, BorderLayout.CENTER);
		// Show Button Click Event
		jbClick.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				jChooser.showOpenDialog(null);

				File file = jChooser.getSelectedFile();
				if (!file.getName().endsWith("xls")) {
					JOptionPane.showMessageDialog(null, "Please select only Excel file.", "Error",
							JOptionPane.ERROR_MESSAGE);
				} else {
					try {
						fillData(file);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					model = new DefaultTableModel(data, headers);
					tableWidth = model.getColumnCount() * 150;
					tableHeight = model.getRowCount() * 25;
					table.setPreferredSize(new Dimension(tableWidth, tableHeight));

					table.setModel(model);
				}
			}
		});

		table = new JTable();
		table.setAutoCreateRowSorter(true);
		model = new DefaultTableModel(data, headers);

		table.setModel(model);
		table.setBackground(Color.pink);

		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setEnabled(false);
		table.setRowHeight(25);
		table.setRowMargin(4);

		tableWidth = model.getColumnCount() * 150;
		tableHeight = model.getRowCount() * 25;
		table.setPreferredSize(new Dimension(tableWidth, tableHeight));

		scroll = new JScrollPane(table);
		scroll.setBackground(Color.pink);
		scroll.setPreferredSize(new Dimension(300, 300));
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		getContentPane().add(buttonPanel, BorderLayout.NORTH);
		getContentPane().add(scroll, BorderLayout.CENTER);
		setSize(600, 600);
		setResizable(true);
		setVisible(true);
	}

	/**
	 * Fill JTable with Excel file data.
	 * 
	 * @param file
	 *            file :contains xls file to display in jTable
	 * @throws FileNotFoundException
	 */
	void fillData(File file) throws FileNotFoundException {

		HSSFWorkbook workbook = null;
		FileInputStream fis = new FileInputStream(file);
		try {
			try {
				workbook = new HSSFWorkbook(fis);
			} catch (IOException ex) {
				JOptionPane.showMessageDialog(null, "Error in excel file - " + ex);
			}

			Sheet sheet = workbook.getSheetAt(0);
			headers.clear();
			for (int i = 0; i < sheet.getRow(0).getLastCellNum(); i++) {
				HSSFRow row = (HSSFRow) sheet.getRow(0);
				HSSFCell cell = row.getCell(i);
				if (cell == null) {
					System.out.println("Cell is empty");
				} else {
					String value = cellToString.cellTostring_HSSF(cell);
					headers.add(value);
				}

				/*
				 * Cell cell1 = sheet.getCell(i, 0);
				 * headers.add(cell1.getContents());
				 */
			}

			data.clear();
			for (int j = 1; j < sheet.getLastRowNum(); j++) {
				Vector d = new Vector();
				HSSFRow row = (HSSFRow) sheet.getRow(j);
				for (int i = 0; i < sheet.getRow(0).getLastCellNum(); i++) {

					HSSFCell cell = row.getCell(i);
					if (cell == null) {
						System.out.println("Cell is empty");
					} else {
						String value = cellToString.cellTostring_HSSF(cell);
						d.add(value);
					}

					/*
					 * Cell cell = sheet.getCell(i, j);
					 * d.add(cell.getContents());
					 */

				}
				d.add("\n");
				data.add(d);
			}
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}

	public static void main(String[] args) {

		new ExcelToJTable();
	}
}

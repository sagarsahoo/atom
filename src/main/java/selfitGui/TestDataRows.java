package selfitGui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TestDataRows extends JFrame {

	private static JPanel contentPane;
	static TestDataRows testDataRowFrame;
	private static JLabel lbl_Field;
	private static JTextField[] textField_srcLocatorValue = new JTextField[30];
	private static JButton btnAdd;
	static int ObjectCnt;
	static int textboxcounter;
	static List<String> testDataColumnList = new ArrayList<String>();
	static List<String> testDataList = new ArrayList<String>();


	//define Frame
	public static void getKeywordFrame(){
		testDataRowFrame = new TestDataRows();
		testDataRowFrame.setVisible(true);
		testDataRowFrame.setLocationRelativeTo(null);
		testDataRowFrame.setTitle("Test Data");
		testDataRowFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}

	public TestDataRows() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 496, 424);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
				
		System.out.println("User Action is - "+ TestDataManagement.testDataAction);
		if(TestDataManagement.testDataAction.equalsIgnoreCase("edit")){
			btnAdd = new JButton("Update");
		}else{
			btnAdd = new JButton("Add");
		}
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				insertTestData();//Insert test data row
				TestDataManagement.displayTestDataInTable();
				actionAfterClose();
			}
		});
		btnAdd.setBounds(252, 336, 89, 23);
		contentPane.add(btnAdd);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actionAfterClose();
			}
		});
		btnCancel.setBounds(365, 336, 89, 23);
		contentPane.add(btnCancel);

		ObjectCnt=0;
		textboxcounter=0;
		displayTestDataFields();
	}

	// *********************************************************************************************************************

	public static void displayTestDataFields(){
		try{
			testDataColumnList.clear();
			testDataColumnList.addAll(ExcelDataList.getExcelDataInListByRowIndex(TestCreation.TestDataFile, TestCreation.TCSelected, 0));
			for(int cnt=2;cnt<testDataColumnList.size();cnt++){
				createColumnLabel(testDataColumnList.get(cnt));
				createColumnTextBox();
			}
			if(TestDataManagement.testDataAction.equals("edit")){
				testDataList.clear();
				testDataList.addAll(ExcelDataList.getExcelDataInListByRowIndex(TestCreation.TestDataFile, TestCreation.TCSelected, TestDataManagement.selectedTestDataRow));
				for(int cnt=2;cnt<testDataColumnList.size();cnt++){
					System.out.println("Value to be set - "+ testDataList.get(cnt) );
					textField_srcLocatorValue[cnt-2].setText(testDataList.get(cnt));
				}
			}

		}catch(Exception e){
			System.out.println("Exception in displaying the test data columns - "+ e);
		}
	}

	private static void insertTestData(){
		try{
			//For new Testdata row
			testDataList.clear();
			if(TestDataManagement.testDataAction.equalsIgnoreCase("Add")){
				String noOfRows = Integer.toString(ExcelDataList.getTotalRows(TestCreation.TestDataFile, TestCreation.TCSelected));
				testDataList.add(noOfRows); //Inser the sl. no as total no of rows
				testDataList.add("NA"); //Inserting the result as NA
				//Inserting all the user provided test data
				for(int cnt=2;cnt<testDataColumnList.size();cnt++){
					System.out.println("Value to be added - "+ textField_srcLocatorValue[cnt-2].getText());
					testDataList.add(textField_srcLocatorValue[cnt-2].getText());
				}
				ExcelDataList.insertRowIntoExcel(TestCreation.TestDataFile, TestCreation.TCSelected, testDataList, (Integer.parseInt(noOfRows)-1));
			}else{
				for(int cnt=2;cnt<testDataColumnList.size();cnt++){
					System.out.println("updated value - "+ textField_srcLocatorValue[cnt-2].getText());
					testDataList.add(textField_srcLocatorValue[cnt-2].getText());
				}
				ExcelDataList.updateRowByRowNum(TestCreation.TestDataFile, TestCreation.TCSelected, TestDataManagement.selectedTestDataRow, testDataList);
			}
		}catch(Exception e){
			System.out.println("Exception in inserying test row data into Excel - "+ e);
		}
	}

	private static void actionAfterClose(){
		try{
			testDataRowFrame.dispose();
			TestDataManagement.testDataFrame.setEnabled(true);
		}catch(Exception e){
			System.out.println("Exception in the action after frame completion - "+ e);
		}
	}

	// Creating the Label Object
	private static void createColumnLabel(String labelName){
		try{
			lbl_Field = new JLabel(labelName);
			int depth = (10+(ObjectCnt*40));
			lbl_Field.setBounds(40, depth, 114, 22); //40, 101, 114, 22
			contentPane.add(lbl_Field);
		}catch(Exception e){
			System.out.println("Exception in creating label is - "+ e);
		}		
	}

	//Creating the TextBox Object
	private  static void createColumnTextBox(){
		try{
			textField_srcLocatorValue[textboxcounter] = new JTextField();
			int depth = (10+(ObjectCnt*40));
			textField_srcLocatorValue[textboxcounter].setBounds(180, depth, 200, 20);					
			contentPane.add(textField_srcLocatorValue[textboxcounter]);
			textField_srcLocatorValue[textboxcounter].setColumns(10);
			ObjectCnt++;
			textboxcounter++;
		}catch(Exception e){
			System.out.println("Exception in text box creation is - "+ e);
		}
	}
}

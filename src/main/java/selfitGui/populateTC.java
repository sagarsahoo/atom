package selfitGui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.testng.annotations.Test;

import utilityPackage.cellToString;

public class populateTC extends JFrame {
	
	static Vector headers = new Vector(); // header is Vector contains table Column
	static Vector data = new Vector();
	
  @Test
  public void getTCSteps() {
	  try{
		  
		  File TCFile = new File("C:\\Accenture\\Project\\selenium\\workspace\\Automation\\TestData\\abcd\\ABCD_Scripting.xls");
		  String SheetTC = "Add_account";
		  fillData(TCFile,SheetTC);
	  }catch(Exception e){
		 JOptionPane.showMessageDialog(null, "Error in getting TC steps - "+ e); 
	  }
  }
  
  //
  void fillData(File file,String TCsheet) throws FileNotFoundException {
		 

	  HSSFWorkbook workbook = null;
	  FileInputStream fis =  new FileInputStream(file);
	  try {
	   try {
	    workbook = new HSSFWorkbook (fis);
	   } catch (IOException ex) {
	    JOptionPane.showMessageDialog(null, "Error in excel file - "+ ex);
	   }
	   
	   Sheet sheet = workbook.getSheet(TCsheet);
	   headers.clear();
	   for (int i = 0; i < sheet.getRow(0).getLastCellNum(); i++) {
		   HSSFRow row = (HSSFRow) sheet.getRow(0);
		   HSSFCell cell = row.getCell(i);
		   String value = cellToString.cellTostring_HSSF(cell);
		   headers.add(value);
		  
	    /*Cell cell1 = sheet.getCell(i, 0);
	    headers.add(cell1.getContents());*/
	   }

	   data.clear();
	   for (int j = 1; j < sheet.getLastRowNum(); j++) {
	    Vector d = new Vector();
	    HSSFRow row = (HSSFRow) sheet.getRow(j);
	    for (int i = 0; i < sheet.getRow(0).getLastCellNum(); i++) {
	    	
	    	HSSFCell cell = row.getCell(i);
			String value = cellToString.cellTostring_HSSF(cell);
			d.add(value);

	    /* Cell cell = sheet.getCell(i, j);
	     d.add(cell.getContents());*/

	    }
	    d.add("\n");
	    data.add(d);
	   }
	  } catch (Exception exp) {
	   exp.printStackTrace();
	  }
	 }
}

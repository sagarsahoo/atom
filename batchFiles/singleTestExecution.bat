TITLE SINGLE TEST EXECUTION

@echo off
echo.
echo "!!!!!!!!!!!!!!!!!! Go to project directory path"
set currentDirectory=%~dp0
cd %~dp0
cd ..
rem call mvn test -U -PsingleTest
call mvn test -PsingleTest -DxmlFileName=SingleTest.xml
rem For XSLT report
rem call mvn test -U -PsingleTest site org.reportyng:reporty-ng:1.2:reportyng

rem java -cp C:\Accenture\Project\selenium\workspace\Automation\TestResources\*;C:\Accenture\Project\selenium\workspace\Automation\* org.testng.TestNG SingleTest.xml
rem java -cp %currentDirectory%\TestResources\*;%currentDirectory%\* org.testng.TestNG SingleTest.xml
exit
TITLE SELENIUM JAR FIle 

@echo off
echo.
echo "!!!!!!!!!!!!!!!!!! Go to project directory path"
cd %~dp0..
call mvn clean package -Pselfit -DskipTests=true
rem call java -cp %~dp0/target/abcd-0.0.1-SNAPSHOT-jar-with-dependencies.jar utilityPackage.SeleniumSetup
echo jar file creation completed
echo.
echo copying the jar file from target folder to project location
xcopy /s/y %~dp0..\target\Automation-0.0.1-SNAPSHOT-jar-with-dependencies.jar %~dp0..\
pause
exit
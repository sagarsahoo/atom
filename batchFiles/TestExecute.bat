TITLE TEST EXECUTION

rem @echo off
echo.
echo "!!!!!!!!!!!!!!!!!! Go to project directory path"
cd %~dp0
echo current directory - %~dp0
rem do not provide space after or before the assignment statements
set packageName=SGR
set testNGxml= %packageName%%_suiteTestNG.xml

echo.
echo package name is - %packageName%
echo testNG xml file name is - %testNGxml%
echo.


call java -cp %~dp0/target/Automation-0.0.1-SNAPSHOT-jar-with-dependencies.jar utilityPackage.MasterClass %packageName%
rem call java -jar %~dp0/target/abcd-0.0.1-SNAPSHOT-jar-with-dependencies.jar %testNGxml%
pause
exit
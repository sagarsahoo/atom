@echo off
echo %cd%
echo.
rem echo Current path of batch file - %~dp0
echo Current path of batch file using cd - %cd%
set "current-path=%cd%"
echo.
echo Current path from the variable is - %current-path%

echo.
set /p UserInputProjectPath=Please provide the Project path : 
cd %UserInputProjectPath%
rem echo Please provide the Project path  - %1
echo listing the files in the project
echo.
dir
echo.


echo For Installation of ojdbc jar file *************************************
echo.

set jdbc-jar_path=%current-path%\jdbc_jar\ojdbc6.jar
echo.
echo -Dfile for ojdbc jar file is - %jdbc-jar_path%
echo.
echo installing the ojdbc6.jar file
echo.
echo.
call mvn install:install-file -Dfile="%jdbc-jar_path%" -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0 -Dpackaging=jar
echo.
echo Execution Completed !!!!!!!!!!!!!!!!
echo.


echo.
echo For Installation of SIKULI jar file *******************************
echo.

set sikuli-jar_path=%current-path%\SIKULI_jar\sikulixapi.jar
echo.

echo -Dfile for SIKULI jar file is - %sikuli-jar_path%
echo installing the sikuli jar file
echo.
echo.
call mvn install:install-file -Dfile="%sikuli-jar_path%" -DgroupId=sikuliX -DartifactId=sikuli -Dversion=1.0 -Dpackaging=jar
echo.
echo Execution Completed !!!!!!!!!!!!!!!!
echo.

echo Copying testNG folder
echo.
echo PLEASE PRESS d
set source-path=%current-path%\reportyng
set target-path=C:\Users\%username%\.m2\repository\org\
XCOPY %source-path% %target-path% /S

echo.
echo Installation completed

pause

